> Author: Zhou zitang<br/>
> Date: 2023/01/31<br/>
> Revisor: Falcon<br/>
> Project: [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal: [从零开始嵌入式 Linux (RISC-V + Linux v6.x)](https://gitee.com/tinylab/riscv-linux/issues/I61K05)<br/>
> Sponsor: PLCT Lab, ISCAS

# Linux 常用命令 - 第 1 部分

## 简介

### 背景介绍

泰晓社区今年开展了一系列 [RISC-V Linux 内核技术调研](https://tinylab.org/riscv-linux) 活动。几个月以来，数十位同学一起学习、交流、协作，陆续输出了上百篇分析文章，开展了近 50 次在线视频直播分享，并逐渐往上游项目社区贡献代码。

该活动目前的门槛相对比较高一些，主要面向有一定经验的同学。在活动开展的过程中，泰晓社区收到了很多同学的反馈，有些同学希望推荐一些门槛相对低一点的入门文章。为了满足更多社区用户的需要，泰晓社区决定启动一个《从零开始嵌入式 Linux（基于 RISC-V + Linux v6.x）》专项，该系列基于 RISC-V 处理器架构和 Linux 内核 v6.x 展开，以便帮助更多的同学快速上手。

该专项信息如下：

- 提案：[从零开始嵌入式 Linux (RISC-V + Linux v6.x)](https://gitee.com/tinylab/riscv-linux/issues/I61K05)
- 仓库：https://gitee.com/tinylab/elfs
- 文章：所有文字类成果目前还是统一提交到 RISC-V Linux 仓库的 articles/ 目录下，后续可能会组织线上技术分享，上述仓库用于公开课的组织与开展。

欢迎感兴趣的同学们在提案后面回复认领感兴趣的章节，也可以回复提出各种需求与建议。

### 本文说明

本文主要就是带领大家一起来学习 Linux 的常用命令，并简单演示它们的用法。

### 实验准备

在本系列的第一篇文章中对实验环境进行了详细的说明，可以翻看 [第一篇文章](20221129-elfs-riscv-linux-env.md) 进行了解。

## Shell 程序的启动

要使用命令，就要先启动 Shell 程序。在泰晓社区研发的 Linux 实验盘中进入 Linux Lab Shell 后，Shell 就启动了。
普通用户的 Shell 的启动界面将会如下所示：

```
ubuntu@linux-lab:/labs/linux-lab$ 
```

当然，大家也可以根据自己的喜好选用其他的 Shell 程序，不同的 Linux 发行版提供了不同的 Shell 前端（terminal）选择，不过背后的 Shell 解释程序（interpreter）不外乎 Bash、Dash、Zsh 等，该系列主要选用 Bash 作为命令行解释器，Linux Lab Shell 背后的命令行解释器就是 Bash。



### 命令的格式

Shell 命令是由命令名和多个选项以及参数组成的，各部分之间用空格分隔。Shell 命令严格区分大小写。

Shell 命令的格式如下：

```
命令名 [-选项···][参数···]
```

## 文件管理命令

### 目录操作基本命令

#### ls 命令

ls 命令是最常用的命令之一。用户可以利用 ls 命令查看某个目录下的所有内容。在默认情况下，显示的内容是按字母顺序排列的。

`ls` 单独使用时是显示当前目录下的所有文件及子目录：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  boards  build  cloud-lab  COPYING  doc  hostshare  Makefile  prebuilt  README.md  README_zh.md  src  tftpboot  TODO.md  tools  VERSION
```

`-s` 是显示每个文件的大小：

```
ubuntu@linux-lab:/labs/linux-lab$ ls -s
total 352
  4 AUTHORS    0 boards    0 build    0 cloud-lab   20 COPYING    0 doc    0 hostshare  124 Makefile    0 prebuilt   88 README.md  108 README_zh.md    0 src    0 tftpboot    4 TODO.md    0 tools    4 VERSION
```

`-S` 是按文件的大小排序：

```
ubuntu@linux-lab:/labs/linux-lab$ ls -S
Makefile  README_zh.md  README.md  COPYING  TODO.md  cloud-lab  tools  AUTHORS  src  boards  prebuilt  doc  tftpboot  hostshare  VERSION  build
```

`-a` 显示目录中的所有文件，包括隐藏文件：

```
ubuntu@linux-lab:/labs/linux-lab$ ls -a
.  ..  AUTHORS  .board_config  boards  build  cloud-lab  COPYING  doc  .gdb  .git  .gitignore  .gitmodules  hostshare  .labbegin  .labinit  Makefile  prebuilt  README.md  README_zh.md  src  tftpboot  TODO.md  tools  VERSION
```

`-l` 通过列表格式显示文件的详细信息：

```
ubuntu@linux-lab:/labs/linux-lab$ ls -l
total 352
-rw-rw-r-- 1 ubuntu ubuntu    161 5月   1  2022 AUTHORS
drwxrwxr-x 1 ubuntu ubuntu    122 5月   1  2022 boards
drwxr-xr-x 1 ubuntu ubuntu      6 6月  27  2022 build
drwxr-xr-x 1 ubuntu ubuntu    220 11月 20 20:46 cloud-lab
-rw-rw-r-- 1 ubuntu ubuntu  18238 5月   1  2022 COPYING
drwxrwxr-x 1 ubuntu ubuntu     86 5月   1  2022 doc
drwxrwxr-x 1 ubuntu ubuntu     24 5月   1  2022 hostshare
-rw-r--r-- 1 ubuntu ubuntu 125501 6月  27  2022 Makefile
drwxrwxr-x 1 ubuntu ubuntu    112 5月   1  2022 prebuilt
-rw-r--r-- 1 ubuntu ubuntu  87979 6月  27  2022 README.md
-rw-r--r-- 1 ubuntu ubuntu 107784 6月  27  2022 README_zh.md
drwxrwxr-x 1 ubuntu ubuntu    128 5月   1  2022 src
drwxrwxr-x 1 ubuntu ubuntu     78 11月 29 23:27 tftpboot
-rw-rw-r-- 1 ubuntu ubuntu   3043 5月   1  2022 TODO.md
drwxrwxr-x 1 ubuntu ubuntu    176 5月   1  2022 tools
-rw-r--r-- 1 ubuntu ubuntu     15 6月  27  2022 VERSION

```

`-t` 按文件的修改时间排序：

```
ubuntu@linux-lab:/labs/linux-lab$ ls -t
tftpboot  cloud-lab  build  README.md  README_zh.md  VERSION  Makefile  src  tools  prebuilt  doc  hostshare  boards  TODO.md  AUTHORS  COPYING
```

`-F` 显示文件类型描述符：

```
ubuntu@linux-lab:/labs/linux-lab$ ls -F
AUTHORS  boards/  build/  cloud-lab/  COPYING  doc/  hostshare/  Makefile  prebuilt/  README.md  README_zh.md  src/  tftpboot/  TODO.md  tools/  VERSION
```

#### cd 命令

用户可以利用 cd 命令转换所在的目录。

`cd ..` 代表回退到上一层：

```
ubuntu@linux-lab:/labs/linux-lab/src/examples$ cd ..
```

`cd /src/examples` 代表进入到目录 /src/examples：

```
ubuntu@linux-lab:/labs/linux-lab$ cd /src/examples
```

#### pwd 命令

用 `pwd` 可以显示当前工作目录的绝对路径：

```
ubuntu@linux-lab:/labs/linux-lab$ pwd
/labs/linux-lab
```

#### mkdir 命令

用 `mkdir` 命令创建了一个名为 dir 的新目录：

```
ubuntu@linux-lab:/labs/linux-lab$ mkdir dir
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  boards  build  cloud-lab  COPYING  dir  doc  hostshare  Makefile  prebuilt  README.md  README_zh.md  src  tftpboot  TODO.md  tools  VERSION
```

#### rmdir 命令

用户使用这个命令可以删除目录。

用 `rmdir` 命令删除了 dir 目录：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  boards  build  cloud-lab  COPYING  dir  doc  hostshare  Makefile  prebuilt  README.md  README_zh.md  src  tftpboot  TODO.md  tools  VERSION
ubuntu@linux-lab:/labs/linux-lab$ rmdir dir
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  boards  build  cloud-lab  COPYING  doc  hostshare  Makefile  prebuilt  README.md  README_zh.md  src  tftpboot  TODO.md  tools  VERSION
```

### 文件操作基本命令

#### touch 命令

`touch` 命令用于创建文件，如果文件名不存在，则创建一个新的空文件夹，这个文件夹不包含任何格式，大小为 0。

用 `touch` 命令创建名为 mylife 的文件：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  COPYING  hostshare  prebuilt   README_zh.md  tftpboot  tools
boards   doc      Makefile   README.md  src           TODO.md   VERSION
ubuntu@linux-lab:/labs/linux-lab$ touch mylife
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  COPYING  hostshare  mylife    README.md     src       TODO.md  VERSION
boards   doc      Makefile   prebuilt  README_zh.md  tftpboot  tools
```

#### cat 命令

`cat` 命令的用法有很多，基本作用是：合并文件，在屏幕上显示文件的内容。在这里我们就简单了解一下基本作用，如果对 cat 命令的其他用法感兴趣，可以自己搜索相关的资料。

* 显示某文件的内容：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  COPYING  hostshare  mylife    README.md     src       TODO.md  VERSION
boards   doc      Makefile   prebuilt  README_zh.md  tftpboot  tools
ubuntu@linux-lab:/labs/linux-lab$ cat mylife
hello world!
hello everyone!
```

* 实现文件的合并：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cat hi
hi!
ubuntu@linux-lab:/labs/linux-lab$ cat mylife
hello world!
hello everyone!
ubuntu@linux-lab:/labs/linux-lab$ cat hi mylife > hello
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cat hello
hi!
hello world!
hello everyone!
```

#### cp 命令

`cp` 命令可以实现文件的复制，类似 Windows 中的 copy 命令。

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cd src
ubuntu@linux-lab:/labs/linux-lab/src$ ls
buildroot  feature       modules  qemu    u-boot
examples   linux-stable  patch    system
ubuntu@linux-lab:/labs/linux-lab/src$ cd ..
ubuntu@linux-lab:/labs/linux-lab$ cp mylife src
ubuntu@linux-lab:/labs/linux-lab$ cd src
ubuntu@linux-lab:/labs/linux-lab/src$ ls
buildroot  feature       modules  patch  system
examples   linux-stable  mylife   qemu   u-boot
```

#### rm 命令

`rm` 命令的作用是删除指定的文件。

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cd src
ubuntu@linux-lab:/labs/linux-lab/src$ ls
buildroot  feature       modules  patch  system
examples   linux-stable  mylife   qemu   u-boot
ubuntu@linux-lab:/labs/linux-lab/src$ rm mylife
ubuntu@linux-lab:/labs/linux-lab/src$ ls
buildroot  feature       modules  qemu    u-boot
examples   linux-stable  patch    system
```

#### chmod 命令

`chmod` 命令是 Linux 系统中一个非常重要的命令，它可以修改文件的权限和文件的属性。

格式：

```
chmod [<文件使用者>+|-|=<权限类型>] 文件名1 文件名2 ···
```

1. 文件使用者有4种类型。
   * “u”：user，文件主
   * “g”：group，文件主所在用户组
   * “o”：others，其他用户
   * “a”：all，所有用户

2. +、-、=的含义如下：
   * +：代表增加权限。
   * -：代表取消权限。
   * =：代表赋予指定的权限，并取消其他权限（如果有）。

3. 权限类型有3种： r 、 w 、 x 。在 chmod 命令中，可采用这3种权限类型的组合。
   * r ：代表读权限。
   * w ：代表写权限。
   * x ：代表可执行权限

利用 `chomd` 命令修改某文件的权限，具体步骤如下：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ ls -l mylife
-rw-r--r-- 1 ubuntu ubuntu 29 1月  30 18:54 mylife
ubuntu@linux-lab:/labs/linux-lab$ chmod a-rwx mylife
ubuntu@linux-lab:/labs/linux-lab$ ls -l mylife
---------- 1 ubuntu ubuntu 29 1月  30 18:54 mylife
ubuntu@linux-lab:/labs/linux-lab$ chmod u+rwx mylife
ubuntu@linux-lab:/labs/linux-lab$ ls -l mylife
-rwx------ 1 ubuntu ubuntu 29 1月  30 18:54 mylife
ubuntu@linux-lab:/labs/linux-lab$ chmod g+r mylife
ubuntu@linux-lab:/labs/linux-lab$ ls -l mylife
-rwxr----- 1 ubuntu ubuntu 29 1月  30 18:54 mylife
ubuntu@linux-lab:/labs/linux-lab$ chmod o+r mylife
ubuntu@linux-lab:/labs/linux-lab$ ls -l mylife
-rwxr--r-- 1 ubuntu ubuntu 29 1月  30 18:54 mylife
```
### 文件处理命令

#### grep 命令

`grep` 命令可以实现在指定的文件中查找某个特定的字符串。

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cat mylife
hello world!
hello everyone!
ubuntu@linux-lab:/labs/linux-lab$ grep hello mylife
hello world!
hello everyone!
```

#### head 命令和 tail 命令

`head` 命令可以查看文件开头部分的内容，`tail` 命令可以查看文件结尾部分的内容。

可以指定要显示的行数。“-5”，指定显示前五行或者结尾五行。

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cat mylife
hello world!
hello everyone!
ubuntu@linux-lab:/labs/linux-lab$ head -1 mylife
hello world!
ubuntu@linux-lab:/labs/linux-lab$ tail -1 mylife
hello everyone!
```

#### wc 命令

`wc` 命令可以对文件的行数、单词数、字符数进行统计:

* `-l`显示行数
* `-w`显示单词数
* `-m`显示字符数

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cat mylife
hello world!
hello everyone!
ubuntu@linux-lab:/labs/linux-lab$ wc -l mylife
2 mylife
ubuntu@linux-lab:/labs/linux-lab$ wc -w mylife
4 mylife
ubuntu@linux-lab:/labs/linux-lab$ wc -m mylife
29 mylife
```

#### sort 命令

`sort` 命令可以对文件内容或查询结果进行排序：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ cat mylife
hello world!
hello everyone!
ubuntu@linux-lab:/labs/linux-lab$ sort -t":" -k2 mylife
hello everyone!
hello world!
```
其中，`-t":"` 设置分隔符为冒号，`-k2` 选项指定根据第二列排序。

#### find 命令

`find` 命令可以查找文件或目录：

```
ubuntu@linux-lab:/labs/linux-lab$ ls
AUTHORS  doc    hostshare  prebuilt      src       tools
boards   hello  Makefile   README.md     tftpboot  VERSION
COPYING  hi     mylife     README_zh.md  TODO.md
ubuntu@linux-lab:/labs/linux-lab$ find hello
hello
```

#### which 命令

`which` 命令可以查找命令并显示该命令的绝对路径。

```
ubuntu@linux-lab:/labs/linux-lab$ which ls
/usr/bin/ls
ubuntu@linux-lab:/labs/linux-lab$ which cp
/usr/bin/cp
ubuntu@linux-lab:/labs/linux-lab$ which which
/usr/bin/which
```

## 用户管理命令

### 激活 root 用户

激活 root 用户的方法：

1. 以普通用户身份登录，看到命令提示符是 "$"，表明目前为普通用户。
2. 执行命令 `sudo passwd root` 后要输入密码，密码正确后会需要为 root 用户设定密码。
3. 设定成功后，就成功激活了 root 用户。
4. 执行命令 `su root` 转换到 root 用户，命令提示符转为 “#”，就表明转换成功了。

```
ubuntu@linux-lab-host:~/Develop/cloud-lab$ sudo passwd root
[sudo] password for ubuntu: 
New password: 
Retype new password: 
passwd: password updated successfully
ubuntu@linux-lab-host:~/Develop/cloud-lab$ su root
Password: 
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# 
```

### 重新锁定 root 用户

在不需要以 root 用户执行操作时，最好将 root 用户即时锁定，防止用户的不当操作破坏系统。

1. 先利用 `su` 命令进行用户身份的转变，从 root 用户转为 ubuntu 用户

2. `sudo passwd -l root` 锁定 root 用户。

3. 再次利用 su 命令转换到 root 用户，会发现在输人密码后仍然无法唤醒 root 用户，说明 `sudo passwd -l root` 是有效的。

4. 在当前的状况下，如果需要激活 root 用户，应再次按照上面演示的激活 root 的方法进行操作。

```
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# su ubuntu
ubuntu@linux-lab-host:~/Develop/cloud-lab$ sudo passwd -l root
[sudo] password for ubuntu: 
passwd: password expiry information changed.
ubuntu@linux-lab-host:~/Develop/cloud-lab$ su root
Password: 
su: Authentication failure
```

### 其他用户管理命令

1. `sudo` 命令：以超级管理员的身份执行某种操作
2. `passwd` 命令：修改用户密码
3. `su` 命令：转换用户
4. `useradd` 命令：创建一个新用户

综合演示这几个命令，具体步骤如下：

1. 利用 `sudo passwd root` 命令可以使用超级管理员身份更新 root 用户密码并激活 root 用户。
2. `su root` 的作用是转换为 root 用户。
3. 使用 `useradd zzt` 命令可以创建普通用户 zzt。
4. 使用 `passwd zzt` 命令可以为用户 zzt 设置密码。至此用户 zzt 已经创建好了。
5. 第 8 条命令 `su zzt` 转换为 zzt。该命令执行结束后，屏幕上出现了 "$" 提示符，和前面的提示符形式不同。要想获得和上面相同的提示符，可以输入 bash 命令并按 Enter 键，随后就可以看到熟悉的提示符了。

```
ubuntu@linux-lab-host:~/Develop/cloud-lab$ sudo passwd root
New password: 
Retype new password: 
passwd: password updated successfully
ubuntu@linux-lab-host:~/Develop/cloud-lab$ su root
Password: 
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# useradd zzt
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# passwd zzt
New password: 
Retype new password: 
passwd: password updated successfully
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# ls
base	 COPYING  images  README.md	recordings  TODO.md  VERSION
configs  doc	  labs	  README_zh.md	releasings  tools
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# mkdir zzt
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# ls
base	 COPYING  images  README.md	recordings  TODO.md  VERSION
configs  doc	  labs	  README_zh.md	releasings  tools    zzt
root@linux-lab-host:/home/ubuntu/Develop/cloud-lab# su zzt
$ bash
zzt@linux-lab-host:/home/ubuntu/Develop/cloud-lab$ 
```

## 常用网络命令

### ifconfig 命令

`ifconfig` 命令可以查看和更改网络接口的地址和参数，仅限超级用户拥有使用权限。通常用来查看 IP 地址。

```
ubuntu@linux-lab-host:~/Develop/cloud-lab$ ifconfig
br-cb818dec92ad: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.20.19.1  netmask 255.255.0.0  broadcast 172.20.255.255
        inet6 fe80::42:32ff:feb3:91e3  prefixlen 64  scopeid 0x20<link>
        ether 02:42:32:b3:91:e3  txqueuelen 0  (Ethernet)
        RX packets 2  bytes 80 (80.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 55  bytes 6736 (6.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

docker0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 172.17.255.255
        ether 02:42:41:83:09:23  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.2.15  netmask 255.255.255.0  broadcast 10.0.2.255
        inet6 fe80::88e:3e3c:8d65:38ce  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:f6:3f:4c  txqueuelen 1000  (Ethernet)
        RX packets 230837  bytes 338118104 (338.1 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 26389  bytes 1716710 (1.7 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 40  bytes 3665 (3.6 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 40  bytes 3665 (3.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

veth040ee03: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet6 fe80::58b0:19ff:fe13:ebb7  prefixlen 64  scopeid 0x20<link>
        ether 5a:b0:19:13:eb:b7  txqueuelen 0  (Ethernet)
        RX packets 2  bytes 108 (108.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 97  bytes 11334 (11.3 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vethaf52de9: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet6 fe80::6439:71ff:fe59:58cc  prefixlen 64  scopeid 0x20<link>
        ether 66:39:71:59:58:cc  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 103  bytes 11815 (11.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

### ping 命令

`ping` 命令可以测试主机网络是否畅通，所有用户都拥有使用权限。

使用 `ping` 命令查看本机的网络状况，5 次后自动停止。

```
ubuntu@linux-lab-host:~/Develop/cloud-lab$ ping  -c5 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.137 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.062 ms
64 bytes from 127.0.0.1: icmp_seq=3 ttl=64 time=0.071 ms
64 bytes from 127.0.0.1: icmp_seq=4 ttl=64 time=0.052 ms
64 bytes from 127.0.0.1: icmp_seq=5 ttl=64 time=0.044 ms

--- 127.0.0.1 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4143ms
rtt min/avg/max/mdev = 0.044/0.073/0.137/0.033 ms
```

## 总结

通过这篇文章，我们顺利地启动了 Shell ，开启了 Linux 常用命令的学习之路，并实际演示了一些常用命令的用法。

在后续文章中，我们将对 Linux 的其他命令进行演示，如有兴趣，且听下回分解。

如果对该系列感兴趣，赶紧联系我们吧，联系微信：tinylab。

## 参考资料

* Ubuntu Linux 基础教程（第2版）（微课版）
* Linux 命令行与 shell 脚本编程大全（第4版）
