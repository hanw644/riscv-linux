> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [spaces tables pangu]<br/>
> Author:    Kepontry <Kepontry@163.com><br/>
> Date:      2023/3/23<br/>
> Revisor:   Falcon <falcon@tinylab.org>; Bin Meng <bmeng@tinylab.org>; Walimis <walimis@walimis.org><br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [VisionFive 2 开发板软硬件评测及软件 gap 分析](https://gitee.com/tinylab/riscv-linux/issues/I64ESM)<br/>
> Sponsor:   PLCT Lab, ISCAS

# VisionFive 2 开发板的 CoreMark 评测、优化与对比

## 简介

### VisionFive 2 开发板

VisionFive 2 开发板采用的是赛昉科技的 JH7110 高性能 RISC-V SoC，它是一款 64 位四核处理器，共享 2MB 一致性缓存，工作频率为 1.5GHz。JH7110 支持 Linux 操作系统，拥有集成 GPU 以及视频编解码 ISP，支持 OpenCL, OpenGL ES, Vulkan 等并行计算库和图形渲染库，支持 H.264, H.265, JPEG 等格式的编解码。本文作为评测系列的第一篇，将主要关注 CPU 性能。

### CoreMark 测试集

[CoreMark][006] 是由 EEMBC (Embedded Microprocessor Benchmark Consortium) 开发的一项基准测试程序，主要为 MCU 或 CPU 提供轻量级评测。主要测试的是单核性能，但本文中也对多核评测做了简单的尝试。测试标准是在配置参数的组合下单位时间内运行的 CoreMark 程序次数（单位：CoreMark/MHz），该数字值越大则说明测试的性能越好。

Dhrystone 和 Coremark 都是广泛使用的的性能测试程序，都拥有着体积小、方便移植、易于理解、免费等优点。但 Dhrystone 的结果更容易受编译器优化能力的影响，从而影响对 CPU 的评测。而 Coremark 在设计中动态生成关键值，使得在编译阶段不能提前计算出结果，缩小了编译优化空间，从而避免了上述问题。此外，Coremark 还在其官网上提供了测试结果的验证与排行，方便进行比较。

## 测试集编译与运行

### 源码获取与配置

CoreMark 在 GitHub 上有对应的 [源码仓库][001]，但我们这里采用 BOOM 处理器核团队移植到 RISC-V 上的 [riscv-coremark][003]。CoreMark 提供了可移植的接口，从而能够在核心评测代码不变的前提下，在多种架构环境中执行。riscv-coremark 提供了 riscv64 和 riscv64-baremetal 两种构建方式，使测试程序分别能在 Linux 和裸机上运行。CoreMark 原版代码以子模块的形式包含在 riscv-coremark 中，执行以下命令以获取全套代码。

以下步骤我们直接在开发板上进行代码的编译和运行。

```shell
$ git clone https://github.com/riscv-boom/riscv-coremark.git
$ cd riscv-coremark
$ tree -L 1
.
|-- LICENSE
|-- README.md
|-- build-coremark.sh	# 构建脚本
|-- coremark	# 原版 CoreMark 代码
|-- riscv64	# 运行在 Linux 或者 Proxy Kernel 上的版本
`-- riscv64-baremetal	# 运行在裸机上的版本，手动实现了用到的 syscall
$ git submodule update --init
```

为了使待测 CPU 发挥出全部性能，需要将 CPU 频率固定为其所支持的最高运行频率（即 1.5GHz），不动态调节。执行以下命令，安装 cpupower 工具，设置频率策略为 performance 模式。

```shell
$ sudo apt-get install linux-cpupower
$ cpupower frequency-set -g performance
```

从下面构建脚本 `build-coremark.sh` 的内容来看，它只是在 coremark 目录下执行类似 `make PORT_DIR=../xxx compile` 的命令，该命令指定移植代码的目录并编译，最后把生成的文件复制到上一级目录。后面为了便于调试，我们可以直接在 coremark 目录下执行该命令。因为我们是运行在 Linux 上，所以选择 riscv64 移植版本构建并运行。

```shell
$ cat build-coremark.sh
#!/bin/bash

set -e

BASEDIR=$PWD
CM_FOLDER=coremark

cd $BASEDIR/$CM_FOLDER

# run the compile
echo "Start compilation"
make PORT_DIR=../riscv64 compile
mv coremark.riscv ../

make PORT_DIR=../riscv64-baremetal compile
mv coremark.bare.riscv ../
```

首先修改 `core_portme.mak` 文件，指定编译工具链的前缀 `RISCVTYPE=riscv64-linux-gnu` 与存放目录 `RISCVTOOLS=/usr`，这里大家可以结合自己的环境来进行修改。代码中的 `CC = $(RISCVTOOLS)/bin/$(RISCVTYPE)-gcc` 最终指向了编译用的 gcc。为了获得较高性能，`PORT_CFLAGS` 也需要按如下代码设置。

```shell
$ vim riscv64/core_portme.mak
...
RISCVTOOLS=/usr
# Flag: RISCVTYPE
#   Type of toolchain to use
RISCVTYPE=riscv64-linux-gnu
# Flag: OUTFLAG
#	Use this flag to define how to to get an executable (e.g -o)
OUTFLAG= -o
# Flag: CC
#	Use this flag to define compiler to use
CC = $(RISCVTOOLS)/bin/$(RISCVTYPE)-gcc
# Flag: CFLAGS
#       Use this flag to define compiler options. Note, you can add compiler options from the command line using XCFLAGS="other flags"
PORT_CFLAGS = -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2
...
```

然后修改头文件，将 `HAS_TIME_H` 宏变量设置为 1，表示 `clock_t` 变量使用 `<time.h>` 中的定义而不需要手动定义，避免报定义重复的错误。

```shell
$ vim riscv64/core_portme.h
...
/* Configuration: HAS_TIME_H
	Define to 1 if platform has the time.h header file,
	and implementation of functions thereof.
 */
#ifndef HAS_TIME_H
#define HAS_TIME_H 1
#endif
...
```

### 单核测试

按默认配置进行编译即可生成单核测试程序。指定移植目录 `PORT_DIR` 为 riscv64 并编译，在当前目录生成 `coremark.riscv` 测试程序。运行该程序，得到每秒迭代次数为 6809，对应的 CoreMark 分数为 4.54 分/MHz。

```shell
$ cd coremark
$ make PORT_DIR=../riscv64 compile
$ ./coremark.riscv
2K performance run parameters for coremark.
CoreMark Size    : 666
Total ticks      : 16153
Total time (secs): 16.153000
Iterations/Sec   : 6809.880518
Iterations       : 110000
Compiler version : GCC12.2.0
Compiler flags   : -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2
Memory location  : Please put data memory location here
			(e.g. code in flash, data on heap etc)
seedcrc          : 0xe9f5
[0]crclist       : 0xe714
[0]crcmatrix     : 0x1fd7
[0]crcstate      : 0x8e3a
[0]crcfinal      : 0x33ff
Correct operation validated. See README.md for run and reporting rules.
CoreMark 1.0 : 6809.880518 / GCC12.2.0 -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2    / Heap
```

### PGO 优化测试

PGO (Profile Guided Optimization) 是一种编译优化技术，它利用实际运行中收集的性能分析数据重新编译应用，以提高应用程序的运行时性能。PGO 优化与传统的静态优化相比，能够获取应用程序的运行时数据，从而能够针对热代码块在布局上进行优化，提高指令 Cache 的命中率。

使用如下 make 命令构建测试程序，通过指定 `PGO=1` 开启 PGO 优化。编译器首先构建用于性能分析的二进制程序。向 GCC 传递 `-fprofile-generate` 参数，以收集运行数据。之后再用生成的 `*.gcda` 文件重新编译测试程序，设置 GCC 的 `-fprofile-use` 标志，表示使用上述运行时性能数据进行 PGO 优化，生成优化后的测试程序。

如果提示 `'xxx.gcda' profile count data file not found`，可以删除 `coremark.riscv` 文件后再重新编译。

运行经 PGO 优化后的测试程序 `coremark.riscv`，得到每秒迭代次数为 7016，相比之前约有 3% 的性能提升。

```shell
$ make PORT_DIR=../riscv64 PGO=1
$ ./coremark.riscv
2K performance run parameters for coremark.
CoreMark Size    : 666
Total ticks      : 15678
Total time (secs): 15.678000
Iterations/Sec   : 7016.201046
Iterations       : 110000
Compiler version : GCC12.2.0
Compiler flags   : -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2 -DPERFORMANCE_RUN=1
Memory location  : Please put data memory location here
			(e.g. code in flash, data on heap etc)
seedcrc          : 0xe9f5
[0]crclist       : 0xe714
[0]crcmatrix     : 0x1fd7
[0]crcstate      : 0x8e3a
[0]crcfinal      : 0x33ff
Correct operation validated. See README.md for run and reporting rules.
CoreMark 1.0 : 7016.201046 / GCC12.2.0 -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2 -DPERFORMANCE_RUN=1   / Heap
```

### 多核测试尝试

通过观察头文件 `riscv64/core_portme.h`，发现移植者提供了多核测试的代码与设置项。通过 `MULTITHREAD` 宏变量指定线程数，`USE_PTHREAD` 宏表示通过 pthread 库使用多线程。我们在 Makefile 文件的 `CFLAGS` 项中添加这两项，同时在 `LFLAGS_END` 项中加上 `-lpthread`，使链接器将 pthread 库链接进来。

```shell
$ vim riscv64/core_portme.mak
...
CFLAGS = $(PORT_CFLAGS) -DMULTITHREAD=4 -DUSE_PTHREAD -I$(PORT_DIR) -I. -DFLAGS_STR=\"$(FLAGS_STR)\"
#Flag: LFLAGS_END
#	Define any libraries needed for linking or other flags that should come at the end of the link line (e.g. linker scripts).
#	Note: On certain platforms, the default clock_gettime implementation is supported but requires linking of librt.
LFLAGS_END += -lpthread
# Flag: PORT_SRCS
...
```

使用 make 命令编译，生成可执行文件 `coremark.riscv` 并运行，可以看到多线程程序的每秒迭代次数是 27185。这个结果大致等于单核的 4 倍，由于该测试集本身不是专门为多核设计的，并没有太多核间的竞争与同步，所以这个得分的参考意义等同于单核得分。

```shell
$ make PORT_DIR=../riscv64 compile
$ ./coremark.riscv
2K performance run parameters for coremark.
CoreMark Size    : 666
Total ticks      : 16185
Total time (secs): 16.185000
Iterations/Sec   : 27185.665740
Iterations       : 440000
Compiler version : GCC12.2.0
Compiler flags   : -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2 -DPERFORMANCE_RUN=1  -lpthread
Parallel PThreads : 4
Memory location  : Please put data memory location here
			(e.g. code in flash, data on heap etc)
seedcrc          : 0xe9f5
[0]crclist       : 0xe714
[1]crclist       : 0xe714
[2]crclist       : 0xe714
[3]crclist       : 0xe714
[0]crcmatrix     : 0x1fd7
[1]crcmatrix     : 0x1fd7
[2]crcmatrix     : 0x1fd7
[3]crcmatrix     : 0x1fd7
[0]crcstate      : 0x8e3a
[1]crcstate      : 0x8e3a
[2]crcstate      : 0x8e3a
[3]crcstate      : 0x8e3a
[0]crcfinal      : 0x33ff
[1]crcfinal      : 0x33ff
[2]crcfinal      : 0x33ff
[3]crcfinal      : 0x33ff
Correct operation validated. See README.md for run and reporting rules.
CoreMark 1.0 : 27185.665740 / GCC12.2.0 -march=rv64gc_zba_zbb -O3 -fno-common -funroll-loops -finline-functions -funroll-all-loops --param max-inline-insns-auto=20 -falign-functions=8 -falign-jumps=8 -falign-loops=8 --param inline-min-speedup=10 -mtune=sifive-7-series -ffast-math -fno-if-conversion2 -DPERFORMANCE_RUN=1  -lpthread / Heap / 4:PThreads
```

## 性能调优小结

在以上代码中，编译参数 `CFLAGS` 的内容是经过多轮测试得到的较优版本，测试中参考了论坛中一篇关于 CoreMark 测试的 [帖子][007]。下表展示了具体的优化参数以及对应的 CoreMark 分值，添加优化参数后，编译时间增加，调试难度增加，但程序的运行时性能得到提升。

以 GCC 11.3.0 和优化选项 `-O2` 为基础版本，以下优化的改动分别为：
  * 优化 1 将优化选项提升为 `-O3`，启用编译器提供的更多优化技术。
  * 优化 2 将 GCC 版本升级为 12.2.0。
  * 优化 3 使用循环展开技术，以增加可执行文件大小为代价，减少因为分支预测错误造成的流水线停顿。应用该技术后，可执行文件大小从 23K 增大到 31K。
  * 优化 4 使用存储对齐技术，将函数起始位置、跳转指令位置和循环指令位置都按 8 字节对齐，优化取指。
  * 优化 5 设置了代码内联的参数，主要是限制自动内联的最大指令数。
  * 优化 6 是针对数学运算和 U74 CPU 的性能调优。
  * 优化 7 指定编译架构，因为 JH7110 支持 zba 和 zbb 等用于位运算加速的 RISC-V B 扩展指令集，所以设置该参数能够进一步提升 CoreMark 分值。

| 序号 | GCC 版本 |                                       编译参数                                        | 每秒迭代次数 | CoreMark 分值 |
|------|:--------:|:-------------------------------------------------------------------------------------:|:------------:|:-------------:|
|      |  11.3.0  |                                          -O2                                          |     4986     |     3.32      |
| 1    |  11.3.0  |                                          -O3                                          |     5235     |     3.49      |
| 2    |  12.2.0  |                                          -O3                                          |     5322     |     3.55      |
| 3    |  12.2.0  |                   -O3 -fno-common -funroll-loops -funroll-all-loops                   |     5437     |     3.62      |
| 4    |  12.2.0  |                ... -falign-functions=8 -falign-jumps=8 -falign-loops=8                |     5473     |     3.65      |
| 5    |  12.2.0  | ... -finline-functions --param max-inline-insns-auto=10 --param inline-min-speedup=10 |     6015     |     4.01      |
| 6    |  12.2.0  |                        ... -mtune=sifive-7-series -ffast-math                         |     6320     |     4.21      |
| 7    |  12.2.0  |                             ... -march=rv64gc_zba_zbb...                              |     6809     |     4.54      |

通过对生成的二进制文件 `coremark.riscv` 反汇编，可以观察到有较多的 `add.uw` 指令，这些便是 RISC-V B 扩展中的指令，用于加速位运算。使用这些指令后，CoreMark 分数提升了 7.8%。

```shell
$ riscv64-linux-gnu-objdump -d coremark.riscv | grep add.uw
    ...
    3958:	08c587bb          	add.uw	a5,a1,a2
    39dc:	08c582bb          	add.uw	t0,a1,a2
    39e4:	08c78ebb          	add.uw	t4,a5,a2
    39ec:	08c70f3b          	add.uw	t5,a4,a2
    39f4:	08c68fbb          	add.uw	t6,a3,a2
    39fc:	08c883bb          	add.uw	t2,a7,a2
    3a04:	08c3043b          	add.uw	s0,t1,a2
    3a10:	08c802bb          	add.uw	t0,a6,a2
    3a14:	08ce04bb          	add.uw	s1,t3,a2
    ...
```

## 哪吒 D1 开发板性能对比

我们在哪吒 D1 开发板上使用 GCC11.3.0 编译 CoreMark 程序，进行对比测试，其在 `-O2` 和 `-O3` 优化参数下的得分分别为 2.31 和 2.49。与上表中的基础版本与应用优化 1 后的版本进行对比可以发现，VisionFive 2 开发板的性能要优于哪吒 D1 开发板。

```shell
$ ./coremark.riscv
2K performance run parameters for coremark.
CoreMark Size    : 666
Total ticks      : 12869
Total time (secs): 12.869000
Iterations/Sec   : 2331.183464
Iterations       : 30000
Compiler version : GCC11.3.0
Compiler flags   : -O2
Memory location  : Please put data memory location here
                        (e.g. code in flash, data on heap etc)
seedcrc          : 0xe9f5
[0]crclist       : 0xe714
[0]crcmatrix     : 0x1fd7
[0]crcstate      : 0x8e3a
[0]crcfinal      : 0x5275
Correct operation validated. See README.md for run and reporting rules.
CoreMark 1.0 : 2331.183464 / GCC11.3.0 -O2    / Heap

$ ./coremark.riscv.o3
2K performance run parameters for coremark.
CoreMark Size    : 666
Total ticks      : 15926
Total time (secs): 15.926000
Iterations/Sec   : 2511.616225
Iterations       : 40000
Compiler version : GCC11.3.0
Compiler flags   : -O3
Memory location  : Please put data memory location here
                        (e.g. code in flash, data on heap etc)
seedcrc          : 0xe9f5
[0]crclist       : 0xe714
[0]crcmatrix     : 0x1fd7
[0]crcstate      : 0x8e3a
[0]crcfinal      : 0x25b5
Correct operation validated. See README.md for run and reporting rules.
CoreMark 1.0 : 2511.616225 / GCC11.3.0 -O3    / Heap
```

## 总结

本文使用 CoreMark 测试集评测了 JH7110 这款 CPU，给出了其单核、多核与 PGO 优化后的测试结果，并与哪吒 D1 开发板进行对比。本次实验中，JH7110 的 CoreMark 分数为 4.54 分/MHz，尽管采用了升级 GCC、添加编译参数等手段，但与官方的测试结果 5.09 分/MHz 相比仍有些差距。本文的实验调试参考了论坛上一篇关于 JH7110 处理器 CoreMark 测试分数复现的 [帖子][007]，该作者实验达到的 CoreMark 分数为 4.83 分/MHz。今后将进一步分析原因，并使用更加专业的测试集 [CoreMark-Pro][002] 评测多核性能。

## 参考资料

- [CPU 性能测试——CoreMark 篇][005]
- [CPU 性能测试——CoreMark-pro 篇][004]

[001]: https://github.com/eembc/coremark
[002]: https://github.com/eembc/coremark-pro
[003]: https://github.com/riscv-boom/riscv-coremark
[004]: https://www.cnblogs.com/ImagineMiracle-wxn/p/CPU_CoreMark-pro_test.html
[005]: https://www.cnblogs.com/ImagineMiracle-wxn/p/CPU_CoreMark_test.html
[006]: https://www.eembc.org/coremark/
[007]: https://www.reddit.com/r/RISCV/comments/11rh53q/anyone_reproduced_sifive_starfive_claimed/
