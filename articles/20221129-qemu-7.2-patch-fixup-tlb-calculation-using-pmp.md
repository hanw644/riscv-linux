> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [spaces toc comments codeblock urls pangu autocorrect epw]<br/>
> Author:    sfpyjs <Yuandao170625@gmail.com><br/>
> Date:      20221129<br/>
> Revisor:   Falcon <falcon@tinylab.org>;Bin Meng <bmeng.cn@gmail.com><br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [QEMU 模拟器观察 - v7.2](https://gitee.com/tinylab/riscv-linux/issues/I60FB1)<br/>
> Sponsor:   PLCT Lab, ISCAS

# QEMU-7.2 补丁 Fixup TLB size calculation when using PMP 分析

## 前言

本文将介绍 QEMU-7.2 版本中[补丁][001] Fixup TLB size calculation when using PMP 的改动逻辑。

## 基础知识

### PMP

PMP (Physical Memory Protection) 是一种针对于物理地址区域的访问控制机制。
> To support secure processing and contain faults, it is desirable to limit the physical addresses accessible by software running on a hart. ("3.7 Physical Memory Protection" in "riscv-privileged.pdf")

1.9 版本的特权指令集规范已提出 PMP 机制，但是该版本规范中 PMP 机制所使用的机制原理相对简单，其中使用的寄存器组合于后续的特权指令集规范版本（1.10）中弃用。
1.10 版本特权指令集规范的前言部分明确提到：一种可选的 PMP (Physical Memory Protection) 范式已被提出。此处保护范式便是目前在 RISC-V 系统中使用广泛的物理内存保护机制。
> An optional physical-memory protection (PMP) scheme has been proposed. ("Preface to Version 1.10" in "riscv-privileged.pdf")


#### 寄存器设施

PMP 机制由 PMP 项中存储的信息进行访问控制。PMP 项对应于 PMP CSRs (Control/Status Registers)。具体来说，每个 PMP 项由一个 8 位 PMP 配置寄存器值（pmpcfg）和 XLEN 位 PMP 地址寄存器值（pmpaddr）组成。8 位的 PMP 配置寄存器值 pmpXcfg 将会压缩存放于 XLEN 位长度的配置寄存器中 pmpcfgX 中。而 PMP 地址寄存器值则恰好对应一个地址寄存器 pmpaddrX 的字长并存储其中。
> PMP entries are described by an 8-bit configuration register and one MXLEN-bit address register.


#### 机制

##### 设置 PMP

PMP 项，更具体说是 PMP CSRs，只能有 M 态的 hart 访问。
> PMP CSRs are only accessible to M-mode. ("3.7 Physical Memory Protection" in "riscv-privileged.pdf")

当 PMP 项中的配置寄存器的 L(Lock) 字段被设置为 1 时，任何 hart（包括 M 态的 hart）失去修改该 “上锁” PMP 项的能力（直到 hart 复位）。同时，该字段会要求 M 态 hart 也要遵守该 PMP 项所指定的访问规则。
> The L bit indicates that the PMP entry is locked, i.e., writes to the configuration register and associated address registers are ignored. Locked PMP entries remain locked until the hart is reset. In addition to locking the PMP entry, the L bit indicates whether the R/W/X permissions are enforced on M-mode accesses. ("Locking and Privilege Mode" in "3.7 Physical Memory Protection" of "riscv-privileged.pdf")

##### PMP 生效

任何访存操作都需要经过 PMP 机制的检查。按照 QEMU 中的具体实现，当虚拟地址转换为物理地址之后，PMP 机制会检查本次访问的地址是否满足其要求。<br />PMP 项通过配置寄存器中的 A 字段来表征地址匹配方式即检查方式。具体细节可以查看章节（"Address Matching" in "3.7 Physical Memory Protection" of "riscv-privileged.pdf"）

### TCG

TCG（Tiny Code Generation）是 QEMU 中使用的动态二进制翻译引擎。动态二进制翻译就是将有“一种”指令集表示的程序翻译成由”另一种“指令集表示的程序。在虚拟化场景中，“一种”是客户机的指令集，”另一种“是宿主机的指令集。TCG 的工作模式是先将客户机的指令片段转化为 TCG 中间表示，再将 TCG 中间表示转化为宿主机的代码。<br />不妨让我们以与本补丁相关的 CPU 访存虚拟过程为例，浅显解释 QEMU 的虚拟化逻辑：首先，利用 TCG 的 QEMU 将虚拟化出 vCPU，vCPU 将会在这些客户机指令块上取指执行。取指是一种访存操作，QEMU 将会把虚拟内存中的代码取出，翻译，执行。翻译会将客户机的指令以及指令对应的操作数（自然包括地址）翻译成宿主机上的表示。具体过程如下：
1. 客户机访问 GVA 处代码，虚拟机会在 vMMU 上进行寻址。
2. 寻址后得到 GPA。
3. 同时，客户机会启用寻址缓存即 TLB 机制，将寻址得到的 GPA 和 GVA 成对存储到 vTLB 中。
4. QEMU 负责将 GPA 转换为 HVA，宿主机 OS 将 HVA 转换为 HPA，从而真正取到客户机要求的指令片段。
5. 取得的指令片段经过 TCG 引擎翻译，指令以及指令操作数将会从客户机视角转换为宿主机视角，从而使得客户机逻辑在 QEMU 向宿主机 OS 所申请的资源内执行，完成模拟。

## 补丁描述

读者可以从补丁对应的 [提交][001] 以及 [邮件记录][003] 中得知，本次补丁是为了修复 CPU 设置 TLB 表项时触发断言异常。但是一个补丁的资料相对匮乏，让我们从代码角度入手，了解问题的原因以及修复问题的手段。

```c
cpu_exec{
    tb_lookup{
    	tb_htable_lookup{
            get_page_addr_code{ /* 1 */
                get_page_addr_code_hostp{
                    probe_access_internal{
                        ...
                        if (!tlb_hit_page(tlb_addr, page_addr)) { /* 未命中 vTLB 时 */
                            ...
                            cs->cc->tcg_ops->tlb_fill /* 2,3 */
                            ...
                        }
                        ...
                        *phost = (void *)((uintptr_t)addr + entry->addend); /* 4 */
                        ...
                    }
                }
            }
        }
    }
}
```

以上代码可以对应于文章 TCG 部分所述的例子。下面，我们更为详细的展示 tlb_fill 函数的代码逻辑：

```c
/* TLB 未命中 */
cs->cc->tcg_ops->tlb_fill{ /* tlb_fill 函数逻辑将会转到体系结构相关的代码逻辑中 */
	riscv_tcg_ops.tlb_fill = riscv_cpu_tlb_fill{
        ...
        if (riscv_cpu_virt_enabled(env) ...){ /* 判断是否开启了虚拟内存以及两阶段寻址 */
            ...
        } else { /* 以简单的一阶段寻址来说明问题 */
            get_physical_address /* 1 */
            get_physical_address_pmp{
                pmp_hart_has_privs /* PMP 对本次访问进行检查 */
                pmp_is_range_in_tlb{ /* PMP 所限制的区域与 TLB 将要记录的页面区域有无交集，该功能由 pmp_get_tlb_size 具体实施 */
                	...
                    pmp_get_tlb_size
                    /*
						补丁判断：pmp_get_tlb_size 的结果是否为 0。
                        如果是 0，则说明 PMP 和本次 TLB 将要填充的表项无交集
                    	如果非 0，则说明有交集。此时又分为两种情况：
                    	1.如果交集区域为一整页，不操作
                    	2.如果交集区域小于一整页，将 tlb_size 值设置为 1。
                    */
                    // if (*tlb_size != 0) {
                    //     if (*tlb_size < TARGET_PAGE_SIZE) {
                    //         *tlb_size = 1;
                    //     }
                    // }
                	...
                }
            }
        } /* 2 寻址成功，且寻址结果通过 PMP 检查 */
        ...
        tlb_set_page{
        	tlb_set_page_with_attrs{
                ...
				assert(is_power_of_2(size)); /* 3 检测 size 是否为 2 的次幂 */
            	tlb_set_page_full{ /* 设置 TLB 表项 */
                    ...
                    if (full->lg_page_size < TARGET_PAGE_BITS) { /* 如果 size 小于页面大小，本次 TLB 设置失败 */
                        address |= TLB_INVALID_MASK;
                    }
                    ...
                }
            }
        }
        ...
    }
}
```

1. CPU 发射访存指令，在 TLB 未命中情况下，不得不进行访存寻址。
2. 成功获得物理地址后，并且通过 PMP 检查，尝试将本次寻址所得到的物理地址存储于 TLB 中以加速之后对同一地址访存的过程。
3. 在进行 TLB 表项设置过程中，新设置的 TLB 表项必须经过检查以确认该 TLB 表项的合理性。
 
TLB 表项设置过程中有一项操作会检查传入的参数 size，size 表征了通过 PMP 检查过程中能够合理设置到 TLB 中以加速地址转换的区域大小（同时，size 也会设置为将要添加的新 CPUTLBEntryFull 表项的 lg_page_size 字段）。断言语句保证，size 必须是 2 的次幂。<br />其中，PMP 检查的工作就是查看 PMP 中限制的区域与 TLB 表项将要设置的页面区域之间的关系。我们不妨先回忆一下，TLB 中一个表项所映射的大小为一个页面，当 PMP 表项所限制的区域不是包含该页面或者与该页面重合时，我们是不能在通过 PMP 检查后将该页面填充到 TLB 中的。因为，按照以上的代码逻辑，当 TLB 命中时，可以绕过 PMP 检查，这是违反使用 PMP 的初衷的。所以，QEMU 的开发者就会在寻址完成后（TLB 填充前），对 PMP 与 TLB 进行检查。有 pmp_get_tlb_size 给出本次将要填充的 TLB 项对应的有效区域（对应于该 [补丁][004]）。<br />之后，TLB 填充过程中会检查其区域大小：首先，tlb_set_page_with_attrs 会检查交集大小是否为 2 的次幂，由于 PMP 保护区域的多样性，其表示的区域与一个页面相交会出现多种形式，因此，我们需要修改 size 以通过该检查。接着，在 tlb_set_page_full 函数中，还有一系列关于 TLB 的检查，其中一项便是对其有效区域的判别：当非整页有效时，本次 TLB 设置无效。<br />综上，我们需要修改 size，修改成一个可以通过 tlb_set_page_with_attrs 中检查，并且不影响之后语义的值，我认为 0、1 等小于一整页大小的 2 的次幂都是可以的。Francis 在这里选的是 1。

## 补丁复现

本次实验我们分别基于 MIT-6.828 实验的 xv6 操作系统和 5.9 版本的 Linux 操作系统复现补丁引入前所造成的 Bug。

### xv6 OS

```shell
# Download the QEMU-7.2.0
wget https://download.qemu.org/qemu-7.2.0-rc0.tar.xz
tar -xvf qemu-7.2.0-rc0.tar.xz
cd qemu-7.2.0-rc0
./configure --target-list=riscv64-softmmu,riscv64-linux-user --enable-debug
make
make install
# Download the xv6 OS
git clone git://g.csail.mit.edu/xv6-labs-2020
cd xv6-labs-2020
# edit .gdbinit
vi .gdbinit
git checkout util
# Obtain the infrastructure for debugging QEMU e.g. fs.img
make qemu
Ctrl+A x
gdb --args qemu-system-riscv64 -machine virt -bios none -kernel kernel/kernel -m 128M -smp 1 -nographic -drive file=fs.img,if=none,format=raw,id=x0 -device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0
```

```shell
b riscv_cpu_tlb_fill
b target/riscv/cpu_helper.c:801
b target/riscv/pmp.c:629

commands 1
printf "The uninitialized PMP is as follows:\n"
p/x cpu->env->pmp_state.addr
end

commands 2
printf "The virtual address is %x\n", addr
printf "The physical address is %x\n", *physical
cpuvirt
printf "Tamper of PMP ... \n"
set  cpu->env->pmp_state.addr.sa = 0x1000
set  cpu->env->pmp_state.addr.ea = 0x1004
end

commands 3
p/x *tlb_size
end

define cpuvirt
printf "The CPU virt enabled is "
p riscv_cpu_virt_enabled(env)
printf "\n"
end
```

### Linux
RISC-V Linux 构建流程主要参考[该博客][005]
```shell
# Download the riscv-gnu-toolchain FROM https://github.com/riscv-collab/riscv-gnu-toolchain
tar -zxvf riscv64-glibc-ubuntu-20.04-nightly-2022.12.17-nightly.tar.gz 

# Download the QEMU-7.2.0
wget https://download.qemu.org/qemu-7.2.0-rc0.tar.xz
tar -xvf qemu-7.2.0-rc0.tar.xz
cd qemu-7.2.0-rc0
./configure --target-list=riscv64-softmmu,riscv64-linux-user --enable-debug
make
make install

mkdir riscv64-linux

# Download the Linux v5.9 FROM MIRROR https://mirrors.tuna.tsinghua.edu.cn/help/linux.git/
# (riscv64-linux)
git clone https://mirrors.tuna.tsinghua.edu.cn/git/linux.git -b v5.9 --depth=1
cd linux
PATH=$PATH:<riscv-toolchain>/bin make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- defconfig
PATH=$PATH:<riscv-toolchain>/bin make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- menuconfig
PATH=$PATH:<riscv-toolchain>/bin make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j$(nproc)

# Download the busybox
git clone git://git.busybox.net/busybox
git checkout -b 1_35_0_stable
PATH=$PATH:/home/qemu/riscv/bin make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- menuconfig
PATH=$PATH:/home/qemu/riscv/bin make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu-
PATH=$PATH:/home/qemu/riscv/bin make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- install

# (riscv64-linux)
qemu-img create rootfs.img  1g
mkfs.ext4 rootfs.img
mkdir rootfs
sudo mount -o loop rootfs.img  rootfs
cd rootfs
sudo cp -r <busybox>/_install/* .
sudo mkdir proc sys dev etc etc/init.d
cd etc/init.d/
sudo vi rcS
# #!/bin/sh
# mount -t proc none /proc
# mount -t sysfs none /sys
# /sbin/mdev -s
sudo chmod +x rcS
sudo umount rootfs

# (riscv64-linux)
gdb --args qemu-system-riscv64 -M virt -m 256M -nographic -kernel <linux>/arch/riscv/boot/Image -drive file=rootfs.img,format=raw,id=hd0  -device virtio-blk-device,drive=hd0 -append "root=/dev/vda rw console=ttyS0"
```

### 复现结果

xv6 和 Linux 在引入补丁前会因恶意构造的 PMP 项导致无法通过 TLB 尺寸检查声明。

## 总结

本文从 QEMU 虚拟化实现和 PMP 检查机制等方面出发，说明了 QEMU-7.2 版本引入的补丁改进。

## 参考资料

- [RISC-V 特权指令集规范][002]

[001]: https://github.com/qemu/qemu/commit/47566421f029b0a489b63f8195b3ff944e017056
[002]: https://github.com/riscv/riscv-isa-manual/releases
[003]: https://patchwork.kernel.org/project/qemu-devel/patch/20221012011449.506928-1-alistair.francis@opensource.wdc.com/
[004]: https://patchwork.kernel.org/project/qemu-devel/patch/6b0bf48662ef26ab4c15381a08e78a74ebd7ca79.1595924470.git.zong.li@sifive.com/
[005]: https://zhuanlan.zhihu.com/p/258394849
