> Corrector:   [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1-rc3 - [spaces codeinline]<br/>
> Author:      ZhaoSQ <1044239768@qq.com><br/>
> Date:        2022/07/18<br/>
> Revisor:     Falcon <falcon@tinylab.org><br/>
> Project:     [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Environment: [Linux Lab Disk][006]<br/>
> Sponsor:     PLCT Lab, ISCAS

# 在泰晓 Linux 实验盘中构建 QEMU 并引导 openEuler for RISC-V

[泰晓 Linux 实验盘][006] 已经提供了一些基础包（包含了编译器和编译工具：gcc、git、make 等），但是在构建 QEMU 之前需要安装一些额外的包。

## 安装编译所需的软件包

### 基础软件包

* glib2.0-dev (9 MiB)，这会自动包含 zlib1g-dev。
* libfdt-devel。

对于 Ubuntu LTS（可能还有其他基于 Debian 的发行版），所有必须的附加软件包都可以像这样安装：

```
$ sudo apt install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev ninja-build
```

### 额外的软件包

* git-email, used for sending patches
* libsdl2-dev, needed for the SDL based graphical user interface
* gtk3-devel, for a simple UI instead of VNC
* vte-devel, for access to QEMU monitor and serial/console devices via the GTK interface
* libcapstone-dev, for disassembling CPU instructions

上边的列表并不是 QEMU 完整的软件包。为了尽可能多的使用 QEMU 的功能，在执行 `configure` 前，应该安装更多的软件包。

对于 Ubuntu 系统，推荐安装额外的软件包，以实现最大的功能范围，可以安装如下内容：

```
$ sudo apt install git-email
$ sudo apt install libaio-dev libbluetooth-dev libcapstone-dev libbrlapi-dev libbz2-dev
$ sudo apt install libcap-ng-dev libcurl4-gnutls-dev libgtk-3-dev
$ sudo apt install libibverbs-dev libjpeg8-dev libncurses5-dev libnuma-dev
$ sudo apt install librbd-dev librdmacm-dev
$ sudo apt install libsasl2-dev libsdl2-dev libseccomp-dev libsnappy-dev libssh-dev
$ sudo apt install libvde-dev libvdeplug-dev libvte-2.91-dev libxen-dev liblzo2-dev
$ sudo apt install valgrind xfslibs-dev
```

较新版本的 Debian / Ubuntu 也可能会尝试这些附加软件包：

```
$ sudo apt install libnfs-dev libiscsi-dev
```

## 获取源代码

如果想要使用最新的代码，甚至为代码做出贡献，建议通过 git 获取 QEMU 的代码，仓库地址为：[QEMU][004]。

推荐使用如下命令下载软件：

```
$ git clone https://gitlab.com/qemu-project/qemu.git
```

## 简单的构建和测试

QEMU 支持在根目录下的任意文件夹进行构建，因此可以有很多不同的构建文件夹。比如，构建在 `QEMU` 根目录下的 `bin/debug/native` 中。

这是我的构建过程：

```
// 打开 QEMU 根目录
$ cd qemu

// 准备构建文件夹，bin/debug/native
$ mkdir -p bin/debug/native
$ cd bin/debug/native

// 配置 QEMU 并开始构建
$ ../../../configure --enable-debug
$ make

// 返回 QEMU 根目录
$ cd ../../..
```

构建完成，开始一个简单测试：

```
$ bin/debug/native/x86_64-softmmu/qemu-system-x86_64 -L pc-bios
```

此测试运行启动 PC BIOS 的 QEMU 系统仿真。

## 编译支持 RISC-V 的 QEMU

如果希望仿真 RISC-V 处理器，则在配置时需要加上 `--target-list="riscv32-softmmu riscv64-softmmu riscv64-linux-user"`。

> `riscv32-softmmu`, `riscv64-softmmu` 为系统模式，`riscv64-linux-user` 为用户模式。为了测试方便，都安装。

下面是完成的配置与编译过程（在 QEMU 根目录中，执行以下命令）：

```
// 准备构建文件夹，bin/debug/riscv
$ mkdir -p bin/debug/riscv
$ cd bin/debug/riscv

// 配置 QEMU 并开始构建
$ ../../../configure --target-list="riscv32-softmmu riscv64-softmmu riscv64-linux-user"
$ make

// 返回 QEMU 根目录
$ cd ../../..
```

构建完成，开始一个简单测试，在 QEMU 的根目录下，执行以下命令。

```
$ bin/debug/riscv/qemu-system-riscv64 --version
```

如出现类似如下输出表示 QEMU 成功安装并正常工作。

> QEMU emulator version 7.0.50 (v7.0.0-2400-g63b38f6c85)
>
> Copyright (c) 2003-2022 Fabrice Bellard and the QEMU Project developers

## 下载并运行 openEuler for RISC-V

### 下载 openEuler RISC-V 系统镜像

我们的目标是运行的 openEuler RISC-V OS，本次使用的版本为 20.03。

下载地址：<https://repo.openeuler.org/openEuler-preview/RISC-V/Image/>

其中的两个文件是启动 openEuler RISC-V 移植版所必须的：

* `fw_payload_oe.elf/fw_payload_oe_docker.elf` 利用 `openSBI` 将 kernel-5.5 的 image 作为 payload 所制作的用于 QEMU 启动的 image。这两个文件选择一个就可以，前者不支持 Docker，后者支持。
* `openEuler-preview.riscv64.qcow2` 是 `openEuler RISC-V` 移植版的 rootfs 镜像。

### 通过 QEMU 启动一个 openEuler RISC-V

在 QEMU 的同级目录下，新建一个名为 `openeuler/` 的文件夹，将 `fw_payload_oe.elf` 和 `openEuler-preview.riscv64.qcow2` 文件拷贝进去，然后用 QEMU 启动系统：

```
$ mkdir openeuler/ && cd openeuler/
// 把 `fw_payload_oe.elf` 和 `openEuler-preview.riscv64.qcow2` 拷贝进来，然后执行下述命令
$ ~/Desktop/workspaces/qemu/bin/debug/riscv/qemu-system-riscv64 \
  -nographic -machine virt \
  -smp 2 -m 2G \
  -kernel fw_payload_oe_docker.elf \
  -drive file=openEuler-preview.riscv64.qcow2,format=qcow2,id=hd0 \
  -object rng-random,filename=/dev/urandom,id=rng0 \
  -device virtio-rng-device,rng=rng0 \
  -device virtio-blk-device,drive=hd0 \
  -device virtio-net-device,netdev=usernet \
  -netdev user,id=usernet,hostfwd=tcp::12055-:22 \
  -append 'root=/dev/vda1 rw console=ttyS0 systemd.default_timeout_start_sec=600 selinux=0 highres=off earlycon' \
  -bios none
```

注意 `-smp` 选项为 CPU 核数，`-m` 为虚拟机内存大小，请根据宿主机配置酌情修改。

启动命令可做成 Shell 脚本，其中 `fw_payload_oe_docker.elf` 与 `openEuler-preview.riscv64.qcow2` 需修改为实际下载的文件名，并注意相对和绝对路径，如做成启动脚本则可与脚本放在同一目录下。

### 登录 openEuler 系统

* 登录用户：`root`
* 默认密码：`openEuler12#$`

建议在登录成功之后立即修改 root 用户密码，登陆成功之后，可以看到如下的信息：

```
openEuler 20.03 (LTS)
Kernel 5.5.19 on an riscv64

login: er-RISCV-rare
openEuler 20.03 (LTS)
Kernel 5.5.19 on an riscv64

openEuler-RISCV-rare login: root
Password:
Last login: Tue Sep  3 09:30:22 on ttyS0

Welcome to 5.5.19

System information as of time: 	Tue Sep  3 09:30:17 UTC 2019

System load: 	0.64
Processes: 	65
Memory used: 	2.6%
Swap used: 	0.0%
Usage On: 	12%
Users online: 	1

[root@openEuler-RISCV-rare ~]#
```

建议在 Host Linux 上通过 ssh 登录到运行在 QEMU 模拟器之上的 openEuler OS，如直接使用，可能会出现 VIM 键盘输入异常。

```
$ ssh -p 12055 root@localhost
```

## 小结

本文详细介绍了如何在 [泰晓 Linux 实验盘][006] 中，快速从源码构建一个用于仿真 RISC-V 处理器架构的 QEMU，并演示了如何下载并启动一个最新版的 openEuler for RISC-V 系统，希望对大家有所帮助。

根据 [Linux Lab][003] 项目的最新开发动态，我们发现在它的 next 分支中已经可以直接编译 QEMU v7.0.0，预计在新版 [泰晓 Linux 实验盘][006] 中，也将很快支持 QEMU 的自动构建，敬请期待，我们下回再进行相关演示。

## 参考资料

1. [Linux 主机上构建 QEMU][007]
2. [在 Ubuntu 上安装 GitHub desktop][001]
3. [Running 64- and 32-bit RISC-V Linux on QEMU][005]
4. [通过 QEMU 仿真 RISC-V 环境并启动 OpenEuler RISC-V 系统][002]

[001]: https://gist.github.com/berkorbay/6feda478a00b0432d13f1fc0a50467f1
[002]: https://gitee.com/openeuler/RISC-V/blob/master/doc/tutorials/vm-qemu-oErv.md
[003]: https://gitee.com/tinylab/linux-lab
[004]: https://gitlab.com/qemu-project/qemu.git
[005]: https://risc-v-getting-started-guide.readthedocs.io/en/latest/linux-qemu.html
[006]: https://tinylab.org/linux-lab-disk
[007]: https://wiki.qemu.org/Hosts/Linux
