> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [urls autocorrect]<br/>

# 会议或直播安排与记录

该目录用于记录“RISC-V Linux 内核兴趣小组”的会议或直播分享记录和安排。

## 时间安排

如果没有特别的说明，会议和直播安排都是 **每周六晚上**：

* 会议时间：20:00 PM - 20:30 PM
* 直播时间：20:30 PM - 21:30 PM

## 讲师须知

1. 关于大纲

   - 大纲发送之前可以先给指导老师和吴老师进行审核，两者确认无误之后再发给 @晓怡
   - 最好提前一周发送大纲，最晚时间为您进行直播分享周的星期三

2. 直播分享时使用的 PPT 模板

   - [ppt](https://gitee.com/tinylab/riscv-linux/tree/master/ppt) 目录下有统一模板，检索 template 就好，记得在线下载最新的版本
   - 也可以使用 [Markdown Lab](https://gitee.com/tinylab/markdown-lab) 中的 slides 模板

3. 提前十分钟左右进入会议室

   - 进会议室需要测试网络；如果电脑音频不好可以用手机登陆当音频输入设备
   - 会议地址见下一节

4. 提交幻灯 PR

   演讲完以后，请把幻灯提交进上述 ppt 目录

## 会议地址

会议和直播均采用腾讯会议软件，2022/07/23 添加哔哩哔哩直播。

- 会议主题：RISC-V Linux 内核技术分享会
- 会议时间：2023/01/07-2026/10/31 20:30-21:30，每周（周六）
- 参会方式
    - 点击链接入会：<https://meeting.tencent.com/dw/YcrpcIPiiIIb>
    - 腾讯会议入口：915-4115-9120
    - 哔哩哔哩直播：<https://live.bilibili.com/25518693>


Linux 内核观察 —— 即时剖析每一个 Linux 内核大版本的关键特性。

- 会议主题：Linux 内核观察
- 会议时间：每个 Linux 内核大版本发布后的一周内开展线上『Linux 内核观察』线上技术分享活动
- 参会方式
    - 点击链接入会：<https://meeting.tencent.com/dw/YcrpcIPiiIIb>
    - 腾讯会议入口：915-4115-9120
    - 哔哩哔哩直播：<https://live.bilibili.com/25518693>

## 直播计划

### 回放发布

回放发布在如下三个视频频道：

- [泰晓学院][202208204]
- [B 站频道][202208203]
- [知乎专栏][202208205]

### 分享安排

部分未确定时间的分享还处在预约状态，具体分享时间待定，先完成的先做分享。

#### TODO

- 实习生技术分享
    - 3D 外壳设计 @Reset12138

- YocTo 项目介绍 @linuxer
    - D1 开发板

- RISC-V 内核基础函数与汇编库分析 @郭天佑 TanyoKwok

- 微架构优化与验证 @falcon
    - 以 Ftrace & Tracepiont 为例，介绍内核 JIT 思想，优化原理，并通过分析汇编和二进制确认最终优化效果

- 不同架构基础指令的性能测试与数据分析 @hev @iOSDevLog @dlan17
    - 设计测试案例实测不同架构上的多个基础指令的性能数据并分析数据背后的处理器设计差异（欢迎 CPU 设计人员参与）

- 内核运行时代码修改的原理与实现
    - 结合 Ftrace 与 Tracepoint 源码分析如何在运行时修改内核代码以及可能要注意的事项，包括多核竞争与内存管理等

### 已完成

- 20230805:初探内存一致性 - 苏老师 - 哔哩哔哩 -  @苏伟帅
    - 这次邀请苏老师来分享 初探内存一致性 相关知识。
    - 已上传视频剪辑

- 20230729:x86 架构下 unikernelLinux 的分析与总结 - 王老师 - 哔哩哔哩   @Gege-Wang
    - 这次邀请王老师来分享 x86 架构下 unikernelLinux 的分析与总结 相关知识。
    - 已上传视频剪辑

- 20230722:Kernellibc 的介绍 - 姜老师 - 哔哩哔哩   @jingqing3948
    - 这次邀请姜老师来分享 Kernellibc 的介绍 相关知识。
    - 已上传视频剪辑

- 20230715:SBI 入门介绍 - 刘老师 - 哔哩哔哩  @groot00114
    - 这次邀请刘老师来分享 SBI 入门介绍 相关知识。
    - 已上传视频剪辑

- 20230708:RISC-V Linux v6.4-rc linear mapping practise - 宋老师 - 哔哩哔哩  @sugarfillet
    - 这次邀请宋老师来分享 RISC-V Linux v6.4-rc linear mapping practise 相关知识。
    - 已上传视频剪辑

- 20230701: Linux 包管理器的演进与现状 - 张老师 - 哔哩哔哩  @IIE
    - 这次邀请张老师来分享 Linux 包管理器的演进与现状 相关知识。
    - 已上传视频剪辑

- 20230624: 泰晓社区-TSoC2023-暑期实习-启动会 - 谭老师 - 哔哩哔哩  @Reset12138
    - 这次邀请谭老师来分享 泰晓社区-TSoC2023-暑期实习-启动会 相关知识。
    - 已上传视频剪辑

- 20230617: QEMU debug & upstream practice - 吴老师 - 哔哩哔哩  @falcon
    - 这次邀请吴老师来分享 QEMU debug & upstream practice 相关知识。
    - 已上传视频剪辑

- 20230610: RISC-V Semihosting 是个什么鬼？ - 蒙老师 - 哔哩哔哩 @lbmeng
    - 这次邀请蒙老师来分享 RISC-V Semihosting 是个什么鬼？ 相关知识。
    - 已上传视频剪辑

- 20230603: PCIe 基本原理和资源访问会议 - 王老师 - 哔哩哔哩 @walimis
    - 这次邀请王老师来分享 PCIe 基本原理和资源访问会议 相关知识。
    - 已上传视频剪辑

- 20230527: 捡到枪的系统软件工程师 (RISC-V Linux 为例) - wayling - 哔哩哔哩 
    - 这次邀请 wayling 老师来分享 捡到枪的系统软件工程师 (RISC-V linux 为例) 相关知识。
    - 已上传视频剪辑

- 20230520: 从零开始写一个 RISC-V 模拟器（2）- trap 机制与 RISC V-tests - 张老师 - 哔哩哔哩 @yjmstr 
    - 这次邀请张老师来分享 从零开始写一个 RISC-V 模拟器（2）- trap 机制与 RISC V-tests 相关知识。
    - 已上传视频剪辑

- 20230507: Linux 内核观察 - v6.3 - 吴老师 - 哔哩哔哩   @falcon
    - 这次邀请吴老师来分享 Linux 内核观察 - v6.3 相关知识。
    - 已上传视频剪辑

- 20230423: QEMU 8.0观察分享 - 蒙老师 - 哔哩哔哩  @lbmeng
    - 这次邀请蒙老师来分享 QEMU 8.0观察分享 相关知识。
    - 已上传视频剪辑

- 20230415: VisionFive 2开发板介绍与评测 - 周老师 - 哔哩哔哩  @Kepontry
    - 这次邀请周老师来分享 VisionFive 2开发板介绍与评测 相关知识。
    - 已上传视频剪辑

- 20230408: Static Call - 牛老师 - 哔哩哔哩  @nfk1996
    - 这次邀请牛老师来分享 Static Call 相关知识。
    - 已上传视频剪辑

- 20230401: fuse文件系统浅析 - 张老师 - 哔哩哔哩  @张毅峰老师
    - 这次邀请张老师来分享 fuse文件系统浅析 相关知识。
    - 已上传视频剪辑

- 20230325: 从零开始写一个 RISC-V 模拟器 - TinyEMU - 张老师 - 哔哩哔哩  @yjmstr 
    - 这次邀请张老师来分享 从零开始写一个 RISC-V 模拟器 - TinyEMU 相关知识。
    - 已上传视频剪辑

- 20230318: RISC-V 启动流程中的重定位（近期 upstream 工作介绍） - 蒙老师 - 哔哩哔哩  @lbmeng
    - 这次邀请蒙老师分享 RISC-V 启动流程中的重定位（近期 upstream 工作介绍）相关知识。
    - 已上传视频剪辑

- 20230311: RISC-V Linux 缺页异常 - 汤老师 - 哔哩哔哩   @tangjinyu1994
    - 这次邀请汤老师分享 RISC-V Linux 缺页异常相关知识。
    - 已上传视频剪辑

- 20230304: 硬件虚拟化在不同平台的实现：X86、ARM 和 RISC-V - 王老师 - 哔哩哔哩  @walimis
    - 这次邀请到王老师分享 3 大主流处理器架构（X86、ARM 和 RISC-V）的硬件虚拟化实现相关知识
    - 已上传视频剪辑

- 20230225: v6.2 内核观察 - 吴老师 - 哔哩哔哩   @falcon
    - 这次邀请到吴老师来分享 v6.2 内核观察相关知识
    - 已上传视频剪辑

- 20230218: 近期 upstream 工作介绍（RISC-V PLIC 相关） - 蒙老师 - 哔哩哔哩  @lbmeng
    - 这次邀请到蒙老师来分享 近期 upstream 工作介绍（RISC-V PLIC 相关）相关知识
    - 已上传视频剪辑

- 20230107: QEMU 7.2 观察 - 蒙老师 - 哔哩哔哩  @lbmeng
    - 这次邀请到蒙老师来分享 QEMU 7.2 观察相关知识
    - 已上传视频剪辑

- 20230103:Linux 内核观察 - v6.1 - Linux 内核观察小组 - 哔哩哔哩   
    - 这次邀请到Linux 内核观察小组来分享Linux 内核观察 - v6.1 观察相关知识
    - 已上传视频剪辑

- 20221224: RISC-V 平台上 benchmark 基准测试调研 - 徐老师 - 哔哩哔哩  @xuyq0306
    - 这次邀请到徐宇奇同学来分享 RISC-V 平台上 benchmark 基准测试调研相关知识
    - 已上传视频剪辑

- 20221217: Kernel Build System-Kbuild 子系统详解 - 贾老师 - 哔哩哔哩  @iOSDevLog
    - 这次邀请到贾老师来分享 Kernel Build System-Kbuild 子系统详解相关知识
    - 已上传视频剪辑

- 20221210: RISC-V Linux 内核技术调研活动寒假实习宣讲会 - 贾老师 - 哔哩哔哩  @iOSDevLog
    - 这次邀请到贾老师来分享 RISC-V Linux 内核技术调研活动寒假实习相关的部分
    - 已上传视频剪辑

- 20221203: RISC-V SBI 规范的 HSM 扩展，暨 SMP 支持优化 - 张老师 - 哔哩哔哩  @yjmstr 
    - 这次邀请到张炀杰同学来分享 RISC-V SBI 规范的 HSM 扩展相关知识
    - 已上传视频剪辑

- 20221126: RISC-V 虚拟化技术调研第一阶段简报 - 潘老师 - 哔哩哔哩  @XiakaiPan
    - 这次邀请到潘夏凯同学来分享 RISC-V 虚拟化技术相关知识
    - 已上传视频剪辑

- 20221119：[一条 ADD 指令的前世今生，RISC-V 处理器设计分享 - 王老师][202211191] @bosswangst
    - 这次邀请到王宇豪同学来分享 RISC-V 处理器设计相关知识
    - 已上传视频剪辑

- 20221112：[RISC-V Linux 内核中断管理技术分享 - 牛老师 - 哔哩哔哩][202211121] @nfk1996
    - 这次邀请到牛老师来分享 RISC-V Linux 内核中断管理相关的部分
    - 已上传视频剪辑

- 20221105：[基于 RISC-V 开发板实现实时人物检测与监控 - 周老师 - 哔哩哔哩][202211051] @zjw961204
    - 这次由小周同学给大家分享，如何基于 RISC-V 开发板 + NCNN AI 库实现实时人物检测与监控
    - 已上传视频剪辑

- 20221022：[Linux 内核观察 - v6.0-哔哩哔哩][202210291] @falcon, @walimis, @tangjinyu1994, @sugarfillet, @司延腾
    - Falcon 老师介绍 Linux v6.0 内核关键变更
    - 王利明老师介绍 RISC-V 主要变更
    - 汤进宇老师介绍内存管理 mprotect 主要变更
    - 宋帅帅老师介绍 Ftrace 主要变更
    - 司延腾老师介绍 Linux 内核文档中文化项目
    - 已上传视频剪辑

- 20221022：[RISC-V Linux 设备树详解之 Overlay - 贾老师 - 哔哩哔哩][202210221]
    - 本次分享由由贾老师分享 Linux 设备树 —— DTS，介绍设备树 Overlay 的用法
    - 已上传视频剪辑

- 20221015：[RISC-V Ftrace 实现原理 - 宋老师 - 哔哩哔哩][202210151] @sugarfillet
    - 本次分享由宋帅帅老师主讲，内容围绕 RISC-V 架构 Linux Ftrace 的实现原理展开
    - 已上传视频剪辑

- 20221008：
    - 19:30-20:30：[中文排版纠错实践 - TinyCorrect 的设计、实现与用法演示 - 哔哩哔哩][202210081] @falcon
    - 详细介绍了 TinyCorrect 的由来，源码组织及用法
    - 已上传视频剪辑
    - 20:30-21:30：[RISC-V QEMU 的各种启动方式分析 - 张老师 - 哔哩哔哩][202210082] @YJMSTR
    - 详细分析了 QEMU 模拟目标平台的各种启动方式
    - 已上传视频剪辑

- 20220924：[RISC-V Linux 内核技术调研半年度简报 - 哔哩哔哩][202209241] @falcon, @iOSDevLog
    - 本项目从 2022 年 3 月开始，到 9 月已经有半年时间，本周对这半年以来的工作做一个总结报告
    - 已上传视频剪辑

- 20220917：[RISC-V Linux 内核进程创建流程分析 - 汤老师 - 哔哩哔哩][202209171] @tangjinyu1994
    - 详细分析了 Linux 进程创建流程
    - 已上传视频剪辑

- 20220903：[RISC-V Linux 设备树详解之源码分析 - 贾老师 - 哔哩哔哩][202209031] @iOSDevLog
    - 介绍《RISC-V Linux 设备树详解》第二期：源码剖析
    - 已上传视频剪辑

- 20220827: [RISC-V Linux 内核技术调研暑期实习座谈会暨中期进展汇报 - 哔哩哔哩][202208271]
    - 由几位实习生分享实习心得 @bosswangst, @xiakaipan, @YJMSTR
    - 由几位代码提交者分享心得 @tangjinyu1994, @sugarfillet
    - 介绍我们目前正在收集新特性和工具的情况 @nfk1996, @iOSDevLog
    - 自由讨论：内容排版；进度瓶颈；互动交流；开学后，学习和项目怎么兼顾
    - 已上传视频剪辑

- 20220820: [《从零开始 RISC-V 硬件设计》之 MIPI+USB+GPIO 总线使用经验 - 倪老师 - 哔哩哔哩][202208201] @taotieren
    - 介绍《从零开始 RISC-V 硬件设计》项目中的 MIPI，USB 和 GPIO 总线使用经验
    - 已上传视频剪辑

- 20220813：[Linux 内核观察 - v5.19-哔哩哔哩][202208131] @falcon, @walimis, @陈华才
    - Falcon 老师介绍 Linux v5.19 内核关键变更
    - 陈华才老师介绍 LoongArch Linux 内核
    - 王利明老师介绍 RISC-V Linux 内核
    - 已上传视频剪辑

- 20220806：[《RISC-V Linux 设备树详解》之语法与文件格式 - 贾老师 - 哔哩哔哩][202208061] @iOSDevLog
    - 介绍《RISC-V Linux 设备树详解》第一期：设备树语法与文件格式
    - 已上传视频剪辑

- 20220730：[《从零开始 RISC-V 硬件设计》之原理图简介 + SDIO 总线使用经验 - 倪老师][202207301] @taotieren
    - 介绍《从零开始 RISC-V 硬件设计》项目中的原理图和 SDIO 总线使用经验
    - 已上传视频剪辑

- 20220723：[《从零开始 RISC-V 硬件设计》之 I2C+SPI 总线使用经验 - 倪老师][202207231] @taotieren
    - 介绍《从零开始 RISC-V 硬件设计》项目中的 I2C+SPI 总线使用经验
    - 已上传视频剪辑

- 20220716：[RISC-V 操作系统/RVOS 视频公开课讲师导读 - 汪老师][202207161] @unicornx
    - 介绍开放课程《循序渐进，学习开发一个 RISC-V 上的操作系统》及课程设计体会
    - 已上传视频剪辑

- 20220715：[CTF 赛事介绍与 PWN 环境研讨会 - Retro][202207151] @Retro
    - 介绍 CTF 赛事，讨论暑期项目 CTF PWN Lab 这套 PWN 专用训练环境的开发需求
    - 已上传视频剪辑

- 20220709：GPU 虚拟化技术分享 @walimis
    - 系统地梳理了 GPU 虚拟化技术的工作原理，图文并茂的展示 GPU 虚拟化技术实现
    - 已上传视频剪辑

- 20220702：RISC-V Linux 内核技术调研暑期实习宣讲会 @falcon 等十多位老师
    - 面向在校大学生的首场暑期实习宣讲会，由十多位老师讲解项目提案，并详细介绍了实习参与流程
    - 已上传视频剪辑

- 20220701：RISC-V Time&Timer 硬件介绍与软件实现 @liaoyu
    - 系统地梳理 Linux Time&Timer 框架以及 RISC-V 的硬件支持和软件实现
    - 已上传视频剪辑

- 20220625：RISC-V Linux 系统调用详解 @envestcc
    - 系统地梳理了 RISC-V Linux 系统调用的工作原理并结合代码进行了详细地讲解。
    - 已上传视频剪辑

- 20220618：RISC-V Linux 内核调度详解 @Jack. Y
    - 从基本概念、__schedule 函数剖析、Schedule Classes 到 RISC-V Context Switch，较为全面地介绍了内核调度的知识
    - 已上传视频剪辑

- 20220611：RISC-V printk 初探，从 Linux v0.01 到 v5.0 @iOSDevLog
    - 对比大家熟悉的 printf 介绍 printk，从 Linux v0.01-v5.0 探索 printk 的演进历史，并通过源码分析了 printk 的基本工作原理
    - 已上传视频剪辑

- 20220521：RISC-V CPU 原理与设计简介 @谭源，@魏人
    - 结合兰州大学一生一芯项目实践介绍了 RISC-V 处理器的设计流程、方法和工具，并分享了处理器设计的学习路径
    - 已上传视频剪辑

- 20220514：RISC-V StackTrace 原理与实现 @Jeff
    - 对照 X86 和 RISC-V，介绍了 StackTrace 的基本工作原理与代码实现
    - 已上传视频剪辑

- 20220508：RISC-V Linux 内核启动流程分析 @通天塔
    - 结合 RISC-V 源码和 Linux Porting 系列译文，分析 RISC-V Linux 启动流程
    - 已上传视频剪辑

- 20220416：RISC-V 指令编码实例 @falcon
    - 以 Jump Label 为例，介绍如何参考 ISA 手册并编码 RISC-V 的 nop 与 jal 指令
    - 已上传视频剪辑

- 20220409：RISC-V rustSBI 知识分享 @luojia
    - 分享 RISC-V SBI 引导固件相关知识
    - 已上传视频剪辑

- 20220402：RISC-V Atomics 知识分享 @pingbo
    - 分享 RISC-V Atomics 指令和相关内核实现
    - 已上传视频剪辑

- 20220326：RISC-V ISA 简介 @iOSDevLog
    - RISC-V ISA 规范介绍和演示
    - 已上传视频剪辑

- 20220319：RISC-V Paging&MMU 知识分享 @pwl999
    - 比照 64 位 X86 介绍 RISC-V Paging&MMU
    - 已上传视频剪辑

## 会议记录

### 20230805：第七十一周
 
 这次邀请苏老师来分享 初探内存一致性 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享四笔; 增 !778 articles: Add a doc for device tree overlay internals。
* @YJMSTR: 增 !758 [v2] add article 20230715-riscv-isa-extensions-discovery-1; !771 [v1]add article riscv-isa-extensions-discovery-2-gcc。
* @groot00114: 增 !776 添加 SBI Spec 2.0-rc1 翻译文档 sbi-specification-translation。
* @jjl9807: 增!764 revise article qemu-system-parameters;  !769 add article qemu-system-event-loop-part1。
* @sugarfillet: 增!772 add riscv_linux_v6.4_linear_mapping_practise。
* @Reset12138: 增!762 add articles/20230730-riscv-linux-extension-and-alternative;!763 add articles/20230730-section-gc-no-more-keep-part1。
* @wangjiexun: 增 !753 add article 20230726-licheepi4a-linux。
* @Kepontry: 增 !766 modify articles/20230726-riscv-pmp-1。
* @nfk1996: 增 !675 add 20230504-static-call-part4-analysis-static-call。
* @jjl9807: 增 !764 revise article qemu-system-parameters。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230729：第七十周
 
 这次邀请王老师来分享 x86 架构下 unikernelLinux 的分析与总结 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。 
* @lbmeng: 知识星球分享:参数留空的宏函数调用。
* @Reset12138：知识星球分享两笔。
* @王杰迅：知识星球分享两笔。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230722：第六十九周
 
 这次邀请姜老师来分享 Kernellibc 的介绍 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享两笔。
* @Reset12138：知识星球分享两笔。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230715：第六十八周
 
 这次邀请刘老师来分享 SBI 入门介绍 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：详解 dtc 的 -@ 选项。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230708：第六十七周

 这次邀请宋老师来分享 RISC-V Linux v6.4-rc linear mapping practise 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @YJMSTR： 知识星球分享：RISC-V 的指令集扩展命名约定。
* @lbmeng: 知识星球分享：设备树 overlay 之语法糖。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230701：第六十六周

 这次邀请张老师来分享 Linux 包管理器的演进与现状 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @iOSDevLog：知识星球分享两篇。
* @Kepontry：增 !724 modify articles/20230617-software-prefetch。
* @lbmeng: 知识星球分享四篇。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230624：第六十五周

 这次邀请谭老师来分享 泰晓社区-TSoC2023-暑期实习-启动会 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @Reset12138: 知识星球分享：如何对 GCC 进行调试。增 !720 add article 20230615-section-gc-part3。
* @groot00114：增 !710 introduction-to-riscv-sbi 文章第二次提交。
* @Jingqing3948：增 !715 Add 20230617-summary-of-optimization-content-for-str-and-mem-functions。
* @lbmeng: 增 719 ppt: Add riscv-semihosting。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230617：第六十四周

 这次邀请吴老师来分享 QEMU debug & upstream practice 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：如何优雅地添加无密码访问远程服务器的账号。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230610：第六十三周

 这次邀请蒙老师来分享 RISC-V Semihosting 是个什么鬼？ 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：如何查看当前系统中的 glibc 版本。
* @walimis：增 !702 ppt: add pcie-1-intro-resource。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230603：第六十二周

 这次邀请王老师来分享 PCIe 基本原理和资源访问会议 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：QEMU 默认设备的秘密。
* @Reset12138：增 !695 add section gc part 1 and 2。
* @aabb5566：增 !699 20230527 sharing。
* @sugarfillet：增 !694 add 3 hibernation articles。
* @Kepontry：增 !697 modify articles/20230425-vf2-mem-mountain.md。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230527：第六十一周

 这次邀请 wayling 老师来分享 捡到枪的系统软件工程师 (RISC-V linux 为例) 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 增 !690 articles: Add an introduction to RISC-V semihosting。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230520：第六十周

 这次邀请张老师来分享 从零开始写一个 RISC-V 模拟器（2）- trap 机制与 RISC V-tests 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：手搓 semihosting 裸金属程序，支持 semihosting 的 C 库。
* @Kepontry：增 !687 modify articles/20230511-l2-prefetch-driver，!686 modify articles/20230509-vf2-hw-prefetch，!685 modify articles/20230510-vf2-spec06。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230513：第五十九周

本周休息。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @YJMSTR：增 update articles/20230427-tinyemu-exception.md。
* @lbmeng: 知识星球分享：coreboot 是一个开源固件项目。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230506：第五十八周

这次邀请吴老师来分享 Linux 内核观察 - v6.3 相关知识。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 增 !670 articles: Update "gdb & QEMU gdbstub debug"。
* @Kepontry：知识星球分享：kstrtouint 函数的使用。
* @nfk1996：增 !677 add riscv-linux-static-call-20230422.pptx。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230429：第五十七周

五一假期，本周暂停线上分享。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：Windows 平台的 RISC-V GCC 工具链。增 !663 articles: Add an introduction to gdb & QEMU gdbstub debug。
* @Kepontry：增 !662 add articles/20230425-vf2-mem-mountain。
* @YJMSTR：增 ![v1]add articles/20230427-tinyemu-exception。
* @sugarfillet：增 !660 add two articles about the UEFI boot process in RISC-V Linux。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230422：第五十六周

本周六有线下活动，直播分享改为周日分享。
这次邀请蒙老师来分享 QEMU 8.0观察分享 相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。。
* @lbmeng: 知识星球分享：Armv8 体系架构的 nGnRnE 设备内存属性；Little Kernel 之RISC-V 体（cai）验（keng）。
* @Kepontry：增 !655 add ppt/riscv-visionfive2-coremark。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230415：第五十五周

这次邀请周老师来分享 VisionFive 2开发板介绍与评测 相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @nfk1996：评论 #I5Y585 。
* @lbmeng: 知识星球分享：RISC-V 规范（指令集扩展、特权级相关扩展等）状态一览。
* @YJMSTR：增 !651 fix typo in articles/20230315-tinyemu-csr-and-privileged-isa。 
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230408：第五十四周

这次邀请牛老师来分享 Static Call 相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @sugarfillet：增 !637 add 20230323-smp-riscv-topology。
* @iOSDevLog：知识星球分享：深入理解 #RISC-V Linux#，开发 #RISC-V# 模拟器。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230401：第五十三周

这次邀请张老师来分享 fuse文件系统浅析 相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：配置多个邮箱使用“ git send-email ”，一个简单的 GOT 覆盖示例程序
* @Kepontry：增 !642 add 20230323-vf2-coremark，644 update articles/20230323-vf2-coremark。
* @YJMSTR：增 !638 add tinyemu1。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230325：第五十二周

这次邀请张老师分享 从零开始写一个 RISC-V 模拟器 - TinyEMU 相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @sugarfillet：知识星球分享：如何在 Qemu sifive_u 中 gdb 调试 Linux。
* @lbmeng: 增 ppt: Rename riscv-linux-recent-upstream-report-20230218，Add riscv-relocation-in-boot-process。
* @YJMSTR：增 add articles/20230315-tinyemu-csr-and-privileged-isa。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230318：第五十一周

这次邀请蒙老师分享 RISC-V 启动流程中的重定位（近期 upstream 工作介绍）相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @YJMSTR：增 update articles/20230121-tinyemu-cpu-and-isa。更新 #I5ZM22 进度：目前实现了 Zicsr 扩展，正在根据 riscv-tests 的结果 debug 。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230311：第五十周

这次邀请汤老师分享 RISC-V Linux 缺页异常相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @walimis：增 add hw virt intro for x86,armv8, risc-v。
* @lbmeng: 知识星球分享：如何编译 upstream binutils ? ; RISC-V Linux 内核 v5.16 以下版本不能被 binutils 2.38 以上版本编译的迷思。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230304：第四十九周

这次邀请到王老师分享 3 大主流处理器架构（X86、ARM 和 RISC-V）的硬件虚拟化实现相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：用国内邮箱向开源社区邮件列表发送 patch 防坑指南。
* @Kepontry：增 modify 20230227-vf2-lan-wlan， modify 20230227-vf2-kernel-compile。
* @iOSDevLog：增 add articles/20230210-riscv-real-time-linux-2
* @sugarfillet：增  add 20230209-riscv-ipi。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230225：第四十八周

这次邀请到吴老师来分享 v6.2 内核观察相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @sugarfillet：知识星球分享：Linux gdb helpers 是 Linux 源码。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230218：第四十七周

这次邀请到蒙老师来分享 近期 upstream 工作介绍（RISC-V PLIC 相关）相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @Kepontry：增 modify 20230213-vf2-uboot-spl。
* @lbmeng: 知识星球分享：QEMU gdbstud 调试技巧；怎么调试 gdb 自己的问题；增 ppt: Add riscv-linux-recent-upstream-report。
* @walimis： 知识星球分享：RISC-V Linux 下的 vCPU IPI 处理流程。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230211：第四十六周

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @xiakaipan：增 riscv-kvm-int-impl-2。
* @sugarfillet：增 riscv-cpuidle。
* @iOSDevLog：知识星球分享：rust 模块开发初探。
* @lbmeng: 知识星球分享：QEMU 命令行参数 “- serial stadio” 的坑。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230204：第四十五周

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @YJMSTR：增 add articles 20230119-tinyemu-introduction.md, 20230121-tinyemu-cpu-and-isa。
* @zhou-zitang：增 add articles/20230131-elfs-riscv-linux-basic-cmd。
* @tangjinyu1994：增 update articles/20221103-riscv-page-fault-part2。
* @sugarfillet：知识星球分享：RISC-V qemu-virt 配置 idle 状态；增 smp-linux-boot。
* @iOSDevLog：知识星球分享：Core Dump File 简介。
* @lbmeng: 知识星球分享：LLVM 16 RISC-V 新引入的 -mmo-implict-float 的选项的副作用；RISC-V 没有 Zicsr 扩展会怎样？
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20230107：第四十四周

这次邀请到蒙老师来分享 QEMU 7.2 观察相关知识。视频剪辑已经上传，见“回放发布”一节链接。
这次邀请到Linux 内核观察小组来分享Linux 内核观察 - v6.1 观察相关知识。视频剪辑已经上传，见“回放发布”一节链接。（补上周六）

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @lbmeng: 知识星球分享：如何手动补发一个补丁系列中的某个或某几个补丁邮件？
* @nfk1996：增 update static call series。
* @tangjinyu1994：提交 新增 multi-gen 的翻译，并增加了部分译者注 ，open。
* @iOSDevLog：增 add riscv-linux-twoc-2022.pdf。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20221231：第四十三周

由于内核观察小队的多位老师身体还处于恢复期，本周线上分享暂时取消。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @lbmeng: 知识星球分享：RISC-V Spike 的 HTIF 规范。
* @nfk1996：提交 add 20221207-static-call-part3-analysis-static-call.md ，open。
* @walimis：评论 #I64ESM ：参考两篇文档，输出一篇文档。
* @sugarfillet：增 revise ftrace-impl-4-replace-trace-function.md，add and update 20221220-ftrace-impl-6-ringbuffer.md。
* @NTH20：增 modify the file name into english and refering details。
* @sfpyjs：增 QEMU-7.2 patch analysis: Fixup TLB size calculation when using PMP。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20221224：第四十二周

这次邀请到徐宇奇同学来分享 RISC-V 平台上 benchmark 基准测试调研相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @sugarfillet：知识星球分享：如何将一条 RISC-V 指令编码为机器码。
* @lbmeng: 知识星球分享：RISC-V 扩展命名规范;创建 issue #I66OG4:【老师提案】QEMU 模拟器观察 - v8.0。
* @yjmstr : 增 add article 20221212-riscv-smp-in-boot-flow.md。
* @xuyq0306: 直播分享: RISC-V 平台上 benchmark 基准测试调研。
* @iOSDevLog：知识星球分享：Arm 教育计划教育套件；增 PPT [riscv-linux-lab.pdf]。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221217：第四十一周

这次邀请到贾老师来分享 Kernel Build System-Kbuild 子系统详解相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @XiakaiPan : 知识星球分享两笔。
* @lbmeng: 知识星球分享两笔。
* @yjmstr : 提交 add article 20221212-riscv-smp-in-boot-flow.md，open。
* @iOSDevLog：知识星球分享：免费 RISC-V 培训探索编译器工具链和优化的内部结构；直播分享 Kernel Build System-Kbuild 子系统详解。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221210：第四十周

这次邀请到贾老师来分享 RISC-V Linux 内核技术调研活动寒假实习相关的部分。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @XiakaiPan : 增 int-impl-1.md。
* @sugarfillet：增 update the ftrace-impl-7-riscv.md。
* @yjmstr : 增 ppt [sbi-hsm-extension.pptx]； 更新 #I60FB1 进度：认领 Add checks for supported extension combinations；知识星球：模拟器与仿真器的指令执行方式。
* @zjw961204：增 ppt [riscv-d1-ai-lab.pdf]。
* @walimis：增 add two erratas for syscall part2；知识星球分享：异常返回后不同架构的 epc 地址设定差异。
* @lbmeng: 更新 #I60FB1 进度：认领 Removal of the "slirp" submodule (affects "-netdev user") 和 Semihosting calls from userspace。
* @nfk1996：创建 issue #I64R6O：【老师提案】RISC-V Generic library routines and assembly 技术调研、分析与优化。
* @iOSDevLog：创建 issue #I64YC4: 【老师提案】RISC-V Linux 内核 SBI 调用技术分析；知识星球分享两笔；直播分享 RISC-V Linux 内核技术调研活动寒假实习宣讲会。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221203：第三十九周

这次邀请到张炀杰同学来分享 RISC-V SBI 规范的 HSM 扩展相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo: 新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @XiakaiPan : 新增 ppt [riscv-virt-report-1.pdf]; 知识星球分享：RISC-V 虚拟化调研第一阶段小结。
* @yjmstr : 更新 #I5ZM22 进度：NJU PA 完成 PA2 所有内容，已能够运行 FCEUX 并运行超级玛丽。
* @walimis：创建 issue #I64ESM: 【老师提案】 VisionFive 2 开发版软硬件调研。
* @lbmeng: 创建 issue #I64FSG: 【老师提案】RISC-V UEFI 启动流程分析与 EDK2 移植。
* @sugarfillet：增 ftrace-impl-7-riscv.md 。
* @GoodBoyCC：新增文章 《RISC-V eBPF 系列2：bpf SYSCALL》。
* @iOSDevLog：知识星球分享两笔；项目同步；安排下周会议；完成本次会议记录。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221126：第三十八周

这次邀请到潘夏凯同学来分享 RISC-V 虚拟化技术相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @walimis：创建 issue #I6224E：【老师提案】 RISC-V 下的 AI 研究和应用；创建 issue #I6224C：【老师提案】 RISC-V 虚拟化高级技术调研(AIA，IOMMU等)
* @tangjinyu1994：知识星球分享：Multi-Gen LRU ( MGLRU ) 和 DAMON LRU SORT 是如何结合的？；评论 #I5E5H5，#I5TX4H：最近正在学习 MGLRU，非常综合。可以把很多知识点串在一起，非常值得学习，后续会输出相关文章。
* @sugarfillet：知识星球分享：checkpatch -- strict 选项说明。
* @nfk1996：新增文章 retpolines，merged。
* @iOSDevLog：新增访谈：我的第一笔 Patch 之 DTS 篇；项目同步；安排下周会议；完成本次会议记录。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221119：第三十七周

本周邀请到王宇豪同学来分享 RISC-V 处理器设计相关知识。视频剪辑已经上传，见“回放发布”一节链接。

* @zjw961204：目前已实现实时人物检测监控的基本功能，现关闭该 issue [#I5GYUU][#I5GYUU]。
* @Reset12138：更新 [#I5WN7A][#I5WN7A] 进度：移植部分，已经尽可能替换了内联汇编为 c，目前在研究引导部分。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：更新 [#I5ZM22][#I5ZM22] 进度：学习《The Rust Programming Language》前 5 章。
* @walimis：提交更新：[add missing package when building qemu/linux][202207081]。
* @lbmeng：新增 [正确使用邮件列表参与开源社区的协作][202211193]；创建 issues [【老师提案】QEMU 系统模拟模式分析][#I61KIY]，[【老师提案】向 QEMU 添加 瑞萨电子 RZ/Five SMARC 开发板支持][#I61K8X]，[【老师提案】QEMU 用户态模拟模式分析][#I61K5B]。
* @taotieren：更新 [#I5F0PU][#I5F0PU] 进度：核心板上电测试，5V/25mA 左右，上电测试正常。
* @iOSDevLog：创建 issue [【老师提案】从零开始嵌入式 Linux (RISC-V + Linux v6.x)][#I61K05]；提交大纲 [Embedded Linux From Scratch (RISC-V + Linux v6.x)][202211192]；项目同步；安排下周会议；完成本次会议记录。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221112：第三十六周

本周分享由实习生小周老师分享基于 RISC-V 开发板实现实时人物检测与监控。视频剪辑已经上传，见“回放发布”一节链接。

* @xiakaipan：更新 [#I5E4VB][#I5E4VB] 进度：Framework of interrupts handling in KVM and kvmtool has been figured out。
* @Reset12138：更新 [#I5WN7A][#I5WN7A] 进度：io 可以考虑 virtio。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：不同版本的 OpenSBI 对于不同版本 Linux 内核的支持仍在调研与实验中；认领 [#I60FB1][#I60FB1]：'virt' machine updates。
* @tangjinyu1994：评论 [#I5TX4H][#I5TX4H]：arm64 架构下的已经更新到 v5 了，可以进行参考。
* @walimis：更新文章 [用 QEMU/Spike+KVM 运行 Host/Guest Linux][202207081]。
* @lbmeng：评论 [#I60FB1][#I60FB1]：建议关注以下一些改动：Generic，RISC-V。
* @nfk1996：新增 [Meltdown 和 Spectre][202211122]，merged；直播分享 [RISC-V Linux 内核中断管理技术分享 - 牛老师 - 哔哩哔哩][202211121]。
* @iOSDevLog：Review [Meltdown 和 Spectre][202211122]；项目同步；安排下周会议；完成本次会议记录。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221105：第三十五周

本周分享由实习生小周老师分享基于 RISC-V 开发板实现实时人物检测与监控。视频剪辑已经上传，见“回放发布”一节链接。

* @xiakaipan：提交 [RISC-V 异常处理在 KVM 中的实现][202211054], merged；提交 [RISC 内存虚拟化在 KVM 及 kvmtool 中的实现][202211053]。
* @xuyq0306：更新 [#I5WN7A][#I5WN7A] 进度：opensbi + uboot + linux 内核启动的记录；提交“将 Linux 0.11 移植到 RISC-V 架构上————准备工作（1）”，open。
* @Reset12138：Review“将 Linux 0.11 移植到 RISC-V 架构上————准备工作（1）”；新增 [RISV-V 硬件产品开发 - 外壳设计][202211057]。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：新增 [QEMU 启动方式分析（4）: OpenSBI 固件分析与 SBI 规范的 HSM 扩展][202211052]，merged；创建 issus [【学生提案】用 C 语言实现一个简单的 RISC-V 架构模拟器-TinyEMU][#I5ZM22]。
* @trueptolemy：Review [RISC-V 异常处理在 KVM 中的实现][202211054]。
* @jc2870：创建 issus [内核观察 - v6.1 - statx][#I5YRKJ]。
* @xingmz：创建 issus [内核观察 - v6.1 - bpf][#I5YS2B]。
* @GoodBoyCC：提交 [RISC-V eBPF 系列 1：eBPF 简介][202211056]，merged。
* @zjw961204：直播分享 [基于 RISC-V 开发板实现实时人物检测与监控 - 周老师 - 哔哩哔哩][202211051]。
* @tangjinyu1994：创建 issus [内核观察 - v6.1 - MGLRU][I5ZD1W]；新增 [缺页异常处理程序分析（2）：handle_pte_fault() 和 do_anonymous_page()][202211055]，merged；新增 [缺页异常处理程序分析（3）：文件映射缺页异常分析][202211058], merged。
* @walimis：创建 issue [内核观察 - v6.1 - RISC-V Linux][#I5YS1M]。
* @iOSDevLog：创建 issue [内核观察 - v6.1 - rust][#I5Z8C6]；更新 [【开发成果】本地开发与上游贡献成果汇总][I5X9Q5]；项目同步；安排下周会议；完成本次会议记录。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221029：第三十四周

本周由社区负责人 Falcon 特别邀请了王利明、汤进宇、宋帅帅、司延腾等分享 Linux v6.0 内核观察。视频剪辑已经上传，见“回放发布”一节链接。

* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：发现 OpenSBI 文档中的错误。
* @tangjinyu1994：[Linux 内核观察 - v6.0-哔哩哔哩][202210291] 中介绍内存管理 mprotect 主要变更。
* @sugarfillet：[Linux 内核观察 - v6.0-哔哩哔哩][202210291] 中介绍 Ftrace 主要变更。
* @nfk1996：新增 [RISC-V 缺失的 Linux 内核功能-Part2][202210292]，merged。
* @walimis：[Linux 内核观察 - v6.0-哔哩哔哩][202210291] 中介绍 RISC-V 主要变更。
* @iOSDevLog：项目同步；安排下周会议；完成本次会议记录。
* @falcon：主持 [Linux 内核观察 - v6.0-哔哩哔哩][202210291]；更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221022：第三十三周

本周分享继续由贾老师分享 Linux 设备树 —— DTS，介绍设备树 Overlay 的用法。

* @xuyq0306：更新 [#I5WN7A][#I5WN7A] 进度：Linux Lab 中的 Linux 0.11 Lab 可以很方便用 QEMU 进行实验。
* @xiakaipan：更新 [#I5E4VB][#I5E4VB] 进度：Finish code analysis of page fault handling in KVM。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：新增 ppt [qemu-riscv-boot-flow.pdf][202210158]。
* @sugarfillet：评论 [#I5W8R8][#I5W8R8]：6.3 ftrace: host klp and bpf trampoline together。
* @nfk1996：新增 [Generic entry RISC-V 补丁分析][202210222]，merged。
* @walimis：评论 [#I5QNSE][#I5QNSE]：xfel，使用 U-Boot 的 fastboot 功能。
* @iOSDevLog：直播分享 [RISC-V Linux 设备树详解之 Overlay - 贾老师 - 哔哩哔哩][202210221]；项目同步；安排下周会议；剪辑本周在线分享视频；完成本次会议记录。
* @falcon：组织《Linux 内核观察 - v6.0》准备工作；更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；发布本周在线分享视频。

### 20221015：第三十二周

本周分享由宋帅帅老师主讲《RISC-V Ftrace 实现原理》，内容围绕 RISC-V 架构 Linux Ftrace 的实现原理展开。

* @xuyq0306：新增 [RISC-V 性能实测，以平头哥 C906 为例][202210155]，merged。
* @xiakaipan：更新 [#I5E4VB][#I5E4VB] 进度：Go through the inner function calling situation in KVM。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：完成视频分析。
* @tangjinyu1994：新增 [缺页异常处理程序分析（1）：do_page_fault() 和 handle_mm_fault()][202210156]，merged。
* @sugarfillet：新增 ppt [riscv-ftrace-impl-20221015.pdf][202210159]；分享《RISC-V Ftrace 实现原理》。
* @nfk1996：提交“Generic entry RISC-V 补丁分析”，closed。
* @walimis：2 篇 PR Review。
* @iOSDevLog：项目同步；安排下周会议；制作直播分享的 PPT 模板；完成本次会议记录。
* @falcon：更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221008：第三十一周

本周由社区负责人兼 [tinycorrect][202208206] 作者 Falcon 介绍《中文排版纠错实践 - TinyCorrect 的设计、实现与用法演示》；再由实习生张炀杰介绍《RISC-V QEMU 的各种启动方式分析》。

* @xuyq0306：新增 [RISC-V 性能实测，以平头哥 C906 为例][202210155]，merged。
* @xiakaipan：新增 [RISC-V 架构 H 扩展中的 Trap 处理][202210153]，merged；新增 [RISC-V 内存虚拟化简析（二）][202210152]，merged。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @YJMSTR：新增 [QEMU 启动方式分析（3）: QEMU 代码与 RISCV 'virt' 平台 ZSBL 分析][202210154]，merged；直播分享 [RISC-V QEMU 的各种启动方式分析 - 张老师 - 哔哩哔哩][202210082]。
* @zjw961204：新增 [D1 开发板实时人物检测推流的功能实现][202210157]，merged。
* @tangjinyu1994：新增 [缺页异常处理程序分析（1）：do_page_fault() 和 handle_mm_fault()][202210156]，merged。
* @iOSDevLog：项目同步；安排下周会议；协调 D1 开发板的使用；完成本次会议记录。
* @falcon：完善 Review 辅助工具 [tinycorrect][202208206]；更新多个 issues 进度；介绍《中文排版纠错实践 - TinyCorrect 的设计、实现与用法演示》；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20221001：第三十周

国庆假期，本周暂停线上分享。

* @xuyq0306：提交“RISC-V 性能实测，以平头哥 C906 为例”，closed。
* @xiakaipan：更新 [#I5E4VB][#I5E4VB] 进度：Try TinyCorrect to auto review articles, find out several shortcuts；提交“RISC-V 架构 H 扩展中的 Trap 处理”，closed；提交“RISC-V 内存虚拟化简析（二）”，closed。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @sugarfillet：提交 [ftrace 实现原理（5）- 动态函数图跟踪][202209243]，merged。
* @YJMSTR：提交“QEMU 启动方式分析（3）: QEMU 代码与 ZSBL 分析”，closed。
* @tangjinyu1994：创建 [#I5TX4H][#I5TX4H]【老师提案】Try to Support batch-unmap-tlb-flush for RISC-V。
* @nfk1996：提交“Generic entry RISC-V 补丁分析”，closed。
* @lbmeng：PR Review “QEMU 启动方式分析（3）: QEMU 代码与 ZSBL 分析”。
* @iOSDevLog：Review 多篇 [tinycorrect][202208206] 修复的文章；新增 ppt [riscv-linux-half-year-report-20220924.pdf][202209244]；项目同步；安排下周会议；协调 D1 开发板的使用；完成本次会议记录。
* @falcon：完善 Review 辅助工具 [tinycorrect][202208206]；更新多个 issues 进度；[#I5T3XB][#I5T3XB]【老师提案】Embedded Linux 系统 for RISC-V；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20220924：第二十九周

本周由社区负责人 Falcon 和本项目第一期参与人员贾老师共同总结的《RISC-V Linux 内核技术调研半年度简报》。

* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @sugarfillet：提交 [ftrace 实现原理（4）- 替换跟踪函数][202209242]，merged。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：完成对 QEMU 中用于模拟 virt 机器相关的 QOM 类，对象的分析。
* @xiakaipan：更新 [#I5EE48][#I5EE48] 进度：Modify last article about memory management。
* @nfk1996：提交“Generic entry RISC-V 补丁分析”，closed。
* @iOSDevLog：Review 多篇 [tinycorrect][202208206] 修复的文章；项目同步；完成本次会议记录。
* @falcon：完善 Review 辅助工具 [tinycorrect][202208206]；更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布最近 3 期在线分享视频。

### 20220917：第二十八周

本周由汤老师介绍《Linux 进程创建流程分析》。

* @tangjinyu1994：更新 [#I5LIEI][#I5LIEI] 进度：如何使用 RISC-V 的 LLVM 编译内核；分享《Linux 进程创建流程分析》。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @sugarfillet：提交 [提交“ftrace 实现原理（3）- 替换函数入口][202208276]，merged。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：结合 gdb 对 QEMU 运行过程中调用的函数进行分析。
* @iOSDevLog：Review 多篇 [tinycorrect][202208206] 修复的文章；向 [dtc/dtc][202208277] 提交 patch；项目同步；安排下周会议；完成本次会议记录。
* @falcon：完善 Review 辅助工具 [tinycorrect][202208206]；更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20220910：第二十七周

中秋假期，本周暂停线上分享。

* @xuyq0306：提交“下常用压力测试工具一览”，closed。
* @tangjinyu1994：提交 “缺页异常处理程序分析 1 do_page_fault() 和 handle_mm_fault()”，closed。
* @williamsun0122：提交 [RISC-V 异常处理流程介绍][202208273]，merged。
* @zjw961204：提交 [使用 ffmpeg 和 D1 开发板进行直播推流][202208275]，merged。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @sugarfillet：提交 [ftrace 实现原理（2）- 编译时原理][202208272]，merged；提交“ftrace 实现原理（3）- 替换函数入口”，closed。
* @bosswangst：更新 [#I5EIOA][#I5EIOA] 进度：在导师前辈的指引下对 SpinalHDL 中 Masked Literal 进行学习：提交 [RV64I CPU 控制器模块设计思路与实现][202208274]，merged。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：结合 QEMU 'virt' 代码分析 ZSBL 的行为。
* @iOSDevLog：Review [使用 ffmpeg 和 D1 开发板进行直播推流][202208275]；安排下周会议；项目同步；完成本次会议记录。
* @falcon：完善 Review 辅助工具 [tinycorrect][202208206]；更新多个 issues 进度；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20220903：第二十六周

本周由贾老师介绍《RISC-V Linux 设备树详解》之源码剖析。

本周纪要：

* @williamsun0122：提交“RISC-V 异常处理流程介绍”，closed。
* @zjw961204：提交“使用 ffmpeg 和 D1 开发板进行直播推流”，closed。
* @xiakaipan：提交“RISC-V 内存虚拟化简析（二）”，closed。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @sugarfillet：提交“ftrace 实现原理（2）- 编译时原理”，closed。
* @bosswangst：更新 [#I5EIOA][#I5EIOA] 进度：对 CPU 控制器模块设计文章进行了修订。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：成功用 U-Boot 在 QEMU virt 中引导起了 RISCV64 Linux Kernel 并能够进入命令行界面；提交 [QEMU 启动方式分析（2）: QEMU 'virt' 平台下通过 OpenSBI + U-Boot 引导 RISCV64 Linux Kernel][251]。
* @trueptolemy：提交 [从嵌入式系统视角初次展望 RISC-V 虚拟化（A First Look at RISC-V Virtualization from an Embedded Systems Perspective）][252]。
* @lbmeng：PR Review [QEMU 启动方式分析（2）: QEMU 'virt' 平台下通过 OpenSBI + U-Boot 引导 RISCV64 Linux Kernel][251]。
* @nfk1996：新增文章 [RISC-V 中断子系统分析——中断优先级][253] 和 [RISC-V 缺失的 Linux 内核功能][254]。
* @walimis：PR Review [从嵌入式系统视角初次展望 RISC-V 虚拟化（A First Look at RISC-V Virtualization from an Embedded Systems Perspective）][252]。
* @iOSDevLog：分享《RISC-V Linux 设备树详解》之源码剖析；新增 ppt (dts-source-code.pdf)[https://gitee.com/tinylab/riscv-linux/blob/master/ppt/dts-source-code.pdf]；项目同步；完成本次会议记录。
* @falcon：创建文章 Review 辅助工具 [tinycorrect][202208206]；更新多个 issues 进度；提议实习生会议；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章。

### 20220827：第二十五周

本周由贾老师主持一场 RISC-V Linux 座谈会。主要内容有：由几位实习生分享实习心得；由几位代码提交者分享心得；介绍我们目前正在收集新特性和工具的情况；自由讨论。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @aabb5566：认领 Synchronization 中多项任务。
* @zhou-zitang：新增 ppt [riscv-microbench-tsoc2022.pdf][241]，merged。
* @tangjinyu1994：更新 [#I5LIEI][#I5LIEI] 进度：成功运行了 KMSAN。
* @zjw961204：更新 [#I5GYUU][#I5GYUU] 进度：转成 ncnn 需要的.param 和.bin 文件，转换遇到了一些错误。
* @xiakaipan：提交 [KVM 虚拟化：用户态程序][242]，merged；提交 [RISC-V 内存虚拟化简析（一）][243]，merged；提交“RISC-V 内存虚拟化简析（二）”，closed。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @trueptolemy：《A First Look at RISC-V Virtualization from an Embedded Systems Perspective》的翻译，closed。
* @sugarfillet：提交“ftrace 实现原理 2 - 编译时原理”，closed。
* @bosswangst：提交“RV64I CPU 控制器模块设计思路与实现”，closed。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：成功用 U-Boot 在 QEMU virt 中引导起了 RISCV64 Linux Kernel 并能够进入命令行界面；提交 “QEMU 启动方式分析（2）: QEMU 'virt' 平台下通过 U-Boot 引导 RISCV64 Linux Kernel”。
* @lbmeng：PR Review [KVM 虚拟化：用户态程序][242]，“QEMU 启动方式分析（2）: QEMU 'virt' 平台下通过 U-Boot 引导 RISCV64 Linux Kernel”。
* @walimis：PR Review [KVM 虚拟化：用户态程序][242]，[RISC-V 内存虚拟化简析（一）][243] 和“RISC-V 内存虚拟化简析（二）”。
* @taotieren: PR Review [RISC-V 内存虚拟化简析（一）][243]；创建 [#I5OF32 关于使用 po4a 进行翻译协作][#I5OF32]。
* @iOSDevLog：提交 [扁平化设备树（DTB）格式剖析之三：扁平化设备树示例][244]，merged；主持坐谈会；安排下周会议；项目同步；完成本次会议记录。
* @falcon：更新 articles/README.md；更新多个 issues 进度；提议实习生会议；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220820：第二十四周

本周由倪老师使用思维导图介绍《从零开始 RISC-V 硬件设计》之 MIPI+USB+GPIO 总线使用经验。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @williamsun0122：认领 IRQs。
* @xuyq0306: 更新 [#I5F0PU][#I5F0PU] 进度：调研常用 benchmark 工具。
* @tangjinyu1994：更新 [#I5LIEI][#I5LIEI] 进度：用 LLVM 编译，一旦加上 KMSAN 选项，就无法 QEMU 启动加载。
* @zjw961204：更新 [#I5GYUU][#I5GYUU] 进度：使用 nginx 和 ffmpeg 实现了摄像头直播推流。
* @xiakaipan：更新 [#I5EE48][#I5EE48] 进度：可以在 QEMU 中加载自定义的虚拟设备，而非之前提示“无设备”错误；提交“KVM 虚拟化：用户态程序”，closed。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @trueptolemy：《A First Look at RISC-V Virtualization from an Embedded Systems Perspective》的翻译，closed。
* @sugarfillet：提交 [ftrace 实现原理 1 - 函数跟踪][231]，merged；提交“ftrace 实现原理 2 - 编译时原理”，closed。
* @bosswangst：PR Review [扁平化设备树（DTB）格式剖析之一：版本，报头和内存保留块][235]，[扁平化设备树（DTB）格式剖析之二：结构体块，字符串块和对齐][236]；提交 [RISC-V CPU 设计模块软件行为仿真与下板实验调试][232]。
* @YJMSTR：更新 [#I5EE48][#I5EE48] 进度：阅读 U-Boot 和 QEMU 的源码；提交 [QEMU 启动方式分析（1）：QEMU 及 RISC-V 上游启动流程简介][233]，merged。
* @GoodBoyCC：提交 [RISC-V Syscall 系列 4：vDSO 实现原理分析][234]，merged。
* @lbmeng：PR Review [QEMU 启动方式分析（1）：QEMU 及 RISC-V 上游启动流程简介][233]。
* @walimis：PR Review [ftrace 实现原理 1 - 函数跟踪][231] 和“ftrace 实现原理 2 - 编译时原理”。
* @taotieren: PR Review [RISC-V Syscall 系列 4：vDSO 实现原理分析][234]，[扁平化设备树（DTB）格式剖析之一：版本，报头和内存保留块][235]，[扁平化设备树（DTB）格式剖析之二：结构体块，字符串块和对齐][236]；分享 [《从零开始 RISC-V 硬件设计》之 MIPI+USB+GPIO 总线使用经验][202208201]。
* @iOSDevLog：提交 [扁平化设备树（DTB）格式剖析之一：版本，报头和内存保留块][235]，merged；[扁平化设备树（DTB）格式剖析之二：结构体块，字符串块和对齐][236]，merged；登记 D1 开发板，协调 D1 开发板的使用；安排下周会议；项目同步；完成本次会议记录。
* @falcon：更新多个 issues 进度；直播分享 Review 相关工作；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220813：第二十三周

本周由社区负责人 Falcon 特别邀请了龙芯 Linux Maintainer 陈华才老师和资深 Linux 与虚拟化技术专家王利明老师以人物访谈的形式分享主题：Linux v5.19 内核观察。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @xuyq19 徐宇奇，@蒋之凡，@凌宇哲，@谭源，@taotieren, @falcon, @iOSDevLog：参加 RISC-V 硬件设计小组内部会议，自我介绍及后续协作沟通。
* @zjw961204：新增 [D1 开机入门][221]，merged。提交 [用 D1 进行图片采集和人体识别][222]，merged。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @nfk1996：整理 [Missing Features/Tools for RISC-V][224]：optprobes, user-ret-profiler。
* @trueptolemy：提交“x86 下 kvm 的 steal time 机制探究”，closed。
* @xiakaipan：提交“KVM 虚拟化：用户态程序”，closed。
* @sugarfillet：认领 Ftrace；提交“ftrace 实现原理 1 - 函数跟踪”，closed。
* @tangjinyu1994：新增老师提案 [Try to Support KMSAN for RISC-V][223]。
* @walimis：PR Review “KVM 虚拟化：用户态程序” 和“ftrace 实现原理 1 - 函数跟踪”。
* @iOSDevLog：安排下周会议；项目同步；完成本次会议记录。
* @falcon：新增老师提案 [Missing Features/Tools for RISC-V][224]；邀请陈华才和王老师分享 [《Linux 内核观察》v5.19][202208131]；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220806：第二十二周

本周由贾老师介绍《RISC-V Linux 设备树详解》之语法与文件格式。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @YJMSTR：更新 issue 进度：继续按照 review 意见进行修改，并修复了原先编译运行 U-Boot, U-Boot SPL 以及 OpenSBI 时遇到的问题，现在能够正常运行这几个软件，目前还差引导内核，以及分析 ZSBL 代码。
* @bosswangst：更新 issue 进度：准备构思数据通路搭建；新增 [RISC-V CPU 设计理论分析与主要模块的实现][211]，merged。
* @tangjinyu1994：新增 [RISC-V 架构下内核线程返回函数探究][212]，merged。
* @zjw961204：更新 issue 进度：实现了摄像头采集画面的实时检测，对目前工作的文档记录完成提交；提交“D1 开机入门”，closed；提交“用 D1 进行图片采集和人体识别”，closed。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]。
* @walimis：新增 ppt [gpu-virtualization-intro.pdf][213]，merged。Review。
* @iOSDevLog：分享 [《RISC-V Linux 设备树详解》之语法与文件格式][202208061]；新增 ppt [dts.pdf][214]，merged；完成本次会议记录。
* @falcon：整理 v5.19 内核动态资料 kernel newbies；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220730：第二十一周

本周由倪老师使用思维导图介绍《从零开始 RISC-V 硬件设计》之 SDIO 总线。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @cynault：创建 [〖学生提案〗RISC-V 架构下的二进制攻防实战][201]。
* @zjw961204：提交进度：摄像头模块完成采图功能。
* @bosswangst：提交 CPU 设计的总体思路。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @xiakaipan：提交“x86 架构下基于 KVM 的虚拟化的实现”，closed；新增 [RISC-V 虚拟化模式切换简析][202]，merged。
* @YJMSTR：提交“QEMU 启动方式分析（1）：QEMU 及 RISC-V 上游启动流程简介”，closed。
* @tangjinyu1994：提交“RISC-V 架构下内核线程返回函数探究”，closed。
* @iOSDevLog：更新 D1-H 哪吒开发板 microbench 结果；邀请倪老师分享 [《从零开始 RISC-V 硬件设计》之原理图简介 + SDIO 总线使用经验][202207301]；完成本次会议记录。
* @falcon：新增 [5 秒内在 X86_64 笔记本上运行 RISC-V Ubuntu 22.04 + xfce4 桌面系统][203]，merged；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220723：第二十周

本周由倪老师使用思维导图介绍《从零开始 RISC-V 硬件设计》之 I2C+SPI 总线使用经验。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @zjw961204：提交 D1 ncnn 框架资料。
* @YJMSTR：提交 Linux 内核编译资料。
* @GoodBoyCC：新增 [RISC-V Syscall 系列 3：什么是 vDSO？][191]，merged。
* @ZhaoSQ-CH：新增 [在泰晓 Linux 实验盘中构建 QEMU 并引导 openEuler for RISC-V][193]，merged。
* @bosswangst：新增 [CPU 设计——数电基本知识与基于 Scala 的硬件设计框架 SpinalHDL][194]，merged。
* @xiakaipan：提交“RISC-V 虚拟化模式切换简析”，closed。
* @ENJOU1224：Review“CPU 设计——数电基本知识与基于 Scala 的硬件设计框架 SpinalHDL”。
* @iOSDevLog：邀请倪老师分享 [《从零开始 RISC-V 硬件设计》之 I2C+SPI 总线使用经验][202207231]；完成本次会议记录。
* @falcon：新增 [RISC-V jump_label 详解，第 5 部分：优化案例][192]，merged；所有 PR Review。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220716：第十九周

本周开展了两次在线分享，7/15 由实习生 Retro 享了 CTF 赛事介绍与 PWN 环境研讨会，7/16 由开放课程《循序渐进，学习开发一个 RISC-V 上的操作系统》作者汪老师分享了 RISC-V 操作系统/RVOS 视频公开课讲师导读。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @GoodBoyCC：提交 vDSO 资料。
* @YJMSTR：提交 QEMU 资料。
* @bosswangst：提交 RISC-V CPU Design 资料。
* @zhou-zitang：更新测试代码 test/microbench。
* @tangjinyu1994：新增文章 [memblock 内存分配器原理和代码分析][181]，merged。
* @nfk1996：新增文章 [RISC-V 中断子系统分析——CPU 中断处理][182]，merged。
* @yooyoyo：新增新闻 [RISC-V Linux 内核及周边技术动态][news]，merged。
* @walimis：Review [memblock 内存分配器原理和代码分析][181]。
* @iOSDevLog：完成本次会议记录；协助处理本周活动进度。
* @falcon：所有 PR Review；安排 CTF 分享，邀请汪老师分享“RISC-V 操作系统/RVOS 视频公开课讲师导读”。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220709：第十八周

本周由王老师图文并茂的展示了 GPU 虚拟化技术实现，系统地梳理了 GPU 虚拟化技术的工作原理。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @YJMSTR：新增“用纯 C 语言写一个简单的 RISC-V 模拟器（支持基础整数指令集，乘法指令集与 CSR 指令）”，merged。
* @bosswangst：新增“RISC-V 特权指令架构”，merged。
* @xiakaipan：新增 “用 QEMU/Spike+KVM 运行 Host/Guest Linux”，merged。
* @yooyoyo：新增 [ISC-V Linux 内核及周边技术动态][news]，merged。
* @ZhaoSQ-CH：提交 “为 Linux Lab 构建 QEMU”，closed。
* @GoodBoyCC：整理了 vDSO 技术。
* @iOSDevLog：完成本次会议记录；提交 dts part1, merged。
* @walimis：Reivew；开展本次 GPU 虚拟化技术分享；指导实习生开展虚拟化专项，布置&Review 考核任务，推荐阅读书籍并安排制定项目计划。
* @Bin Meng：Review 实习生的考核任务，指导实习生开展 QEMU 模拟器相关任务。
* @falcon：Review；调整 Issues 中 Proposals 进度；安排 CTF 分享；安排该活动的项目管理人员。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频；为实习人员寄送泰晓 Linux 实验盘和 D1 开发板。

### 20220702：第十七周

本周开展了两次在线分享，7/1 由廖老师分享了 RISC-V Linux Time 技术原理与实现，7/2 开展了暑期实习宣讲会。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @zi-tang：新增 AMD Ryzen 测试结果；新增 loongarch64 架构支持和测试数据；新增 loongarch64 support 文档，all merged。
* @通天塔：新增 irq part2, merged。
* @呀：新增 [第一期 RISC-V Linux 内核及周边技术动态][news]，merged。
* @Fajie：新增 CPU Design 系列文章第一篇：RV32 指令集介绍，merged。
* @iOSDevLog：dts part1 merged。
* @zhaosq：提交 QEMU RISC-V 相关视频资料整理 v2，closed。
* @yjmstr：提交 v2 of RISC-V emulator 译文，待提交合并。
* @kyrie.xi：v2 of swtimer，待再次提交合并。
* @falcon：Review；宣讲会准备与实施；实习生资格审核等。
* @walimis：Reivew；准备并宣讲虚拟化计划；指导实习生开展工作。
* @tinylab：本周所有文字与视频类资料处理与发布；会议纪要；讲师邀请等。

### 20220625：第十六周

本周由小陈老师结合内核源码详细地讲解了 RISC-V 系统调用的工作原理与实现，视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @dlan17：订正译文并 Merge。
* @Jack.Y：提交 v2 演示幻灯。
* @envestcc：提交并订正 syscall part2，Merged；准备并开展本周的在线分享。
* @yjmstr：认领 RISC-V emulator 相关译文一则。
* @Fajie：认领 RV64I CPU Desgin 任务。
* @通天塔：提交 irq part2 一文，待 Review。
* @kyrie.xi：提交 swtimer 一文，待 Review。
* @falcon：Review 上周与本周提交的多篇 PR；正式启动 2022 RISC-V Linux 师友计划，已邀请 10 多位老师，正在 Issues 中创建 Proposals，预计 6.28 发预告，7.2 面向学生做宣讲。
* @walimis：同步 Review 上周与本周提交的多篇 PR，准备虚拟化相关的项目 Proposal。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频；完成本次会议记录。

### 20220618：第十五周

本周由小尹老师结合 RISC-V 处理器架构介绍了 Linux 内核调度方面的内容，内容非常精彩，讨论气氛很热烈。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @dlan17：提交译文，LWN-867818: Not-so-anonymous virtual memory areas
* @tangjinyu1994：新增认领 ioremap，修订 memblock 一文。
* @envestcc：根据 Review 建议修订 syscall part1，Merged。
* @jingwei.xi：认领 SMP Support。
* @kyrie.xi：认领 swtimer。
* @xiakaipan：认领 CMA、DMA、Hugetlbfs。
* @falcon：Review 上周与本周提交的多篇 PR；邀请到新的 Review 老师；组织暑期的实习生计划。
* @walimis：同步 Review 上周与本周提交的多篇 PR；准备 GPU 虚拟化 相关的在线技术分享。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220611：第十四周

本周由小贾老师结合 Linux v0.01 到 v5.0 的代码分享了 printk 的演进历史，并基于源码分析了 printk 的核心工作原理。与此同时，吴老师介绍了 RISC-V Linux 活动下一阶段的工作计划和目标。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @iOSDevLog：提交了“printk 初探”一文，Merged，他还主讲了本周直播分享。
* @通天塔：提交了 riscv-irq-analysis 一文，正在修订。
* @wufse：提交了 irq-implementation 和 ipi-implementation 两篇文章，正在修订。
* @envestcc：提交了 syscall 系列 part1，等待 Review。
* @falcon：提交并 Merge 了 Jump Label Part4，并提交了 1 笔 RFC Patch 到 Linux 社区；Review 本周数篇文章；在本周分享中介绍第 2 阶段活动目标与计划并宣布正式开放实习与兼职岗位。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220604：第十三周

本周端午节，未预约分享活动。

本周纪要：

* @Jack.Y：订正并 Merge Linux Schedule 一文。
* @hev, @taotieren：Review Summer2022 的项目申请书。
* @falcon：规划 RISC-V Linux 第 2 阶段，由于各项分析活动逐渐进入深水区，考虑类似 Summer2022 活动，采用 Mentor & Student 的方式开展接下来的工作，各个大的 Topic 在已参与的同学中遴选一批 Mentor，由他们制定各个 Topic 的方案，并招募 2-3 位在校实习生来具体开展实施，Mentor 负责规划、指导、Review 进度和输出等。
* @tinylab：在多个渠道发布本周技术文章。

### 20220528：第十二周

由于预约的分享老师有突发的加班安排，本周的分享时间另行安排。

本周纪要：

* @RXD：新建 Ftrace Issue，收集了相关的资料。
* @tyjtimi：订正 task implementation 后再次提交并 Merge。
* @tinylab：在多个渠道发布本周技术文章。

### 20220521：第十一周

本周邀请到了来自兰州大学一生一芯项目的大三学生谭源和魏人两位同学来做了 RISC-V 处理器设计方面的技术分享，内容非常精彩。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @hev, @taotieren：处理来自开源之夏 Microbench 和 OpenHW 项目的学员咨询邮件并协助 Review 项目申请书。
* @tangjinyu1994：认领 setup_bootmem 和 fixed mapping。
* @通天塔：提交 RISC-V Boot Flow Analyze 一文并 Merge；认领 IRQs。
* @juliwang：认领 Kprobes。
* @marycarry：认领 setup_arch。
* @envestcc：认领 Syscall。
* @falcon：Review RISC-V Boot Flow 并组织首次开源之夏项目启动会；组织本周在先技术分享。
* @tinylab：在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220514：第十周

本周由 RISC-V Linux StackTrace 文章作者 @Jeff 老师主讲，介绍了 StraceTrace 原理与实现。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要：

* @hev, @taotieren：新增开源之夏 Microbench 和 OpenHW 项目两位 Mentor 进协作仓库。
* @walimis：新增一位 Reviewer。
* @通天塔：提交 RISC-V Linux 启动流程一文。
* @tangjinyu1994：提交 Task Implementation 一文并认领多笔新任务。
* @falcon：组织开源之夏 RISC-V 相关项目启动会；Review 本周技术文章。
* @tinylab：预约未来四周的在线分享安排；在多个渠道发布本周技术文章；剪辑并发布本周在线分享视频。

### 20220508：第九周

本周由 Linux Porting 系列文章的译者 @通天塔 结合 RISC-V 分享 Linux 内核启动流程。视频剪辑已经上传，见“回放发布”一节链接。

本周纪要（0423-0508）：

* @pwl999：新增 kfence 一文并 Merge。
* @pingbo：新增 RISC-V 特权指令介绍并 Merge。
* @iOSDevLog：提交 earlycon 初稿，待订正。
* @nanan01：增 Timer v2 一文并 Merge。
* @Jack Y.：新增 Context Swtich v1/v2 一文并 Merge；认领 Multi-tasking。
* @hev：提交 microbench mips64 支持。
* @falcon：新增 OpenSBI quickstart 一文；Review 本周所有提交（5 篇文章 + microbench mips64）。
* @tinylab：在知乎、公众号等渠道发布 OpenSBI quickstart；剪辑本周视频并在知乎、B 站、Cctalk 等渠道发布。

### 20220430：第八周

五一假期，本周暂停线上分享。

### 20220423：第七周

经过连续不间断的 6 周线上分享后，由于大家比较忙，本周暂停线上分享，预计会在五一后恢复。

本周纪要：

* @iOSDevLog：认领 earlycon 和 dts。
* @tangjinyu1994：认领 Task Implementation 并启动分析过程。
* @通天塔：完成 Linux Porting 系列第 3 篇译稿并 Merge。
* @Jack Y：更新 Sparsemem，按 @falcon Review 建议新增多幅图片。
* @falcon：Review & Merge Linux Porting Part3；Merge Sparsemem。
* @tinylab：在多个渠道发布 Linux Porting part3。

### 20220416：第六周

本周由吴老师分享了 RISC-V Linux Tracepoint/Jump Label 的前两部分，即 Jump Label 核心原理介绍与 RISC-V 指令编码实战。演示和讨论视频稍后上传，见本文“回放发布”一节链接。

Jump Label 技术是内核中的经典类 “JIT” 思想，可以根据需要动态生成并替换指令，从而提升系统性能，确保 Tracepoint 等技术可以直接编译进内核，按需在线上启停，大大提升了 Linux Tracing 的可用性。

吴老师结合真实的性能数据详细讲解了其核心原理，之后介绍了如何参考 RISC-V 指令手册逐个编码 Jump Label 需要用到的两条基础指令，最后演示了通过 objdump、gdb 以及 pwn asm 三种工具实现快速指令编码的技巧。

期间，有同学介绍了 RISC-V Static PIE（静态编译 + 位置无关运行）的开发进展，讨论了 RISC-V 16/32 指令混排（RVI 基础指令与 RVC 扩展如何同时开启）与 ARM 16/32 指令混排（ARM 与 Thumb 指令集如何混排）等内容。

本周纪要：

* @cursor_risc-v：认领 Barriers。
* @davidlamb：认领 setup_vm。
* @sunnybird：认领 Security -> Penglai for RISC-V。
* @jacob：提交第 4 版 UEFI 相关文章并 Merge。
* @pingbo：研究 RISC-V SMP 动流程，特权指令，以及和 ARM 多核启动的对比。
* @通天塔：认领 Linux Porting 系列第 3 篇。
* @pwl999：研究 D1 在 RTOS 中使用 tspend 方式进行进程切换的原理。
* @Jack Y：提交并 Merge Sparsemem 一文，新增认领 Context Switch。
* @iOSDevLog：调试 RVB2601RISC-V 生态开发板，比较 ARM 和 RISC-V；完善 microbench for ARMv7 支持。
* @falcon：提交并 Merge Jump Label 系列第 3 篇核心实现分析，开展本周技术直播分享；完善 Linux Lab 中的 RISC-V 汇编例子；Review Sparsemem 与 UEFI v4 等多笔 PR；订正历史文章。
* @tinylab：协助剪辑本周视频；并在社区网站、B 站、知乎、Cctalk、公众号、视频号、知识星球等多个渠道发布相关文章和视频；发布过程中协助订正多篇历史文章的个别错别字；协助更新 plan。

### 20220409：第五周

本周邀请了 RustSBI 作者洛佳老师做了“RISC-V 引导程序环境：应用&规范”的主题分享。演示和讨论视频稍后上传，见本文“回放发布”一节链接。

SBI 是 RISC-V 定义的一套 Supervisor Binary Interface，规范了 OS 调用 Machine Mode 服务的方式，而 RustSBI 是 SBI 标准的 Rust 语言实现，是 [RISC-V SBI][202208202] 规范标准中收录的实现之一。截止到分享当天，RustSBI 已支持 SBI 标准 v1.0，而另外一个 C 语言实现的 OpenSBI 仅支持 SBI v0.2（SBI v1.0 之前还有一个 v0.3）。

本周纪要：

* @Alony.Quan：认领 XIP。
* @Luo Xiaogang：认领 Ftrace。
* @zhao305149619：认领 Kexec & Kdump。
* @通天塔：提交并合并第 2 篇 Linux porting 译文，认领该系列最后 1 篇的翻译任务。
* @iOSDevLog：为 microbench 适配 aarch64 和 armv7，并提交相应测试结果。
* @pwl999：在知识星球内分享了一个 RISC-V 逆向工具 ghidra。
* @pingbo：继续研究 SMP 相关功能，已经收集了一些资料。
* @jacob：提交第 3 版 UEFI 相关文章，增补了 OpenSBI 和 U-Boot 相关知识，正在根据 Review 意见修订。
* @liaoyu：提交了第 1 版 RISC-V timer 相关分析文章，正在根据 Review 意见修订。
* @TanyoKwok：提交了第 1 版 Syscall 相关分析文章，正在根据 Review 意见修订。
* @falcon：Review @iOSDevLog microbench 新增架构代码；详细 Review 译文、UEFI、Timer 以及 Syscall 等数篇文章。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220402：第四周

本周由小文老师分享了原子指令及其用法。演示和讨论视频稍后上传，见本文“回放发布”一节链接。

有多位芯片设计与验证工程师和内核工程师参与了讨论，内容精彩，气氛热烈。

本周纪要：

* @pingbo：准备和开展了本次分享。
* @通天塔：第 2 篇翻译稿已提交 PR，@falcon 和 @iOSDevLog 完成了 Review。
* @iOSDevLog：提交了 RISC-V ISA 介绍与演示 一文；在 D1 上运行 microbench 并提交了测试结果。
* @Jack Y, @liuxig, @liaoyu, @Stan Wang：分别认领了不同的任务，见 plan/README.md
* @falcon：撰写了 jump label part2；撰写了 RISC-V Linux Distributions 一文；继续迭代 microbench 并撰写了首份 microbench 测试报告。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220326：第三周

本周分享由小贾老师主讲，主要介绍了 RISC-V ISA 相关的知识并做了简要演示。演示和讨论视频稍后上传，见本文“回放发布”一节链接。

会后大家进行了热烈的讨论，涉及本次活动的实验环境、任务认领、任务列表、UEFI、如何入门等议题，会前吴老师特别介绍了 RISC-V Linux 活动过程中提交成果时的注意事项。

**今天的分享非常适合有兴趣参加 RISC-V Linux 兴趣小组的同学们，在正式参与前建议先简单看一下。**

本周纪要：

* @通天塔：第 2 篇翻译稿已基本完成，准备提交 PR for Review。
* @pwl999：正在分析内核运行时代码修改相关内存管理部分。在星球分享了 RISC-V RTOS 相关资料。
* @pingbo：已经提交第 1 篇 Atomic Locking 相关的技术文章，merged。
* @iOSDevLog：准备和开展本周分享。
* @jeff.zhao：第 2 次提交 StackTrace 一文，merged。
* @jacob：提交了 UEFI 第 1 篇文章，reviewed，等待修订后再 merge。
* @falcon：完善 microbench 测试框架并提交了 x86_64 平台的测试代码和测试数据，提交了 riscv64 测试代码；基于 Linux v5.17 录制了 RISC-V Linux 内核实验教程。
* @dlan17：测试并提交了 Unmatched 这款 RISC-V 平台的 microbench 数据，另外也新增了一笔 PMU 任务。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220319：首次分享

本次分享由彭伟林老师主讲，比照 X86 介绍了 RISC-V Paging & MMU，并展开介绍了内核相关的公共部分。次日已剪辑并发布到三个视频频道，见本文“回放发布”一节链接。

本周纪要：

* @通天塔：已经提交 Linux Porting to New Arch 系列文章第 1 篇的译稿，正在翻译剩下的 2 篇。
* @pwl999：开展本周分享，后续继续分析 psABI，并协助 @falcon 分析内核运行时代码修改相关内存管理部分，比如 `patch_map/patch_unmap`。
* @pingbo：继续阅读 ISA 部分，准备第 3 场分享并开始分析 Atomic Locking 部分。在星球分享了 RISC-V ISA 相关资料。
* @iOSDevLog：准备第 2 场分享。
* @郭天佑 TanyoKwok：认领 Generic library routines and assembly
* @falcon：校订 @通天塔 的译文；发布 Jump Label 源码分析第 1 节，接下来继续撰写另外 2 篇文章，并基于 21 日发布的 Linux v5.17 录制 RISC-V Linux 内核实验教程。
* @hev, @pingbo, @dlan17：协同设计测试用例，测试不同架构基础指令的性能，部分结果已经整理到 Jump Label 分析第 1 节，接下来继续完善测试用例，新增 D1 和 Unmatched 两个 RISC-V 平台的数据。
* @tinylab：协助剪辑视频并在各个渠道发布相关文章和视频。

### 20220312：首次周会

本次周会回顾了上周的动态并制定了下周计划，下周开始轮流作视频分享，分享时间从 `8:30-9:30`。

本周纪要：

* @pwl999：本周已经收集和整理了部分 MM 的资料，计划下周六（20220319）做直播分享，接下来计划分析 ABI 部分。在星球分享了 T-head 相关资料。
* @pingbo：本周正在学习相关 Spec，初步了解 RISC-V Linux 启动过程，计划 20220402 做直播分享，接下来计划横向对比 RISC-V 和其他架构，分析 RISC-V 关键特点，远期计划移植一款 rtos 到 RISC-V。
* @iOSDevLog：正在学习 RVOS 并购买了相关书籍，正在整理 RISC-V Spec 相关资料，计划于 20220326 做分享，接下来有意向调研一下开源的 RISC-V 核，尝试用 FPGA 跑起来。
* @falcon：调研了最新 RISC-V Linux 社区动态，发布了兴趣小组招募信息，并撰写了首篇分析文章，同时分享了如何用 Linux Lab Disk 开展 RISC-V 汇编语言实验，下周继续介绍如何用 Linux Lab Disk 开展 RISC-V Linux 内核实验。

另有多位同学参与了讨论和交流：

* 一位同学认领了 "Porting Linux to a new processor architecture" 的翻译部分。
* 另外一位同学咨询了开发与测试环境部分，这部分我们统一采用 Linux Lab + riscv64/virt 或 D1 开发板。
* 还有一位同学建议是否可以从 Fixup 的角度来参与，我们把招募信息中收集的 RISC-V Linux 最近三个月的关键动态追加到了 plan/README.md，后续大家也可以认领。

### 20220305：项目启动会

本次会议正式启动了“RISC-V Linux 内核兴趣小组”，讨论了首期 3 个月的目标：完成 Linux RISC-V 架构相关部分的剖析，并现场创建了 RISC-V Linux 协作仓库。

本周纪要：

* @pwl999：认领了 Paging & MMU。
* @iOSDevLog：认领了 ISA，后续兼任项目会议组织和进度管理。
* @pingbo：认领了 Atomic and Locking Code。
* @falcon：创建了 RISC-V Linux 协作仓库，整理了 riscv-linux.xmind 以及相关资料，初步整理了任务列表。

[#I5E4VB]: https://gitee.com/tinylab/riscv-linux/issues/I5E4VB
[#I5EE48]: https://gitee.com/tinylab/riscv-linux/issues/I5EE48
[#I5EIOA]: https://gitee.com/tinylab/riscv-linux/issues/I5EIOA
[#I5F0PU]: https://gitee.com/tinylab/riscv-linux/issues/I5F0PU
[#I5GYUU]: https://gitee.com/tinylab/riscv-linux/issues/I5GYUU
[#I5LIEI]: https://gitee.com/tinylab/riscv-linux/issues/I5LIEI
[#I5OF32]: https://gitee.com/tinylab/riscv-linux/issues/I5OF32
[#I5T3XB]: https://gitee.com/tinylab/riscv-linux/issues/I5T3XB
[#I5TX4H]: https://gitee.com/tinylab/riscv-linux/issues/I5TX4H
[#I5W8R8]: https://gitee.com/tinylab/riscv-linux/issues/I5W8R8
[#I5WN7A]: https://gitee.com/tinylab/riscv-linux/issues/I5WN7A
[#I5X9Q5]: https://gitee.com/tinylab/riscv-linux/issues/I5WN7A
[#I5YRKJ]: https://gitee.com/tinylab/linux-observe/issues/I5YRKJ
[#I5YS1M]: https://gitee.com/tinylab/linux-observe/issues/I5YS1M
[#I5YS2B]: https://gitee.com/tinylab/linux-observe/issues/I5YS2B
[#I5Z8C6]: https://gitee.com/tinylab/linux-observe/issues/I5Z8C6
[#I5ZD1W]: https://gitee.com/tinylab/linux-observe/issues/I5ZD1W
[#I5ZM22]: https://gitee.com/tinylab/riscv-linux/issues/I5ZM22
[#I60FB1]: https://gitee.com/tinylab/riscv-linux/issues/I60FB1
[#I61K05]: https://gitee.com/tinylab/riscv-linux/issues/I61K05
[#I61K5B]: https://gitee.com/tinylab/riscv-linux/issues/I61K5B
[#I61K8X]: https://gitee.com/tinylab/riscv-linux/issues/I61K8X
[#I61KIY]: https://gitee.com/tinylab/riscv-linux/issues/I61KIY
[news]: https://gitee.com/tinylab/riscv-linux/tree/master/news
[181]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220710-riscv-memblock.md
[182]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220712-riscv-irq-analysis-part3-Interrupt-handling-cpu.md
[191]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220717-riscv-syscall-part3-vdso-overview.md
[192]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220717-riscv-tracepoint-jump-label-part5-examples.md
[193]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220718-build-qemu-and-boot-openeuler-for-risc-v-in-tinylab-linux-lab-disk.md
[194]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220722-digital-electronic-with-spinalhdl.md
[201]: https://gitee.com/tinylab/riscv-linux/issues/I5JILY
[202]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220724-virt-mode.md
[203]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220727-run-riscv-ubuntu22.04-over-x86_64.md
[211]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220803-cpu-design-analysis-and-main-module-implement.md
[212]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220804-riscv-ret-from-kernel-thread.md
[213]: https://gitee.com/tinylab/riscv-linux/raw/master/ppt/gpu-virtualization-intro.pdf
[214]: https://gitee.com/tinylab/riscv-linux/raw/master/ppt/dts.pdf
[221]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220809-d1-lab-step1.md
[222]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220809-d1-lab-step2.md
[223]: https://gitee.com/tinylab/riscv-linux/issues/I5LIEI
[224]: https://gitee.com/tinylab/riscv-linux/issues/I5L9H0
[231]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220812-ftrace-impl-1-mcount.md
[232]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220816-cpu-design-module-board-test.md
[233]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220816-introduction-to-qemu-and-riscv-upstream-boot-flow.md
[234]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220816-riscv-syscall-part4-vdso-implementation.md
[235]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220817-linux-dts-2.md
[236]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220818-linux-dts-3.md
[241]: https://gitee.com/tinylab/riscv-linux/blob/master/ppt/riscv-microbench-tsoc2022.pdf
[242]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220802-kvm-user-app.md
[243]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220812-riscv-kvm-mem-virt-1.md
[244]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220822-linux-dts-4.md
[251]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220823-boot-riscv-linux-kernel-with-uboot-on-qemu-virt-machine.md
[252]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220811-a-fast-look-at-riscv-virtualization.md
[253]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220724-riscv-irq-analysis-part4-interrupt-priority.md
[254]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220827-missing-features-tools-for-riscv.md
[202207081]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220708-riscv-kvm-qemu-spike-linux-usage.md
[202207151]: https://b23.tv/bnV92b1
[202207161]: https://b23.tv/zU4DPXa
[202207231]: https://b23.tv/f8GJPSg
[202207301]: https://b23.tv/LUiQaMl
[202208061]: https://b23.tv/g2NIYc8
[202208131]: https://b23.tv/VNOIPGn
[202208201]: https://b23.tv/VWrUb2s
[202208202]: https://github.com/riscv-non-isa/riscv-sbi-doc/blob/master/riscv-sbi.adoc#49-sbi-implementation-ids
[202208203]: https://space.bilibili.com/687228362/channel/collectiondetail?sid=273934
[202208204]: https://www.cctalk.com/m/group/90251209
[202208205]: https://www.zhihu.com/column/tinylab
[202208206]: https://gitee.com/tinylab/tinycorrect
[202208271]: https://b23.tv/FtbuJqV
[202208272]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220819-ftrace-impl-2-build.md
[202208273]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220819-riscv-irq-pipeline-introduction.md
[202208274]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220826-riscv-cpu-cotroller-module-design.md
[202208275]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220908-d1-lab-step3.md
[202208276]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220909-ftrace-impl-3-replace.md
[202208277]: https://git.kernel.org/pub/scm/utils/dtc/dtc.git/commit/?id=c6e92108bcd9c13ebbbcab44a49fa5f39c21621e
[202209031]: https://b23.tv/Yhc2Ml5
[202209171]: https://b23.tv/qXQL6k9
[202209241]: https://b23.tv/OaScNVe
[202209242]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220910-ftrace-impl-4-replace-trace-function.md
[202209243]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220928-ftrace-impl-5-fgraph.md
[202209244]: https://gitee.com/tinylab/riscv-linux/blob/master/ppt/riscv-linux-half-year-report-20220924.pdf
[202210081]: https://b23.tv/n3kkG2B
[202210082]: https://b23.tv/DFbGfpE
[202210151]: https://b23.tv/qBN4yX8
[202210152]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220812-riscv-kvm-mem-virt-2.md
[202210153]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220905-virt-kvm-trap.md
[202210154]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220911-qemu-riscv-zsbl.md
[202210155]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220922-d1h-benchmark.md
[202210156]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221004-riscv-page-fault-part1.md
[202210157]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221006-d1-lab-step4-and-try-ubuntu-official.md
[202210158]: https://gitee.com/tinylab/riscv-linux/blob/master/ppt/qemu-riscv-boot-flow.pdf
[202210159]: https://gitee.com/tinylab/riscv-linux/blob/master/ppt/riscv-ftrace-impl-20221015.pdf
[202210221]: https://b23.tv/x12mCZU
[202210222]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20220907-riscv-irq-analysis-part5-generic-entry-backgroud-and-status.md
[202210291]: https://b23.tv/pUwcKAr
[202210292]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221020-missing-features-tools-for-riscv-part2.md
[202211051]: https://b23.tv/QIy2l7T
[202211052]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221005-opensbi-firmware-and-sbi-hsm.md
[202211053]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221011-riscv-kvm-mem-virt-impl.md
[202211054]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221021-riscv-kvm-excp-impl.md
[202211055]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221103-riscv-page-fault-part2.md
[202211056]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221104-riscv-ebpf-overview.md
[202211057]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221104-riscv-hardware-design-3d.md
[202211058]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221104-riscv-page-fault-part3.md
[202211121]: https://b23.tv/fO8CdId
[202211122]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221031-static-call-part1-meltdown-and-spectre.md
[202211191]: https://b23.tv/wLfdkPE
[202211192]: https://gitee.com/tinylab/elfs
[202211193]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221112-mailing-list-intro.md