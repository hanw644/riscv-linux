# RISC-V Linux 内核及周边技术动态

## 往年技术动态汇总

* [2022 年](2022.md)
* [2023 年 - 上半年](2023-1st-half.md)

## 20230804：第 55 期

### 内核动态

#### RISC-V 架构支持

**[v1: RISC-V: ACPI: Add external interrupt controller support](http://lore.kernel.org/linux-riscv/20230803175916.3174453-1-sunilvl@ventanamicro.com/)**

> This series adds support for the below ECRs approved by ASWG recently.
> 1) MADT - https://drive.google.com/file/d/1oMGPyOD58JaPgMl1pKasT-VKsIKia7zR/view?usp=sharing
> 2) RHCT - https://drive.google.com/file/d/1sKbOa8m1UZw1JkquZYe3F1zQBN1xXsaf/view?usp=sharing
>

**[v4: RISC-V: KVM: change get_reg/set_reg error code](http://lore.kernel.org/linux-riscv/20230803163302.445167-1-dbarboza@ventanamicro.com/)**

> This version includes a diff that Andrew mentioned in v2 [1] that I
> missed. They were squashed into patch 1.
>
> No other changes made. Patches rebased on top of riscv_kvm_queue.
>

**[v1: pwm: Manage owner assignment implicitly for drivers](http://lore.kernel.org/linux-riscv/20230803140633.138165-1-u.kleine-koenig@pengutronix.de/)**

> while working on the pwm core I noticed that pwm-crc doesn't assign
> .owner in its pwm_ops structure. This isn't a problem in practise though
> as this driver can only be compiled built-in (up to now).
>
> Still prevent future pwm drivers not assigning .owner by wrapping
> pwmchip_add() in a macro that automates owner handling simplifying each
> driver a (tiny) bit.
>

**[v4: Change PWM-controlled LED pin active mode and algorithm](http://lore.kernel.org/linux-riscv/20230803085734.340-1-nylon.chen@sifive.com/)**

> According to the circuit diagram of User LEDs - RGB described in themanual hifive-unleashed-a00.pdf[0] and hifive-unmatched-schematics-v3.pdf[1].
>

**[v7: Linux RISC-V AIA Support](http://lore.kernel.org/linux-riscv/20230802150018.327079-1-apatel@ventanamicro.com/)**

> The RISC-V AIA specification is now frozen as-per the RISC-V international
> process. The latest frozen specifcation can be found at:
> https://github.com/riscv/riscv-aia/releases/download/1.0/riscv-interrupts-1.0.pdf
>

**[v1: Add I2S support for the StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230802084301.134122-1-xingyu.wu@starfivetech.com/)**

> This patch series adds I2S support for the StarFive JH7110 RISC-V
> SoC based on Designware I2S controller. There has three I2S channels
> (RX/TX0/TX1) on the JH7110 SoC, one of which is for record(RX) and
> two for playback(TX).
>

**[v6: riscv: Allow userspace to directly access perf counters](http://lore.kernel.org/linux-riscv/20230802080328.1213905-1-alexghiti@rivosinc.com/)**

> riscv used to allow direct access to cycle/time/instret counters,
> bypassing the perf framework, this patchset intends to allow the user to
> mmap any counter when accessed through perf.
>

**[v1: riscv: Using TOOLCHAIN_HAS_ZIHINTPAUSE marco replace zihintpause](http://lore.kernel.org/linux-riscv/20230802064215.31111-1-minda.chen@starfivetech.com/)**

> Actually it is a part of Conor's
> commit aae538cd03bc ("riscv: fix detection of toolchain
> Zihintpause support").
> It is looks like a merge issue. Samuel's
> commit 0b1d60d6dd9e ("riscv: Fix build with
> CONFIG_CC_OPTIMIZE_FOR_SIZE=y") do not base on Conor's commit and
> revert to __riscv_zihintpause. So this patch can fix it.
>

**[v2: RISC-V: cpu: refactor deprecated strncpy](http://lore.kernel.org/linux-riscv/20230802-arch-riscv-kernel-v2-1-24266e85bc96@google.com/)**

> `strncpy` is deprecated for use on NUL-terminated destination strings [1].
>
> Favor not copying strings onto stack and instead use strings directly.
> This avoids hard-coding sizes and buffer lengths all together.
>

**[v5: riscv: entry: set a0 = -ENOSYS only when syscall != -1](http://lore.kernel.org/linux-riscv/20230801141607.435192-1-CoelacanthusHex@gmail.com/)**

> When we test seccomp with 6.4 kernel, we found errno has wrong value.
> If we deny NETLINK_AUDIT with EAFNOSUPPORT, after f0bddf50586d, we will
> get ENOSYS instead. We got same result with commit 9c2598d43510 ("riscv:
> entry: Save a0 prior syscall_enter_from_user_mode()").
>

**[v1: riscv: Correct the MODULES_VADDR](http://lore.kernel.org/linux-riscv/20230801111014.1432679-1-suagrfillet@gmail.com/)**

> As Documentation/riscv/vm-layout.rst describes, the 2G-sized "modules, BPF"
> area should lie right before the "kernel" area. But the current definition
> of MODULES_VADDR isn't consistent with that, so correct it.
>

**[v1: riscv: Flush stale TLB entry with VMAP_STACK enabled](http://lore.kernel.org/linux-riscv/20230801090927.2018653-1-dylan@andestech.com/)**

> When VMAP_STACK is enabled, the kernel stack will be obtained through
> vmalloc(). Normally, we rely on the logic in vmalloc_fault() to update stale
> P*D entries covering the vmalloc space in a task's page tables when it first
> accesses the problematic region. Unfortunately, this is not sufficient when
> the kernel stack resides in the vmalloc region, because vmalloc_fault() is a
> C function that needs a stack to run. So we need to ensure that these P*D
> entries are up to date *before* the MM switch.
>

**[v1: DRM driver for verisilicon](http://lore.kernel.org/linux-riscv/20230801101030.2040-1-keith.zhao@starfivetech.com/)**

> This patch is a drm driver for Starfive Soc JH7110,
> I am sending Drm driver part and HDMI driver part.
>
> We used GEM framework for buffer management , and
> for buffer allocation,we use DMA APIs.
>

**[v3: riscv: tlb flush improvements](http://lore.kernel.org/linux-riscv/20230801085402.1168351-1-alexghiti@rivosinc.com/)**

> This series optimizes the tlb flushes on riscv which used to simply
> flush the whole tlb whatever the size of the range to flush or the size
> of the stride.
>
> Patch 3 introduces a threshold that is microarchitecture specific and
> will very likely be modified by vendors, not sure though which mechanism
> we'll use to do that (dt? alternatives? vendor initialization code?).
>

**[v4: KVM: selftests: Guest printf extra prep patches](http://lore.kernel.org/linux-riscv/20230731203026.1192091-1-seanjc@google.com/)**

> Delta patches for v4 of the guest printf series to enable guest_printf_test
> for all architectures.  Sending all 36 patches seemed counter-productive,
> and I want to get input on adding ucall.h asap.
>

**[v1: riscv: correct riscv_insn_is_c_jr() and riscv_insn_is_c_jalr()](http://lore.kernel.org/linux-riscv/20230731183925.152145-1-namcaov@gmail.com/)**

> The instructions c.jr and c.jalr must have rs1 != 0, but
> riscv_insn_is_c_jr() and riscv_insn_is_c_jalr() do not check for this. So,
> riscv_insn_is_c_jr() can match a reserved encoding, while
> riscv_insn_is_c_jalr() can match the c.ebreak instruction.
>

**[v8: mm-unstable: Split ptdesc from struct page](http://lore.kernel.org/linux-riscv/20230731170332.69404-1-vishal.moola@gmail.com/)**

> The MM subsystem is trying to shrink struct page. This patchset
> introduces a memory descriptor for page table tracking - struct ptdesc.
>
> This patchset introduces ptdesc, splits ptdesc from struct page, and
> converts many callers of page table constructor/destructors to use ptdescs.
>

**[v3: riscv: Handle zicsr/zifencei issue between gcc and binutils](http://lore.kernel.org/linux-riscv/20230731095936.23397-1-xingmingzheng@iscas.ac.cn/)**

> Binutils-2.38 and GCC-12.1.0 bumped[0][1] the default ISA spec to the newer
> Zicsr and Zifencei extensions. So if one of the binutils and GCC exceeds
> that version, we should explicitly specifying Zicsr and Zifencei via -march
> to cope with the new changes. but this only occurs when binutils >= 2.36
> and GCC >= 11.1.0. It's a different story when binutils < 2.36.
>

**[v2: Add PWM-DAC audio support for StarFive JH7110 RISC-V SoC](http://lore.kernel.org/linux-riscv/20230731032829.127864-1-hal.feng@starfivetech.com/)**

> This patchset adds PWM-DAC audio support for the StarFive JH7110 SoC.
> The PWM-DAC module does not require a hardware codec, but a dummy codec is
> needed for the driver. The dummy spdif codec driver, which is already
> upstream, is compatible with the one which JH7110 PWM-DAC needed. So we
> use it as the dummy codec driver for the JH7110 PWM-DAC module.
>

**[v1: riscv: dts: allwinner: d1: Specify default CAN pins](http://lore.kernel.org/linux-riscv/20230731023701.2581713-1-contact@jookia.org/)**

> There are only one set of CAN pins available on these chips.
> Specify these as the default to avoid redundancy in board device trees.
>

**[v2: asm-generic: ticket-lock: Optimize arch_spin_value_unlocked](http://lore.kernel.org/linux-riscv/20230731023308.3748432-1-guoren@kernel.org/)**

> The arch_spin_value_unlocked would cause an unnecessary memory
> access to the contended value. Although it won't cause a significant
> performance gap in most architectures, the arch_spin_value_unlocked
> argument contains enough information. Thus, remove unnecessary
> atomic_read in arch_spin_value_unlocked().
>

**[v1: riscv: kprobes: simulate some instructions](http://lore.kernel.org/linux-riscv/cover.1690704360.git.namcaov@gmail.com/)**

> Simulate some currently rejected instructions. Still to be simulated are:
>     - c.jal
>     - c.ebreak
>

**[v1: KVM: Wrap kvm_{gfn,hva}_range.pte in a per-action union](http://lore.kernel.org/linux-riscv/20230729004144.1054885-1-seanjc@google.com/)**

> Wrap kvm_{gfn,hva}_range.pte in a union so that future notifier events can
> pass event specific information up and down the stack without needing to
> constantly expand and churn the APIs.  Lockless aging of SPTEs will pass
> around a bitmap, and support for memory attributes will pass around the
> new attributes for the range.
>

**[v2: RISC-V: KVM: provide UAPI for host SATP mode](http://lore.kernel.org/linux-riscv/20230728210122.175229-1-dbarboza@ventanamicro.com/)**

> This new version is just a rebase after the recent changes made by Anup
> that landed in riscv_kvm_queue (introduction of vcpu_onereg.c). No other
> changes were made.
>

**[v1: hwrng: Enable COMPILE_TEST for more drivers](http://lore.kernel.org/linux-riscv/20230728195015.1198427-1-robh@kernel.org/)**

> There's quite a few hwrng drivers which are easily enabled for
> COMPILE_TEST, so let's enable them.
>
> The dependency on HW_RANDOM is redundant, so drop that while we're here.
>

#### 进程调度

**[v4: sched/rt: move back to RT_GROUP_SCHED and rename it child](http://lore.kernel.org/lkml/20230803050317.2240948-1-yajun.deng@linux.dev/)**

> The member back in struct sched_rt_entity only related to RT_GROUP_SCHED,
> it should not place out of RT_GROUP_SCHED, move back to RT_GROUP_SCHED
> and rename it child.
>

**[v1: sched: Use lock guards, wave 1](http://lore.kernel.org/lkml/20230801204121.929256934@infradead.org/)**

> This is the first of two series converting kernel/sched/core.c to use the shiny
> new Scope-Based Resource Management machinery that has recently been merged:
>
>   https://lkml.kernel.org/r/20230612093537.614161713%40infradead.org
>

**[v1: sched/rt: Don't try push tasks if there are none.](http://lore.kernel.org/lkml/20230801152648._y603AS_@linutronix.de/)**

> I have a RT task X at a high priority and cyclictest on each CPU with
> lower priority than X's. If X is active and each CPU wakes their own
> cylictest thread then it ends in a longer rto_push storm.
> A random CPU determines via balance_rt() that the CPU on which X is
> running needs to push tasks. X has the highest priority, cyclictest is
> next in line so there is nothing that can be done since the task with
> the higher priority is not touched.
>

**[v3: add to report task state in symbolic chars from sched tracepoint](http://lore.kernel.org/lkml/20230801090124.8050-1-zegao@tencent.com/)**

> This is the 3rd attempt to fix the report task state issue in sched
> tracepint, you can check out previous discussions here:
>

**[v1: sched/topology: Covered all cpus of same node in asymmetric node setups](http://lore.kernel.org/lkml/20230729222203.5601-1-huangbing775@126.com/)**

> in asymmetric node setups, if one cpu'sd spans is empty in some depth,
> others in same node do
>

**[v2: report task state in symbolic chars from sched tracepoint](http://lore.kernel.org/lkml/20230726121618.19198-1-zegao@tencent.com/)**

> This is the 2nd attempt to fix the report task state issue in sched
> tracepint, here is the first version:
>
> https://lore.kernel.org/linux-trace-kernel/20230725072254.32045-1-zegao@tencent.com
>
> Against v1, add a new var to report task state in symbolic char instead
> of replacing the old one and to not to break anything.
>

**[v1: sched/fair: Skip idle CPU search on busy system](http://lore.kernel.org/lkml/20230726093612.1882644-1-sshegde@linux.vnet.ibm.com/)**

> When the system is fully busy, there will not be any idle CPU's.
> In that case, load_balance will be called mainly with CPU_NOT_IDLE
> type. In should_we_balance its currently checking for an idle CPU if
> one exist. When system is 100% busy, there will not be an idle CPU and
> these idle_cpu checks can be skipped. This would avoid fetching those rq
> structures.
>

**[v1: -next: sched/fair: Use struct_size()](http://lore.kernel.org/lkml/20230725195020.3469132-1-ruanjinjie@huawei.com/)**

> Use struct_size() instead of hand-writing it, when allocating a structure
> with a flex array.
>
> This is less verbose.
>

**[v1: report task state in symbolic chars in sched tracepoints](http://lore.kernel.org/lkml/20230725072254.32045-1-zegao@tencent.com/)**

> In the status quo, we should see three different outcomes of the reported
> sched-out task state from perf-script, perf-sched-timehist, and Tp_printk
> of tracepoint sched_switch.  And it's not hard to figure out that the
> former two are built upon the third one, and the reason why we see this
> inconsistency is that the former two does not catch up with the internal
> change of reported task state definitions as the kernel evolves.
>

**[v3: net/sched: mqprio: Add length check for TCA_MQPRIO_{MAX/MIN}_RATE64](http://lore.kernel.org/lkml/20230725024227.426561-1-linma@zju.edu.cn/)**

> The nla_for_each_nested parsing in function mqprio_parse_nlattr() does
> not check the length of the nested attribute. This can lead to an
> out-of-attribute read and allow a malformed nlattr (e.g., length 0) to
> be viewed as 8 byte integer and passed to priv->max_rate/min_rate.
>

#### 内存管理

**[v3: mm/filemap: change ->index to PAGE_SIZE for hugetlb pages](http://lore.kernel.org/linux-mm/20230803174817.320816-1-sidhartha.kumar@oracle.com/)**

> This patchset attempts to implement a listed filemap TODO which is
> changing hugetlb folios to have ->index in PAGE_SIZE. This simplifies many
> functions within filemap.c as they have to special case hugetlb pages.
> New wrappers for hugetlb code are used to interact with the page cache
> using a linear index.
>

**[v3: make vma locking more obvious](http://lore.kernel.org/linux-mm/20230803172652.2849981-1-surenb@google.com/)**

> During recent vma locking patch reviews Linus and Jann Horn noted a number
> of issues with vma locking and suggested improvements:
>
> walk_page_range() does not have ability to write-lock a vma during the
> walk when it's done under mmap_write_lock. For example s390_reset_cmma().
>

**[v1: -next: mm: use helper function put_resv_map()](http://lore.kernel.org/linux-mm/20230803120312.945774-1-ruanjinjie@huawei.com/)**

> This code is already duplicated 3 times, use helper function
> put_resv_map() to release reserved and allocated regions instead of
> open code it to help improve code readability a bit.
>
> No functional change involved.
>

**[v1: mm/mm_init: use helper macro BITS_PER_LONG](http://lore.kernel.org/linux-mm/20230803114051.637709-1-linmiaohe@huawei.com/)**

> It's more readable to use helper macro BITS_PER_LONG. No functional
> change intended.
>

**[v2: -next: mm/z3fold: use helper function put_z3fold_locked() and put_z3fold_locked_list()](http://lore.kernel.org/linux-mm/20230803113824.886413-1-ruanjinjie@huawei.com/)**

> This code is already duplicated six times, use helper function
> put_z3fold_locked() to release z3fold page instead of open code it
> to help improve code readability a bit. And add
> put_z3fold_locked_list() helper function to
> be consistent with it. No functional change involved.
>

**[v5: rcu: Dump memory object info if callback function is invalid](http://lore.kernel.org/linux-mm/20230803101754.1149-1-thunder.leizhen@huaweicloud.com/)**

>    The discussion about vmap_area_lock deadlock in v2:
>    https://lkml.org/lkml/2022/11/11/493
>
> 2. Provide static inline empty functions for kmem_valid_obj() and kmem_dump_obj()
>    when CONFIG_PRINTK=n.
>

**[v1: -next: mm: zswap: use helper function put_z3fold_locked()](http://lore.kernel.org/linux-mm/20230803070820.3775663-1-ruanjinjie@huawei.com/)**

> This code is already duplicated six times, use helper function
> put_z3fold_locked() to release z3fold page instead of open code it
> to help improve code readability a bit. No functional change involved.
>

**[v1: Extedn DAMOS filters for address ranges and DAMON monitoring targets](http://lore.kernel.org/linux-mm/20230802214312.110532-1-sj@kernel.org/)**

> There are use cases that need to apply DAMOS schemes to specific address
> ranges or DAMON monitoring targets.  NUMA nodes in the physical address
> space, special memory objects in the virtual address space, and
> monitoring target specific efficient monitoring results snapshot
> retrieval could be examples of such use cases.  This patchset extends
> DAMOS filters feature for such cases, by implementing two more filter
> types, namely address ranges and DAMON monitoring types.
>

**[v1: mm/mm_init: Ignore kernelcore=mirror boot option when no mirror memory presents.](http://lore.kernel.org/linux-mm/20230802183614.15520-1-ppbuk5246@gmail.com/)**

> In the machine where no mirror memory is set,
> All memory region in ZONE_NORMAL is used as ZONE_MOVABLE
> when kernelcore=mirror boot option is used.
> So, ZONE_NORMAL couldn't be populated properly
> because all of ZONE_NORMAL pages is absent.
>

**[v6: New page table range API](http://lore.kernel.org/linux-mm/20230802151406.3735276-1-willy@infradead.org/)**

> For some reason, I didn't get an email when v5 was dropped from linux-next
> (I got an email from Andrew saying he was going to, but when I didn't get
> the automated emails, I assumed he'd changed his mind).  So here's v6,
> far later than I would have resent it.
>

**[v1: nodemask: Use nr_node_ids](http://lore.kernel.org/linux-mm/20230802112458.230221601@infradead.org/)**

> While working on some NUMA code recently, I stumbled over how nodemask
> operations are unconditionally using MAX_NUMNODES length, which is typically
>
> OTOH typical machines only have <=4 nodes, so doing these crazy long bitmap ops
> is silly.
>

**[v1: mm: migrate: more folio conversion](http://lore.kernel.org/linux-mm/20230802095346.87449-1-wangkefeng.wang@huawei.com/)**

> This patch series converts several functions to use folio in migrate.c.
>

**[v2: mm: disable kernelcore=mirror when no mirror memory](http://lore.kernel.org/linux-mm/20230802072328.2107981-1-mawupeng1@huawei.com/)**

> For system with kernelcore=mirror enabled while no mirrored memory is
> reported by efi. This could lead to kernel OOM during startup since
> all memory beside zone DMA are in the movable zone and this prevents
> the kernel to use it.
>

**[v3: mm: use memmap_on_memory semantics for dax/kmem](http://lore.kernel.org/linux-mm/20230801-vv-kmem_memmap-v3-0-406e9aaf5689@intel.com/)**

> The dax/kmem driver can potentially hot-add large amounts of memory
> originating from CXL memory expanders, or NVDIMMs, or other 'device
> memories'. There is a chance there isn't enough regular system memory
> available to fit the memmap for this new memory. It's therefore
> desirable, if all other conditions are met, for the kmem managed memory
> to place its memmap on the newly added memory itself.
>

**[v4: arm64: hugetlb: enable __HAVE_ARCH_FLUSH_HUGETLB_TLB_RANGE](http://lore.kernel.org/linux-mm/20230802012731.62512-1-wangkefeng.wang@huawei.com/)**

> It is better to use huge page size instead of PAGE_SIZE
> for stride when flush hugepage, which reduces the loop
> in __flush_tlb_range().
>
> Let's support arch's flush_hugetlb_tlb_range(), which is
> used in hugetlb_unshare_all_pmds(), move_hugetlb_page_tables()
> and hugetlb_change_protection() for now.
>

**[v1: vmrd: dynamic guest VM memory resizing daemon](http://lore.kernel.org/linux-mm/cover.1690836010.git.quic_sudaraja@quicinc.com/)**

> VM Memory Resizing Daemon (vmrd)
>
> Initial idea of dynamic memory resizing for guest VMs was discussed here
> in this thread below as a RFC.
> https://lore.kernel.org/linux-arm-kernel/1bf30145-22a5-cc46-e583-25053460b105@redhat.com/T/
>

**[v1: tmpfs: verify {g,u}id mount options correctly](http://lore.kernel.org/linux-mm/20230801-vfs-fs_context-uidgid-v1-1-daf46a050bbf@kernel.org/)**

> A while ago we received the following report:
>
> "The other outstanding issue I noticed comes from the fact that
> fsconfig syscalls may occur in a different userns than that which
> called fsopen. That means that resolving the uid/gid via
> current_user_ns() can save a kuid that isn't mapped in the associated
> namespace when the filesystem is finally mounted. This means that it
> is possible for an unprivileged user to create files owned by any
> group in a tmpfs mount (since we can set the SUID bit on the tmpfs
> directory), or a tmpfs that is owned by any user, including the root
> group/user."
>

**[v2: -mm: arm64: tlbflush: Add some comments for TLB batched flushing](http://lore.kernel.org/linux-mm/20230801124203.62164-1-yangyicong@huawei.com/)**

> Add comments for arch_flush_tlb_batched_pending() and
> arch_tlbbatch_flush() to illustrate why only a DSB is
> needed.
>

**[v1: mm/page_alloc: avoid unneeded alike_pages calculation](http://lore.kernel.org/linux-mm/20230801123723.2225543-1-linmiaohe@huawei.com/)**

> When free_pages is 0, alike_pages is not used. So alike_pages calculation
> can be avoided by checking free_pages early to save cpu cycles. Also fix
> typo 'comparable'. It should be 'compatible' here.
>

**[v7: Add support for memmap on memory feature on ppc64](http://lore.kernel.org/linux-mm/20230801044116.10674-1-aneesh.kumar@linux.ibm.com/)**

> This patch series update memmap on memory feature to fall back to
> memmap allocation outside the memory block if the alignment rules are
> not met. This makes the feature more useful on architectures like
> ppc64 where alignment rules are different with 64K page size.
>

**[v3: 0/4: Add KFENCE support for LoongArch](http://lore.kernel.org/linux-mm/20230801025815.2436293-1-lienze@kylinos.cn/)**

> This patchset adds KFENCE support on LoongArch.
>
> To run the testcases, you will need to enable the following options,
>
> -> Kernel hacking
>    [*] Tracers
>        [*] Support for tracing block IO actions (NEW)
>    -> Kernel Testing and Coverage
>       <*> KUnit - Enable support for unit tests
>
> and then,
>
> -> Kernel hacking
>    -> Memory Debugging
>       [*] KFENCE: low-overhead sampling-based memory safety error detector (NEW)
>           <*> KFENCE integration test suite (NEW)
>
> With these options enabled, KFENCE will be tested during kernel startup.
> And normally, you might get the following feedback,
>

**[v1: fs/proc/kcore: reinstate bounce buffer for KCORE_TEXT regions](http://lore.kernel.org/linux-mm/20230731215021.70911-1-lstoakes@gmail.com/)**

> Some architectures do not populate the entire range categorised by
> KCORE_TEXT, so we must ensure that the kernel address we read from is
> valid.
>
> Unfortunately there is no solution currently available to do so with a
> purely iterator solution so reinstate the bounce buffer in this instance so
> we can use copy_from_kernel_nofault() in order to avoid page faults when
> regions are unmapped.
>

#### 文件系统

**[v2: Supporting same fsid mounting through a compat_ro feature](http://lore.kernel.org/linux-fsdevel/20230803154453.1488248-1-gpiccoli@igalia.com/)**

> on btrfs. V1 is here:
> https://lore.kernel.org/linux-btrfs/20230504170708.787361-1-gpiccoli@igalia.com/
>
> The mechanism used to achieve that in V2 was a mix between the suggestion
> from JohnS (spoofed fsid) and Qu (a single-dev compat_ro flag) - it is
> still based in the metadata_uuid feature, leveraging that infrastructure
> since it prevents lots of corner cases, like sysfs same-fsid crashes.
>

**[v2: fstests: add helper to canonicalize devices used to enable persistent disks](http://lore.kernel.org/linux-fsdevel/20230802191535.1365096-1-mcgrof@kernel.org/)**

> The filesystem configuration file does not allow you to use symlinks to
> real devices given the existing sanity checks verify that the target end
> device matches the source. Device mapper links work but not symlinks for
> real drives do not.
>

**[v2: fs: add FSCONFIG_CMD_CREATE_EXCL](http://lore.kernel.org/linux-fsdevel/20230802-vfs-super-exclusive-v2-0-95dc4e41b870@kernel.org/)**

> This introduces FSCONFIG_CMD_CREATE_EXCL which will allows userspace to
> implement something like mount -t ext4 --exclusive /dev/sda /B which
> fails if a superblock for the requested filesystem does already exist:
>
> Before this patch
>

**[v2: bootconfig: Distinguish bootloader and embedded kernel parameters](http://lore.kernel.org/linux-fsdevel/db98cbbf-2205-40d2-9fa1-f1c135cc151c@paulmck-laptop/)**

> This series provides a /proc interface parallel to /proc/cmdline that
> provides only those kernel boot parameters that were provided by the
> bootloader in a new /proc/cmdline_load.  This is especially important
> when these parameters are presented to the boot loader by automation
> that might gather them from diverse sources, and also when a kexec-based
> reboot process pulls the kernel boot parameters from /proc.  If such a
> reboot process uses /proc/cmdline, the kernel parameters from the image
> are replicated on every reboot, which can be frustrating when the new
> kernel has different embedded kernel boot parameters.
>

**[v2: kernfs: attach uuid for every kernfs and report it in fsid](http://lore.kernel.org/linux-fsdevel/20230731184731.64568-1-ivan@cloudflare.com/)**

> The following two commits added the same thing for tmpfs:
>
> * commit 2b4db79618ad ("tmpfs: generate random sb->s_uuid")
> * commit 59cda49ecf6c ("shmem: allow reporting fanotify events with file handles on tmpfs")
>

**[v3: arm64/gcs: Provide support for GCS in userspace](http://lore.kernel.org/linux-fsdevel/20230731-arm64-gcs-v3-0-cddf9f980d98@kernel.org/)**

> The arm64 Guarded Control Stack (GCS) feature provides support for
> hardware protected stacks of return addresses, intended to provide
> hardening against return oriented programming (ROP) attacks and to make
> it easier to gather call stacks for applications such as profiling.
>

**[v2: sysctl: Add a size argument to register functions in sysctl](http://lore.kernel.org/linux-fsdevel/20230731071728.3493794-1-j.granados@samsung.com/)**

> This is a preparation patch set that will make it easier for us to apply
> subsequent patches that will remove the sentinel element (last empty element)
> in the ctl_table arrays.
>
> In itself, it does not remove any sentinels but it is needed to bring all the
> advantages of the removal to fruition which is to help reduce the overall build
> time size of the kernel and run time memory bloat by about
> 64 bytes per
> sentinel. Without this patch set we would have to put everything into one big
> commit making the review process that much longer and harder for everyone.
>

**[v1: fs: compare truncated timestamps in current_mgtime](http://lore.kernel.org/linux-fsdevel/20230728-mgctime-v1-1-5b0ddc5df08e@kernel.org/)**

> current_mgtime compares the ctime (which has already been truncated) to
> the value from ktime_get_coarse_real_ts64 (which has not). All of the
> existing filesystems that enable mgtime have 1ns granularity, so this is
> not a problem today, but it is more correct to compare truncated
> timestamps instead.
>

**[v4: Support negative dentries on case-insensitive ext4 and f2fs](http://lore.kernel.org/linux-fsdevel/20230727172843.20542-1-krisman@suse.de/)**

> This is the v4 of the negative dentry support on case-insensitive
> directories.  It doesn't have any functional changes from v1. It applies
> Eric's comments to bring the flags check closet together, improve the
> documentation and improve comments in the code.  I also relooked at the
> locks to ensure the inode read lock is indeed enough in the lookup_slow
> path.
>

**[v1: fuse: enable larger read buffers for readdir [v2].](http://lore.kernel.org/linux-fsdevel/20230727081237.18217-1-jaco@uls.co.za/)**

> This patch does not mess with the caching infrastructure like the
> previous one, which we believe caused excessive CPU and broke directory
> listings in some cases.
>
> This version only affects the uncached read, which then during parse adds an
> entry at a time to the cached structures by way of copying, and as such,
> we believe this should be sufficient.
>

#### 网络设备

**[v3: net-next: tcp: Disable header prediction for MD5.](http://lore.kernel.org/netdev/20230803224552.69398-1-kuniyu@amazon.com/)**

> The 1st patch disable header prediction for MD5 flow and the 2nd
> patch updates the stale comment in tcp_parse_options().
>

**[GIT PULL: Networking for v6.5-rc5](http://lore.kernel.org/netdev/20230803204953.2556070-1-kuba@kernel.org/)**

> The following changes since commit 57012c57536f8814dec92e74197ee96c3498d24e:
>
>   Merge tag 'net-6.5-rc4' of git://git.kernel.org/pub/scm/linux/kernel/git/netdev/net (2023-07-27 12:27:37 -0700)
>
> are available in the Git repository at:
>
>   git://git.kernel.org/pub/scm/linux/kernel/git/netdev/net.git net-6.5-rc5
>
> for you to fetch changes up to 0765c5f293357ee43eca72e27c3547f9d99ac355:
>

**[v1: drivers: vxlan: vnifilter: free percpu vni stats on error path](http://lore.kernel.org/netdev/20230803193834.23340-1-pchelkin@ispras.ru/)**

> In case rhashtable_lookup_insert_fast() fails inside vxlan_vni_add(), the
> allocated percpu vni stats are not freed on the error path.
>

**[[PATCH 6.1 5.15] net/mlx5: Free irqs only on shutdown callback](http://lore.kernel.org/netdev/20230803192832.22966-1-hargar@linux.microsoft.com/)**

> commit 9c2d08010963 ("net/mlx5: Free irqs only on shutdown callback")
> backport this v6.4 commit to v6.1 and v5.15
>
> Whenever a shutdown is invoked, free irqs only and keep mlx5_irq
> synthetic wrapper intact in order to avoid use-after-free on
> system shutdown.
>

**[v3: net-next: page_pool: a couple of assorted optimizations](http://lore.kernel.org/netdev/20230803182038.2646541-1-aleksander.lobakin@intel.com/)**

> That initially was a spin-off of the IAVF PP series[0], but has grown
> (and shrunk) since then a bunch. In fact, it consists of three
> semi-independent blocks:
>
> * #1-2: Compile-time optimization. Split page_pool.h into 2 headers to
>   not overbloat the consumers not needing complex inline helpers and
>   then stop including it in skbuff.h at all. The first patch is also
>   prereq for the whole series.
> * #3: Improve cacheline locality for users of the Page Pool frag API.
> * #4-6: Use direct cache recycling more aggressively, when it is safe
>   obviously. In addition, make sure nobody wants to use Page Pool API
>   with disabled interrupts.
>

**[v1: net-next: net: phy: move marking PHY on SFP module into SFP code](http://lore.kernel.org/netdev/E1qRaga-001vKt-8X@rmk-PC.armlinux.org.uk/)**

> Move marking the PHY as being on a SFP module into the SFP code between
> getting the PHY device (and thus initialising the phy_device structure)
> and registering the discovered device.
>
> This means that PHY drivers can use phy_on_sfp() in their match and
> get_features methods.
>

**[v1: net: net/packet: annotate data-races around tp->status](http://lore.kernel.org/netdev/20230803145600.2937518-1-edumazet@google.com/)**

> Another syzbot report [1] is about tp->status lockless reads
> from __packet_get_status()
>
> [1]
> BUG: KCSAN: data-race in __packet_rcv_has_room / __packet_set_status
>

**[[RFC Optimizing veth xsk performance 00/10]](http://lore.kernel.org/netdev/20230803140441.53596-1-huangjie.albert@bytedance.com/)**

> [-- Warning: decoded text below may be mangled, UTF-8 assumed --]
>
> AF_XDP is a kernel bypass technology that can greatly improve performance.
> However, for virtual devices like veth, even with the use of AF_XDP sockets,
> there are still many additional software paths that consume CPU resources.
> This patch series focuses on optimizing the performance of AF_XDP sockets
> for veth virtual devices. Patches 1 to 4 mainly involve preparatory work.
> Patch 5 introduces tx queue and tx napi for packet transmission, while
> patch 9 primarily implements zero-copy, and patch 10 adds support for
> batch sending of IPv4 UDP packets. These optimizations significantly reduce
> the software path and support checksum offload.
>

**[v9: bpf-next: bpf: Force to MPTCP](http://lore.kernel.org/netdev/cover.1691069778.git.geliang.tang@suse.com/)**

> As is described in the "How to use MPTCP?" section in MPTCP wiki [1]:
>
> "Your app should create sockets with IPPROTO_MPTCP as the proto:
> ( socket(AF_INET, SOCK_STREAM, IPPROTO_MPTCP); ). Legacy apps can be
> forced to create and use MPTCP sockets instead of TCP ones via the
> mptcpize command bundled with the mptcpd daemon."
>

**[v1: net-next: net/smc: serveral features's implementation for smc v2.1](http://lore.kernel.org/netdev/20230803132422.6280-1-guangguan.wang@linux.alibaba.com/)**

> This patch set implement serveral new features in SMC v2.1(https://
> www.ibm.com/support/pages/node/7009315), including vendor unique
> experimental options, max connections per lgr negotiation, max links
> per lgr negotiation.
>

**[v1: net-next: net: renesas: rswitch: Add speed change support](http://lore.kernel.org/netdev/20230803120621.1471440-1-yoshihiro.shimoda.uh@renesas.com/)**

> Add speed change support at runtime for the latest SoC version.
> Also, add ethtool .[gs]et_link_ksettings.
>

**[v1: net-next: sfc: basic conntrack offload](http://lore.kernel.org/netdev/cover.1691063675.git.ecree.xilinx@gmail.com/)**

> Support offloading tracked connections and matching against them in
>  TC chains on the PF and on representors.
> Later patch serieses will add NAT and conntrack-on-tunnel-netdevs;
>  keep it simple for now.
>

**[v3: stmmac: Add Loongson platform support](http://lore.kernel.org/netdev/cover.1691047285.git.chenfeiyang@loongson.cn/)**

> Extend stmmac functions and macros for Loongson DWMAC.
> Add LS7A support for dwmac_loongson.
>

**[v3: net-next: devlink: use spec to generate split ops](http://lore.kernel.org/netdev/20230803111340.1074067-1-jiri@resnulli.us/)**

> This is an outcome of the discussion in the following thread:
> https://lore.kernel.org/netdev/20230720121829.566974-1-jiri@resnulli.us/
> It serves as a dependency on the linked selector patchset.
>

**[v1: Introduce IEP driver and packet timestamping support](http://lore.kernel.org/netdev/20230803110153.3309577-1-danishanwar@ti.com/)**

> This series introduces Industrial Ethernet Peripheral (IEP) driver to
> support timestamping of ethernet packets and thus support PTP and PPS
> for PRU ICSSG ethernet ports.
>
> This series also adds 10M full duplex support for ICSSG ethernet driver.
>

**[v1: net/tls: avoid TCP window full during ->read_sock()](http://lore.kernel.org/netdev/20230803100809.29864-1-hare@suse.de/)**

> When flushing the backlog after decoding each record in ->read_sock()
> we may end up with really long records, causing a TCP window full as
> the TCP window would only be increased again after we process the
> record. So we should rather process the record first to allow the
> TCP window to be increased again before flushing the backlog.
>

**[v3: bluetooth: qca: enable WCN7850 support](http://lore.kernel.org/netdev/20230803-topic-sm8550-upstream-bt-v3-0-6874a1507288@linaro.org/)**

> This serie enables WCN7850 on the Qualcomm SM8550 QRD
> reference platform.
>
> The WCN7850 is close to the WCN6855 but uses different
> firmware names.
>

**[v1: -next: net: lan966x: Do not check 0 for platform_get_irq_byname()](http://lore.kernel.org/netdev/20230803082900.14921-1-wangzhu9@huawei.com/)**

> Since platform_get_irq_byname() never returned zero, so it need not to
> check whether it returned zero, it returned -EINVAL or -ENXIO when
> failed, so we replace the return error code with the result it returned.
>

**[v1: net/ethernet/realtek: Add Realtek automotive PCIe driver](http://lore.kernel.org/netdev/20230803082513.6523-1-justinlai0215@realtek.com/)**

> This patch is to add the ethernet device driver for the PCIe interface of Realtek Automotive Ethernet Switch,
> applicable to RTL9054, RTL9068, RTL9072, RTL9075, RTL9068, RTL9071.
>

**[v1: net-next: pull-request: can-next 2023-08-03](http://lore.kernel.org/netdev/20230803080830.1386442-1-mkl@pengutronix.de/)**

> this is a pull request of 9 patches for net-next/master.
>
> The 1st patch is by Ruan Jinjie, targets the flexcan driver, and
> cleans up the error handling of platform_get_irq() in the
> flexcan_probe() function.
>
> Markus Schneider-Pargmann contributes 6 patches for the tcan4x5x M_CAN
> driver, consisting of some cleanups, and adding support for the
> tcan4552/4553 chips.
>

**[v1: net-next: tcp/dccp: cache line align inet_hashinfo](http://lore.kernel.org/netdev/20230803075334.2321561-1-edumazet@google.com/)**

> I have seen tcp_hashinfo starting at a non optimal location,
> forcing input handlers to pull two cache lines instead of one,
> and sharing a cache line that was dirtied more than necessary:
>
> ffffffff83680600 b tcp_orphan_timer
> ffffffff83680628 b tcp_orphan_cache
> ffffffff8368062c b tcp_enable_tx_delay.__tcp_tx_delay_enabled
> ffffffff83680630 B tcp_hashinfo
> ffffffff83680680 b tcp_cong_list_lock
>
> After this patch, ehash, ehash_locks, ehash_mask and ehash_locks_mask
> are located in a read-only cache line.
>

**[v2: net: tcp: Enable header prediction for active open connections with MD5.](http://lore.kernel.org/netdev/20230803042214.38309-1-kuniyu@amazon.com/)**

> TCP socket saves the minimum required header length in tcp_header_len
> of struct tcp_sock, and later the value is used in __tcp_fast_path_on()
> to generate a part of TCP header in tcp_sock(sk)->pred_flags.
>
> In tcp_rcv_established(), if the incoming packet has the same pattern
> with pred_flags, we enter the fast path and skip full option parsing.
>
> The MD5 option is parsed in tcp_v[46]_rcv(), so we need not parse it
> again later in tcp_rcv_established() unless other options exist.  Thus,
> MD5 should add TCPOLEN_MD5SIG_ALIGNED to tcp_header_len and avoid the
> slow path.
>

**[v2: bpf-next: net: struct netdev_rx_queue and xdp.h reshuffling](http://lore.kernel.org/netdev/20230803010230.1755386-1-kuba@kernel.org/)**

> While poking at struct netdev_rx_queue I got annoyed by
> the huge rebuild times. I split it out from netdevice.h
> and then realized that it was the main reason we included
> xdp.h in there. So I removed that dependency as well.
>
> This gives us very pleasant build times for both xdp.h
> and struct netdev_rx_queue changes.
>

**[v1: net-next: Add QPL mode for DQO descriptor format](http://lore.kernel.org/netdev/20230802213338.2391025-1-rushilg@google.com/)**

> GVE supports QPL ("queue-page-list") mode where
> all data is communicated through a set of pre-registered
> pages. Adding this mode to DQO.
>

**[v2: ARM: Add GXP UMAC Support](http://lore.kernel.org/netdev/20230802201824.3683-1-nick.hawkins@hpe.com/)**

> The GXP contains two Ethernet MACs that can be
> connected externally to several physical devices. From an external
> interface perspective the BMC provides two SERDES interface connections
> capable of either SGMII or 1000Base-X operation. The BMC also provides
> a RMII interface for sideband connections to external Ethernet controllers.
>

**[v1: V5,net-next: net: mana: Add page pool for RX buffers](http://lore.kernel.org/netdev/1690999650-9557-1-git-send-email-haiyangz@microsoft.com/)**

> Add page pool for RX buffers for faster buffer cycle and reduce CPU
> usage.
>
> The standard page pool API is used.
>

#### 安全增强

**[v1: next: RDMA/irdma: Replace one-element array with flexible-array member](http://lore.kernel.org/linux-hardening/ZMpsQrZadBaJGkt4@work/)**

> One-element and zero-length arrays are deprecated. So, replace
> one-element array in struct irdma_qvlist_info with flexible-array
> member.
>
> A patch for this was sent a while ago[1]. However, it seems that, at
> the time, the changes were partially folded[2][3], and the actual
> flexible-array transformation was omitted. This patch fixes that.
>

**[v1: alpha: Replace one-element array with flexible-array member](http://lore.kernel.org/linux-hardening/ZMpZZBShlLqyD3ax@work/)**

> One-element and zero-length arrays are deprecated. So, replace
> one-element array in struct osf_dirent with flexible-array
> member.
>
> This results in no differences in binary output.
>

**[v1: next: i40e: Replace one-element array with flex-array member in struct i40e_profile_aq_section](http://lore.kernel.org/linux-hardening/8b945fa3afeb26b954c400c5b880c0ae175091ac.1690938732.git.gustavoars@kernel.org/)**

> One-element and zero-length arrays are deprecated. So, replace
> one-element array in struct i40e_profile_aq_section with
> flexible-array member.
>
> This results in no differences in binary output.
>

**[v1: next: i40e: Replace one-element array with flex-array member in struct i40e_section_table](http://lore.kernel.org/linux-hardening/ddc1cde5fe6cb6a0865ae96d0d064298e343720d.1690938732.git.gustavoars@kernel.org/)**

> One-element and zero-length arrays are deprecated. So, replace
> one-element array in struct i40e_section_table with flexible-array
> member.
>
> This results in no differences in binary output.
>

**[v1: next: i40e: Replace one-element array with flex-array member in struct i40e_profile_segment](http://lore.kernel.org/linux-hardening/52da391229a45fe3dbd5c43167cdb0701a17a361.1690938732.git.gustavoars@kernel.org/)**

> One-element and zero-length arrays are deprecated. So, replace
> one-element array in struct i40e_profile_segment with flexible-array
> member.
>
> This results in no differences in binary output.
>

**[v1: next: i40e: Replace one-element array with flex-array member in struct i40e_package_header](http://lore.kernel.org/linux-hardening/768db2c3764a490118f6850d24f6e49998494b6c.1690938732.git.gustavoars@kernel.org/)**

> One-element and zero-length arrays are deprecated. So, replace
> one-element array in struct i40e_package_header with flexible-array
> member.
>
> The `+ sizeof(u32)` adjustments ensure that there are no differences
> in binary output.
>

**[v1: next: i40e: Replace one-element arrays with flexible-array members](http://lore.kernel.org/linux-hardening/cover.1690938732.git.gustavoars@kernel.org/)**

> Replace one-element arrays with flexible-array members in multiple
> structures.
>
> This results in no differences in binary output.
>

**[v2: wifi: ipw2x00: refactor to use kstrtoul](http://lore.kernel.org/linux-hardening/20230802-wifi-ipw2x00-refactor-v2-1-d33f765e9cd5@google.com/)**

> The current implementation seems to reinvent what `kstrtoul` already does
> in terms of functionality and error handling. Remove uses of `simple_strtoul()`
> in favor of `kstrtoul()`.
>
> There is the following note at `lib/vsprintf.c:simple_strtoull()` which
> further backs this change:
> | * This function has caveats. Please use kstrtoull (or kstrtoul) instead.
>

**[v1: ocfs2: Use regular seq_show_option for osb_cluster_stack](http://lore.kernel.org/linux-hardening/20230726215919.never.127-kees@kernel.org/)**

> While cleaning up seq_show_option_n()'s use of strncpy, it was noticed
> that the osb_cluster_stack member is always NUL-terminated, so there is
> no need to use the special seq_show_option_n() routine. Replace it with
> the standard seq_show_option() routine.
>

**[v3: kunit: Add test attributes API](http://lore.kernel.org/linux-hardening/20230725212522.1622716-1-rmoar@google.com/)**

> This patch series adds a test attributes framework to KUnit.
>
> There has been interest in filtering out "slow" KUnit tests. Most notably,
> a new config, CONFIG_MEMCPY_SLOW_KUNIT_TEST, has been added to exclude a
> particularly slow memcpy test
> (https://lore.kernel.org/all/20230118200653.give.574-kees@kernel.org/).
>

**[v1: fortify: strnlen: Call fortify_panic() only if the number of bytes read is greater than maxlen](http://lore.kernel.org/linux-hardening/20230724224857.2049906-1-sel4@tilde.club/)**

> If the number of bytes read is p_size and p_size is less than maxlen,
> fortify_panic() will be called incorrectly. Only panic if the number of
> bytes read is greater than the minimum of p_size and maxlen since that is
>  the argument to __real_strnlen().
>

**[v1: arm64: dts: pinephone: Add pstore support for PinePhone A64](http://lore.kernel.org/linux-hardening/20230724213457.24593-1-andrej.skvortzov@gmail.com/)**

> This patch reserves some memory in the DTS and sets up a
> pstore device tree node to enable pstore support.
>
> Gbp-Pq: Topic pinephone
> Gbp-Pq: Name 0161-arm64-dts-pinephone-Add-pstore-support-for-PinePhone.patch
>

**[v1: selinux: Use NULL for pointers](http://lore.kernel.org/linux-hardening/20230721033236.42689-1-jiapeng.chong@linux.alibaba.com/)**

> Replace integer constants with NULL.
>
> security/selinux/hooks.c:251:41: warning: Using plain integer as NULL pointer.
>
> Closes: https://bugzilla.openanolis.cn/show_bug.cgi?id=5958
>

#### 异步 IO

**[v1: io_uring: annotate the struct io_kiocb slab for appropriate user copy](http://lore.kernel.org/io-uring/db807b6b-76ca-4101-844a-aa6da1467b98@kernel.dk/)**

> When compiling the kernel with clang and having HARDENED_USERCOPY
> enabled, the liburing openat2.t test case fails during request setup:
>
> usercopy: Kernel memory overwrite attempt detected to SLUB object 'io_kiocb' (offset 24, size 24)!
>

**[v1: io_uring: gate iowait schedule on having pending requests](http://lore.kernel.org/io-uring/96d20c01-86fb-4c42-514b-7c38b95060a0@kernel.dk/)**

> A previous commit made all cqring waits marked as iowait, as a way to
> improve performance for short schedules with pending IO. However, for
> use cases that have a special reaper thread that does nothing but
> wait on events on the ring, this causes a cosmetic issue where we
> know have one core marked as being "busy" with 100% iowait.
>

**[GIT PULL: Improve iomap async dio performance](http://lore.kernel.org/io-uring/647e79f4-ddaa-7003-6e00-f31e11535082@kernel.dk/)**

> Here's the pull request for improving async dio performance with
> iomap. Contains a few generic cleanups as well, but the meat of it
> is described in the tagged commit message below.
>

#### Rust For Linux

**[v1: Generate API documentation for 'bindings' crate](http://lore.kernel.org/rust-for-linux/20230803093418.51872-1-tmgross@umich.edu/)**

> The 'bindings' crate currently doesn't have API documentation available.
> With this change, it will be generated as part of the output of 'make
> rustdoc' (similar to the 'kernel' crate's docs,
> https://rust-for-linux.github.io/docs/kernel/).
>

**[v3: Quality of life improvements for pin-init](http://lore.kernel.org/rust-for-linux/20230729090838.225225-1-benno.lossin@proton.me/)**

> This patch series adds several improvements to the pin-init api:
> - a derive macro for the `Zeroable` trait,
> - makes hygiene of fields in initializers behave like normal struct
>   initializers would behave,
> - prevent stackoverflow without optimizations
> - add `..Zeroable::zeroed()` syntax to zero missing fields,
> - support arbitrary paths in initializer macros.
>

**[v2: Rust PuzleFS filesystem driver](http://lore.kernel.org/rust-for-linux/20230726164535.230515-1-amiculas@cisco.com/)**

> This is a proof of concept driver written for the PuzzleFS
> next-generation container filesystem [1]. This patch series is based on
> top of the puzzlefs_dependencies [4] branch, which contain the
> backported filesystem abstractions of Wedson Almeida Filho [2][3] and
> the additional third-party crates capnp [5] and hex [6]. The code is
> also available on github [11]. I've adapted the user space puzzlefs code
> [1] so that the puzzlefs kernel module could present the directory
> hierarchy and implement the basic read functionality.  For some
> additional context, puzzlefs was started by Tycho Andersen and it's the
> successor of atomfs. This FOSDEM presentation from 2019 [10] covers the
> rationale for a new oci image format and presents a higher level
> overview of our goals with puzzlefs.
>

**[v1: rework top page and organize toc on the sidebar](http://lore.kernel.org/rust-for-linux/20230724193118.2204673-1-costa.shul@redhat.com/)**

> [-- Warning: decoded text below may be mangled, UTF-8 assumed --]
>
> Template {{ toctree(maxdepth=3) }} in
> Documentation/sphinx/templates/kernel-toc.html
> uses directives toctree and doesn't use sections on the top page
> Documentation/index.rst
> to generate expandable toc on the sidebar.
>
> BTW, other template {{ toc }} uses only sections, and doesn't
> use directives toctree.
>

**[v2: kbuild: rust: avoid creating temporary files](http://lore.kernel.org/rust-for-linux/20230723142128.194339-1-ojeda@kernel.org/)**

> `rustc` outputs by default the temporary files (i.e. the ones saved
> by `-Csave-temps`, such as `*.rcgu*` files) in the current working
> directory when `-o` and `--out-dir` are not given (even if
> `--emit=x=path` is given, i.e. it does not use those for temporaries).
>

#### BPF

**[[RFC Optimizing veth xsk performance 00/10]](http://lore.kernel.org/bpf/20230803140441.53596-1-huangjie.albert@bytedance.com/)**

> [-- Warning: decoded text below may be mangled, UTF-8 assumed --]
>
> AF_XDP is a kernel bypass technology that can greatly improve performance.
> However, for virtual devices like veth, even with the use of AF_XDP sockets,
> there are still many additional software paths that consume CPU resources.
> This patch series focuses on optimizing the performance of AF_XDP sockets
> for veth virtual devices. Patches 1 to 4 mainly involve preparatory work.
> Patch 5 introduces tx queue and tx napi for packet transmission, while
> patch 9 primarily implements zero-copy, and patch 10 adds support for
> batch sending of IPv4 UDP packets. These optimizations significantly reduce
> the software path and support checksum offload.
>

**[v1: -next: bpf: change bpf_alu_sign_string and bpf_movsx_string to static](http://lore.kernel.org/bpf/20230803023128.3753323-1-yangyingliang@huawei.com/)**

> The bpf_alu_sign_string and bpf_movsx_string introduced in commit
> f835bb622299 ("bpf: Add kernel/bpftool asm support for new instructions")
> are only used in disasm.c now, change them to static.
>

**[v3: bpf-next: libbpf: Use local includes inside the library](http://lore.kernel.org/bpf/CAJVhQqUg6OKq6CpVJP5ng04Dg+z=igevPpmuxTqhsR3dKvd9+Q@mail.gmail.com/)**

> In our monrepo, we try to minimize special processing when importing
> (aka vendor) third-party source code. Ideally, we try to import
> directly from the repositories with the code without changing it, we
> try to stick to the source code dependency instead of the artifact
> dependency. In the current situation, a patch has to be made for
> libbpf to fix the includes in bpf headers so that they work directly
> from libbpf/src.
>

**[v2: net: handle ARPHRD_PPP in dev_is_mac_header_xmit()](http://lore.kernel.org/bpf/20230802122106.3025277-1-nicolas.dichtel@6wind.com/)**

> This kind of interface doesn't have a mac header. This patch fixes
> bpf_redirect() to a ppp interface.
>

**[v4: net-next: net: fec: add XDP_TX feature support](http://lore.kernel.org/bpf/20230802024857.3153756-1-wei.fang@nxp.com/)**

> The XDP_TX feature is not supported before, and all the frames
> which are deemed to do XDP_TX action actually do the XDP_DROP
> action. So this patch adds the XDP_TX support to FEC driver.
>
> I tested the performance of XDP_TX feature in XDP_DRV and XDP_SKB
> modes on i.MX8MM-EVK and i.MX8MP-EVK platforms respectively, and
> the test steps and results are as follows.
>

**[v1: bpf-next: BPF Refcount followups 3: bpf_mem_free_rcu refcounted nodes](http://lore.kernel.org/bpf/20230801203630.3581291-1-davemarchevsky@fb.com/)**

> This series is the third of three (or more) followups to address issues
> in the bpf_refcount shared ownership implementation discovered by Kumar.
> This series addresses the use-after-free scenario described in [0]. The
> first followup series ([1]) also attempted to address the same
> use-after-free, but only got rid of the splat without addressing the
> underlying issue. After this series the underyling issue is fixed and
> bpf_refcount_acquire can be re-enabled.
>

**[v1: bpf-next: bpf: Add new bpf helper bpf_for_each_cpu](http://lore.kernel.org/bpf/20230801142912.55078-1-laoar.shao@gmail.com/)**

> Some statistic data is stored in percpu pointer but the kernel doesn't
> aggregate it into a single value, for example, the data in struct
> psi_group_cpu.
>
> Currently, we can traverse percpu data using for_loop and bpf_per_cpu_ptr:
>
>   for_loop(nr_cpus, callback_fn, callback_ctx, 0)
>

**[v3: bpf-next: selftests/bpf: replace fall through comment by fallthrough pseudo-keyword](http://lore.kernel.org/bpf/20230801094833.4146816-1-ruanjinjie@huawei.com/)**

> Replace the existing /* fall through */ comments with the
> new pseudo-keyword macro fallthrough[1].
>
> [1] https://www.kernel.org/doc/html/v5.7/process/deprecated.html?highlight=fallthrough#implicit-switch-case-fall-through
>

**[v3: bpf-next: tracing: perf_call_bpf: use struct trace_entry in struct syscall_tp_t](http://lore.kernel.org/bpf/20230801075222.7717-1-ykaliuta@redhat.com/)**

> bpf tracepoint program uses struct trace_event_raw_sys_enter as
> argument where trace_entry is the first field. Use the same instead
> of unsigned long long since if it's amended (for example by RT
> patch) it accesses data with wrong offset.
>

**[v1: bpf-next: bpf: Support bpf_get_func_ip helper in uprobes](http://lore.kernel.org/bpf/20230801073002.1006443-1-jolsa@kernel.org/)**

> adding support for bpf_get_func_ip helper for uprobe program to return
> probed address for both uprobe and return uprobe as suggested by Andrii
> in [1].
>
> We agreed that uprobe can have special use of bpf_get_func_ip helper
> that differs from kprobe.
>
> The kprobe bpf_get_func_ip returns:
>   - address of the function if probe is attach on function entry
>     for both kprobe and return kprobe
>   - 0 if the probe is not attach on function entry
>

**[v2: -next: selftests/bpf: replace fall through comment by fallthrough pseudo-keyword](http://lore.kernel.org/bpf/20230801065447.3609130-1-ruanjinjie@huawei.com/)**

> Replace the existing /* fall through */ comments with the
> new pseudo-keyword macro fallthrough[1].
>
> [1] https://www.kernel.org/doc/html/v5.7/process/deprecated.html?highlight=fallthrough#implicit-switch-case-fall-through
>

**[v1: -next: selftests/bpf: Use fallthrough pseudo-keyword](http://lore.kernel.org/bpf/20230801034624.3412175-1-ruanjinjie@huawei.com/)**

> Replace the existing /* fall through */ comments with the
> new pseudo-keyword macro fallthrough[1]. Also, remove unnecessary
> fall-through markings when it is the case.
>
> [1] https://www.kernel.org/doc/html/v5.7/process/deprecated.html?highlight=fallthrough#implicit-switch-case-fall-through
>

**[v1: netfilter: bpf: Only define get_proto_defrag_hook() if necessary](http://lore.kernel.org/bpf/b128b6489f0066db32c4772ae4aaee1480495929.1690840454.git.dxu@dxuuu.xyz/)**

> Before, we were getting this warning:
>
>   net/netfilter/nf_bpf_link.c:32:1: warning: 'get_proto_defrag_hook' defined but not used [-Wunused-function]
>
> Guard the definition with CONFIG_NF_DEFRAG_IPV[4|6].
>

**[v4: tracing: Improbe BTF support on probe events](http://lore.kernel.org/bpf/169078860386.173706.3091034523220945605.stgit@devnote2/)**

> Here is the 4th version of series to improve the BTF support on probe events.
> The previous series is here:
>
> https://lore.kernel.org/all/169037639315.607919.2613476171148037242.stgit@devnote2/
>
> This version updates the btf_find_struct_member() to use a simple stack
> to walk through the anonymous unions/structures instead of recursive call.
> Also, returning int error code from query_btf_context()
> if !CONFIG_PROBE_EVENTS_BTF_ARGS.
>

**[v5: net-next: virtio_net: add per queue interrupt coalescing support](http://lore.kernel.org/bpf/20230731070656.96411-1-gavinl@nvidia.com/)**

> Currently, coalescing parameters are grouped for all transmit and receive
> virtqueues. This patch series add support to set or get the parameters for
> a specified virtqueue.
>
> When the traffic between virtqueues is unbalanced, for example, one virtqueue
> is busy and another virtqueue is idle, then it will be very useful to
> control coalescing parameters at the virtqueue granularity.
>

**[v4: bpf-next: bpf, xdp: Add tracepoint to xdp attaching failure](http://lore.kernel.org/bpf/20230730114951.74067-1-hffilwlqm@gmail.com/)**

> This series introduces a new tracepoint in bpf_xdp_link_attach(). By
> this tracepoint, error message will be captured when error happens in
> dev_xdp_attach(), e.g. invalid attaching flags.
>

**[[Bpf] v1: Formalize type notation and function semantics in ISA standard](http://lore.kernel.org/bpf/20230730035155.lK99lY19F4QNcIX84WsRdU0Ztd3AefeMNeqeqBkxUHs@z/)**

> Based on a conversation with Alexei, here is an attempt at condensing
> all the definitions of helper functions and type shorthands in a single
> place.
>
> I hope that this is helpful!
> Will
>
> Bpf@ietf.org
>

**[v1: net-next: bnxt_en: Add support for page pool](http://lore.kernel.org/bpf/20230728231829.235716-1-michael.chan@broadcom.com/)**

> The first patch fixes the page pool implementation for XDP with page size
> >= 64K.  The next 2 patches unifies the page pool implementation between
> XDP and the normal RX path.
>
> [-- Attachment #2: S/MIME Cryptographic Signature --]
> [-- Type: application/pkcs7-signature, Size: 4209 bytes --]
>

**[v1: V4,net-next: net: mana: Add page pool for RX buffers](http://lore.kernel.org/bpf/1690580767-18937-1-git-send-email-haiyangz@microsoft.com/)**

> Add page pool for RX buffers for faster buffer cycle and reduce CPU
> usage.
>
> The standard page pool API is used.
>

**[v5: bpf-next: libbpf: Expose API to consume one ring at a time](http://lore.kernel.org/bpf/20230728093346.673994-1-adam@wowsignal.io/)**

> We already provide ring_buffer__epoll_fd to enable use of external
> polling systems. However, the only API available to consume the ring
> buffer is ring_buffer__consume, which always checks all rings. When
> polling for many events, this can be wasteful.
>

**[v1: Simplify C/C++ compiler flags](http://lore.kernel.org/bpf/20230728064917.767761-1-irogers@google.com/)**

> Some compiler flags have been brought forward in the perf build but
> without any explicit need, for example -ggdb3. Some warnings were
> disabled but the underlying warning could be addressed. Try to reduce
> the number of compiler options used in the perf build, to enable
> Wextra for C++, and to disable fewer compiler warnings.
>

### 周边技术动态

#### Qemu

**[v1: target/riscv: Implement WARL behaviour for mcountinhibit/mcounteren](http://lore.kernel.org/qemu-devel/20230802124906.24197-1-rbradford@rivosinc.com/)**

> These are WARL fields - zero out the bits for unavailable counters and
> special case the TM bit in mcountinhibit which is hardwired to zero.
> This patch achieves this by modifying the value written so that any use
> of the field will see the correctly masked bits.
>

**[v1: disas/riscv: Further correction to LUI disassembly](http://lore.kernel.org/qemu-devel/20230731183320.410922-1-rbagley@ventanamicro.com/)**

> The recent commit 36df75a0a9 corrected one aspect of LUI disassembly
> by recovering the immediate argument from the result of LUI with a
> shift right by 12. However, the shift right will left-fill with the
> sign. By applying a mask we recover an unsigned representation of the
> 20-bit field (which includes a sign bit).
>

**[v1: linux-user/elfload: Set V in ELF_HWCAP for RISC-V](http://lore.kernel.org/qemu-devel/c8b63aa1-8929-e46f-3a26-a5d9944c473c@xiph.org/)**

> One line patch that fixes the issue reported in:
>
> https://gitlab.com/qemu-project/qemu/-/issues/1793
>
> [-- Attachment #2: 0001-linux-user-elfload-Set-V-in-ELF_HWCAP-for-RISC-V.patch --]
> [-- Type: text/x-patch, Size: 988 bytes --]
>
> From 7122a450d745325ce250785e58c543481054bec6 Mon Sep 17 00:00:00 2001
> Date: Mon, 31 Jul 2023 03:45:13 -0400
>

**[v1: target/riscv: Use accelerated helper for AES64KS1I](http://lore.kernel.org/qemu-devel/20230731093902.1796249-1-ardb@kernel.org/)**

> Use the accelerated SubBytes/ShiftRows/AddRoundKey AES helper to
> implement the first half of the key schedule derivation. This does not
> actually involve shifting rows, so clone the same uint32_t 4 times into
> the AES vector to counter that.
>

**[v2: target/riscv: Use existing lookup tables for MixColumns](http://lore.kernel.org/qemu-devel/20230731084043.1791984-1-ardb@kernel.org/)**

> The AES MixColumns and InvMixColumns operations are relatively
> expensive 4x4 matrix multiplications in GF(2^8), which is why C
> implementations usually rely on precomputed lookup tables rather than
> performing the calculations on demand.
>

**[v1: hw/riscv: split RAM into low and high memory](http://lore.kernel.org/qemu-devel/20230731015317.1026996-1-fei2.wu@intel.com/)**

> riscv virt platform's memory started at 0x80000000 and
> straddled the 4GiB boundary. Curiously enough, this choice
> of a memory layout will prevent from launching a VM with
> a bit more than 2000MiB and PCIe pass-thru on an x86 host, due
> to identity mapping requirements for the MSI doorbell on x86,
> and these (APIC/IOAPIC) live right below 4GiB.
>

**[v1: riscv: detecting user choice in TCG extensions](http://lore.kernel.org/qemu-devel/20230728131520.110394-1-dbarboza@ventanamicro.com/)**

> This series, based on the work done in "v6: for-8.2: riscv:
> add 'max' CPU, deprecate 'any'", aims to solve two problem we have in
> TCG properties handling:
>
> - we are not checking for priv_ver when auto-enabling extensions during
>   realize();
>
> - we are not able to honor user choice during validation because we're
>   not able to tell if the user set an extension flag or not.
>

**[v6: riscv: add 'max' CPU, deprecate 'any'](http://lore.kernel.org/qemu-devel/20230727220927.62950-1-dbarboza@ventanamicro.com/)**

> I decided to spin a new version of this work after chatting with Conor
> about some log messages about 'vector version is not specified' message
> when using the 'max' CPU.
>

**[v7: Add RISC-V KVM AIA Support](http://lore.kernel.org/qemu-devel/20230727102439.22554-1-yongxuan.wang@sifive.com/)**

> This series adds support for KVM AIA in RISC-V architecture.
>
> In order to test these patches, we require Linux with KVM AIA support which can
> be found in the riscv_kvm_aia_hwaccel_v1 branch at
> https://github.com/avpatel/linux.git
>

**[v1: target/riscv: Use existing lookup tables for AES MixColumns](http://lore.kernel.org/qemu-devel/20230727070303.1220037-1-ardb@kernel.org/)**

> The AES MixColumns and InvMixColumns operations are relatively
> expensive 4x4 matrix multiplications in GF(2^8), which is why C
> implementations usually rely on precomputed lookup tables rather than
> performing the calculations on demand.
>

**[v1: hw/riscv: hart: allow other cpu instance](http://lore.kernel.org/qemu-devel/20230727080545.7908-1-nikita.shubin@maquefel.me/)**

> Allow using instances derivative from RISCVCPU
>

**[v2: target/riscv: don't read write-only CSR](http://lore.kernel.org/qemu-devel/20230727081726.12650-1-nikita.shubin@maquefel.me/)**

> In case of write-only CSR don't return illegal inst error when CSR is
> written and lacks read op.
>

**[v3: target/riscv: Add Zihintntl extension ISA string to DTS](http://lore.kernel.org/qemu-devel/20230726074049.19505-1-jason.chien@sifive.com/)**

> In v2, I rebased the patch on
> https://github.com/alistair23/qemu/tree/riscv-to-apply.next
> However, I forgot to add "Reviewed-by" in v2, so I add them in v3.
>

**[v1: risc-v vector (RVV) emulation performance issues](http://lore.kernel.org/qemu-devel/0e54c6c1-2903-7942-eff2-2b8c5e21187e@ventanamicro.com/)**

> As some of you are already aware the current RVV emulation could be faster.
> We have at least one commit (bc0ec52eb2, "target/riscv/vector_helper.c:
> skip set tail when vta is zero") that tried to address at least part of the
> problem.
>

**[v3: target/riscv: Clearing the CSR values at reset and syncing the MPSTATE with the host](http://lore.kernel.org/qemu-devel/20230724062534.5634-1-18622748025@163.com/)**

> Fix the guest reboot error when using KVM
> There are two issues when rebooting a guest using KVM
> 1. When the guest initiates a reboot the host is unable to stop the vcpu
> 2. When running a SMP guest the qemu monitor system_reset causes a vcpu crash
>

**[v1: riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20230723093414.859532-1-alistair.francis@wdc.com/)**

> The following changes since commit d1181d29370a4318a9f11ea92065bea6bb159f83:
>
>   Merge tag 'pull-nbd-2023-07-19' of https://repo.or.cz/qemu/ericb into staging (2023-07-20 09:54:07 +0100)
>

**[v1: target/riscv/cpu.c: do not run 'host' CPU with TCG](http://lore.kernel.org/qemu-devel/20230721133411.474105-1-dbarboza@ventanamicro.com/)**

> The 'host' CPU is available in a CONFIG_KVM build and it's currently
> available for all accels, but is a KVM only CPU. This means that in a
> RISC-V KVM capable host we can do things like this:
>
> $ ./build/qemu-system-riscv64 -M virt,accel=tcg -cpu host --nographic
> qemu-system-riscv64: H extension requires priv spec 1.12.0
>

#### Buildroot

**[package/acpica: bump to version 20230628](http://lore.kernel.org/buildroot/20230727103601.F2B79836EB@busybox.osuosl.org/)**

> commit: https://git.buildroot.net/buildroot/commit/?id=0403bdd9297ba4d58f515f1a9d5bd80ac7789b32
> branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/master
>
> For change log since 20220331, see:
> - https://github.com/acpica/acpica/blob/R06_28_23/documents/changes.txt
>
> This commit also drop the patch removing -Werror in CFLAGS, as an
> alternative is now available upstream, merged in [1]. This commit is
> included in release 20221020. The build commands are updated
> accordingly to set the new NOWERROR=TRUE option to achieve the same
> behavior.
>

**[arch/riscv: enable RISC-V Toolchain with Vector Extension](http://lore.kernel.org/buildroot/20230723101018.1BB648263F@busybox.osuosl.org/)**

> commit: https://git.buildroot.net/buildroot/commit/?id=4d70454754142a6334ec4a7e8cc6582bbf2f4b9d
> branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/master
>
> This commits adds support for building a RISC-V toolchain with the
> vector extension, available since gcc 12.
>

#### U-Boot

**[v1: Add VF2 USB host support.](http://lore.kernel.org/u-boot/20230803032210.77371-1-minda.chen@starfivetech.com/)**

> StarFive VF2 VTI usb-host controller connect to pcie0 RC.
> Enable pcie0 first the enable USB function.
>
> patch1 is Get the correct ECAM offset in multiple PCIe RC.
> patch2 is enable pcie0 dts node.
> patch3 is enable SYS_CACHE_LINE_SIZE
> patch4 is Add VF2 USB related configuration.
>

**[v1: treewide: rework linker symbol declarations and references](http://lore.kernel.org/u-boot/TYAP286MB0315C961D2F585B2593EFC4ABC08A@TYAP286MB0315.JPNP286.PROD.OUTLOOK.COM/)**

> 1. Convert all linker symbol declarations to char[] type.
> 2. Unify the linker symbol reference format.
>
> CI check:
> https://github.com/u-boot/u-boot/pull/401
>

**[[PULL, v2] u-boot-riscv/master](http://lore.kernel.org/u-boot/ZMoyhcLqFtfaMB8b@ubuntu01/)**

> The following changes since commit 7755b2200777f72dca87dd169138e95f011bbcb9:
>
>   Merge tag 'x86-pull-20230801' of https://source.denx.de/u-boot/custodians/u-boot-x86 (2023-08-01 11:57:55 -0400)
>
> are available in the Git repository at:
>
>   https://source.denx.de/u-boot/custodians/u-boot-riscv.git
>
> for you to fetch changes up to 02be57caf730e2213bc844bf1dbe58bedd2c3734:
>
>   riscv: qemu: Enable usb keyboard as an input device (2023-08-02 16:32:44 +0800)
>
> CI result shows no issue: https://source.denx.de/u-boot/custodians/u-boot-riscv/-/pipelines/17179
>

**[v1: u-boot-riscv/master](http://lore.kernel.org/u-boot/ZMnuPiom%2Fggd274A@ubuntu01/)**

> The following changes since commit 7755b2200777f72dca87dd169138e95f011bbcb9:
>
>   Merge tag 'x86-pull-20230801' of https://source.denx.de/u-boot/custodians/u-boot-x86 (2023-08-01 11:57:55 -0400)
>
> are available in the Git repository at:
>
>   https://source.denx.de/u-boot/custodians/u-boot-riscv.git
>
> for you to fetch changes up to 093bd0354e5b947b0bd634bf5ed4041ba075b57d:
>
>   acpi: Add missing RISC-V acpi_table header (2023-08-02 11:02:33 +0800)
>
> CI result shows no issue: https://source.denx.de/u-boot/custodians/u-boot-riscv/-/pipelines/17177
>

**[v7: Add StarFive JH7110 PCIe drvier support](http://lore.kernel.org/u-boot/20230725094650.128325-1-minda.chen@starfivetech.com/)**

> These PCIe series patches are based on the JH7110 RISC-V SoC and VisionFive V2 board.
>
> The PCIe driver depends on gpio, pinctrl, clk and reset driver to do init.
> The PCIe dts configuation includes all these setting.
>
> The PCIe drivers codes has been tested on the VisionFive V2 boards.
> The test devices includes M.2 NVMe SSD and Realtek 8169 Ethernet adapter.
>

**[v1: sunxi: Allwinner T113s support](http://lore.kernel.org/u-boot/20230721134606.4505-1-andre.przywara@arm.com/)**

> this is finally the series adding support for the new SoC series that
> covers the Allwinner D1 siblings R528 and T113s. They all share the
> same die, although the D1 and D1s use RISC-V cores, which requires more
> plumbing, to use the sunxi code across two architectures. Getting the
> R528 support in should help a bit in sorting out what's new peripheral
> code and what is architecture dependent. IIUC, Samuel has that running,
> although with some hacks, the number of which this series tries to reduce.
>

## 20230721：第 54 期

### 内核动态

#### RISC-V 架构支持

**[v1: bpf-next: bpf, riscv: use BPF prog pack allocator in BPF JIT](http://lore.kernel.org/linux-riscv/20230720154941.1504-1-puranjay12@gmail.com/)**

> BPF programs currently consume a page each on RISCV. For systems with many BPF
> programs, this adds significant pressure to instruction TLB. High iTLB pressure
> usually causes slow down for the whole system.
>

**[v4: riscv: entry: set a0 = -ENOSYS only when syscall != -1](http://lore.kernel.org/linux-riscv/20230720140348.4716-1-CoelacanthusHex@gmail.com/)**

> When we test seccomp with 6.4 kernel, we found errno has wrong value.
> If we deny NETLINK_AUDIT with EAFNOSUPPORT, after f0bddf50586d, we will
> get ENOSYS instead. We got same result with commit 9c2598d43510 ("riscv: entry:
> Save a0 prior syscall_enter_from_user_mode()").
>

**[v2: Add SiFive Private L2 cache and PMU driver](http://lore.kernel.org/linux-riscv/20230720135125.21240-1-eric.lin@sifive.com/)**

> This patch series adds the SiFive Private L2 cache controller
> driver and Performance Monitoring Unit (PMU) driver.
>

**[v1: riscv: add SBI SUSP extension support](http://lore.kernel.org/linux-riscv/tencent_B931BF1864B6AE8C674686ED9852ACFA0609@qq.com/)**

> RISC-V SBI spec 2.0 [1] introduces System Suspend Extension which can be
> used to suspend the platform via SBI firmware.
>

**[v1: Linux RISC-V IOMMU Support](http://lore.kernel.org/linux-riscv/cover.1689792825.git.tjeznach@rivosinc.com/)**

> The RISC-V IOMMU specification is now ratified as-per the RISC-V international
> process [1]. The latest frozen specifcation can be found at:
> https://github.com/riscv-non-isa/riscv-iommu/releases/download/v1.0/riscv-iommu.pdf
>

**[GIT PULL: StarFive clock driver additions for v6.6](http://lore.kernel.org/linux-riscv/20230719-trough-frisk-40b92acb485a@spud/)**

> Please pull some clock driver additions for StarFive. I've had these
> commits, other than a rebase to pick up R-b tags from Emil, out for LKP
> to have a look at for a few days and they've gotten a clean bill of
> health. Some of the dt-binding stuff "only" has a review from me, but
> since I am a dt-binding maintainer that's fine, although maybe not
> common knowledge yet.
>

**[v2: gpio: sifive: Module support](http://lore.kernel.org/linux-riscv/20230719163446.1398961-1-samuel.holland@sifive.com/)**

> With the call to of_irq_count() removed, the SiFive GPIO driver can be
> built as a module. This helps to minimize the size of a multiplatform
> kernel, and is required by some downstream distributions (Android GKI).
>

**[v1: Risc-V Kvm Smstateen](http://lore.kernel.org/linux-riscv/20230719160316.4048022-1-mchitale@ventanamicro.com/)**

> This series adds support to detect the Smstateen extension for both, the
> host and the guest vcpu. It also adds senvcfg and sstateen0 to the ONE_REG
> interface and the vcpu context save/restore.
>

**[v6: Linux RISC-V AIA Support](http://lore.kernel.org/linux-riscv/20230719113542.2293295-1-apatel@ventanamicro.com/)**

> The RISC-V AIA specification is now frozen as-per the RISC-V international
> process. The latest frozen specifcation can be found at:
> https://github.com/riscv/riscv-aia/releases/download/1.0/riscv-interrupts-1.0.pdf
>

**[v1: Refactoring Microchip PolarFire PCIe driver](http://lore.kernel.org/linux-riscv/20230719102057.22329-1-minda.chen@starfivetech.com/)**

> This patchset final purpose is add PCIe driver for StarFive JH7110 SoC.
> JH7110 using PLDA XpressRICH PCIe IP. Microchip PolarFire Using the
> same IP and have commit their codes, which are mixed with PLDA
> controller codes and Microchip platform codes.
>

**[v5: Add initialization of clock for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230719092545.1961401-1-william.qiu@starfivetech.com/)**

> This patchset adds initial rudimentary support for the StarFive
> Quad SPI controller driver. And this driver will be used in
> StarFive's VisionFive 2 board. In 6.4, the QSPI_AHB and QSPI_APB
> clocks changed from the default ON state to the default OFF state,
> so these clocks need to be enabled in the driver.At the same time,
> dts patch is added to this series.
>

**[v3: riscv: Reduce ARCH_KMALLOC_MINALIGN to 8](http://lore.kernel.org/linux-riscv/20230718152214.2907-1-jszhang@kernel.org/)**

> Currently, riscv defines ARCH_DMA_MINALIGN as L1_CACHE_BYTES, I.E
> 64Bytes, if CONFIG_RISCV_DMA_NONCOHERENT=y. To support unified kernel
> Image, usually we have to enable CONFIG_RISCV_DMA_NONCOHERENT, thus
> it brings some bad effects to coherent platforms:
>
> Firstly, it wastes memory, kmalloc-96, kmalloc-32, kmalloc-16 and
> kmalloc-8 slab caches don't exist any more, they are replaced with
> either kmalloc-128 or kmalloc-64.
>

**[v1: asm-generic: ticket-lock: Optimize arch_spin_value_unlocked](http://lore.kernel.org/linux-riscv/20230719070001.795010-1-guoren@kernel.org/)**

> Using arch_spinlock_is_locked would cause another unnecessary memory
> access to the contended value. Although it won't cause a significant
> performance gap in most architectures, the arch_spin_value_unlocked
> argument contains enough information. Thus, remove unnecessary
> atomic_read in arch_spin_value_unlocked().
>

**[v2: riscv: entry: set a0 prior to syscall_enter_from_user_mode](http://lore.kernel.org/linux-riscv/20230718162940.226118-1-CoelacanthusHex@gmail.com/)**

> When we test seccomp with 6.4 kernel, we found errno has wrong value.
> If we deny NETLINK_AUDIT with EAFNOSUPPORT, after f0bddf50586d, we will
> get ENOSYS instead. We got same result with 9c2598d43510 ("riscv: entry: Save a0
> prior syscall_enter_from_user_mode()").
>

**[v1: riscv: Move the "Call Trace" to dump_backrace().](http://lore.kernel.org/linux-riscv/20230718023201.16018-1-minachou@andestech.com/)**

> It would be appropriate to show "Call Trace" within the dump_backtrace
> function to ensure that some kernel dumps include this information.
>

**[v2: usb: Explicitly include correct DT includes](http://lore.kernel.org/linux-riscv/20230718143027.1064731-1-robh@kernel.org/)**

> The DT of_device.h and of_platform.h date back to the separate
> of_platform_bus_type before it as merged into the regular platform bus.
> As part of that merge prepping Arm DT support 13 years ago, they
> "temporarily" include each other. They also include platform_device.h
> and of.h. As a result, there's a pretty much random mix of those include
> files used throughout the tree. In order to detangle these headers and
> replace the implicit includes with struct declarations, users need to
> explicitly include the correct includes.
>

**[v1: irqchip/sifive-plic: Avoid clearing the per-hart enable bits](http://lore.kernel.org/linux-riscv/20230717185841.1294425-1-samuel.holland@sifive.com/)**

> Writes to the PLIC completion register are ignored if the enable bit for
> that (interrupt, hart) combination is cleared. This leaves the interrupt
> in a claimed state, preventing it from being triggered again.
>

**[v11: KVM: guest_memfd() and per-page attributes](http://lore.kernel.org/linux-riscv/20230718234512.1690985-1-seanjc@google.com/)**

> This is the next iteration of implementing fd-based (instead of vma-based)
> memory for KVM guests.  If you want the full background of why we are doing
> this, please go read the v10 cover letter[1].
>
> The biggest change from v10 is to implement the backing storage in KVM
> itself, and expose it via a KVM ioctl() instead of a "generic" sycall.
> See link[2] for details on why we pivoted to a KVM-specific approach.
>

**[v1: riscv: dts: starfive: jh71x0: Add temperature sensor nodes and thermal-zones](http://lore.kernel.org/linux-riscv/20230718034937.92999-1-hal.feng@starfivetech.com/)**

> These patches add temperature sensor nodes and thermal-zones for the
> StarFive JH71X0 SoC. I have tested them on the BeagleV Starlight board
> and StarFive VisionFive 1 / 2 board. Thanks.
>

**[v1: clk: Explicitly include correct DT includes](http://lore.kernel.org/linux-riscv/20230714174342.4052882-1-robh@kernel.org/)**

> The DT of_device.h and of_platform.h date back to the separate
> of_platform_bus_type before it as merged into the regular platform bus.
> As part of that merge prepping Arm DT support 13 years ago, they
> "temporarily" include each other. They also include platform_device.h
> and of.h. As a result, there's a pretty much random mix of those include
> files used throughout the tree. In order to detangle these headers and
> replace the implicit includes with struct declarations, users need to
> explicitly include the correct includes.
>

**[v1: riscv: kernel: insert space before the open parenthesis '('](http://lore.kernel.org/linux-riscv/b90d162c4fb8062355634fb53b05173d@208suo.com/)**

> Fix below checkpatch error:
>
> /riscv/kernel/smp.c:93:ERROR: space required before the open parenthesis
> '('
>

**[v7: Add PLL clocks driver and syscon for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230717023040.78860-1-xingyu.wu@starfivetech.com/)**

> This patch serises are to add PLL clocks driver and providers by writing
> and reading syscon registers for the StarFive JH7110 RISC-V SoC. And add
> documentation and nodes to describe StarFive System Controller(syscon)
> Registers. This patch serises are based on Linux 6.4.
>

**[v2: riscv: support PREEMPT_DYNAMIC with static keys](http://lore.kernel.org/linux-riscv/20230716164925.1858-1-jszhang@kernel.org/)**

> Currently, each architecture can support PREEMPT_DYNAMIC through
> either static calls or static keys. To support PREEMPT_DYNAMIC on
> riscv, we face three choices:
>
> only add static calls support to riscv
> As Mark pointed out in commit 99cf983cc8bc ("sched/preempt: Add
> PREEMPT_DYNAMIC using static keys"), static keys "...should have
> slightly lower overhead than non-inline static calls, as this
> effectively inlines each trampoline into the start of its callee. This
> may avoid redundant work, and may integrate better with CFI schemes."
> So even we add static calls(without inline static calls) to riscv,
> static keys is still a better choice.
>

**[v1: riscv: Add HAVE_IOREMAP_PROT support](http://lore.kernel.org/linux-riscv/20230716152033.3713581-1-guoren@kernel.org/)**

> Add pte_pgprot macro, then riscv could have HAVE_IOREMAP_PROT,
> which will enable generic_access_phys() code, it is useful for
> debug, eg, gdb.
>
> Because generic_access_phys() would call ioremap_prot()->
> pgprot_nx() to disable excutable attribute, add definition
> of pgprot_nx() for riscv.
>

**[v1: Add support for Allwinner D1 CAN controllers](http://lore.kernel.org/linux-riscv/20230715112523.2533742-1-contact@jookia.org/)**

> This patch series adds support for the Allwinner D1 CAN controllers.
> It requires adding a new device tree compatible and driver support to
> work around some hardware quirks.
>

**[v9: Add support for Allwinner GPADC on D1/T113s/R329/T507 SoCs](http://lore.kernel.org/linux-riscv/20230715091816.3074375-1-bigunclemax@gmail.com/)**

> This series adds support for general purpose ADC (GPADC) on new
> Allwinner's SoCs, such as D1, T113s, T507 and R329. The implemented driver
> provides basic functionality for getting ADC channels data.
>

**[v1: bpf: riscv, bpf: Adapt bpf trampoline to optimized riscv ftrace framework](http://lore.kernel.org/linux-riscv/20230715090137.2141358-1-pulehui@huaweicloud.com/)**

> Commit 6724a76cff85 ("riscv: ftrace: Reduce the detour code size to
> half") optimizes the detour code size of kernel functions to half with
> T0 register and the upcoming DYNAMIC_FTRACE_WITH_DIRECT_CALLS of riscv
> is based on this optimization, we need to adapt riscv bpf trampoline
> based on this. One thing to do is to reduce detour code size of bpf
> programs, and the second is to deal with the return address after the
> execution of bpf trampoline. Meanwhile, add more comments and rename
> some variables to make more sense. The related tests have passed.
>

**[v1: pwm: Constistenly name pwm_chip variables "chip"](http://lore.kernel.org/linux-riscv/20230714205623.2496590-1-u.kleine-koenig@pengutronix.de/)**

> The first offenders I found were the core and the atmel-hlcdc driver.
> After I found these I optimistically assumed these were the only ones
> with the unusual names and send patches for these out individually
> before checking systematically.
>

**[v1: soc: microchip: Explicitly include correct DT includes](http://lore.kernel.org/linux-riscv/20230714175139.4067685-1-robh@kernel.org/)**

> The DT of_device.h and of_platform.h date back to the separate
> of_platform_bus_type before it as merged into the regular platform bus.
> As part of that merge prepping Arm DT support 13 years ago, they
> "temporarily" include each other. They also include platform_device.h
> and of.h. As a result, there's a pretty much random mix of those include
> files used throughout the tree. In order to detangle these headers and
> replace the implicit includes with struct declarations, users need to
> explicitly include the correct includes.
>

**[v1: reset: Explicitly include correct DT includes](http://lore.kernel.org/linux-riscv/20230714174939.4063667-1-robh@kernel.org/)**

> The DT of_device.h and of_platform.h date back to the separate
> of_platform_bus_type before it as merged into the regular platform bus.
> As part of that merge prepping Arm DT support 13 years ago, they
> "temporarily" include each other. They also include platform_device.h
> and of.h. As a result, there's a pretty much random mix of those include
> files used throughout the tree. In order to detangle these headers and
> replace the implicit includes with struct declarations, users need to
> explicitly include the correct includes.
>

**[v6: RISC-V: mm: Make SV48 the default address space](http://lore.kernel.org/linux-riscv/20230714165508.94561-1-charlie@rivosinc.com/)**

> Make sv48 the default address space for mmap as some applications
> currently depend on this assumption. Users can now select a
> desired address space using a non-zero hint address to mmap. Previously,
> requesting the default address space from mmap by passing zero as the hint
> address would result in using the largest address space possible. Some
> applications depend on empty bits in the virtual address space, like Go and
> Java, so this patch provides more flexibility for application developers.
>

**[v1: Add ethernet nodes for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230714104521.18751-1-samin.guo@starfivetech.com/)**

> This series adds ethernet nodes for StarFive JH7110 RISC-V SoC,
> and has been tested on StarFive VisionFive-2 v1.2A and v1.3B SBC boards.
>
> The first patch adds ethernet nodes for jh7110 SoC, the second patch
> adds ethernet nodes for visionfive 2 SBCs.
>

**[v4: RESEND: riscv: Introduce KASLR](http://lore.kernel.org/linux-riscv/20230713150800.120821-1-alexghiti@rivosinc.com/)**

> The following KASLR implementation allows to randomize the kernel mapping:
>
> - virtually: we expect the bootloader to provide a seed in the device-tree
> - physically: only implemented in the EFI stub, it relies on the firmware to
>   provide a seed using EFI_RNG_PROTOCOL. arm64 has a similar implementation
>   hence the patch 3 factorizes KASLR related functions for riscv to take
>   advantage.
>

**[v4: riscv: Introduce KASLR](http://lore.kernel.org/linux-riscv/20230713133401.116506-1-alexghiti@rivosinc.com/)**

> The following KASLR implementation allows to randomize the kernel mapping:
>
> - virtually: we expect the bootloader to provide a seed in the device-tree
> - physically: only implemented in the EFI stub, it relies on the firmware to
>   provide a seed using EFI_RNG_PROTOCOL. arm64 has a similar implementation
>   hence the patch 3 factorizes KASLR related functions for riscv to take
>   advantage.
>

#### 进程调度

**[v1: sched/debug: Print tgid in sched_show_task()](http://lore.kernel.org/lkml/20230720080516.1515297-1-yajun.deng@linux.dev/)**

> Multiple blocked tasks are printed when the system hangs. They may have
> the same parent pid, but belong to different task groups.
>
> Printing tgid lets users better know whether these tasks are from the same
> task group or not.
>

**[v9: sched/fair: Scan cluster before scanning LLC in wake-up path](http://lore.kernel.org/lkml/20230719092838.2302-1-yangyicong@huawei.com/)**

> This is the follow-up work to support cluster scheduler. Previously
> we have added cluster level in the scheduler for both ARM64[1] and
> X86[2] to support load balance between clusters to bring more memory
> bandwidth and decrease cache contention. This patchset, on the other
> hand, takes care of wake-up path by giving CPUs within the same cluster
> a try before scanning the whole LLC to benefit those tasks communicating
> with each other.
>

**[v2: sched: Optimize in_task() and in_interrupt() a bit](http://lore.kernel.org/lkml/453f675efb082e08068736bf69293d48ff3129a7.1689641959.git.fthain@linux-m68k.org/)**

> Except on x86, preempt_count is always accessed with READ_ONCE.
> Repeated invocations in macros like irq_count() produce repeated loads.
> These redundant instructions appear in various fast paths. In the one
> shown below, for example, irq_count() is evaluated during kernel entry
> if !tick_nohz_full_cpu(smp_processor_id()).
>

**[v2: sched/core: Use empty mask to reset cpumasks in sched_setaffinity()](http://lore.kernel.org/lkml/20230717180243.3607603-1-longman@redhat.com/)**

> Since commit 8f9ea86fdf99 ("sched: Always preserve the user requested
> cpumask"), user provided CPU affinity via sched_setaffinity(2) is
> perserved even if the task is being moved to a different cpuset. However,
> that affinity is also being inherited by any subsequently created child
> processes which may not want or be aware of that affinity.
>

**[v1: sched/fair: Add SMT4 group_smt_balance handling](http://lore.kernel.org/lkml/20230717145823.1531759-1-sshegde@linux.vnet.ibm.com/)**

> For SMT4, any group with more than 2 tasks will be marked as
> group_smt_balance. Retain the behaviour of group_has_spare by marking
> the busiest group as the group which has the least number of idle_cpus.
>

**[GIT PULL: sched/urgent for v6.5-rc2](http://lore.kernel.org/lkml/20230716183726.GEZLQ45tOt9L548BJ4@fat_crate.local/)**

> please pull two urgent scheduler fixes for 6.5.
>

**[v1: sched: Rename DIE domain](http://lore.kernel.org/lkml/20230712141056.GI3100107@hirez.programming.kicks-ass.net/)**

> Thomas just tripped over the x86 topology setup creating a 'DIE' domain
> for the package mask :-)
>
> Since these names are SCHED_DEBUG only, rename them.
> I don't think anybody *should* be relying on this, but who knows.
>

**[v2: sched: Implement shared runqueue in CFS](http://lore.kernel.org/lkml/20230710200342.358255-1-void@manifault.com/)**

> This is v2 of the shared wakequeue (now called shared runqueue)
> patchset. The following are changes from the RFC v1 patchset
> (https://lore.kernel.org/lkml/20230613052004.2836135-1-void@manifault.com/).
>

**[v1: net: sched: Replace strlcpy with strscpy](http://lore.kernel.org/lkml/20230710030711.812898-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>
 
**[[PATCH AUTOSEL 4.14] sched/fair: Don't balance task to its current running CPU](http://lore.kernel.org/lkml/20230709150618.512785-1-sashal@kernel.org/)**

> The new_dst_cpu is chosen from the env->dst_grpmask. Currently it
> contains CPUs in sched_group_span() and if we have overlapped groups it's
> possible to run into this case. This patch makes env->dst_grpmask of
> group_balance_mask() which exclude any CPUs from the busiest group and
> solve the issue. For balancing in a domain with no overlapped groups
> the behaviour keeps same as before.
>

#### 内存管理

**[v2: context_tracking,x86: Defer some IPIs until a user->kernel transition](http://lore.kernel.org/linux-mm/20230720163056.2564824-1-vschneid@redhat.com/)**

> The heart of this series is the thought that while we cannot remove NOHZ_FULL
> CPUs from the list of CPUs targeted by these IPIs, they may not have to execute
> the callbacks immediately. Anything that only affects kernelspace can wait
> until the next user->kernel transition, providing it can be executed "early
> enough" in the entry code.
>

**[v3: Convert several functions in page_io.c to use a folio](http://lore.kernel.org/linux-mm/20230720130147.4071649-1-zhangpeng362@huawei.com/)**

> This patch series converts several functions in page_io.c to use a
> folio, which can remove several implicit calls to compound_head().
>

**[v3: Optimize large folio interaction with deferred split](http://lore.kernel.org/linux-mm/20230720112955.643283-1-ryan.roberts@arm.com/)**

> [Sending v3 to replace yesterday's v2 after Yu Zhou's feedback]
>
> This is v3 of a small series in support of my work to enable the use of large
> folios for anonymous memory (known as "FLEXIBLE_THP" or "LARGE_ANON_FOLIO") [1].
> It first makes it possible to add large, non-pmd-mappable folios to the deferred
> split queue. Then it modifies zap_pte_range() to batch-remove spans of
> physically contiguous pages from the rmap, which means that in the common case,
> we elide the need to ever put the folio on the deferred split queue, thus
> reducing lock contention and improving performance.
>

**[v4: mm/slub: Optimize slub memory usage](http://lore.kernel.org/linux-mm/20230720102337.2069722-1-jaypatel@linux.ibm.com/)**

> In the current implementation of the slub memory allocator, the slab
> order selection process follows these criteria:
>
> 1) Determine the minimum order required to serve the minimum number of
> objects (min_objects). This calculation is based on the formula (order
> = min_objects * object_size / PAGE_SIZE).
> 2) If the minimum order is greater than the maximum allowed order
> (slub_max_order), set slub_max_order as the order for this slab.
> 3) If the minimum order is less than the slub_max_order, iterate
> through a loop from minimum order to slub_max_order and check if the
> condition (rem <= slab_size / fract_leftover) holds true. Here,
> slab_size is calculated as (PAGE_SIZE << order), rem is (slab_size %
> object_size), and fract_leftover can have values of 16, 8, or 4. If
> the condition is true, select that order for the slab.
>

**[v3: Invalidate secondary IOMMU TLB on permission upgrade](http://lore.kernel.org/linux-mm/cover.b24362332ec6099bc8db4e8e06a67545c653291d.1689842332.git-series.apopple@nvidia.com/)**

> The main change is to move secondary TLB invalidation mmu notifier
> callbacks into the architecture specific TLB flushing functions. This
> makes secondary TLB invalidation mostly match CPU invalidation while
> still allowing efficient range based invalidations based on the
> existing TLB batching code.
>

**[v2: mm: use memmap_on_memory semantics for dax/kmem](http://lore.kernel.org/linux-mm/20230720-vv-kmem_memmap-v2-0-88bdaab34993@intel.com/)**

> The dax/kmem driver can potentially hot-add large amounts of memory
> originating from CXL memory expanders, or NVDIMMs, or other 'device
> memories'. There is a chance there isn't enough regular system memory
> available to fit the memmap for this new memory. It's therefore
> desirable, if all other conditions are met, for the kmem managed memory
> to place its memmap on the newly added memory itself.
>

**[v1: memory recharging for offline memcgs](http://lore.kernel.org/linux-mm/20230720070825.992023-1-yosryahmed@google.com/)**

> This patch series implements the proposal in LSF/MM/BPF 2023 conference
> for reducing offline/zombie memcgs by memory recharging [1]. The main
>

**[v1: shmem: add support for user extended attributes](http://lore.kernel.org/linux-mm/20230720065430.2178136-1-ovt@google.com/)**

> User extended attributes are not enabled in tmpfs because
> the size of the value is not limited and the memory allocated
> for it is not counted against any limit. Malicious
> non-privileged user can exhaust kernel memory by creating
> user.* extended attribute with very large value.
>

**[v1: mm,memblock: reset memblock.reserved to system init state to prevent UAF](http://lore.kernel.org/linux-mm/20230719154137.732d8525@imladris.surriel.com/)**

> The memblock_discard function frees the memblock.reserved.regions
> array, which is good.
>
> However, if a subsequent memblock_free (or memblock_phys_free) comes
> in later, from for example ima_free_kexec_buffer, that will result in
> a use after free bug in memblock_isolate_range.
>

**[v2: mm/hugetlb: get rid of page_hstate()](http://lore.kernel.org/linux-mm/20230719184145.301911-1-sidhartha.kumar@oracle.com/)**

> Converts the last page_hstate() user to use folio_hstate() so
> page_hstate() can be safely removed.
>

**[v1: mm: memcg: use rstat for non-hierarchical stats](http://lore.kernel.org/linux-mm/20230719174613.3062124-1-yosryahmed@google.com/)**

> Currently, memcg uses rstat to maintain hierarchical stats. The rstat
> framework keeps track of which cgroups have updates on which cpus.
>
> For non-hierarchical stats, as memcg moved to rstat, they are no longer
> readily available as counters. Instead, the percpu counters for a given
> stat need to be summed to get the non-hierarchical stat value. This
> causes a performance regression when reading non-hierarchical stats on
> kernels where memcg moved to using rstat. This is especially visible
> when reading memory.stat on cgroup v1. There are also some code paths
> internal to the kernel that read such non-hierarchical stats.
>

**[v2: mm: convert to vma_is_initial_heap/stack()](http://lore.kernel.org/linux-mm/20230719075127.47736-1-wangkefeng.wang@huawei.com/)**

> Add vma_is_initial_stack() and vma_is_initial_heap() helper and use
> them to simplify code.
>

**[v1: mm: hugetlb_vmemmap: use PageCompound() instead of PageReserved()](http://lore.kernel.org/linux-mm/20230719063132.37676-1-songmuchun@bytedance.com/)**

> The ckeck of PageReserved() is easy to be broken in the future, PageCompound()
> is more stable to check if the page should be split.
>

**[v1: udmabuf: Replace pages when there is FALLOC_FL_PUNCH_HOLE in memfd](http://lore.kernel.org/linux-mm/20230718082858.1570809-1-vivek.kasireddy@intel.com/)**

> This patch series attempts to solve the coherency problem seen when
> a hole is punched in the region(s) of the mapping (associated with
> the memfd) that overlaps with pages registered with a udmabuf fd.
>

**[v2: udmabuf: Add back support for mapping hugetlb pages (v2)](http://lore.kernel.org/linux-mm/20230718082605.1570740-1-vivek.kasireddy@intel.com/)**

> The first patch ensures that the mappings needed for handling mmap
> operation would be managed by using the pfn instead of struct page.
> The second patch restores support for mapping hugetlb pages where
> subpages of a hugepage are not directly used anymore (main reason
> for revert) and instead the hugetlb pages and the relevant offsets
> are used to populate the scatterlist for dma-buf export and for
> mmap operation.
>

**[v3: mm: kfence: allocate kfence_metadata at runtime](http://lore.kernel.org/linux-mm/20230718073019.52513-1-zhangpeng.00@bytedance.com/)**

> kfence_metadata is currently a static array. For the purpose of allocating
> scalable __kfence_pool, we first change it to runtime allocation of
> metadata. Since the size of an object of kfence_metadata is 1160 bytes, we
> can save at least 72 pages (with default 256 objects) without enabling
> kfence.
>

**[v1: add page_ext_data to get client data in page_ext](http://lore.kernel.org/linux-mm/20230718145812.1991717-1-shikemeng@huaweicloud.com/)**

> Current client get data from page_ext by adding offset which is auto
> generated in page_ext core and expose the data layout design insdie
> page_ext core. This series adds a page_ext_data to hide offset from
> client. Thanks!
>

**[v1: mm/damon/core-test: Initialise context before test in damon_test_set_attrs()](http://lore.kernel.org/linux-mm/20230718052811.1065173-1-feng.tang@intel.com/)**

> Running kunit test for 6.5-rc1 hits one bug:
>
>         ok 10 damon_test_update_monitoring_result
>

**[v4: Add support for memmap on memory feature on ppc64](http://lore.kernel.org/linux-mm/20230718024409.95742-1-aneesh.kumar@linux.ibm.com/)**

> This patch series update memmap on memory feature to fall back to
> memmap allocation outside the memory block if the alignment rules are
> not met. This makes the feature more useful on architectures like
> ppc64 where alignment rules are different with 64K page size.
>

**[v5: Add support for DAX vmemmap optimization for ppc64](http://lore.kernel.org/linux-mm/20230718022934.90447-1-aneesh.kumar@linux.ibm.com/)**

> This patch series implements changes required to support DAX vmemmap
> optimization for ppc64. The vmemmap optimization is only enabled with radix MMU
> translation and 1GB PUD mapping with 64K page size. The patch series also split
> hugetlb vmemmap optimization as a separate Kconfig variable so that
> architectures can enable DAX vmemmap optimization without enabling hugetlb
> vmemmap optimization. This should enable architectures like arm64 to enable DAX
> vmemmap optimization while they can't enable hugetlb vmemmap optimization. More
> details of the same are in patch "mm/vmemmap optimization: Split hugetlb and
> devdax vmemmap optimization"
>

**[v1: 5.15.y: mm/damon/ops-common: atomically test and clear young on ptes and pmds](http://lore.kernel.org/linux-mm/20230717193008.122040-1-sj@kernel.org/)**

> commit c11d34fa139e4b0fb4249a30f37b178353533fa1 upstream.
>
> It is racy to non-atomically read a pte, then clear the young bit, then
> write it back as this could discard dirty information.  Further, it is bad
> practice to directly set a pte entry within a table.  Instead clearing
> young must go through the arch-provided helper,
> ptep_test_and_clear_young() to ensure it is modified atomically and to
> give the arch code visibility and allow it to check (and potentially
> modify) the operation.
>

#### 文件系统

**[v1: Various Rust bindings for files](http://lore.kernel.org/linux-fsdevel/20230720152820.3566078-1-aliceryhl@google.com/)**

> This contains bindings for various file related things that binder needs
> to use.
>
> I would especially like feedback on the SAFETY comments. Particularly,
> the safety comments in patch 4 and 5 are non-trivial. For example:
>

**[v1: vboxsf: Use flexible arrays for trailing string member](http://lore.kernel.org/linux-fsdevel/20230720151458.never.673-kees@kernel.org/)**

> The declaration of struct shfl_string used trailing fake flexible arrays
> for the string member. This was tripping FORTIFY_SOURCE since commit
> df8fc4e934c1 ("kbuild: Enable -fstrict-flex-arrays=3"). Replace the
> utf8 and utf16 members with actual flexible arrays, drop the unused ucs2
> member, and retriain a 2 byte padding to keep the structure size the same.
>

**[v1: fs/nls: make load_nls() take a const parameter](http://lore.kernel.org/linux-fsdevel/20230720063414.2546451-1-wentao@uniontech.com/)**

> load_nls() take a char * parameter, use it to find nls module in list or
> construct the module name to load it.
>
> This change make load_nls() take a const parameter, so we don't need do
> some cast like this:
>
>         ses->local_nls = load_nls((char *)ctx->local_nls->charset);
>
> Also remove the cast in cifs code.
>

**[v1: fstests: add helper to canonicalize devices used to enable persistent disks](http://lore.kernel.org/linux-fsdevel/20230720061727.2363548-1-mcgrof@kernel.org/)**

> The filesystem configuration file does not allow you to use symlinks to
> devices given the existing sanity checks verify that the target end
> device matches the source.
>
> Using a symlink is desirable if you want to enable persistent tests
> across reboots. For example you may want to use /dev/disk/by-id/nvme-eui.*
> so to ensure that the same drives are used even after reboot. This
> is very useful if you are testing for example with a virtualized
> environment and are using PCIe passthrough with other qemu NVMe drives
> with one or many NVMe drives.
>

**[v3: Support negative dentries on case-insensitive ext4 and f2fs](http://lore.kernel.org/linux-fsdevel/20230719221918.8937-1-krisman@suse.de/)**

> V3 applies the fixes suggested by Eric Biggers (thank you for your
> review!).  Changelog inlined in the patches.
>
> Retested with xfstests for ext4 and f2fs.
>
> cover letter from v1.
>
> This patchset enables negative dentries for case-insensitive directories
> in ext4/f2fs.  It solves the corner cases for this feature, including
> those already tested by fstests (generic/556).  It also solves an
> existing bug with the existing implementation where old negative
> dentries are left behind after a directory conversion to
> case-insensitive.
>

**[v1: nfsd: inherit required unset default acls from effective set](http://lore.kernel.org/linux-fsdevel/20230719-nfsd-acl-v1-1-eb0faf3d2917@kernel.org/)**

> A well-formed NFSv4 ACL will always contain OWNER@/GROUP@/EVERYONE@
> ACEs, but there is no requirement for inheritable entries for those
> entities. POSIX ACLs must always have owner/group/other entries, even for a
> default ACL.
>

**[v1: fs: export emergency_sync](http://lore.kernel.org/linux-fsdevel/20230718214540.1.I763efc30c57dcc0284d81f704ef581cded8960c8@changeid/)**

> emergency_sync forces a filesystem sync in emergency situations.
> Export this function so it can be used by modules.
>

**[v4: io_uring getdents](http://lore.kernel.org/linux-fsdevel/20230718132112.461218-1-hao.xu@linux.dev/)**

> This series introduce getdents64 to io_uring, the code logic is similar
> with the snychronized version's. It first try nowait issue, and offload
> it to io-wq threads if the first try fails.
>

**[v2: xarray: Document necessary flag in alloc functions](http://lore.kernel.org/linux-fsdevel/20230718072533.4305-2-pstanner@redhat.com/)**

> Adds a new line to the docstrings of functions wrapping __xa_alloc() and
> __xa_alloc_cyclic(), informing about the necessity of flag XA_FLAGS_ALLOC
> being set previously.
>
> The documentation so far says that functions wrapping __xa_alloc() and
> __xa_alloc_cyclic() are supposed to return either -ENOMEM or -EBUSY in
> case of an error. If the xarray has been initialized without the flag
> XA_FLAGS_ALLOC, however, they fail with a different, undocumented error
> code.
>

**[GIT PULL: Create large folios in iomap buffered write path](http://lore.kernel.org/linux-fsdevel/ZLVrEkVU2YCneoXR@casper.infradead.org/)**

> The following changes since commit 5b8d6e8539498e8b2fa67fbcce3fe87834d44a7a:
>
>   Merge tag 'xtensa-20230716' of https://github.com/jcmvbkbc/linux-xtensa (2023-07-16 14:12:49 -0700)
>

**[v1: fs/filesystems.c: ERROR: "(foo*)" should be "(foo *)"](http://lore.kernel.org/linux-fsdevel/a456720721d2f8fc33bb0befbe2ad115@208suo.com/)**

> Fix five occurrences of the checkpatch.pl error:
> ERROR: "(foo*)" should be "(foo *)"
>

**[v2: fs/address_space: add alignment padding for i_map and i_mmap_rwsem to mitigate a false sharing.](http://lore.kernel.org/linux-fsdevel/20230716145450.20108-1-lipeng.zhu@intel.com/)**

> When running UnixBench/Shell Scripts, we observed high false sharing
> for accessing i_mmap against i_mmap_rwsem.
>

**[v1: fs: inode: return proper errno on bmap()](http://lore.kernel.org/linux-fsdevel/20230715060217.1469690-1-lsahn@wewakecorp.com/)**

> It better returns -EOPNOTSUPP instead of -EINVAL which has meaning of
> the argument is an inappropriate value. It doesn't make sense in the
> case of that a file system doesn't support bmap operation.
>
> -EINVAL could make confusion in the userspace perspective.
>

**[v1: exfat: release s_lock before calling dir_emit()](http://lore.kernel.org/linux-fsdevel/20230714084354.1959951-1-sj1557.seo@samsung.com/)**

> WARNING: possible circular locking dependency detected
> 6.4.0-next-20230707-syzkaller #0 Not tainted
> syz-executor330/5073 is trying to acquire lock:
> ffff8880218527a0 (&mm->mmap_lock){++++}-{3:3}, at: mmap_read_lock_killable include/linux/mmap_lock.h:151 [inline]
> ffff8880218527a0 (&mm->mmap_lock){++++}-{3:3}, at: get_mmap_lock_carefully mm/memory.c:5293 [inline]
> ffff8880218527a0 (&mm->mmap_lock){++++}-{3:3}, at: lock_mm_and_find_vma+0x369/0x510 mm/memory.c:5344
> but task is already holding lock:
> ffff888019f760e0 (&sbi->s_lock){+.+.}-{3:3}, at: exfat_iterate+0x117/0xb50 fs/exfat/dir.c:232
>

**[[fstests PATCH] generic: add a test for multigrain timestamps](http://lore.kernel.org/linux-fsdevel/20230713230939.367068-1-jlayton@kernel.org/)**

> Ensure that the mtime and ctime apparently change, even when there are
> multiple writes in quick succession. Older kernels didn't do this, but
> there are patches in flight that should help ensure it in the future.
>

**[v5: fs: implement multigrain timestamps](http://lore.kernel.org/linux-fsdevel/20230713-mgctime-v5-0-9eb795d2ae37@kernel.org/)**

> The VFS always uses coarse-grained timestamps when updating the
> ctime and mtime after a change. This has the benefit of allowing
> filesystems to optimize away a lot metadata updates, down to around 1
> per jiffy, even when a file is under heavy writes.
>

**[v2: procfs: block chmod on /proc/thread-self/comm](http://lore.kernel.org/linux-fsdevel/20230713141001.27046-1-cyphar@cyphar.com/)**

> Due to an oversight in commit 1b3044e39a89 ("procfs: fix pthread
> cross-thread naming if !PR_DUMPABLE") in switching from REG to NOD,
> chmod operations on /proc/thread-self/comm were no longer blocked as
> they are on almost all other procfs files.
>

**[v4: RESEND: shmem: Add user and group quota support for tmpfs](http://lore.kernel.org/linux-fsdevel/20230713134848.249779-1-cem@kernel.org/)**

> This is a resend of the quota support for tmpfs. This has been rebased on
> today Linus's TOT. These patches conflicted with Luis Chamberlain's series to
> include 'noswap' mount option to tmpfs, there was no code change since the
> previous version, other than moving the implementation of quota options 'after'
> 'noswap'.
>

**[v1: exfat: check if filename entries exceeds max filename length](http://lore.kernel.org/linux-fsdevel/20230713130310.8445-1-linkinjeon@kernel.org/)**

> exfat_extract_uni_name copies characters from a given file name entry into
> the 'uniname' variable. This variable is actually defined on the stack of
> the exfat_readdir() function. According to the definition of
> the 'exfat_uni_name' type, the file name should be limited 255 characters
> (+ null teminator space), but the exfat_get_uniname_from_ext_entry()
> function can write more characters because there is no check if filename
> entries exceeds max filename length. This patch add the check not to copy
> filename characters when exceeding max filename length.
>

**[v1: fs: proc: Add error checking for d_hash_and_lookup()](http://lore.kernel.org/linux-fsdevel/20230713113303.6512-1-machel@vivo.com/)**

> In case of failure, d_hash_and_lookup() returns NULL or an error
> pointer. The proc_fill_cache() needs to add the handling of the
> error pointer returned by d_hash_and_lookup().
>

**[v25: Implement IOCTL to get and optionally clear info about PTEs](http://lore.kernel.org/linux-fsdevel/20230713101415.108875-1-usama.anjum@collabora.com/)**

> *Changes in v25*:
> - Do proper filtering on hole as well (hole got missed earlier)
>

**[v1: eventfd: simplify signal helpers](http://lore.kernel.org/linux-fsdevel/20230713-vfs-eventfd-signal-v1-0-7fda6c5d212b@kernel.org/)**

> This simplifies the eventfd_signal() and eventfd_signal_mask() helpers
> by removing the count argument which is effectively unused.
>

**[v1: More filesystem folio conversions for 6.6](http://lore.kernel.org/linux-fsdevel/20230713035512.4139457-1-willy@infradead.org/)**

> Remove the only spots in affs which actually use a struct page; there
> are a few places where one is mentioned, but it's part of the interface.
>

#### 网络设备

**[v3: net-next: vsock/virtio/vhost: MSG_ZEROCOPY preparations](http://lore.kernel.org/netdev/20230720214245.457298-1-AVKrasnov@sberdevices.ru/)**

> this patchset is first of three parts of another big patchset for
> MSG_ZEROCOPY flag support:
> https://lore.kernel.org/netdev/20230701063947.3422088-1-AVKrasnov@sberdevices.ru/
>

**[GIT PULL: Networking for v6.5-rc3](http://lore.kernel.org/netdev/20230720214559.163647-1-kuba@kernel.org/)**

> The following changes since commit b1983d427a53911ea71ba621d4bf994ae22b1536:
>
>   Merge tag 'net-6.5-rc2' of git://git.kernel.org/pub/scm/linux/kernel/git/netdev/net (2023-07-13 14:21:22 -0700)
>

**[v5: bpf-next: Support defragmenting IPv(4|6) packets in BPF](http://lore.kernel.org/netdev/cover.1689884827.git.dxu@dxuuu.xyz/)**

> In the context of a middlebox, fragmented packets are tricky to handle.
> The full 5-tuple of a packet is often only available in the first
> fragment which makes enforcing consistent policy difficult. There are
> really only two stateless options, neither of which are very nice:
>
> Enforce policy on first fragment and accept all subsequent fragments.
>    This works but may let in certain attacks or allow data exfiltration.
>

**[v1: iproute2: bridge/mdb.c: include limits.h](http://lore.kernel.org/netdev/20230720203726.2316251-1-tgamblin@baylibre.com/)**

> Include limits.h in bridge/mdb.c to fix this issue. This change is based
> on one in Alpine Linux, but the author there had no plans to submit:
> https://git.alpinelinux.org/aports/commit/main/iproute2/include.patch?id=bd46efb8a8da54948639cebcfa5b37bd608f1069
>

**[v4: net-next: ionic: add FLR support](http://lore.kernel.org/netdev/20230720190816.15577-1-shannon.nelson@amd.com/)**

> Add support for handing and recovering from a PCI FLR event.
> This patchset first moves some code around to make it usable
> from multiple paths, then adds the PCI error handler callbacks
> for reset_prepare and reset_done.
>

**[v1: net-next: page_pool: add a lockdep check for recycling in hardirq](http://lore.kernel.org/netdev/20230720173752.2038136-1-kuba@kernel.org/)**

> Page pool use in hardirq is prohibited, add debug checks
> to catch misuses. IIRC we previously discussed using
> DEBUG_NET_WARN_ON_ONCE() for this, but there were concerns
> that people will have DEBUG_NET enabled in perf testing.
> I don't think anyone enables lockdep in perf testing,
> so use lockdep to avoid pushback and arguing :)
>

**[v3: bpf-next: bpf, xdp: Add tracepoint to xdp attaching failure](http://lore.kernel.org/netdev/20230720155228.5708-1-hffilwlqm@gmail.com/)**

> This series introduces a new tracepoint in bpf_xdp_link_attach(). By
> this tracepoint, error message will be captured when error happens in
> dev_xdp_attach(), e.g. invalid attaching flags.
>

**[v6: bpf-next: Add SO_REUSEPORT support for TC bpf_sk_assign](http://lore.kernel.org/netdev/20230720-so-reuseport-v6-0-7021b683cdae@isovalent.com/)**

> We want to replace iptables TPROXY with a BPF program at TC ingress.
> To make this work in all cases we need to assign a SO_REUSEPORT socket
> to an skb, which is currently prohibited. This series adds support for
> such sockets to bpf_sk_assing.
>

**[v1: net-next: net: dsa: microchip: provide Wake on LAN support](http://lore.kernel.org/netdev/20230720132556.57562-1-o.rempel@pengutronix.de/)**

> This series of patches provides Wake on LAN support for the KSZ9477
> family of switches. It was tested on KSZ8565 Switch with PME pin
> attached to an external PMIC.
>

**[v2: net-next: devlink: introduce dump selector attr and use it for per-instance dumps](http://lore.kernel.org/netdev/20230720121829.566974-1-jiri@resnulli.us/)**

> For SFs, one devlink instance per SF is created. There might be
> thousands of these on a single host. When a user needs to know port
> handle for specific SF, he needs to dump all devlink ports on the host
> which does not scale good.
>

**[v5: Add motorcomm phy pad-driver-strength-cfg support](http://lore.kernel.org/netdev/20230720111509.21843-1-samin.guo@starfivetech.com/)**

> The motorcomm phy (YT8531) supports the ability to adjust the drive
> strength of the rx_clk/rx_data, and the default strength may not be
> suitable for all boards. So add configurable options to better match
> the boards.(e.g. StarFive VisionFive 2)
>

**[v1: net-next: genetlink: add explicit ordering break check for split ops](http://lore.kernel.org/netdev/20230720111354.562242-1-jiri@resnulli.us/)**

> Currently, if cmd in the split ops array is of lower value than the
> previous one, genl_validate_ops() continues to do the checks as if
> the values are equal. This may result in non-obvious WARN_ON() hit in
> these check.
>

**[v2: net: vxlan: calculate correct header length for GPE](http://lore.kernel.org/netdev/544e8c6d0f48af2be49809877c05c0445c0b0c0b.1689843872.git.jbenc@redhat.com/)**

> VXLAN-GPE does not add an extra inner Ethernet header. Take that into
> account when calculating header length.
>
> This causes problems in skb_tunnel_check_pmtu, where incorrect PMTU is
> cached.
>

**[v4: net-next: virtio-net: don't busy poll for cvq command](http://lore.kernel.org/netdev/20230720083839.481487-1-jasowang@redhat.com/)**

> The code used to busy poll for cvq command which turns out to have
> several side effects:
>
> 1) infinite poll for buggy devices
> 2) bad interaction with scheduler
>
> So this series tries to use cond_resched() in the waiting loop. Before
> doing this we need first make sure the cvq command is not executed in
> atomic environment, so we need first convert rx mode handling to a
> workqueue.
>

**[v2: net-next: eth: bnxt: handle invalid Tx completions more gracefully](http://lore.kernel.org/netdev/20230720010440.1967136-1-kuba@kernel.org/)**

> bnxt trusts the events generated by the device which may lead to kernel
> crashes. These are extremely rare but they do happen. For a while
> I thought crashing may be intentional, because device reporting invalid
> completions should never happen, and having a core dump could be useful
> if it does. But in practice I haven't found any clues in the core dumps,
> and panic_on_warn exists.
>

**[v1: net-next: net: Use sockaddr_storage for getsockopt(SO_PEERNAME).](http://lore.kernel.org/netdev/20230720005456.88770-1-kuniyu@amazon.com/)**

> Commit df8fc4e934c1 ("kbuild: Enable -fstrict-flex-arrays=3") started
> applying strict rules to standard string functions.
>

**[v1: net: phy: prevent stale pointer dereference in phy_init()](http://lore.kernel.org/netdev/20230720000231.1939689-1-vladimir.oltean@nxp.com/)**

> mdio_bus_init() and phy_driver_register() both have error paths, and if
> those are ever hit, ethtool will have a stale pointer to the
> phy_ethtool_phy_ops stub structure, which references memory from a
> module that failed to load (phylib).
>

**[v1: net: tcp: add missing annotations](http://lore.kernel.org/netdev/20230719212857.3943972-1-edumazet@google.com/)**

> This series was inspired by one syzbot (KCSAN) report.
>
> do_tcp_getsockopt() does not lock the socket, we need to
> annotate most of the reads there (and other places as well).
>

**[v8: net/tcp: Add TCP-AO support](http://lore.kernel.org/netdev/20230719202631.472019-1-dima@arista.com/)**

> This is version 8 of TCP-AO support. I base it on master and there
> weren't any conflicts on my tentative merge to linux-next.
>
> The good news is that all pre-required patches have merged to
> Torvald's/master. Thanks to Herbert, crypto clone-tfm just works on
> master for all TCP-AO supported algorithms.

#### 安全增强

**[v1: kunit: Add test attributes API](http://lore.kernel.org/linux-hardening/20230719222338.259684-1-rmoar@google.com/)**

> This patch series adds a test attributes framework to KUnit.
>
> There has been interest in filtering out "slow" KUnit tests. Most notably,
> a new config, CONFIG_MEMCPY_SLOW_KUNIT_TEST, has been added to exclude a
> particularly slow memcpy test
> (https://lore.kernel.org/all/20230118200653.give.574-kees@kernel.org/).
>

**[v1: HotBPF: Prevent Kernel Heap-based Exploitation](http://lore.kernel.org/linux-hardening/20230719155032.4972-1-wzc@smail.nju.edu.cn/)**

> Request for Comments, a hot eBPF patch to prevent kernel heap exploitation.
>
> SLUB exploitation poses a significant threat to kernel security. The exploitation
> takes advantage of the fact that kernel objects share `kmalloc` slub caches.
> This sharing setting allows to create overlapping between vulnerable objects that
> introduce corruption, and other objects that contains sensitive data.
> To mitigate this, we introduce HotBPF.
>

**[v1: next: fs: omfs: Use flexible-array member in struct omfs_extent](http://lore.kernel.org/linux-hardening/ZLGodUeD307GlINN@work/)**

> Memory for 'struct omfs_extent' and a 'e_extent_count' number of extent
> entries is indirectly allocated through 'bh->b_data', which is a pointer
> to data within the page. This implies that the member 'e_entry'
> (which is the start of extent entries) functions more like an array than
> a single object of type 'struct omfs_extent_entry'.
>

**[v5: Randomized slab caches for kmalloc()](http://lore.kernel.org/linux-hardening/20230714064422.3305234-1-gongruiqi@huaweicloud.com/)**

> When exploiting memory vulnerabilities, "heap spraying" is a common
> technique targeting those related to dynamic memory allocation (i.e. the
> "heap"), and it plays an important role in a successful exploitation.
> Basically, it is to overwrite the memory area of vulnerable object by
> triggering allocation in other subsystems or modules and therefore
> getting a reference to the targeted memory location. It's usable on
> various types of vulnerablity including use after free (UAF), heap out-
> of-bound write and etc.
>

**[v2: igc: Ignore AER reset when device is suspended](http://lore.kernel.org/linux-hardening/20230714050541.2765246-1-kai.heng.feng@canonical.com/)**

> The issue is that the PTM requests are sending before driver resumes the
> device. Since the issue can also be observed on Windows, it's quite
> likely a firmware/hardware limitation.
>
> So avoid resetting the device if it's not resumed. Once the device is
> fully resumed, the device can work normally.
>

**[v1: tracing: Add back FORTIFY_SOURCE logic to kernel_stack event structure](http://lore.kernel.org/linux-hardening/20230713092605.2ddb9788@rorschach.local.home/)**

> For backward compatibility, older tooling expects to see the kernel_stack
> event with a "caller" field that is a fixed size array of 8 addresses. The
> code now supports more than 8 with an added "size" field that states the
> real number of entries. But the "caller" field still just looks like a
> fixed size to user space.
>

**[v2: ACPI: APEI: Use ERST timeout for slow devices](http://lore.kernel.org/linux-hardening/20230712223448.145079-1-jeshuas@nvidia.com/)**

> Slow devices such as flash may not meet the default 1ms timeout value,
> so use the ERST max execution time value that they provide as the
> timeout if it is larger.
>

**[v3: pstore: Replace crypto API compression with zlib calls](http://lore.kernel.org/linux-hardening/20230712162332.2670437-1-ardb@kernel.org/)**

> The pstore layer implements support for compression of kernel log
> output, using a variety of compression algorithms provided by the
> [deprecated] crypto API 'comp' interface.
>
> This appears to have been somebody's pet project rather than a solution
> to a real problem: the original deflate compression is reasonably fast,
> compresses well and is comparatively small in terms of code footprint,
> and so the flexibility that the crypto API integration provides does
> little more than complicate the code for no reason.
>

**[v1: libxfs: Redefine 1-element arrays as flexible arrays](http://lore.kernel.org/linux-hardening/20230711222025.never.220-kees@kernel.org/)**

> To allow for code bases that include libxfs (e.g. the Linux kernel) and
> build with strict flexible array handling (-fstrict-flex-arrays=3),
> FORTIFY_SOURCE, and/or UBSAN bounds checking, redefine the remaining
> 1-element trailing arrays as true flexible arrays, but without changing
> their structure sizes. This is done via a union to retain a single element
> (named "legacy_padding"). As not all distro headers may yet have the
> UAPI stddef.h __DECLARE_FLEX_ARRAY macro, include it explicitly in
> platform_defs.h.in.
>

**[v1: wifi: mwifiex: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230710030625.812707-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

#### 异步 IO

**[v1: io_uring: treat -EAGAIN for REQ_F_NOWAIT as final for io-wq](http://lore.kernel.org/io-uring/363d8e40-6acc-57bd-feb1-4dbd50e15c31@kernel.dk/)**

> io-wq assumes that an issue is blocking, but it may not be if the
> request type has asked for a non-blocking attempt. If we get
> -EAGAIN for that case, then we need to treat it as a final result
> and not retry or arm poll for it.
>

**[v1: io_uring: Use io_schedule* in cqring wait](http://lore.kernel.org/io-uring/20230718194920.1472184-2-axboe@kernel.dk/)**

> I observed poor performance of io_uring compared to synchronous IO. That
> turns out to be caused by deeper CPU idle states entered with io_uring,
> due to io_uring using plain schedule(), whereas synchronous IO uses
> io_schedule().
>

**[v1: io_uring: don't audit the capability check in io_uring_create()](http://lore.kernel.org/io-uring/20230718115607.65652-1-omosnace@redhat.com/)**

> The check being unconditional may lead to unwanted denials reported by
> LSMs when a process has the capability granted by DAC, but denied by an
> LSM. In the case of SELinux such denials are a problem, since they can't
> be effectively filtered out via the policy and when not silenced, they
> produce noise that may hide a true problem or an attack.
>

**[v1: io_uring: Redefined the meaning of io_alloc_async_data's return value](http://lore.kernel.org/io-uring/20230710090957.10463-1-luhongfei@vivo.com/)**

> Usually, successful memory allocation returns true and failure returns false,
> which is more in line with the intuitive perception of most people. So it
> is necessary to redefine the meaning of io_alloc_async_data's return value.
>

#### Rust For Linux

**[v1: rust: kunit: Support KUnit tests with a user-space like syntax](http://lore.kernel.org/rust-for-linux/20230720-rustbind-v1-0-c80db349e3b5@google.com/)**

> This series was originally written by Jos&#233; Exp&#243;sito, and can be found
> here:
> https://github.com/Rust-for-Linux/linux/pull/950
>
> Add support for writing KUnit tests in Rust. While Rust doctests are
> already converted to KUnit tests and run, they're really better suited
> for examples, rather than as first-class unit tests.
>

**[v1: rust: doctests: Use tabs for indentation in generated C code](http://lore.kernel.org/rust-for-linux/20230720062939.2411889-1-davidgow@google.com/)**

> While Rust uses 4 spaces for indentation, we should use tabs in the
> generated C code. This does result in some scary-looking tab characters
> in a .rs file, but they're in a string literal, so shouldn't make
> anything complain too much.
>

**[v2: Quality of life improvements for pin-init](http://lore.kernel.org/rust-for-linux/20230719141918.543938-1-benno.lossin@proton.me/)**

> This patch series adds several improvements to the pin-init api:
> - a derive macro for the `Zeroable` trait,
> - makes hygiene of fields in initializers behave like normal struct
>   initializers would behave,
> - prevent stackoverflow without optimizations
> - add `..Zeroable::zeroed()` syntax to zero missing fields.
> - support arbitrary paths in initializer macros
>

**[v1: kbuild: rust: avoid creating temporary files](http://lore.kernel.org/rust-for-linux/20230718055235.1050223-1-ojeda@kernel.org/)**

> `rustc` outputs by default the temporary files (i.e. the ones saved
> by `-Csave-temps`, such as `*.rcgu*` files) in the current working
> directory when `-o` and `--out-dir` are not given (even if
> `--emit=x=path` is given, i.e. it does not use those for temporaries).
>

**[v1: rust: init: Implement Zeroable::zeroed()](http://lore.kernel.org/rust-for-linux/20230714-zeroed-v1-1-494d6820d61b@asahilina.net/)**

> By analogy to Default::default(), this just returns the zeroed
> representation of the type directly. init::zeroed() is the version that
> returns an initializer.
>

**[v2: Rust abstractions for Crypto API](http://lore.kernel.org/rust-for-linux/20230710102225.155019-1-fujita.tomonori@gmail.com/)**

> This patchset adds minimum Rust abstractions for Crypto API; message
> digest and random number generator.
>

**[v1: rust: add improved version of `ForeignOwnable::borrow_mut`](http://lore.kernel.org/rust-for-linux/20230710074642.683831-1-aliceryhl@google.com/)**

> Previously, the `ForeignOwnable` trait had a method called `borrow_mut`
> that was intended to provide mutable access to the inner value. However,
> the method accidentally made it possible to change the address of the
> object being modified, which usually isn't what we want. (And when we
> want that, it can be done by calling `from_foreign` and `into_foreign`,
> like how the old `borrow_mut` was implemented.)
>

**[v2: Rust abstractions for network device drivers](http://lore.kernel.org/rust-for-linux/20230710073703.147351-1-fujita.tomonori@gmail.com/)**

> This patchset adds minimum Rust abstractions for network device
> drivers and an example of a Rust network device driver, a simpler
> version of drivers/net/dummy.c.
>

#### BPF

**[v1: bpf: bpf/memalloc: Allow non-atomic alloc_bulk](http://lore.kernel.org/bpf/cover.1689885610.git.zhuyifei@google.com/)**

> This series attempts to add ways where the allocation could occur
> non-atomically, allowing the allocator to take mutexes, perform IO,
> and/or sleep.
>

**[v1: dwarves: dwarves: detect BTF kinds supported by kernel](http://lore.kernel.org/bpf/20230720201443.224040-1-alan.maguire@oracle.com/)**

> When a newer pahole is run on an older kernel, it often knows about BTF
> kinds that the kernel does not support, and adds them to the BTF
> representation.  This is a problem because the BTF generated is then
> embedded in the kernel image.  When it is later read - possibly by
> a different older toolchain or by the kernel directly - it is not usable.
>

**[v3: bpf-next: bpf: Support new insns from cpu v4](http://lore.kernel.org/bpf/20230720000103.99949-1-yhs@fb.com/)**

> This patch set added kernel support for insns proposed in [1] except
> BPF_ST which already has full kernel support. Beside the above proposed
> insns, LLVM will generate BPF_ST insn as well under -mcpu=v4 ([2]).
>

**[v2: bpf-next: selftests/bpf: improve ringbuf benchmark output](http://lore.kernel.org/bpf/20230719201533.176702-1-awerner32@gmail.com/)**

> The ringbuf benchmarks print headers for each section of benchmarks.
> The naming conventions lead a user of the benchmarks to some confusion.
> This change is a cosmetic update to the output of that benchmark; no
> changes were made to what the script actually executes.
>

**[v3: bpf-next: XDP metadata via kfuncs for ice](http://lore.kernel.org/bpf/20230719183734.21681-1-larysa.zaremba@intel.com/)**

> This series introduces XDP hints via kfuncs [0] to the ice driver.
>
> Series brings the following existing hints to the ice driver:
>  - HW timestamp
>  - RX hash with type
>

**[v1: bpf-next: bpf: sync tools/ uapi header with](http://lore.kernel.org/bpf/20230719162257.20818-1-alan.maguire@oracle.com/)**

> Seeing the following:
>
> Warning: Kernel ABI header at 'tools/include/uapi/linux/bpf.h' differs from latest version at 'include/uapi/linux/bpf.h'
>
> ...so sync tools version missing some list_node/rb_tree fields.
>

**[v6: bpf-next: BPF link support for tc BPF programs](http://lore.kernel.org/bpf/20230719140858.13224-1-daniel@iogearbox.net/)**

> This series adds BPF link support for tc BPF programs. We initially
> presented the motivation, related work and design at last year's LPC
> conference in the networking & BPF track [0], and a recent update on
> our progress of the rework during this year's LSF/MM/BPF summit [1].
> The main changes are in first two patches and the last two have an
> extensive batch of test cases we developed along with it, please see
> individual patches for details. We tested this series with tc-testing
> selftest suite as well as BPF CI/selftests. Thanks!
>

**[v7: bpf-next: xsk: multi-buffer support](http://lore.kernel.org/bpf/20230719132421.584801-1-maciej.fijalkowski@intel.com/)**

> This series of patches add multi-buffer support for AF_XDP. XDP and
> various NIC drivers already have support for multi-buffer packets. With
> this patch set, programs using AF_XDP sockets can now also receive and
> transmit multi-buffer packets both in copy as well as zero-copy mode.
> ZC multi-buffer implementation is based on ice driver.
>

**[v1: net-next: page_pool: split types and declarations from page_pool.h](http://lore.kernel.org/bpf/20230719121339.63331-1-linyunsheng@huawei.com/)**

> Split types and pure function declarations from page_pool.h
> and add them in page_page_types.h, so that C sources can
> include page_pool.h and headers should generally only include
> page_pool_types.h as suggested by jakub.
>

**[v1: bpf-next: bpf, x86: initialize the variable "first_off" in save_args()](http://lore.kernel.org/bpf/20230719110330.2007949-1-imagedong@tencent.com/)**

> As Dan Carpenter reported, the variable "first_off" which is passed to
> clean_stack_garbage() in save_args() can be uninitialized, which can
> cause runtime warnings with KMEMsan. Therefore, init it with 0.
>

**[v2: bpf-next: allow bpf_map_sum_elem_count for all program types](http://lore.kernel.org/bpf/20230719092952.41202-1-aspsk@isovalent.com/)**

> This series is a follow up to the recent change [1] which added
> per-cpu insert/delete statistics for maps. The bpf_map_sum_elem_count
> kfunc presented in the original series was only available to tracing
> programs, so let's make it available to all.
>

**[v12: vhost: virtio core prepares for AF_XDP](http://lore.kernel.org/bpf/20230719040422.126357-1-xuanzhuo@linux.alibaba.com/)**

> So rethinking this, firstly, we can support premapped-dma only for devices with
> VIRTIO_F_ACCESS_PLATFORM. In the case of af-xdp, if the users want to use it,
> they have to update the device to support VIRTIO_F_RING_RESET, and they can also
> enable the device's VIRTIO_F_ACCESS_PLATFORM feature.
>

**[v2: net: bpf: do not return NET_XMIT_xxx values on bpf_redirect](http://lore.kernel.org/bpf/ZLdY6JkWRccunvu0@debian.debian/)**

> skb_do_redirect handles returns error code from both rx and tx path. The
> tx path codes are special, e.g. NET_XMIT_CN: they are non-negative, and
> can conflict with LWTUNNEL_XMIT_xxx values. Directly returning such code
> can cause unexpected behavior. We found at least one bug that will panic
> the kernel through KASAN report when we are redirecting packets to a
> down or carrier-down device at lwt xmit hook:
>
> https://gist.github.com/zhaiyan920/8fbac245b261fe316a7ef04c9b1eba48
>

**[v5: net-next: virtio/vsock: support datagrams](http://lore.kernel.org/bpf/20230413-b4-vsock-dgram-v5-0-581bd37fdb26@bytedance.com/)**

> This series introduces support for datagrams to virtio/vsock.
>
> It is a spin-off (and smaller version) of this series from the summer:
>   https://lore.kernel.org/all/cover.1660362668.git.bobby.eshleman@bytedance.com/
>
> Please note that this is an RFC and should not be merged until
> associated changes are made to the virtio specification, which will
> follow after discussion from this series.
>

**[v1: bpf-next: bpf, net: Introduce skb_pointer_if_linear().](http://lore.kernel.org/bpf/20230718234021.43640-1-alexei.starovoitov@gmail.com/)**

> Network drivers always call skb_header_pointer() with non-null buffer.
> Remove !buffer check to prevent accidental misuse of skb_header_pointer().
> Introduce skb_pointer_if_linear() instead.
>

**[v1: V2,net-next: net: mana: Add page pool for RX buffers](http://lore.kernel.org/bpf/1689716837-22859-1-git-send-email-haiyangz@microsoft.com/)**

> Add page pool for RX buffers for faster buffer cycle and reduce CPU
> usage.
>
> The standard page pool API is used.
>

**[v1: bpf: lwt: do not return NET_XMIT_xxx values on bpf_redirect](http://lore.kernel.org/bpf/ZLbYdpWC8zt9EJtq@debian.debian/)**

> skb_do_redirect handles returns error code from both rx and tx path.
> The tx path codes are special, e.g. NET_XMIT_CN: they are
> non-negative, and can conflict with LWTUNNEL_XMIT_xxx values. Directly
> returning such code can cause unexpected behavior. 
>

**[v5: bpf-next: bpf: Force to MPTCP](http://lore.kernel.org/bpf/3076188eb88cca9151a2d12b50ba1e870b11ce09.1689693294.git.geliang.tang@suse.com/)**

> As is described in the "How to use MPTCP?" section in MPTCP wiki [1]:
>
> "Your app can create sockets with IPPROTO_MPTCP as the proto:
> ( socket(AF_INET, SOCK_STREAM, IPPROTO_MPTCP); ). Legacy apps can be
> forced to create and use MPTCP sockets instead of TCP ones via the
> mptcpize command bundled with the mptcpd daemon."
>

**[v2: bpf-next: BPF Refcount followups 2: owner field](http://lore.kernel.org/bpf/20230718083813.3416104-1-davemarchevsky@fb.com/)**

> This series adds an 'owner' field to bpf_{list,rb}_node structs, to be
> used by the runtime to determine whether insertion or removal operations
> are valid in shared ownership scenarios. Both the races which the series
> fixes and the fix itself are inspired by Kumar's suggestions in [0].
>

**[v1: net: igc: Prevent garbled TX queue with XDP ZEROCOPY](http://lore.kernel.org/bpf/20230717175444.3217831-1-anthony.l.nguyen@intel.com/)**

> In normal operation, each populated queue item has
> next_to_watch pointing to the last TX desc of the packet,
> while each cleaned item has it set to 0. In particular,
> next_to_use that points to the next (necessarily clean)
> item to use has next_to_watch set to 0.
>

**[v2: net-next: virtio_net: add per queue interrupt coalescing support](http://lore.kernel.org/bpf/20230717143037.21858-1-gavinl@nvidia.com/)**

> Currently, coalescing parameters are grouped for all transmit and receive
> virtqueues. This patch series add support to set or get the parameters for
> a specified virtqueue.
>
> When the traffic between virtqueues is unbalanced, for example, one virtqueue
> is busy and another virtqueue is idle, then it will be very useful to
> control coalescing parameters at the virtqueue granularity.
>

### 周边技术动态

#### Qemu

**[v5: for-8.2: riscv: add 'max' CPU, deprecate 'any'](http://lore.kernel.org/qemu-devel/20230720171933.404398-1-dbarboza@ventanamicro.com/)**

> I'm sending this new version based on another observation I made during
> another follow-up work (I'll post it shortly).
>
> 'mmu' and 'pmp' aren't really extensions in the most tradicional sense,
> they're more like features. So, in patch 1, I moved both to the new
> riscv_cpu_options array.
>

**[v1: target/riscv: add missing riscv,isa strings](http://lore.kernel.org/qemu-devel/20230720132424.371132-1-dbarboza@ventanamicro.com/)**

> Found these 2 instances while working in more 8.2 material.
>
> I believe both are safe for freeze but I won't lose my sleep if we
> decide to postpone it.
>

**[v1: QEMU RISC-V IOMMU Support](http://lore.kernel.org/qemu-devel/cover.1689819031.git.tjeznach@rivosinc.com/)**

> This series introduces a RISC-V IOMMU device emulation implementation with two stage
> address translation logic, device and process translation context mapping and queue
> interfaces, along with riscv/virt machine bindings (patch 5) and memory attributes
> extensions for PASID support (patch 3,4).
>
> This series is based on incremental patches created during RISC-V International IOMMU
> Task Group discussions and specification development process, with original series
> available in the the maintainer's repository branch [2].
>

**[v1: riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20230719044538.2013401-1-alistair.francis@wdc.com/)**

> The following changes since commit 361d5397355276e3007825cc17217c1e4d4320f7:
>
>   Merge tag 'block-pull-request' of https://gitlab.com/stefanha/qemu into staging (2023-07-17 15:49:27 +0100)
>
> are available in the Git repository at:
>
>   https://github.com/alistair23/qemu.git tags/pull-riscv-to-apply-20230719-1
>
> for you to fetch changes up to 32be32509987fbe42cf5c2fd3cea3c2ad6eae179:
>
>   target/riscv: Fix LMUL check to use VLEN (2023-07-19 14:37:26 +1000)
>

**[v1: risc-v: Add ISA extension smcntrpmf support](http://lore.kernel.org/qemu-devel/cover.1689631639.git.kaiwenx@rivosinc.com/)**

> This patch series adds the support for RISC-V ISA extension smcntrpmf (cycle and
> privilege mode filtering) [1]. QEMU only calculates dummy cycles and
> instructions, so there is no actual means to stop the icount in QEMU. Therefore,
> this series only add the read/write behavior of the relevant CSRs such that the
> implemented firmware support [2] can work without causing unnecessary illegal
> instruction exceptions.
>
> [1] https://github.com/riscv/riscv-smcntrpmf
> [2] https://github.com/rivosinc/opensbi/tree/dev/kaiwenx/smcntrpmf_upstream
>

**[v1: target/riscv: Clearing the CSR values at reset and syncing the MPSTATE with the host](http://lore.kernel.org/qemu-devel/20230718130317.12545-1-18622748025@163.com/)**

> Fix the guest reboot error when using KVM
> There are two issues when rebooting a guest using KVM
> 1. When the guest initiates a reboot the host is unable to stop the vcpu
> 2. When running a SMP guest the qemu monitor system_reset causes a vcpu crash
>
> This can be fixed by clearing the CSR values at reset and syncing the
> MPSTATE with the host.
>

**[v1: for-8.2: target/riscv: add zicntr and zihpm flags](http://lore.kernel.org/qemu-devel/20230717215419.124258-1-dbarboza@ventanamicro.com/)**

> I decided to include flags for both timer/counter extensions to make it
> easier for us later on when dealing with the RVA22 profile (which
> includes both).
>
> The features were already implemented by Atish Patra some time ago, but
> back then these 2 extensions weren't introduced yet. This means that,
> aside from extra stuff in riscv,isa FDT no other functional changes were
> made.
>

**[v1: target/riscv/cpu.c: check priv_ver before auto-enable zca/zcd/zcf](http://lore.kernel.org/qemu-devel/20230717154141.60898-1-dbarboza@ventanamicro.com/)**

> Commit bd30559568 made changes in how we're checking and disabling
> extensions based on env->priv_ver. One of the changes was to move the
> extension disablement code to the end of realize(), being able to
> disable extensions after we've auto-enabled some of them.
>

**[v3: for-8.2: target/riscv: add 'max' CPU, deprecate](http://lore.kernel.org/qemu-devel/20230714174311.672359-1-dbarboza@ventanamicro.com/)**

> This version has changes suggested in v2. The most significant change is
> the deprecation of the 'any' CPU in patch 8.
>
> The reasoning behind it is that Alistair mentioned that the 'any' CPU
> intended to work like the newly added 'max' CPU, so we're better of
> removing the 'any' CPU since it'll be out of place. We can't just
> remove the CPU out of the gate so we'll have to make it do with
> deprecation first.
>

**[v6: Add RISC-V KVM AIA Support](http://lore.kernel.org/qemu-devel/20230714084429.22349-1-yongxuan.wang@sifive.com/)**

> This series adds support for KVM AIA in RISC-V architecture.
>
> In order to test these patches, we require Linux with KVM AIA support which can
> be found in the riscv_kvm_aia_hwaccel_v1 branch at
> https://github.com/avpatel/linux.git
>

**[v2: for-8.2: target/riscv: add 'max' CPU type](http://lore.kernel.org/qemu-devel/20230712205748.446931-1-dbarboza@ventanamicro.com/)**

> This second version has smalls tweak in patch 6 that I found out
> missing while chatting with Conor in the v1 review.
>

**[riscv kvm breakage](http://lore.kernel.org/qemu-devel/629afcc2-ffed-c081-9564-7faa6defc1f4@linaro.org/)**

> This breakage crept in while cross-riscv64-system was otherwise broken in configure:
>
> https://gitlab.com/qemu-project/qemu/-/jobs/4633277557#L4165
>

**[v3: target/riscv: Add Zihintntl extension ISA string to DTS](http://lore.kernel.org/qemu-devel/20230711070402.5846-1-jason.chien@sifive.com/)**

> In v2, I rebased the patch on
> https://github.com/alistair23/qemu/tree/riscv-to-apply.next
> However, I forgot to add "Reviewed-by" in v2, so I add them in v3.
>

**[v8: riscv: Add support for the Zfa extension](http://lore.kernel.org/qemu-devel/20230710071243.282464-1-christoph.muellner@vrull.eu/)**

> Since QEMU does not support the RISC-V quad-precision floating-point
> ISA extension (Q), this patch does not include the instructions that
> depend on this extension. All other instructions are included in this
> patch.
>

#### Buildroot

**[boot/edk2: bump to version edk2-stable202305](http://lore.kernel.org/buildroot/20230713181301.6CE2486F7D@busybox.osuosl.org/)**

> The main motivation of this bump is the RISC-V QEMU Virt support
> introduced in edk2-stable202302 (not yet supported in Buildroot).
>

#### U-Boot

**[v7: Add StarFive JH7110 PCIe drvier support](http://lore.kernel.org/u-boot/20230720112333.9255-1-minda.chen@starfivetech.com/)**

> These PCIe series patches are based on the JH7110 RISC-V SoC and VisionFive V2 board.
>
> The PCIe driver depends on gpio, pinctrl, clk and reset driver to do init.
> The PCIe dts configuation includes all these setting.
>
> The PCIe drivers codes has been tested on the VisionFive V2 boards.
> The test devices includes M.2 NVMe SSD and Realtek 8169 Ethernet adapter.
>

**[Pull request: u-boot-spi/master](http://lore.kernel.org/u-boot/20230713163628.1763568-1-jagan@amarulasolutions.com/)**

> The following changes since commit bf5152d0108683bbaabf9d7a7988f61649fc33f4:
>
>   Merge branch 'master' of https://source.denx.de/u-boot/custodians/u-boot-riscv (2023-07-12 13:10:04 -0400)
>
> are available in the Git repository at:
>
>   https://source.denx.de/u-boot/custodians/u-boot-spi master
>
> for you to fetch changes up to 4a31e145217cecc3d421f96eafcd2cfd9c670929:
>
>   mtd: spi-nor: Add support for w25q256jwm (2023-07-13 14:17:40 +0530)
>

**[Please pull u-boot-marvell/master](http://lore.kernel.org/u-boot/6bebb605-7a3c-0281-d12d-cda1721492fe@denx.de/)**

> please pull the following Marvell MVEBU related patches into master:
>
> - mvebu: Thecus: Misc enhancement and cleanup (Tony)
> - mvebu: Add AC5X Allied Telesis x240 board support incl NAND
>    controller enhancements for this SoC (Chris)
>
> Here the Azure build, without any issues:
>
> https://dev.azure.com/sr0718/u-boot/_build/results?buildId=305&view=results
>

**[v2: riscv: Initial support for Lichee PI 4A board](http://lore.kernel.org/u-boot/20230708112435.23583-1-dlan@gentoo.org/)**

> Sipeed's Lichee PI 4A board is based on T-HEAD's TH1520 SoC which consists of
> quad core XuanTie C910 CPU, plus one C906 CPU and one E902 CPU.
>
> In this series, we add a basic device tree, including UART CPU, PLIC, make it
> capable of running into a serial console.
>

## 20230709：第 53 期

### 内核动态

#### RISC-V 架构支持

**[v3: RISC-V: archrandom support](http://lore.kernel.org/linux-riscv/20230709115549.2666557-1-sameo@rivosinc.com/)**

> This patchset adds support for the archrandom API to the RISC-V
> architecture.
>
> The ratified crypto scalar extensions provide entropy bits via the seed
> CSR, as exposed by the Zkr extension.
>

**[v1: riscv: support PREEMPT_DYNAMIC with static keys](http://lore.kernel.org/linux-riscv/20230709101653.720-1-jszhang@kernel.org/)**

> Currently, each architecture can support PREEMPT_DYNAMIC through
> either static calls or static keys. To support PREEMPT_DYNAMIC on
> riscv, we face three choices:
>
> 1. only add static calls support to riscv
> As Mark pointed out in commit 99cf983cc8bc ("sched/preempt: Add
> PREEMPT_DYNAMIC using static keys"), static keys "...should have
> slightly lower overhead than non-inline static calls, as this
> effectively inlines each trampoline into the start of its callee. This
> may avoid redundant work, and may integrate better with CFI schemes."
> So even we add static calls(without inline static calls) to riscv,
> static keys is still a better choice.
>
> 2. add static calls and inline static calls to riscv
> Per my understanding, inline static calls requires objtool support
> which is not easy.
>

**[v4: RISC-V: mm: Make SV48 the default address space](http://lore.kernel.org/linux-riscv/20230708011156.2697409-1-charlie@rivosinc.com/)**

> Make sv48 the default address space for mmap as some applications
> currently depend on this assumption. Also enable users to select
> desired address space using a non-zero hint address to mmap. Previous
> kernel changes caused Java and other applications to be broken on sv57
> which this patch fixes.
>

**[v2: module: Ignore RISC-V mapping symbols too](http://lore.kernel.org/linux-riscv/20230707160051.2305-2-palmer@rivosinc.com/)**

> RISC-V has an extended form of mapping symbols that we use to encode
> the ISA when it changes in the middle of an ELF.  This trips up modpost
> as a build failure, I haven't yet verified it yet but I believe the
> kallsyms difference should result in stacks looking sane again.
>

**[GIT PULL: RISC-V Patches for the 6.5 Merge Window, Part 2](http://lore.kernel.org/linux-riscv/mhng-4bd23a4e-dd7c-4f62-90c8-804c137c2621@palmer-ri-x1c9/)**

> merged tag 'riscv-for-linus-6.5-mw1'
> The following changes since commit 533925cb760431cb496a8c965cfd765a1a21d37e:
>
>   Merge tag 'riscv-for-linus-6.5-mw1' of git://git.kernel.org/pub/scm/linux/kernel/git/riscv/linux (2023-06-30 09:37:26 -0700)
>
> are available in the Git repository at:
>
>   git://git.kernel.org/pub/scm/linux/kernel/git/riscv/linux.git tags/riscv-for-linus-6.5-mw2
>

**[v6: tools/nolibc: add a new syscall helper](http://lore.kernel.org/linux-riscv/cover.1688739492.git.falcon@tinylab.org/)**

> Here is the v6 of the __sysret series [1], applies your suggestions.
> additionally, the sbrk() also uses the __sysret helper.
>

**[v1: RISC-V: Support querying vendor extensions](http://lore.kernel.org/linux-riscv/20230705-thead_vendor_extensions-v1-0-ad6915349c4d@rivosinc.com/)**

> Introduce extensible method of querying vendor extensions. Keys above
> 1UL<<63 passed into the riscv_hwprobe syscall are reserved for vendor
> extensions. The appropriate vendor is resolved using the discovered
> mvendorid. Vendor specific code is then entered which determines how to
> respond to the input hwprobe key.
>

**[v2: RISC-V: Show accurate per-hart isa in /proc/cpuinfo](http://lore.kernel.org/linux-riscv/20230705172931.1099183-1-evan@rivosinc.com/)**

> In /proc/cpuinfo, most of the information we show for each processor is
> specific to that hart: marchid, mvendorid, mimpid, processor, hart,
> compatible, and the mmu size. But the ISA string gets filtered through a
> lowest common denominator mask, so that if one CPU is missing an ISA
> extension, no CPUs will show it.
>

**[v3: Obtain SMBIOS and ACPI entry from FFI](http://lore.kernel.org/linux-riscv/20230705114251.661-1-cuiyunhui@bytedance.com/)**

> Here's version 3 of patch series.
>

**[v1: RISC-V: KVM: provide UAPI for host SATP mode](http://lore.kernel.org/linux-riscv/20230705091535.237765-1-dbarboza@ventanamicro.com/)**

> KVM userspaces need to be aware of the host SATP to allow them to
> advertise it back to the guest OS.
>
> Since this information is used to build the guest FDT we can't wait for
> the SATP reg to be readable. We just need to read the SATP mode, thus
> we can use the existing 'satp_mode' global that represents the SATP reg
> with MODE set and both ASID and PPN cleared. E.g. for a 32 bit host
> running with sv32 satp_mode is 0x80000000, for a 64 bit host running
> sv57 satp_mode is 0xa000000000000000, and so on.
>

**[v7: -next: support allocating crashkernel above 4G explicitly on riscv](http://lore.kernel.org/linux-riscv/20230704212327.1687310-1-chenjiahao16@huawei.com/)**

> On riscv, the current crash kernel allocation logic is trying to
> allocate within 32bit addressible memory region by default, if
> failed, try to allocate without 4G restriction.
>
> In need of saving DMA zone memory while allocating a relatively large
> crash kernel region, allocating the reserved memory top down in
> high memory, without overlapping the DMA zone, is a mature solution.
> Hence this patchset introduces the parameter option crashkernel=X,[high,low].
>

#### 异步 IO

**[v1: io_uring: A new function has been defined to make get/put exist in pairs](http://lore.kernel.org/io-uring/20230706093208.6072-1-luhongfei@vivo.com/)**

> A new function called io_put_task_refs has been defined for pairing
> with io_get_task_refs.
>
> In io_submit_sqes(), when req is not fully sent(i.e. left != 0), it is
> necessary to call the io_put_task_refs() to recover the current process's
> cached_refs and pair it with the io_get_task_refs(), which is easy to
> understand and looks more regular.
>

### 周边技术动态

#### Qemu

**[v3: target/riscv: improve code accuracy and](http://lore.kernel.org/qemu-devel/20230708091055.38505-1-reaperlu@hust.edu.cn/)**

> I'm so sorry. As a newcomer, I'm not familiar with the patch mechanism. I mistakenly added the reviewer's "Reviewed-by" line into the wrong commit, So I have resent this patchset
>

**[v1: target/riscv KVM_RISCV_SET_TIMER macro is not configured correctly](http://lore.kernel.org/qemu-devel/20230707032306.4606-1-gaoshanliukou@163.com/)**

> Should set/get riscv all reg timer,i.e, time/compare/frequency/state.
>

**[v2: riscv: Generate devicetree only after machine initialization is complete](http://lore.kernel.org/qemu-devel/20230706035937.1870483-1-linux@roeck-us.net/)**

> If the devicetree is created before machine initialization is complete,
> it misses dynamic devices. Specifically, the tpm device is not added
> to the devicetree file and is therefore not instantiated in Linux.
> Load/create devicetree in virt_machine_done() to solve the problem.
>

**[v1: riscv: add config for asid size](http://lore.kernel.org/qemu-devel/20230705105838.68806-1-ben.dooks@codethink.co.uk/)**

> Add a config to the cpu state to control the size of the ASID area
> in the SATP CSR to enable testing with smaller than the default (which
> is currently maximum for both rv32 and rv64). It also adds the ability
> to stop the ASID feature by using 0 to disable it.
>

#### U-Boot

**[v2: riscv: Initial support for Lichee PI 4A board](http://lore.kernel.org/u-boot/20230708112435.23583-1-dlan@gentoo.org/)**

> Sipeed's Lichee PI 4A board is based on T-HEAD's TH1520 SoC which consists of
> quad core XuanTie C910 CPU, plus one C906 CPU and one E902 CPU.
>
> In this series, we add a basic device tree, including UART CPU, PLIC, make it
> capable of running into a serial console.
>
> Please note that, we rely on pre shipped vendor u-boot which run in M-Mode to
> chain load this mainline u-boot either via eMMC storage or from tftp, thus the
> pinctrl and clock setting are not implemented in this series, which certainly
> can be improved later accordingly.
>

**[v1: riscv: (visionfive2:) device tree binding for riscv_timer](http://lore.kernel.org/u-boot/20230707135333.GA30112@lst.de/)**

> following the existing device tree binding[1], here is a draft to use it
> in drivers/timer/riscv_timer.c. This would also fix the regression we see
> with commit 55171aedda8 ("dm: Emit the arch_cpu_init_dm() even only
> before relocation"), at least on the VisionFive2, as sketched out below.
> The device tree addition suits the Linux kernel dirver
>

**[v1: u-boot-riscv/riscv-for-next](http://lore.kernel.org/u-boot/ZKabX3HI7USoCEEt@ubuntu01/)**

> The following changes since commit e80f4079b3a3db0961b73fa7a96e6c90242d8d25:
>
>   Merge tag 'v2023.07-rc6' into next (2023-07-05 11:28:55 -0400)
>
> are available in the Git repository at:
>
>   https://source.denx.de/u-boot/custodians/u-boot-riscv.git riscv-for-next
>

## 20230705：第 52 期

### 内核动态

#### RISC-V 架构支持

**[v7: -next: support allocating crashkernel above 4G explicitly on riscv](http://lore.kernel.org/linux-riscv/20230704212327.1687310-1-chenjiahao16@huawei.com/)**
1 
> On riscv, the current crash kernel allocation logic is trying to
> allocate within 32bit addressible memory region by default, if
> failed, try to allocate without 4G restriction.
>

**[v1: riscv: Start of DRAM should at least be aligned on PMD size for the direct mapping](http://lore.kernel.org/linux-riscv/20230704121837.248976-1-alexghiti@rivosinc.com/)**

> So that we do not end up mapping the whole linear mapping using 4K
> pages, which is slow at boot time, and also very likely at runtime.
>
> So make sure we align the start of DRAM on a PMD boundary.
>

**[v4: Add initialization of clock for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230704091948.85247-4-william.qiu@starfivetech.com/)**

> This patchset adds initial rudimentary support for the StarFive
> Quad SPI controller driver. And this driver will be used in
> StarFive's VisionFive 2 board. In 6.4, the QSPI_AHB and QSPI_APB
> clocks changed from the default ON state to the default OFF state,
> so these clocks need to be enabled in the driver.At the same time,
> dts patch is added to this series.
>

**[v1: Add SPI module for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230704091948.85247-1-william.qiu@starfivetech.com/)**

> This patchset adds initial rudimentary support for the StarFive
> SPI controller. And this driver will be used in StarFive's
> VisionFive 2 board. The first patch constrain minItems of clocks
> for JH7110 SPI and Patch 2 adds support for StarFive JH7110 SPI.
>

**[v6: Add PLL clocks driver and syscon for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230704064610.292603-1-xingyu.wu@starfivetech.com/)**

> This patch serises are to add PLL clocks driver and providers by writing
> and reading syscon registers for the StarFive JH7110 RISC-V SoC. And add
> documentation and nodes to describe StarFive System Controller(syscon)
> Registers. This patch serises are based on Linux 6.4.
>

**[v4: riscv: Allow userspace to directly access perf counters](http://lore.kernel.org/linux-riscv/20230703124647.215952-1-alexghiti@rivosinc.com/)**

> riscv used to allow direct access to cycle/time/instret counters,
> bypassing the perf framework, this patchset intends to allow the user to
> mmap any counter when accessed through perf. But we can't break the
> existing behaviour so we introduce a sysctl perf_user_access like arm64
> does, which defaults to the legacy mode described above.
>

**[v3: RISC-V: Probe DT extension support using riscv,isa-extensions & riscv,isa-base](http://lore.kernel.org/linux-riscv/20230703-repayment-vocalist-e4f3eeac2b2a@wendy/)**

> Based on my latest iteration of deprecating riscv,isa [1], here's an
> implementation of the new properties for Linux. The first few patches,
> up to "RISC-V: split riscv_fill_hwcap() in 3", are all prep work that
> further tames some of the extension related code, on top of my already
> applied series that cleans up the ISA string parser.
> Perhaps "RISC-V: shunt isa_ext_arr to cpufeature.c" is a bit gratuitous,
> but I figured a bit of coalescing of extension related data structures
> would be a good idea. Note that riscv,isa will still be used in the
> absence of the new properties. Palmer suggested adding a Kconfig option
> to turn off the fallback for DT, which I have gone and done. It's locked
> behind the NONPORTABLE option for good reason.
>

**[v1: riscv: optimize ELF relocation function in riscv](http://lore.kernel.org/linux-riscv/1688355132-62933-1-git-send-email-lixiaoyun@binary-semi.com/)**

> The patch can optimize the running times of insmod command by modify ELF
> relocation function.
> In the 5.10 and latest kernel, when install the riscv ELF drivers which
> contains multiple symbol table items to be relocated, kernel takes a lot
> of time to execute the relocation. For example, we install a 3+MB driver
> need 180+s.
> We focus on the riscv architecture handle R_RISCV_HI20 and R_RISCV_LO20
> type items relocation function in the arch\riscv\kernel\module.c and
> find that there are two-loops in the function. If we modify the begin
> number in the second for-loops iteration, we could save significant time
> for installation. We install the same 3+MB driver could just need 2s.
>

**[v10: Add non-coherent DMA support for AX45MP](http://lore.kernel.org/linux-riscv/20230702203429.237615-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)**

> On the Andes AX45MP core, cache coherency is a specification option so it
> may not be supported. In this case DMA will fail. To get around with this
> issue this patch series does the below:
>
> 1] Andes alternative ports is implemented as errata which checks if the
> IOCP is missing and only then applies to CMO errata. One vendor specific
> SBI EXT (ANDES_SBI_EXT_IOCP_SW_WORKAROUND) is implemented as part of
> errata.
>

**[v5: dt-bindings: riscv: deprecate riscv,isa](http://lore.kernel.org/linux-riscv/20230702-eats-scorebook-c951f170d29f@spud/)**

> When the RISC-V dt-bindings were accepted upstream in Linux, the base
> ISA etc had yet to be ratified. By the ratification of the base ISA,
> incompatible changes had snuck into the specifications - for example the
> Zicsr and Zifencei extensions were spun out of the base ISA.
>

**[v5: RISCV: Add KVM_GET_REG_LIST API](http://lore.kernel.org/linux-riscv/cover.1688010022.git.haibo1.xu@intel.com/)**

> KVM_GET_REG_LIST will dump all register IDs that are available to
> KVM_GET/SET_ONE_REG and It's very useful to identify some platform
> regression issue during VM migration.
>

**[GIT PULL: RISC-V Patches for the 6.5 Merge Window, Part 1](http://lore.kernel.org/linux-riscv/mhng-ebcc1b82-5dd0-4f2d-824e-8d9250374abf@palmer-ri-x1c9/)**

> The following changes since commit ac9a78681b921877518763ba0e89202254349d1b:
>
>   Linux 6.4-rc1 (2023-05-07 13:34:35 -0700)
>
> are available in the Git repository at:
>
>   git://git.kernel.org/pub/scm/linux/kernel/git/riscv/linux.git tags/riscv-for-linus-6.5-mw1
>

**[v1: Add missing pins for RZ/Five SoC](http://lore.kernel.org/linux-riscv/20230630120433.49529-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)**

> This patch series intends to incorporate the absent port pins P19 to P28,
> which are exclusively available on the RZ/Five SoC.
>

**[v2: riscv: Add BUG_ON() for no cpu nodes in devicetree](http://lore.kernel.org/linux-riscv/20230630105938.1377262-1-suagrfillet@gmail.com/)**

> When only the ACPI tables are passed to kernel, the tiny devictree created
> by EFI Stub doesn't provide cpu nodes.
>

**[v1: riscv: KCFI support](http://lore.kernel.org/linux-riscv/20230629234244.1752366-8-samitolvanen@google.com/)**

> This series adds KCFI support for RISC-V. KCFI is a fine-grained
> forward-edge control-flow integrity scheme supported in Clang >=16,
> which ensures indirect calls in instrumented code can only branch to
> functions whose type matches the function pointer type, thus making
> code reuse attacks more difficult.
>

**[v1: RISC-V: Provide a more helpful error message on invalid ISA strings](http://lore.kernel.org/linux-riscv/20230629223502.1924-1-palmer@rivosinc.com/)**

> This adds a warning for the cases where the ISA string isn't valid.  It's still
> above the BUG_ON cut, but hopefully it's at least a bit easier for users.
>

**[v4: riscv: Discard vector state on syscalls](http://lore.kernel.org/linux-riscv/20230629142228.1125715-1-bjorn@kernel.org/)**

> The RISC-V vector specification states:
>   Executing a system call causes all caller-saved vector registers
>   (v0-v31, vl, vtype) and vstart to become unspecified.
>
> The vector registers are set to all 1s, vill is set (invalid), and the
> vector status is set to Dirty.
>

**[v1: arch,fbdev: Move screen_info into arch/](http://lore.kernel.org/linux-riscv/20230629121952.10559-1-tzimmermann@suse.de/)**

> The variables screen_info and edid_info provide information about
> the system's screen, and possibly EDID data of the connected display.
> Both are defined and set by architecture code. But both variables are
> declared in non-arch header files. Dependencies are at bease loosely
> tracked. To resolve this, move the global state screen_info and its
> companion edid_info into arch/. Only declare them on architectures
> that define them. List dependencies on the variables in the Kconfig
> files. Also clean up the callers.
>

**[v1: riscv: BUG_ON() for no cpu nodes in setup_smp](http://lore.kernel.org/linux-riscv/20230629105839.1160895-1-suagrfillet@gmail.com/)**

> When booting with ACPI tables, the tiny devictree created by
> EFI Stub doesn't provide cpu nodes.
>
> In setup_smp(), of_parse_and_init_cpus() will bug on !found_boot_cpu
> if acpi_disabled. That's unclear, so bug for no cpu nodes before
> of_parse_and_init_cpus().
>

**[v8: Add JH7110 USB PHY driver support](http://lore.kernel.org/linux-riscv/20230629075115.11934-1-minda.chen@starfivetech.com/)**

> This patchset adds USB and PCIe PHY for the StarFive JH7110 SoC.
> The patch has been tested on the VisionFive 2 board.
>

**[v1: RISC-V: Document the ISA string parsing rules for ACPI](http://lore.kernel.org/linux-riscv/20230629031705.15575-1-palmer@rivosinc.com/)**

> We've had a ton of issues around the ISA string parsing rules elsewhere
> in RISC-V, so let's at least be clear about what the rules are so we can
> try and avoid more issues.
>

**[v1: tools/nolibc: shrink arch support](http://lore.kernel.org/linux-riscv/cover.1687976753.git.falcon@tinylab.org/)**

> This patchset further improves porting of nolibc to new architectures,
> it is based on our previous v5 sysret helper series [1].
>
> It mainly shrinks the assembly _start by moving most of its operations
> to a C version of _start_c() function. and also, it removes the old
> sys_stat() support by using the sys_statx() instead and therefore,
> removes all of the arch specific sys_stat_struct.
>

**[v2: RISC-V: archrandom support](http://lore.kernel.org/linux-riscv/20230628131442.3022772-1-sameo@rivosinc.com/)**

> This patchset adds support for the archrandom API to the RISC-V
> architecture.
>
> The ratified crypto scalar extensions provide entropy bits via the seed
> CSR, as exposed by the Zkr extension.
>

**[v5: tools/nolibc: add a new syscall helper](http://lore.kernel.org/linux-riscv/cover.1687957589.git.falcon@tinylab.org/)**

> It mainly applies the core part of suggestions from Thomas (Many thanks)
> and cleans up the multiple whitespaces issues reported by
> scripts/checkpatch.pl.
>

**[v1: riscv: sigcontext: Correct the comment of sigreturn](http://lore.kernel.org/linux-riscv/20230628091213.2908149-1-guoren@kernel.org/)**

> The real-time signals enlarged the sigset_t type, and most architectures
> have changed to using rt_sigreturn as the only way. The riscv is one of
> them, and there is no sys_sigreturn in it. Only some old architecture
> preserved sys_sigreturn as part of the historical burden.
>

**[GIT PULL: RISC-V: make ARCH_THEAD preclude XIP_KERNEL](http://lore.kernel.org/linux-riscv/20230628-left-attractor-94b7bd5fbb83@wendy/)**

> Randy reported build errors in linux-next where XIP_KERNEL was enabled.
> ARCH_THEAD requires alternatives to support the non-standard ISA
> extensions used by the THEAD cores, which are mutually exclusive with
> XIP kernels. Clone the dependency list from the Allwinner entry, since
> Allwinner's D1 uses T-Head cores with the same non-standard extensions.
>

**[v1: Make SV39 the default address space](http://lore.kernel.org/linux-riscv/20230627222152.177716-1-charlie@rivosinc.com/)**

> Make sv39 the default address space for mmap as some applications
> currently depend on this assumption. The RISC-V specification enforces
> that bits outside of the virtual address range are not used, so
> restricting the size of the default address space as such should be
> temporary. A hint address passed to mmap will cause the largest address
> space that fits entirely into the hint to be used. If the hint is less
> than or equal to 1<<38, a 39-bit address will be used. After an address
> space is completely full, the next smallest address space will be used.
>

**[v3: Add support for Allwinner PWM on D1/T113s/R329 SoCs](http://lore.kernel.org/linux-riscv/20230627082334.1253020-1-privatesub2@gmail.com/)**

> This series adds support for PWM controller on new
> Allwinner's SoCs, such as D1, T113s and R329. The implemented driver
> provides basic functionality for control PWM channels.
>

#### 进程调度

**[v3: sched/core: introduce sched_core_idle_cpu()](http://lore.kernel.org/lkml/1688011324-42406-1-git-send-email-CruzZhao@linux.alibaba.com/)**

> As core scheduling introduced, a new state of idle is defined as
> force idle, running idle task but nr_running greater than zero.
>

**[v1: sched/core: Use empty mask to reset cpumasks in sched_setaffinity()](http://lore.kernel.org/lkml/20230628211637.1679348-1-longman@redhat.com/)**

> Since commit 8f9ea86fdf99 ("sched: Always preserve the user requested
> cpumask"), user provided CPU affinity via sched_setaffinity(2) is
> perserved even if the task is being moved to a different cpuset. However,
> that affinity is also being inherited by any subsequently created child
> processes which may not want or be aware of that affinity.
>

**[v3: Sched/fair: Block nohz tick_stop when cfs bandwidth in use](http://lore.kernel.org/lkml/20230628190227.894195-1-pauld@redhat.com/)**

> CFS bandwidth limits and NOHZ full don't play well together.  Tasks
> can easily run well past their quotas before a remote tick does
> accounting.  This leads to long, multi-period stalls before such
> tasks can run again. Currentlyi, when presented with these conflicting
> requirements the scheduler is favoring nohz_full and letting the tick
> be stopped. However, nohz tick stopping is already best-effort, there
> are a number of conditions that can prevent it, whereas cfs runtime
> bandwidth is expected to be enforced.
>

#### 内存管理

**[v3: MDWE without inheritance](http://lore.kernel.org/linux-mm/20230704153630.1591122-1-revest@chromium.org/)**

> Joey recently introduced a Memory-Deny-Write-Executable (MDWE) prctl which tags
> current with a flag that prevents pages that were previously not executable from
> becoming executable.
> This tag always gets inherited by children tasks. (it's in MMF_INIT_MASK)
>

**[v2: mm/slub: refactor freelist to use custom type](http://lore.kernel.org/linux-mm/20230704135834.3884421-1-matteorizzo@google.com/)**

> Currently the SLUB code represents encoded freelist entries as "void*".
> That's misleading, those things are encoded under
> CONFIG_SLAB_FREELIST_HARDENED so that they're not actually dereferencable.
>

**[v1: block: Make blkdev_get_by_*() return handle](http://lore.kernel.org/linux-mm/20230629165206.383-1-jack@suse.cz/)**

> this patch series implements the idea of blkdev_get_by_*() calls returning
> bdev_handle which is then passed to blkdev_put() [1]. This makes the get
> and put calls for bdevs more obviously matching and allows us to propagate
> context from get to put without having to modify all the users (again!).
> In particular I need to propagate used open flags to blkdev_put() to be able
> count writeable opens and add support for blocking writes to mounted block
> devices. I'll send that series separately.
>

**[v1: mm: memory-failure: add missing set_mce_nospec() for memory_failure()](http://lore.kernel.org/linux-mm/20230704121948.1331846-1-linmiaohe@huawei.com/)**

> If memory_failure() succeeds to hwpoison a page, the set_mce_nospec() is
> expected to be called to prevent speculative access to the page by marking
> it not-present. Add such missing call to set_mce_nospec() in async memory
> failure handling scene.
>

**[v1: mm: page_alloc: avoid false page outside zone error info](http://lore.kernel.org/linux-mm/20230704111823.940331-1-linmiaohe@huawei.com/)**

> If pfn is outside zone boundaries in the first round, ret will be set
> to 1. But if pfn is changed to inside the zone boundaries in zone span
> seqretry path, ret is still set to 1 leading to false page outside zone
> error info.
>

**[v3: Documentation: admin-guide: correct "it's" to possessive "its"](http://lore.kernel.org/linux-mm/20230703232024.8069-1-rdunlap@infradead.org/)**

> Correct 2 uses of "it's" to the possessive "its" as needed.
>

**[v2: variable-order, large folios for anonymous memory](http://lore.kernel.org/linux-mm/20230703135330.1865927-1-ryan.roberts@arm.com/)**

> This is v2 of a series to implement variable order, large folios for anonymous
> memory. The objective of this is to improve performance by allocating larger
> chunks of memory during anonymous page faults. See [1] for background.
>

**[[PATCH v10 rebased on v6.4 00/25] DEPT(Dependency Tracker)](http://lore.kernel.org/linux-mm/20230703094752.79269-1-byungchul@sk.com/)**

> From now on, I can work on LKML again! I'm wondering if DEPT has been
> helping kernel debugging well even though it's a form of patches yet.
>

**[v1: mm: make MEMFD_CREATE into a selectable config option](http://lore.kernel.org/linux-mm/20230630-config-memfd-v1-1-9acc3ae38b5a@weissschuh.net/)**

> The memfd_create() syscall, enabled by CONFIG_MEMFD_CREATE, is useful on
> its own even when not required by CONFIG_TMPFS or CONFIG_HUGETLBFS.
>
> Split it into its own proper bool option that can be enabled by users.
>

**[v2: Documentation: mm/memfd: vm.memfd_noexec](http://lore.kernel.org/linux-mm/20230629233454.4166842-1-jeffxu@google.com/)**

> Add documentation for sysctl vm.memfd_noexec
>
> Link:https://lore.kernel.org/linux-mm/CABi2SkXUX_QqTQ10Yx9bBUGpN1wByOi_=gZU6WEy5a8MaQY3Jw@mail.gmail.com/T/
>

**[v2: mm/slub: disable slab merging in the default configuration](http://lore.kernel.org/linux-mm/20230629221910.359711-1-julian.pidancet@oracle.com/)**

> Make CONFIG_SLAB_MERGE_DEFAULT default to n unless CONFIG_SLUB_TINY is
> enabled. Benefits of slab merging is limited on systems that are not
> memory constrained: the memory overhead is low and evidence of its
> effect on cache hotness is hard to come by.
>

**[v25: crash: Kernel handling of CPU and memory hot un/plug](http://lore.kernel.org/linux-mm/20230629192119.6613-1-eric.devolder@oracle.com/)**

> This series is dependent upon "refactor Kconfig to consolidate
> KEXEC and CRASH options".
>  https://lore.kernel.org/lkml/20230626161332.183214-1-eric.devolder@oracle.com/
>
> Once the kdump service is loaded, if changes to CPUs or memory occur,
> either by hot un/plug or off/onlining, the crash elfcorehdr must also
> be updated.
>

**[v1: mm: Always downgrade mmap_lock if requested](http://lore.kernel.org/linux-mm/20230629191414.1215929-1-willy@infradead.org/)**

> Now that stack growth must always hold the mmap_lock for write, we can
> always downgrade the mmap_lock to read and safely unmap pages from the
> page table, even if we're next to a stack.
>

**[v1: writeback: Account the number of pages written back](http://lore.kernel.org/linux-mm/20230628185548.981888-1-willy@infradead.org/)**

> nr_to_write is a count of pages, so we need to decrease it by the number
> of pages in the folio we just wrote, not by 1.  Most callers specify
> either LONG_MAX or 1, so are unaffected, but writeback_sb_inodes()
> might end up writing 512x as many pages as it asked for.
>

**[v24: crash: Kernel handling of CPU and memory hot un/plug](http://lore.kernel.org/linux-mm/20230628185215.40707-1-eric.devolder@oracle.com/)**

> This series is dependent upon "refactor Kconfig to consolidate
> KEXEC and CRASH options".
>  https://lore.kernel.org/lkml/20230626161332.183214-1-eric.devolder@oracle.com/
>
> Once the kdump service is loaded, if changes to CPUs or memory occur,
> either by hot un/plug or off/onlining, the crash elfcorehdr must also
> be updated.
>

**[v1: fs/address_space: add alignment padding for i_map and i_mmap_rwsem to mitigate a false sharing.](http://lore.kernel.org/linux-mm/20230628105624.150352-1-lipeng.zhu@intel.com/)**

> When running UnixBench/Shell Scripts, we observed high false sharing
> for accessing i_mmap against i_mmap_rwsem.
>
> UnixBench/Shell Scripts are typical load/execute command test scenarios,
> the i_mmap will be accessed frequently to insert/remove vma_interval_tree.
> Meanwhile, the i_mmap_rwsem is frequently loaded. Unfortunately, they are
> in the same cacheline.
>

**[v2: mm/slub: Optimize slub memory usage](http://lore.kernel.org/linux-mm/20230628095740.589893-1-jaypatel@linux.ibm.com/)**

> In the previous version [1], we were able to reduce slub memory
> wastage, but the total memory was also increasing so to solve
> this problem have modified the patch as follow:
>
> 1) If min_objects * object_size > PAGE_ALLOC_COSTLY_ORDER, then it
> will return with PAGE_ALLOC_COSTLY_ORDER.
> 2) Similarly, if min_objects * object_size < PAGE_SIZE, then it will
> return with slub_min_order.
> 3) Additionally, I changed slub_max_order to 2. There is no specific
> reason for using the value 2, but it provided the best results in
> terms of performance without any noticeable impact.
>

#### 文件系统

**[v2: 0/6: block: Add config option to not allow writing to mounted devices](http://lore.kernel.org/linux-fsdevel/20230704122727.17096-1-jack@suse.cz/)**

> This is second version of the patches to add config option to not allow writing
> to mounted block devices. For motivation why this is interesting see patch 1/6.
> I've been testing the patches more extensively this time and I've found couple
> of things that get broken by disallowing writes to mounted block devices:
> 1) Bind mounts get broken because get_tree_bdev() / mount_bdev() first try to
>    claim the bdev before searching whether it is already mounted. Patch 6
>    reworks the mount code to avoid this problem.
> 2) btrfs mounting is likely having the same problem as 1). It should be fixable
>    AFAICS but for now I've left it alone until we settle on the rest of the
>    series.
> 3) "mount -o loop" gets broken because util-linux keeps the loop device open
>    read-write when attempting to mount it. Hopefully fixable within util-linux.
> 4) resize2fs online resizing gets broken because it tries to open the block
>    device read-write only to call resizing ioctl. Trivial to fix within
>    e2fsprogs.
>

**[v1: block: Make blkdev_get_by_*() return handle](http://lore.kernel.org/linux-fsdevel/20230629165206.383-1-jack@suse.cz/)**

> this patch series implements the idea of blkdev_get_by_*() calls returning
> bdev_handle which is then passed to blkdev_put() [1]. This makes the get
> and put calls for bdevs more obviously matching and allows us to propagate
> context from get to put without having to modify all the users (again!).
> In particular I need to propagate used open flags to blkdev_put() to be able
> count writeable opens and add support for blocking writes to mounted block
> devices. I'll send that series separately.
>

**[v5: fanotify accounting for fs/splice.c](http://lore.kernel.org/linux-fsdevel/cover.1688393619.git.nabijaczleweli@nabijaczleweli.xyz/)**

> Previously: https://lore.kernel.org/linux-fsdevel/jbyihkyk5dtaohdwjyivambb2gffyjs3dodpofafnkkunxq7bu@jngkdxx65pux/t/#u
>
> In short:
>   * most read/write APIs generate ACCESS/MODIFY for the read/written file(s)
>   * except the [vm]splice/tee family
>     (actually, since 6.4, splice itself /does/ generate events but only
>      for the non-pipes being spliced from/to; this commit is Fixes:ed)
>   * userspace that registers (i|fa)notify on pipes usually relies on it
>     actually working (coreutils tail -f is the primo example)
>   * it's sub-optimal when someone with a magic syscall can fill up a
>     pipe simultaneously ensuring it will never get serviced
>

**[[PATCH v10 rebased on v6.4 00/25] DEPT(Dependency Tracker)](http://lore.kernel.org/linux-fsdevel/20230703094752.79269-1-byungchul@sk.com/)**

> From now on, I can work on LKML again! I'm wondering if DEPT has been
> helping kernel debugging well even though it's a form of patches yet.
>

**[GIT PULL: iomap: new code for 6.5](http://lore.kernel.org/linux-fsdevel/168831482682.535407.9162875426107097138.stg-ugh@frogsfrogsfrogs/)**

> Please pull this branch with changes for iomap for 6.5-rc1.
>
> As usual, I did a test-merge with the main upstream branch as of a few
> minutes ago, and didn't see any conflicts.  Please let me know if you
> encounter any problems.
>

**[v1: proc: proc_setattr for /proc/$PID/net](http://lore.kernel.org/linux-fsdevel/20230630140609.263790-1-falcon@tinylab.org/)**

> Just applied your patchset on v6.4, and then:
>
>   - revert the 1st patch: 'selftests/nolibc: drop test chmod_net' manually
>
>   - do the 'run' test of nolibc on arm/vexpress-a9
>

**[v3: fuse: add a new fuse init flag to relax restrictions in no cache mode](http://lore.kernel.org/linux-fsdevel/20230630094602.230573-1-hao.xu@linux.dev/)**

> Patch 1 is a fix for private mmap in FOPEN_DIRECT_IO mode
>   This is added here together since the later two depends on it.
> Patch 2 is the main dish
> Patch 3 is to maintain direct write logic for shared mmap in FOPEN_DIRECT_IO mode
>

**[v1: fs: Optimize unixbench's file copy test](http://lore.kernel.org/linux-fsdevel/1688117303-8294-1-git-send-email-zenghongling@kylinos.cn/)**

> The iomap_set_range_uptodate function checks if the file is a private
> mapping,and if it is, it needs to do something about it.UnixBench's
> file copy tests are mostly share mapping, such a check would reduce
> file copy scores, so we added the unlikely macro for optimization.
> and the score of file copy can be improved after branch optimization.
> 

**[v1: fanotify: disallow mount/sb marks on kernel internal pseudo fs](http://lore.kernel.org/linux-fsdevel/20230629042044.25723-1-amir73il@gmail.com/)**

> Hopefully, nobody is trying to abuse mount/sb marks for watching all
> anonymous pipes/inodes.
>
> I cannot think of a good reason to allow this - it looks like an
> oversight that dated back to the original fanotify API.
>

**[GIT PULL: sysctl changes for v6.5-rc1](http://lore.kernel.org/linux-fsdevel/ZJx62RvS9TwjUUCi@bombadil.infradead.org/)**

> The following changes since commit f1fcbaa18b28dec10281551dfe6ed3a3ed80e3d6:
>
>   Linux 6.4-rc2 (2023-05-14 12:51:40 -0700)
>
> are available in the Git repository at:
>
>   git://git.kernel.org/pub/scm/linux/kernel/git/mcgrof/linux.git/ tags/v6.5-rc1-sysctl-next
>
> for you to fetch changes up to 2f2665c13af4895b26761107c2f637c2f112d8e9:
>
>   sysctl: replace child with an enumeration (2023-06-18 02:32:54 -0700)
>

#### 网络设备

**[v3: net: nfp: clean mc addresses in application firmware when closing port](http://lore.kernel.org/netdev/20230705052818.7122-1-louis.peens@corigine.com/)**

> When moving devices from one namespace to another, mc addresses are
> cleaned in software while not removed from application firmware. Thus
> the mc addresses are remained and will cause resource leak.
>

**[v2: iwl-net: ice: prevent call trace during reload](http://lore.kernel.org/netdev/20230705040510.906029-1-michal.swiatkowski@linux.intel.com/)**

> Calling ethtool during reload can lead to call trace, because VSI isn't
> configured for some time, but netdev is alive.
>
> To fix it add rtnl lock for VSI deconfig and config. Set ::num_q_vectors
> to 0 after freeing and add a check for ::tx/rx_rings in ring related
> ethtool ops.
>
> Add proper unroll of filters in ice_start_eth().
>

**[v1: net: octeontx2-af: Promisc enable/disable through mbox](http://lore.kernel.org/netdev/20230705033813.2744357-1-rkannoth@marvell.com/)**

> In Legacy silicon, promisc mode is only modified
> through CGX mbox messages. In CN10KB silicon, it modified
> from CGX mbox and NIX. This breaks legacy application
> behaviour. Fix this by removing call from NIX.
>

**[v2: vduse: add support for networking devices](http://lore.kernel.org/netdev/20230704164045.39119-1-maxime.coquelin@redhat.com/)**

> This small series enables virtio-net device type in VDUSE.
> With it, basic operation have been tested, both with
> virtio-vdpa and vhost-vdpa using DPDK Vhost library series
> adding VDUSE support using split rings layout (merged in
> DPDK v23.07-rc1).
>

**[v1: net: ftmac100: add multicast filtering possibility](http://lore.kernel.org/netdev/20230704154053.3475336-1-saproj@gmail.com/)**

> If netdev_mc_count() is not zero and not IFF_ALLMULTI, filter
> incoming multicast packets. The chip has a Multicast Address Hash Table
> for allowed multicast addresses, so we fill it.
>

**[v1: net: sched: Undo tcf_bind_filter in case of errors in set callbacks](http://lore.kernel.org/netdev/20230704151456.52334-1-victor@mojatatu.com/)**

> Five different classifier (fw, bpf, u32, matchall, and flower) are
> calling tcf_bind_filter in their callbacks, but weren't undoing it by
> calling tcf_unbind_filter if their was an error after binding.
>
> This patch set fixes all this by calling tcf_unbind_filter in such
> cases.
>

**[v5: bpf-next: Add SO_REUSEPORT support for TC bpf_sk_assign](http://lore.kernel.org/netdev/20230613-so-reuseport-v5-0-f6686a0dbce0@isovalent.com/)**

> We want to replace iptables TPROXY with a BPF program at TC ingress.
> To make this work in all cases we need to assign a SO_REUSEPORT socket
> to an skb, which is currently prohibited. This series adds support for
> such sockets to bpf_sk_assing.
>

**[v1: resubmit: net: fec: Refactor: rename `adapter` to `fep`](http://lore.kernel.org/netdev/20230704114058.5785-1-csokas.bence@prolan.hu/)**

> Rename local `struct fec_enet_private *adapter` to `fep` in `fec_ptp_gettime()` to match the rest of the driver
>

**[v1: igb: Add support for AF_XDP zero-copy](http://lore.kernel.org/netdev/20230704095915.9750-1-sriram.yagnaraman@est.tech/)**

> Disclaimer: My first patches to Intel drivers, implemented AF_XDP
> zero-copy feature which seemed to be missing for igb. Not sure if it was
> a conscious choice to not spend time implementing this for older
> devices, nevertheless I send them to the list for review.
>

**[v1: net: phy: at803x: support qca8081 1G version chip](http://lore.kernel.org/netdev/20230704090016.7757-1-quic_luoj@quicinc.com/)**

> This patch series add supporting qca8081 1G version chip, the 1G version
> chip can be identified by the register mmd7.0x901d bit0.
>

**[v1: net-next: bnxt_en: use dev_consume_skb_any() in bnxt_tx_int](http://lore.kernel.org/netdev/20230704085236.9791-1-imagedong@tencent.com/)**

> Replace dev_kfree_skb_any() with dev_consume_skb_any() in bnxt_tx_int()
> to clear the unnecessary noise of "kfree_skb" event.
>

**[v2: net: dsa: SERDES support for mv88e632x family](http://lore.kernel.org/netdev/20230704065916.132486-1-michael.haener@siemens.com/)**

> This patch series brings SERDES support for the mv88e632x family.
>

**[v1: can: j1939: prevent deadlock by changing j1939_socks_lock to rwlock](http://lore.kernel.org/netdev/20230704064710.3189-1-astrajoan@yahoo.com/)**

> The following 3 locks would race against each other, causing the
> deadlock situation in the Syzbot bug report:
>
> - j1939_socks_lock
> - active_session_list_lock
> - sk_session_queue_lock
>
> A reasonable fix is to change j1939_socks_lock to an rwlock, since in
> the rare situations where a write lock is required for the linked list
> that j1939_socks_lock is protecting, the code does not attempt to
> acquire any more locks. This would break the circular lock dependency,
> where, for example, the current thread already locks j1939_socks_lock
> and attempts to acquire sk_session_queue_lock, and at the same time,
> another thread attempts to acquire j1939_socks_lock while holding
> sk_session_queue_lock.
>

**[v2: bpf-next: XDP metadata via kfuncs for ice](http://lore.kernel.org/netdev/20230703181226.19380-1-larysa.zaremba@intel.com/)**

> This series introduces XDP hints via kfuncs [0] to the ice driver.
>
> Series brings the following existing hints to the ice driver:
>  - HW timestamp
>  - RX hash with type
>
> Series also introduces new hints and adds their implementation
> to ice and veth:
>  - VLAN tag with protocol
>  - Checksum level
>

**[v1: net: Replace strlcpy with strscpy](http://lore.kernel.org/netdev/20230703175840.3706231-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
> No return values were used, so direct replacement is safe.
>

**[v1: bpf, net: Allow setting SO_TIMESTAMPING* from BPF](http://lore.kernel.org/netdev/20230703175048.151683-1-jthinz@mailbox.tu-berlin.de/)**

> BPF applications, e.g., a TCP congestion control, might benefit from
> precise packet timestamps. These timestamps are already available in
> __sk_buff and bpf_sock_ops, but could not be requested: A BPF program
> was not allowed to set SO_TIMESTAMPING* on a socket. This change enables
> BPF programs to actively request the generation of timestamps from a
> stream socket.
>

**[v1: bpf-next: xsk: honor SO_BINDTODEVICE on bind](http://lore.kernel.org/netdev/20230703175329.3259672-1-i.maximets@ovn.org/)**

> Initial creation of an AF_XDP socket requires CAP_NET_RAW capability.
> A privileged process might create the socket and pass it to a
> non-privileged process for later use.  However, that process will be
> able to bind the socket to any network interface.  Even though it will
> not be able to receive any traffic without modification of the BPF map,
> the situation is not ideal.
>

**[v3: octeontx2-pf: Add additional check for MCAM rules](http://lore.kernel.org/netdev/20230703170054.2152662-1-sumang@marvell.com/)**

> Due to hardware limitation, MCAM drop rule with
> ether_type == 802.1Q and vlan_id == 0 is not supported. Hence rejecting
> such rules.
>

**[v1: netconsole: Append kernel version to message](http://lore.kernel.org/netdev/20230703154155.3460313-1-leitao@debian.org/)**

> Create a new netconsole Kconfig option that prepends the kernel version in
> the netconsole message. This is useful to map kernel messages to kernel
> version in a simple way, i.e., without checking somewhere which kernel
> version the host that sent the message is using.
>

**[v2: nf: netfilter: conntrack: Avoid nf_ct_helper_hash uses after free](http://lore.kernel.org/netdev/20230703145216.1096265-1-revest@chromium.org/)**

> If nf_conntrack_init_start() fails (for example due to a
> register_nf_conntrack_bpf() failure), the nf_conntrack_helper_fini()
> clean-up path frees the nf_ct_helper_hash map.
>

**[v1: vdpa: reject F_ENABLE_AFTER_DRIVER_OK if backend does not support it](http://lore.kernel.org/netdev/20230703142218.362549-1-eperezma@redhat.com/)**

> With the current code it is accepted as long as userland send it.
>
> Although userland should not set a feature flag that has not been
> offered to it with VHOST_GET_BACKEND_FEATURES, the current code will not
> complain for it.
>

**[v1: Add a driver for the Marvell 88Q2110 PHY](http://lore.kernel.org/netdev/20230703124440.391970-1-eichest@gmail.com/)**

> Add support for 1000BASE-T1 to the phy_device driver and add a first
>

**[[net PATCH] octeontx2-af: Install TC filter rules in hardware based on priority](http://lore.kernel.org/netdev/20230703120536.2148918-1-sumang@marvell.com/)**

> As of today, hardware does not support installing tc filter
> rules based on priority. This patch fixes the issue and install
> the hardware rules based on priority. The final hardware rules
> will not be dependent on rule installation order, it will be strictly
> priority based, same as software.
>

**[v1: net/sched: act_pedit: Add size check for TCA_PEDIT_PARMS_EX](http://lore.kernel.org/netdev/20230703110842.590282-1-linma@zju.edu.cn/)**

> The attribute TCA_PEDIT_PARMS_EX is not be included in pedit_policy and
> one malicious user could fake a TCA_PEDIT_PARMS_EX whose length is
> smaller than the intended sizeof(struct tc_pedit). Hence, the
> dereference in tcf_pedit_init() could access dirty heap data.
>

**[[net PATCH V2] octeontx2-pf: Add additional check for MCAM rules.](http://lore.kernel.org/netdev/20230703095600.2048397-1-sumang@marvell.com/)**

> Due to hardware limitation, MCAM drop rule with
> ether_type == 802.1Q and vlan_id == 0 is not supported. Hence rejecting
> such rules.
>

**[v1: I3C MCTP net driver](http://lore.kernel.org/netdev/20230703053048.275709-1-matt@codeconstruct.com.au/)**

> This series adds an I3C transport for the kernel's MCTP network
> protocol. MCTP is a communication protocol between system components
> (BMCs, drives, NICs etc), with higher level protocols such as NVMe-MI or
> PLDM built on top of it (in userspace). It runs over various transports
> such as I2C, PCIe, or I3C.
>

**[v4: wifi&#65306;mac80211: Replace the ternary conditional operator with conditional-statements](http://lore.kernel.org/netdev/20230703030200.1067-1-youkangren@vivo.com/)**

> Replacing ternary conditional operators with conditional statements
> ensures proper expression of meaning while making it easier for
> the compiler to generate code.
>

**[v5: vsock: MSG_ZEROCOPY flag support](http://lore.kernel.org/netdev/20230701063947.3422088-1-AVKrasnov@sberdevices.ru/)**

> Difference with copy way is not significant. During packet allocation,
> non-linear skb is created and filled with pinned user pages.
> There are also some updates for vhost and guest parts of transport - in
> both cases i've added handling of non-linear skb for virtio part. vhost
> copies data from such skb to the guest's rx virtio buffers. In the guest,
> virtio transport fills tx virtio queue with pages from skb.
>

**[v5: vsock: enable setting SO_ZEROCOPY](http://lore.kernel.org/netdev/20230701062310.3397129-14-AVKrasnov@sberdevices.ru/)**

> For AF_VSOCK, zerocopy tx mode depends on transport, so this option must
> be set in AF_VSOCK implementation where transport is accessible (if
> transport is not set during setting SO_ZEROCOPY: for example socket is
> not connected, then SO_ZEROCOPY will be enabled, but once transport will
> be assigned, support of this type of transmission will be checked).
>

**[v1: selftests/net: Add xt_policy config for xfrm_policy test](http://lore.kernel.org/netdev/20230701044103.1096039-1-daniel.diaz@linaro.org/)**

> This is because IPsec "policy" match support is not available
> to the kernel.
>
> This patch adds CONFIG_NETFILTER_XT_MATCH_POLICY as a module
> to the selftests/net/config file, so that `make
> kselftest-merge` can take this into consideration.
>

**[v1: Add virtio_rtc module and related changes](http://lore.kernel.org/netdev/20230630171052.985577-1-peter.hilber@opensynergy.com/)**

> This patch series adds the virtio_rtc module, and related bugfixes and
> small interface extensions. The virtio_rtc module implements a driver
> compatible with the proposed Virtio RTC device specification [1]. The
> Virtio RTC (Real Time Clock) device provides information about current
> time. The device can provide different clocks, e.g. for the UTC or TAI time
> standards, or for physical time elapsed since some past epoch. The driver
> can read the clocks with simple or more accurate methods.
>

#### 安全增强

**[v1: pstore: Replace crypto API compression with zlib calls](http://lore.kernel.org/linux-hardening/20230704135211.2471371-1-ardb@kernel.org/)**

> The pstore layer implements support for compression of kernel log
> output, using a variety of compressions algorithms provided by the
> [deprecated] crypto API 'comp' interface.
>
> This appears to have been somebody's pet project rather than a solution
> to a real problem: the original deflate compression is reasonably fast,
> compressed well and is comparatively small in terms of code footprint,
> and so the flexibility that the crypto API integration provides does
> little more than complicate the code for no reason.
>

**[v1: Revert "fortify: Allow KUnit test to build without FORTIFY"](http://lore.kernel.org/linux-hardening/20230703220210.never.615-kees@kernel.org/)**

> The standard for KUnit is to not build tests at all when required
> functionality is missing, rather than doing test "skip". Restore this
> for the fortify tests, so that architectures without
> CONFIG_ARCH_HAS_FORTIFY_SOURCE do not emit unsolvable warnings.
>

**[v1: wifi: mt76: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230703181256.3712079-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: kobject: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230703180528.3709258-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: kyber, blk-wbt: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230703172159.3668349-1-azeemshaikh38@gmail.com/)**

> This patch series replaces strlcpy in the kyber and blk-wbt tracing subsystems wherever trivial
> replacement is possible, i.e return value from strlcpy is unused. The patches
> themselves are independent of each other and are applied to different subsystems. They are
> included as a series for ease of review.
>

**[v1: perf: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230703165817.2840457-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
> No return values were used, so direct replacement is safe.
>

**[v1: next: media: venus: Use struct_size_t() helper in pkt_session_unset_buffers()](http://lore.kernel.org/linux-hardening/ZKBfoqSl61jfpO2r@work/)**

> Prefer struct_size_t() over struct_size() when no pointer instance
> of the structure type is present.
>

**[v2: pid: Replace struct pid 1-element array with flex-array](http://lore.kernel.org/linux-hardening/20230630180418.gonna.286-kees@kernel.org/)**

> For pid namespaces, struct pid uses a dynamically sized array member,
> "numbers". This was implemented using the ancient 1-element fake flexible
> array, which has been deprecated for decades. Replace it with a C99
> flexible array, refactor the array size calculations to use struct_size(),
> and address elements via indexes. Note that the static initializer (which
> defines a single element) works as-is, and requires no special handling.
>

**[[GIT PULL v2] flexible-array transformations for 6.5-rc1](http://lore.kernel.org/linux-hardening/ZJ8C4PtPrxr6LTA7@work/)**

> The following changes since commit f1fcbaa18b28dec10281551dfe6ed3a3ed80e3d6:
>
>   Linux 6.4-rc2 (2023-05-14 12:51:40 -0700)
>
> are available in the Git repository at:
>
>   git://git.kernel.org/pub/scm/linux/kernel/git/gustavoars/linux.git tags/flex-array-transformations-6.5-rc1
>

**[v3: Add documentation for sysctl vm.memfd_noexec](http://lore.kernel.org/linux-hardening/20230630032535.625390-1-jeffxu@google.com/)**

> Add documentation for sysctl vm.memfd_noexec
>
> Thanks to Dominique Martinet <asmadeus@codewreck.org> who reported this.
> see [1] for context.
>
> [1] https://lore.kernel.org/linux-mm/CABi2SkXUX_QqTQ10Yx9bBUGpN1wByOi_=gZU6WEy5a8MaQY3Jw@mail.gmail.com/T/
>

**[v1: usb: ch9: Replace bmSublinkSpeedAttr 1-element array with flexible array](http://lore.kernel.org/linux-hardening/20230629190900.never.787-kees@kernel.org/)**

> Since commit df8fc4e934c1 ("kbuild: Enable -fstrict-flex-arrays=3"),
> UBSAN_BOUNDS no longer pretends 1-element arrays are unbounded. Walking
> bmSublinkSpeedAttr will trigger a warning, so make it a proper flexible
> array. Add a union to keep the struct size identical for userspace in
> case anything was depending on the old size.
>

**[v1: next: scsi: aacraid: Replace one-element array with flexible-array member in struct user_sgmap](http://lore.kernel.org/linux-hardening/2ebb702f25c4764fb36ab29f4f40728e12b0e42b.1687974498.git.gustavoars@kernel.org/)**

> Replace one-element array with flexible-array member in struct
> user_sgmap and refactor the rest of the code, accordingly.
>
> Issue found with the help of Coccinelle and audited and fixed,
> manually.
>
> This results in no differences in binary output.
>

**[v1: next: scsi: aacraid: Use struct_size() helper in code related to struct sgmapraw](http://lore.kernel.org/linux-hardening/be2e5ecf1c4410ab419e2290341fbc8a0e2ba963.1687974498.git.gustavoars@kernel.org/)**

> Prefer struct_size() over open-coded versions.
>

**[v1: next: scsi: aacraid: Use struct_size() helper in aac_get_safw_ciss_luns()](http://lore.kernel.org/linux-hardening/cd80ea8f2446fe62ec15ffb0bbcecb69e0c342af.1687974498.git.gustavoars@kernel.org/)**

> Prefer struct_size() over open-coded versions.
>
> This results in no differences in binary output.
>

**[v1: next: scsi: aacraid: Replace one-element arrays with flexible-array members](http://lore.kernel.org/linux-hardening/cover.1687974498.git.gustavoars@kernel.org/)**

> This series aims to replace one-element arrays with flexible-array
> members in multiple structures in drivers/scsi/aacraid/aacraid.h.
>
> This helps with the ongoing efforts to globally enable -Warray-bounds
> and get us closer to being able to tighten the FORTIFY_SOURCE routines
> on memcpy().
>
> These issues were found with the help of Coccinelle and audited and fixed,
> manually.
>

**[GIT PULL: flexible-array transformations for 6.5-rc1](http://lore.kernel.org/linux-hardening/ZJxZJDUDs1ry84Rc@work/)**

> The following changes since commit f1fcbaa18b28dec10281551dfe6ed3a3ed80e3d6:
>
>   Linux 6.4-rc2 (2023-05-14 12:51:40 -0700)
>
> are available in the Git repository at:
>
>   git://git.kernel.org/pub/scm/linux/kernel/git/gustavoars/linux.git tags/flex-array-transformations-6.5-rc1
>

**[v1: pstore: ramoops: support pmsg size larger than kmalloc limitation](http://lore.kernel.org/linux-hardening/20230627202540.881909-2-yuxiaozhang@google.com/)**

> Current pmsg implementation is using kmalloc for pmsg record buffer,
> which has max size limits based on page size. Currently even we
> allocate enough space with pmsg-size, pmsg will still fail if the
> file size is larger than what kmalloc allowed.
>

**[v4: Randomized slab caches for kmalloc()](http://lore.kernel.org/linux-hardening/20230626031835.2279738-1-gongruiqi@huaweicloud.com/)**

> When exploiting memory vulnerabilities, "heap spraying" is a common
> technique targeting those related to dynamic memory allocation (i.e. the
> "heap"), and it plays an important role in a successful exploitation.
> Basically, it is to overwrite the memory area of vulnerable object by
> triggering allocation in other subsystems or modules and therefore
> getting a reference to the targeted memory location. It's usable on
> various types of vulnerablity including use after free (UAF), heap out-
> of-bound write and etc.
>

#### 异步 IO

**[v3: Add a sysctl to disable io_uring system-wide](http://lore.kernel.org/io-uring/20230630151003.3622786-1-matteorizzo@google.com/)**

> Over the last few years we've seen many critical vulnerabilities in
> io_uring[1] which could be exploited by an unprivileged process to gain
> control over the kernel. This patch introduces a new sysctl which disables
> the creation of new io_uring instances system-wide.
>

**[v1: io_uring: Add {} to maintain consistency in code format](http://lore.kernel.org/io-uring/20230630062512.10724-1-luhongfei@vivo.com/)**

> In io_issue_sqe, the if (ret == IOU_OK) branch uses {}, so to maintain code
> format consistency, it is better to add {} in the else branch.
>

**[v4: io_uring: Add io_uring command support for sockets](http://lore.kernel.org/io-uring/20230627134424.2784797-1-leitao@debian.org/)**

> Enable io_uring commands on network sockets. Create two new
> SOCKET_URING_OP commands that will operate on sockets.
>
> In order to call ioctl on sockets, use the file_operations->io_uring_cmd
> callbacks, and map it to a uring socket function, which handles the
> SOCKET_URING_OP accordingly, and calls socket ioctls.
>

#### Rust For Linux

**[v1: rust: types: make `Opaque` be `!Unpin`](http://lore.kernel.org/rust-for-linux/20230630150216.109789-1-benno.lossin@proton.me/)**

> Adds a `PhantomPinned` field to `Opaque<T>`. This removes the last Rust
> guarantee: the assumption that the type `T` can be freely moved. This is
> not the case for many types from the C side (e.g. if they contain a
> `struct list_head`). This change removes the need to add a
> `PhantomPinned` field manually to Rust structs that contain C structs
> which must not be moved.
>

**[v1: rust: macros: add `paste!` proc macro](http://lore.kernel.org/rust-for-linux/20230628171108.1150742-1-gary@garyguo.net/)**

> This macro provides a flexible way to concatenated identifiers together
> and it allows the resulting identifier to be used to declare new items,
> which `concat_idents!` does not allow. It also allows identifiers to be
> transformed before concatenated.
>

**[v1: rust: build: Define MODULE macro iif the CONFIG_MODULES is enabled](http://lore.kernel.org/rust-for-linux/20230627121422.112246-1-wangrui@loongson.cn/)**

> The LoongArch does not currently support modules when built with clang.
> A pre-processor error is expected on building modules, that's caused by:
>
>  #if defined(MODULE) && defined(CONFIG_AS_HAS_EXPLICIT_RELOCS)
>  # if __has_attribute(model)
>  #  define PER_CPU_ATTRIBUTES __attribute__((model("extreme")))
>  # else
>  #  error compiler support for the model attribute is necessary when a recent assembler is used
>  # endif
>  #endif
>

**[v2: rust: alloc: Add realloc and alloc_zeroed to the GlobalAlloc impl](http://lore.kernel.org/rust-for-linux/20230625232528.89306-1-boqun.feng@gmail.com/)**

> While there are default impls for these methods, using the respective C
> api's is faster. Currently neither the existing nor these new
> GlobalAlloc method implementations are actually called. Instead the
> __rust_* function defined below the GlobalAlloc impl are used. With
> rustc 1.71 these functions will be gone and all allocation calls will go
> through the GlobalAlloc implementation.
>

**[v1: Rust device mapper abstractions](http://lore.kernel.org/rust-for-linux/20230625121657.3631109-1-changxian.cqs@antgroup.com/)**

> This is a version of device mapper abstractions. Based on
> these, we also implement a linear target as a PoC.
> Any suggestions are welcomed, thanks!
>

#### BPF

**[v3: um: vector: Replace undo_user_init in old code with out_free_netdev](http://lore.kernel.org/bpf/20230704042942.3984-1-duminjie@vivo.com/)**

> Thanks for your response and suggestions,
> I made some mistakes. This is a resubmitted patch.
> I got some errors with my local repository,
> so I lost the commit SHA-1 ID.
>

**[v9: bpf-next: selftests/bpf: Add benchmark for bpf memory allocator](http://lore.kernel.org/bpf/20230704025039.938914-1-houtao@huaweicloud.com/)**

> The benchmark could be used to compare the performance of hash map
> operations and the memory usage between different flavors of bpf memory
> allocator (e.g., no bpf ma vs bpf ma vs reuse-after-gp bpf ma). It also
> could be used to check the performance improvement or the memory saving
> provided by optimization.
>

**[v1: bpf, net: Allow setting SO_TIMESTAMPING* from BPF](http://lore.kernel.org/bpf/20230703175048.151683-1-jthinz@mailbox.tu-berlin.de/)**

> BPF applications, e.g., a TCP congestion control, might benefit from
> precise packet timestamps. These timestamps are already available in
> __sk_buff and bpf_sock_ops, but could not be requested: A BPF program
> was not allowed to set SO_TIMESTAMPING* on a socket. This change enables
> BPF programs to actively request the generation of timestamps from a
> stream socket.
>

**[v1: x86/BPF: Add new BPF helper call bpf_rdtsc](http://lore.kernel.org/bpf/20230703105745.1314475-1-tero.kristo@linux.intel.com/)**

> This patch series adds a new x86 arch specific BPF helper, bpf_rdtsc()
> which can be used for reading the hardware time stamp counter (TSC.)
> Currently the same counter is directly accessible from userspace
> (using RDTSC instruction), and kernel space using various rdtsc_*()
> APIs, however eBPF lacks the support.
>

**[v1: fs: Add kfuncs to handle idmapped mounts](http://lore.kernel.org/bpf/c35fbb4cb0a3a9b4653f9a032698469d94ca6e9c.1688123230.git.legion@kernel.org/)**

> Since the introduction of idmapped mounts, file handling has become
> somewhat more complicated. If the inode has been found through an
> idmapped mount the idmap of the vfsmount must be used to get proper
> i_uid / i_gid. This is important, for example, to correctly take into
> account idmapped files when caching, LSM or for an audit.
>

**[[v3 PATCH bpf-next 0/6] bpf: add percpu stats for bpf_map](http://lore.kernel.org/bpf/20230630082516.16286-1-aspsk@isovalent.com/)**

> This series adds a mechanism for maps to populate per-cpu counters on
> insertions/deletions. The sum of these counters can be accessed by a new kfunc
> from map iterator and tracing programs.
>

**[v5: RFC: introduce page_pool_alloc() API](http://lore.kernel.org/bpf/20230629120226.14854-1-linyunsheng@huawei.com/)**

> In [1] & [2] & [3], there are usecases for veth and virtio_net
> to use frag support in page pool to reduce memory usage, and it
> may request different frag size depending on the head/tail
> room space for xdp_frame/shinfo and mtu/packet size. When the
> requested frag size is large enough that a single page can not
> be split into more than one frag, using frag support only have
> performance penalty because of the extra frag count handling
> for frag support.
>

**[v1: bpf-next: bpf: Support new insns from cpu v4](http://lore.kernel.org/bpf/20230629063715.1646832-1-yhs@fb.com/)**

> This patch set added kernel support for insns proposed in [1] except
> BPF_ST which already has full kernel support. Beside the above proposed
> insns, LLVM will generate BPF_ST insn as well under -mcpu=v4 ([2]).
>
> The patchset implements interpreter and jit support for these new
> insns. It has minimum verifier support in order to pass bpf selftests.
> More work will be required to cover verification and other aspects
> (e.g. blinding, etc.).
>

**[[PATCH RESEND v3 bpf-next 00/14] BPF token](http://lore.kernel.org/bpf/20230629051832.897119-1-andrii@kernel.org/)**

> This patch set introduces new BPF object, BPF token, which allows to delegate
> a subset of BPF functionality from privileged system-wide daemon (e.g.,
> systemd or any other container manager) to a *trusted* unprivileged
> application. Trust is the key here. This functionality is not about allowing
> unconditional unprivileged BPF usage. Establishing trust, though, is
> completely up to the discretion of respective privileged application that
> would create a BPF token, as different production setups can and do achieve it
> through a combination of different means (signing, LSM, code reviews, etc),
> and it's undesirable and infeasible for kernel to enforce any particular way
> of validating trustworthiness of particular process.
>

**[v1: fprobe: Ensure running fprobe_exit_handler() finished before calling rethook_free()](http://lore.kernel.org/bpf/168796344232.46347.7947681068822514750.stgit@devnote2/)**

> Ensure running fprobe_exit_handler() has finished before
> calling rethook_free() in the unregister_fprobe() so that caller can free
> the fprobe right after unregister_fprobe().
>
> unregister_fprobe() ensured that all running fprobe_entry/exit_handler()
> have finished by calling unregister_ftrace_function() which synchronizes
> RCU. But commit 5f81018753df ("fprobe: Release rethook after the ftrace_ops
> is unregistered") changed to call rethook_free() after
> unregister_ftrace_function(). So call rethook_stop() to make rethook
> disabled before unregister_ftrace_function() and ensure it again.
>

**[v8: bpf-next: bpf, x86: allow function arguments up to 12 for TRACING](http://lore.kernel.org/bpf/20230627115319.13128-1-imagedong@tencent.com/)**

> Therefore, let's enhance it by increasing the function arguments count
> allowed in arch_prepare_bpf_trampoline(), for now, only x86_64.
>
> In the 1st patch, we save/restore regs with BPF_DW size to make the code
> in save_regs()/restore_regs() simpler.
>
> In the 2nd patch, we make arch_prepare_bpf_trampoline() support to copy
> function arguments in stack for x86 arch. Therefore, the maximum
> arguments can be up to MAX_BPF_FUNC_ARGS for FENTRY, FEXIT and
> MODIFY_RETURN. Meanwhile, we clean the potential garbage value when we
> copy the arguments on-stack.
>

**[v1: bpf-next: Support defragmenting IPv(4|6) packets in BPF](http://lore.kernel.org/bpf/cover.1687819413.git.dxu@dxuuu.xyz/)**

> In the context of a middlebox, fragmented packets are tricky to handle.
> The full 5-tuple of a packet is often only available in the first
> fragment which makes enforcing consistent policy difficult.
> So stateful tracking is the only sane option. RFC 8900 [0] calls this
> out as well in section 6.3:
>
>     Middleboxes [...] should process IP fragments in a manner that is
>     consistent with [RFC0791] and [RFC8200]. In many cases, middleboxes
>     must maintain state in order to achieve this goal.
>

**[v1: Interest in additional endianness documentation](http://lore.kernel.org/bpf/CADx9qWgHCC4MML2d+mq25-aeTn+20qxjeTZSHMGPQrMq65a+bQ@mail.gmail.com/)**

> Thank you to everyone in the community for building/working on such a
> great tool! I am helping build a userspace implementation of eBPF and
> following Dave's standardization process closely.
>

### 周边技术动态

#### U-Boot

**[u-boot compilation failure for Sifive unmatched board](http://lore.kernel.org/u-boot/CAK1XJzWofn5+OE7qCyf3nTb+hevULoAVss5dG2OzwbMh0C=YVA@mail.gmail.com/)**

> This is Satish, compiling u-boot code based on the reference page:
> https://github.com/carlosedp/riscv-bringup/blob/master/unmatched/Readme.md#install-toolchain-to-build-kernel
>
> u-boot is failing with following commit id & its tag is
> commit d637294e264adfeb29f390dfc393106fd4d41b17 (HEAD, tag: v2022.01)
>

**[Pull request: u-boot-rockchip-20230629](http://lore.kernel.org/u-boot/20230629121342.72391-1-kever.yang@rock-chips.com/)**

> Please pull the fixex for rockchip platform:
> - rockchip inno phy fix;
> - pinctrl driver in SPL arort in specific case;
> - fix IO port voltage for rock5b-rk3588 board;
>
> CI:
> https://source.denx.de/u-boot/custodians/u-boot-rockchip/-/pipelines/16732
>

**[Trying to boot JH7110 RISCV-V CPU from MMC](http://lore.kernel.org/u-boot/e6cd461d-151e-3557-f58a-6118836f8e6a@ruabmbua.dev/)**

> I am trying to use upstream u-boot + opensbi, to boot my visionfive2 SBC
> I got from external SD card. 
>

**[v1: riscv: sifive: fu70: downclock CPU clock for stability](http://lore.kernel.org/u-boot/20230628081530.3184607-1-uwu@icenowy.me/)**

> When building the package `rustc` for AOSC OS on HiFive Unmatched,
> random SIGSEGV prevents the package from getting correctly built.
> Downclocking the CPU PLL clock seems to allow rustc to be built,
> although taking much more time.
>

## 20230625：第 51 期

### 内核动态

#### RISC-V 架构支持

**[v1: Allwinner R329/D1/R528/T113s Dual/Quad SPI modes support](http://lore.kernel.org/linux-riscv/20230624131632.2972546-1-bigunclemax@gmail.com/)**

> This series extends the previous https://lore.kernel.org/all/20230510081121.3463710-1-bigunclemax@gmail.com
> And adds support for Dual and Quad SPI modes for the listed SoCs.
> Both modes have been tested on the T113s and should work on
> other Allwinner's SoCs that have a similar SPI conttoller.
> It may also work for previous SoCs that support Dual/Quad modes.
> One of them are H6 and H616.
>

**[v1: Add support to handle misaligned accesses in S-mode](http://lore.kernel.org/linux-riscv/20230624122049.7886-1-cleger@rivosinc.com/)**

> Since commit 61cadb9 ("Provide new description of misaligned load/store
> behavior compatible with privileged architecture.") in the RISC-V ISA
> manual, it is stated that misaligned load/store might not be supported.
> However, the RISC-V kernel uABI describes that misaligned accesses are
> supported. In order to support that, this series adds support for S-mode
> handling of misaligned accesses, SBI call for misaligned trap delegation
> as well prctl support for PR_SET_UNALIGN.
>

**[v1: riscv: Select HAVE_ARCH_USERFAULTFD_MINOR](http://lore.kernel.org/linux-riscv/20230624060321.3401504-1-samuel.holland@sifive.com/)**

> This allocates the VM flag needed to support the userfaultfd minor fault
> functionality. Because the flag bit is >= bit 32, it can only be enabled
> for 64-bit kernels. See commit 7677f7fd8be7 ("userfaultfd: add minor
> fault registration mode") for more information.
>

**[v2: Add support for Allwinner PWM on D1/T113s/R329 SoCs](http://lore.kernel.org/linux-riscv/20230623150012.1201552-1-privatesub2@gmail.com/)**

> This series adds support for PWM controller on new
> Allwinner's SoCs, such as D1, T113s and R329. The implemented driver
> provides basic functionality for control PWM channels.
>

**[v5: Risc-V Svinval support](http://lore.kernel.org/linux-riscv/20230623123849.1425805-1-mchitale@ventanamicro.com/)**

> This patch adds support for the Svinval extension as defined in the
> Risc V Privileged specification.
>

**[v4: RISCV: Add KVM_GET_REG_LIST API](http://lore.kernel.org/linux-riscv/cover.1687515463.git.haibo1.xu@intel.com/)**

> KVM_GET_REG_LIST will dump all register IDs that are available to
> KVM_GET/SET_ONE_REG and It's very useful to identify some platform
> regression issue during VM migration.
>

**[v2: RISC-V: T-Head vector handling](http://lore.kernel.org/linux-riscv/20230622231305.631331-1-heiko@sntech.de/)**

> As is widely known the T-Head C9xx cores used for example in the
> Allwinner D1 implement an older non-ratified variant of the vector spec.
>
> While userspace will probably have a lot more problems implementing
> support for both, on the kernel side the needed changes are actually
> somewhat small'ish and can be handled via alternatives somewhat nicely.
>

**[v5: Split ptdesc from struct page](http://lore.kernel.org/linux-riscv/20230622205745.79707-1-vishal.moola@gmail.com/)**

> The MM subsystem is trying to shrink struct page. This patchset
> introduces a memory descriptor for page table tracking - struct ptdesc.
>
> This patchset introduces ptdesc, splits ptdesc from struct page, and
> converts many callers of page table constructor/destructors to use ptdescs.
>

**[v1: riscv: Discard vector state on syscalls](http://lore.kernel.org/linux-riscv/20230622173613.30722-1-bjorn@kernel.org/)**

> The RISC-V vector specification states:
>   Executing a system call causes all caller-saved vector registers
>   (v0-v31, vl, vtype) and vstart to become unspecified.
>

**[GIT PULL: KVM/riscv changes for 6.5](http://lore.kernel.org/linux-riscv/CAAhSdy1iT=SbjSvv_7SDygSo0HhmgLjD-y+DU1_Q+6tnki7w+A@mail.gmail.com/)**

> We have the following KVM RISC-V changes for 6.5:
> 1) Redirect AMO load/store misaligned traps to KVM guest
> 2) Trap-n-emulate AIA in-kernel irqchip for KVM guest
> 3) Svnapot support for KVM Guest
>

**[Patch "riscv: Link with '-z norelro'" has been added to the 6.3-stable tree](http://lore.kernel.org/linux-riscv/2023062221-moving-eastward-9967@gregkh/)**

> This is a note to let you know that I've just added the patch titled
>
>     riscv: Link with '-z norelro'
>
> to the 6.3-stable tree which can be found at:
>     http://www.kernel.org/git/?p=linux/kernel/git/stable/stable-queue.git;a=summary
>
> The filename of the patch is:
>      riscv-link-with-z-norelro.patch
> and it can be found in the queue-6.3 subdirectory.
>

**[v1: RISC-V: make ARCH_THEAD preclude XIP_KERNEL](http://lore.kernel.org/linux-riscv/20230621-panorama-stuffing-f24b26546972@spud/)**

> Randy reported build errors in linux-next where XIP_KERNEL was enabled.
> ARCH_THEAD requires alternatives to support the non-standard ISA
> extensions used by the THEAD cores, which are mutually exclusive with
> XIP kernels. Clone the dependency list from the Allwinner entry, since
> Allwinner's D1 uses T-Head cores with the same non-standard extensions.
>

**[v1: 6.3: riscv: Link with '-z norelro'](http://lore.kernel.org/linux-riscv/20230620-6-3-fix-got-relro-error-lld-v1-1-f3e71ec912d1@kernel.org/)**

> This patch fixes a stable only patch, so it has no direct upstream
> equivalent.
>
> After a stable only patch to explicitly handle the '.got' section to
> handle an orphan section warning from the linker, certain configurations
> error when linking with ld.lld, which enables relro by default:
>
>   ld.lld: error: section: .got is not contiguous with other relro sections
>

**[GIT PULL: RISC-V Devicetrees for v6.5 Part 2](http://lore.kernel.org/linux-riscv/20230620-fidelity-variety-60b47c889e31@spud/)**

> Please pull a second part, if it is not too late for v6.5.
> This lot is based on top of v6.4-rc2, because Randy & Linus did a rejig
> of the MAINTAINERS file. As a result, the diff below includes what was
> in the previous PR. Wasn't sure if there was a request-pull incantation
> to exclude what was in PR #1 (I guess I'd have to do a local merge of my
> first PR & then use that as the base for the request-pull command?)
>

**[v3: RISC-V: Document that V registers are clobbered on syscalls](http://lore.kernel.org/linux-riscv/20230619190142.26498-1-palmer@rivosinc.com/)**

> This is included in the ISA manual, but it's pretty common for bits of
> the ISA manual that are actually ABI to change.  So let's document it
> explicitly.
>

**[v8: Add support for Allwinner GPADC on D1/T113s/R329/T507 SoCs](http://lore.kernel.org/linux-riscv/20230619154252.3951913-1-bigunclemax@gmail.com/)**

> This series adds support for general purpose ADC (GPADC) on new
> Allwinner's SoCs, such as D1, T113s, T507 and R329. The implemented driver
> provides basic functionality for getting ADC channels data.
>

**[v4: tools/nolibc: add a new syscall helper](http://lore.kernel.org/linux-riscv/cover.1687187451.git.falcon@tinylab.org/)**

> Thanks very much for your kindly review.
>
> This is the revision of v3 "tools/nolibc: add a new syscall helper" [1],
> this mainly applies the suggestion from David in this reply [2] and
> rebased everything on the dev.2023.06.14a branch of linux-rcu [3].
>

**[v5: nolibc: add part2 of support for rv32](http://lore.kernel.org/linux-riscv/cover.1687176996.git.falcon@tinylab.org/)**

> This is the revision of the v4 part2 of support for rv32 [1], this
> further split the generic KARCH code out of the old rv32 compile patch
> and also add kernel specific KARCH and nolibc specific NARCH for
> tools/include/nolibc/Makefile too.
>
> This is rebased on the dev.2023.06.14a branch of linux-rcu repo [2] with
> basic run-user and run tests.
>

**[v7: Add JH7110 USB PHY driver support](http://lore.kernel.org/linux-riscv/20230619094759.21013-1-minda.chen@starfivetech.com/)**

> This patchset adds USB and PCIe PHY for the StarFive JH7110 SoC.
> The patch has been tested on the VisionFive 2 board.
>

**[v3: Add initialization of clock for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230619083517.415597-1-william.qiu@starfivetech.com/)**

> This patchset adds initial rudimentary support for the StarFive
> Quad SPI controller driver. And this driver will be used in
> StarFive's VisionFive 2 board. In 6.4, the QSPI_AHB and QSPI_APB
> clocks changed from the default ON state to the default OFF state,
> so these clocks need to be enabled in the driver.At the same time,
> dts patch is added to this series.
>

**[v1: kdump: add generic functions to simplify crashkernel crashkernel in architecture](http://lore.kernel.org/linux-riscv/20230619055951.45620-1-bhe@redhat.com/)**

> In the current arm64, crashkernel=,high support has been finished after
> several rounds of posting and careful reviewing. The code in arm64 which
> parses crashkernel kernel parameters firstly, then reserve memory can be
> a good example for other ARCH to refer to.
>

**[v1: riscv: dts: sort makefile entries by directory](http://lore.kernel.org/linux-riscv/20230617-stimulant-untainted-3fa1955d386f@spud/)**

> New additions to the list have tried to respect alphanumeric ordering,
> but the thing was out of order to start with. Sort it.
>

**[v3: Add Sipeed Lichee Pi 4A RISC-V board support](http://lore.kernel.org/linux-riscv/20230617161529.2092-1-jszhang@kernel.org/)**

> Sipeed's Lichee Pi 4A development board uses Lichee Module 4A core
> module which is powered by T-HEAD's TH1520 SoC. Add minimal device
> tree files for the core module and the development board.
>

#### 进程调度

**[v1: Sched/fair: Block nohz tick_stop when cfs bandwidth in use](http://lore.kernel.org/lkml/20230622132751.2900081-1-pauld@redhat.com/)**

> CFS bandwidth limits and NOHZ full don't play well together.  Tasks
> can easily run well past their quotas before a remote tick does
> accounting.  This leads to long, multi-period stalls before such
> tasks can run again. Currentlyi, when presented with these conflicting
> requirements the scheduler is favoring nohz_full and letting the tick
> be stopped. However, nohz tick stopping is already best-effort, there
> are a number of conditions that can prevent it, whereas cfs runtime
> bandwidth is expected to be enforced.
>

**[v3: sched/isolation: add a workqueue parameter onto isolcpus to constrain unbound CPUs](http://lore.kernel.org/lkml/20230622032133.GA29012@didi-ThinkCentre-M930t-N000/)**

> Motivation of doing this is to better improve boot times for devices when
> we want to prevent our workqueue works from running on some specific CPUs,
> i,e, some CPUs are busy with interrupts.
>

**[v2: sched/cputime: Make IRQ time accounting configurable at boot time](http://lore.kernel.org/lkml/20230620141002.23914-1-bvanassche@acm.org/)**

> IRQ time accounting reduces performance by 40% for some block storage
> workloads on Android. Despite this some producers of Android devices
> want to keep IRQ time accounting enabled.
>

#### 内存管理

**[&#22238;&#22797;: v1: mm: vmscan: export func:shrink_slab](http://lore.kernel.org/linux-mm/TYZPR02MB55950D4E176AEEB2FF8EDF6DC621A@TYZPR02MB5595.apcprd02.prod.outlook.com/)**

> >>> On 16.06.23 11:21, lipeifeng@oppo.com wrote:
> >>>
> >>> Some of shrinkers during shrink_slab would enter synchronous-wait due
> >>> to lock or other reasons, which would causes kswapd or direct_reclaim
> >>> to be blocked.
> >>>
> >>> This patch export shrink_slab so that it can be called in drivers
> >>> which can shrink memory independently.
> >>>

**[v1: memblock: report failures when memblock_can_resize is not set](http://lore.kernel.org/linux-mm/20230624032607.921173-1-songshuaishuai@tinylab.org/)**

> The callers of memblock_reserve() do not check the return value
> presuming that memblock_reserve() always succeeds, but there are
> cases where it may fail.
>
> Having numerous memblock reservations at early boot where
> memblock_can_resize is unset may exhaust the INIT_MEMBLOCK_REGIONS sized
> memblock.reserved regions array and an attempt to double this array via
> memblock_double_array() will fail and will return -1 to the caller.
>

**[v1: memblock: Introduce memblock_reserve_node()](http://lore.kernel.org/linux-mm/20230624024622.2959376-1-yajun.deng@linux.dev/)**

> It only returns address now in memblock_find_in_range_node(), we can add a
> parameter pointing to integer for node id of the range, which can be used
> to pass the node id to the new reserve region.
>

**[v2: seqlock,mm: lockdep annotation + write_seqlock_irqsave()](http://lore.kernel.org/linux-mm/20230623171232.892937-1-bigeasy@linutronix.de/)**

> this has been a single patch (2/2) but then it was pointed out that the
> lockdep annotation in seqlock needs to be adjusted to fully close the
> printk window so that there is no printing after the seq-lock has been
> acquired and before printk_deferred_enter() takes effect.
>

**[v2: Improve hugetlbfs read on HWPOISON hugepages](http://lore.kernel.org/linux-mm/20230623164015.3431990-1-jiaqiyan@google.com/)**

> Today when hardware memory is corrupted in a hugetlb hugepage,
> kernel leaves the hugepage in pagecache [1]; otherwise future mmap or
> read will suject to silent data corruption. This is implemented by
> returning -EIO from hugetlb_read_iter immediately if the hugepage has
> HWPOISON flag set.
>

**[v2: elf: correct note name comment](http://lore.kernel.org/linux-mm/455b22b986de4d3bc6d9bfd522378e442943de5f.1687499411.git.baruch@tkos.co.il/)**

> NT_PRFPREG note is named "CORE". Correct the comment accordingly.
>

**[v1: zsmalloc: small compaction improvements](http://lore.kernel.org/linux-mm/20230623044016.366793-1-senozhatsky@chromium.org/)**

> 	A tiny series that can reduce the number of
> find_alloced_obj() invocations (which perform a linear
> scan of sub-page) during compaction. Inspired by Alexey
> Romanov's findings.
>

**[v1: Transparent Contiguous PTEs for User Mappings](http://lore.kernel.org/linux-mm/20230622144210.2623299-1-ryan.roberts@arm.com/)**

> This is a series to opportunistically and transparently use contpte mappings
> (set the contiguous bit in ptes) for user memory when those mappings meet the
> requirements. It is part of a wider effort to improve performance of the 4K
> kernel with the aim of approaching the performance of the 16K kernel, but
> without breaking compatibility and without the associated increase in memory. It
> also benefits the 16K and 64K kernels by enabling 2M THP, since this is the
> contpte size for those kernels.
>

**[v1: use refcount+RCU method to implement lockless slab shrink](http://lore.kernel.org/linux-mm/20230622085335.77010-1-zhengqi.arch@bytedance.com/)**

> We used to implement the lockless slab shrink with SRCU [1], but then kernel
> test robot reported -88.8% regression in stress-ng.ramfs.ops_per_sec test
> case [2], so we reverted it [3].
>
> This patch series aims to re-implement the lockless slab shrink using the
> refcount+RCU method proposed by Dave Chinner [4].
>

**[v1: udmabuf: Add back support for mapping hugetlb pages](http://lore.kernel.org/linux-mm/20230622072710.3707315-1-vivek.kasireddy@intel.com/)**

> The first patch ensures that the mappings needed for handling mmap
> operation would be managed by using the pfn instead of struct page.
> The second patch restores support for mapping hugetlb pages where
> subpages of a hugepage are not directly used anymore (main reason
> for revert) and instead the hugetlb pages and the relevant offsets
> are used to populate the scatterlist for dma-buf export and for
> mmap operation.
>

**[v1: RESEND: elf: correct note name comment](http://lore.kernel.org/linux-mm/a7e56e9c0f821348a4c833ac07e7518f457cbdb8.1687413763.git.baruch@tkos.co.il/)**

> Only the NT_PRFPREG note is named "LINUX". Correct the comment
> accordingly.
>

**[v2: mm: working set reporting](http://lore.kernel.org/linux-mm/20230621180454.973862-1-yuanchu@google.com/)**

> RFC v1: https://lore.kernel.org/linux-mm/20230509185419.1088297-1-yuanchu@google.com/
> For background and interfaces, see the RFC v1 posting.
>

**[v1: mm/page_alloc: Use write_seqlock_irqsave() instead write_seqlock() + local_irq_save().](http://lore.kernel.org/linux-mm/20230621104034.HT6QnNkQ@linutronix.de/)**

> __build_all_zonelists() acquires zonelist_update_seq by first disabling
> interrupts via local_irq_save() and then acquiring the seqlock with
> write_seqlock(). This is troublesome and leads to problems on
> PREEMPT_RT because the inner spinlock_t is now acquired with disabled
> interrupts.
> The API provides write_seqlock_irqsave() which does the right thing in
> one step.
> printk_deferred_enter() has to be invoked in non-migrate-able context to
> ensure that deferred printing is enabled and disabled on the same CPU.
> This is the case after zonelist_update_seq has been acquired.
>

**[v3: mm/min_free_kbytes: modify min_free_kbytes calculation rules](http://lore.kernel.org/linux-mm/20230621092048.5242-1-liuq131@chinatelecom.cn/)**

> The current calculation of min_free_kbytes only uses ZONE_DMA and
> ZONE_NORMAL pages,but the ZONE_MOVABLE zone->_watermark[WMARK_MIN]
> will also divide part of min_free_kbytes.This will cause the min
> watermark of ZONE_NORMAL to be too small in the presence of ZONE_MOVEABLE.
>

**[v1: mm: page_alloc: use the correct type of list for free pages](http://lore.kernel.org/linux-mm/7e7ab533247d40c0ea0373c18a6a48e5667f9e10.1687333557.git.baolin.wang@linux.alibaba.com/)**

> Commit bf75f200569d ("mm/page_alloc: add page->buddy_list and page->pcp_list")
> introduces page->buddy_list and page->pcp_list as a union with page->lru, but
> missed to change get_page_from_free_area() to use page->buddy_list to clarify
> the correct type of list for a free page.
>

#### 文件系统

**[v1: proc: proc_setattr for /proc/$PID/net](http://lore.kernel.org/linux-fsdevel/20230624-proc-net-setattr-v1-0-73176812adee@weissschuh.net/)**

> /proc/$PID/net currently allows the setting of file attributes,
> in contrast to other /proc/$PID/ files and directories.
>
> This would break the nolibc testsuite so the first patch in the series
> removes the offending testcase.
> The "fix" for nolibc-test is intentionally kept trivial as the series
> will most likely go through the filesystem tree and if conflicts arise,
> it is obvious on how to resolve them.
>

**[v1: pipe: Make a partially-satisfied blocking read wait for more](http://lore.kernel.org/linux-fsdevel/2730511.1687559668@warthog.procyon.org.uk/)**

> Can you consider merging something like the attached patch?  Unfortunately,
> there are applications out there that depend on a read from pipe() waiting
> until the buffer is full under some circumstances.  Patch a28c8b9db8a1
> removed the conditionality on there being an attached writer.
>

**[GIT PULL: vfs: mount](http://lore.kernel.org/linux-fsdevel/20230623-leise-anlassen-5499500f0ce0@brauner/)**

> /* Summary */
> This contains the work to extend move_mount() to allow adding a mount
> beneath the topmost mount of a mount stack.
>
> There are two LWN articles about this. One covers the original patch
> series in [1]. The other in [2] summarizes the session and roughly the
> discussion between Al and me at LSFMM. The second article also goes into
> some good questions from attendees.
>

**[GIT PULL: vfs: file](http://lore.kernel.org/linux-fsdevel/20230623-waldarbeiten-normung-c160bb98bf10@brauner/)**

> /* Summary */
> This contains Amir's work to fix a long-standing problem where an
> unprivileged overlayfs mount can be used to avoid fanotify permission
> events that were requested for an inode or superblock on the underlying
> filesystem.
>

**[GIT PULL: vfs: rename](http://lore.kernel.org/linux-fsdevel/20230623-gebacken-abenteuer-00d6913052b6@brauner/)**

> /* Summary */
> This contains the work from Jan to fix problems with cross-directory
> renames originally reported in [1].
>
> To quickly sum it up some filesystems (so far we know at least about
> ext4, udf, f2fs, ocfs2, likely also reiserfs, gfs2 and others) need to
> lock the directory when it is being renamed into another directory.
>

**[GIT PULL: vfs: misc](http://lore.kernel.org/linux-fsdevel/20230623-motor-quirlig-c6afec03aeb4@brauner/)**

> * Use mode 0600 for file created by cachefilesd so it can be run by
>   unprivileged users. This aligns them with directories which are
>   already created with mode 0700 by cachefilesd.
> * Reorder a few members in struct file to prevent some false sharing
>   scenarios.
> * Indicate that an eventfd is used a semaphore in the eventfd's fdinfo
>   procfs file.
> * Add a missing uapi header for eventfd exposing relevant uapi defines.
> * Let the VFS protect transitions of a superblock from read-only to
>   read-write in addition to the protection it already provides for
>   transitions from read-write to read-only. Protecting read-only to
>   read-write transitions allows filesystems such as ext4 to perform
>   internal writes, keeping writers away until the transition is
>   completed.
>

**[GIT PULL: fs: ntfs](http://lore.kernel.org/linux-fsdevel/20230623-pflug-reibt-3435a40349d3@brauner/)**

> /* Summary */
> This contains a pile of various smaller fixes for ntfs. There's really
> not a lot to say about them. I'm just the messenger, so this is an
> unusually short pull request.
>
> /* Testing */
> clang: Ubuntu clang version 15.0.7
>
> All patches are based on v6.4-rc2 and have been sitting in linux-next.
> No build failures or warnings were observed.
>

**[v1: fcntl.2: document F_UNLCK F_OFD_GETLK extension](http://lore.kernel.org/linux-fsdevel/20230622165225.2772076-4-stsp2@yandex.ru/)**

> F_UNLCK has the special meaning when used as a lock type on input.
> It returns the information about any lock found in the specified
> region on that particular file descriptor. Locks on other file
> descriptors are ignored by F_UNLCK.
>

**[v3: F_OFD_GETLK extension to read lock info](http://lore.kernel.org/linux-fsdevel/20230622165225.2772076-1-stsp2@yandex.ru/)**

> This extension allows to use F_UNLCK on query, which currently returns
> EINVAL. Instead it can be used to query the locks on a particular fd -
> something that is not currently possible. The basic idea is that on
> F_OFD_GETLK, F_UNLCK would "conflict" with (or query) any types of the
> lock on the same fd, and ignore any locks on other fds.
>

**[v1: iomap regression for aio dio 4k writes](http://lore.kernel.org/linux-fsdevel/20230621174114.1320834-1-bongiojp@gmail.com/)**

> There has been a standing performance regression involving AIO DIO
> 4k-aligned writes on ext4 backed by a fast local SSD since the switch
> to iomap. I think it was originally reported and investigated in this
> thread: https://lore.kernel.org/all/87lf7rkffv.fsf@collabora.com/
>

**[v1: minimum folio order support in filemap](http://lore.kernel.org/linux-fsdevel/20230621083823.1724337-1-p.raghav@samsung.com/)**

> There has been a lot of discussion recently to support devices and fs for
> bs > ps. One of the main plumbing to support buffered IO is to have a minimum
> order while allocating folios in the page cache.
>
> Hannes sent recently a series[1] where he deduces the minimum folio
> order based on the i_blkbits in struct inode. This takes a different
> approach based on the discussion in that thread where the minimum and
> maximum folio order can be set individually per inode.
>

**[v20: Implement IOCTL to get and optionally clear info about PTEs](http://lore.kernel.org/linux-fsdevel/20230621072404.2918101-1-usama.anjum@collabora.com/)**

> This syscall is used in Windows applications and games etc. This syscall is
> being emulated in pretty slow manner in userspace. Our purpose is to
> enhance the kernel such that we translate it efficiently in a better way.
> Currently some out of tree hack patches are being used to efficiently
> emulate it in some kernels. We intend to replace those with these patches.
> So the whole gaming on Linux can effectively get benefit from this. It
> means there would be tons of users of this code.
>

**[v4: Add support for Vendor Defined Error Types in Einj Module](http://lore.kernel.org/linux-fsdevel/20230621035102.13463-1-avadhut.naik@amd.com/)**

> This patchset adds support for Vendor Defined Error types in the einj
> module by exporting a binary blob file in module's debugfs directory.
> Userspace tools can write OEM Defined Structures into the blob file as
> part of injecting Vendor defined errors.
>

**[v1: next: readdir: Replace one-element arrays with flexible-array members](http://lore.kernel.org/linux-fsdevel/ZJHiPJkNKwxkKz1c@work/)**

> One-element arrays are deprecated, and we are replacing them with flexible
> array members instead. So, replace one-element arrays with flexible-array
> members in multiple structures.
>

**[v1: Support negative dentry cache for FUSE and virtiofs](http://lore.kernel.org/linux-fsdevel/20230620151328.1637569-1-keiichiw@chromium.org/)**

> This patch series adds a new mount option called negative_dentry_timeout
> for FUSE and virtio-fs filesystems. This option allows the kernel to cache
> negative dentries, which are dentries that represent a non-existent file.
> When this option is enabled, the kernel will skip FUSE_LOOKUP requests for
> second and subsequent lookups to a non-existent file.
>

**[v1: ovl: reserve ability to reconfigure mount options with new mount api](http://lore.kernel.org/linux-fsdevel/20230620-fs-overlayfs-mount-api-remount-v1-1-6dfcb89088e3@kernel.org/)**

> We don't need to carry this issue into the new mount api port. Similar
> to FUSE we can use the fs_context::oldapi member to figure out that this
> is a request coming through the legacy mount api. If we detect it we
> continue silently ignoring all mount options.
>

**[v1: RFC: F_OFD_GETLK should provide more info](http://lore.kernel.org/linux-fsdevel/20230620095507.2677463-1-stsp2@yandex.ru/)**

> This patch-set implements 2 small extensions to the current F_OFD_GETLK,
> allowing it to gather more information than it currently returns.
>
> First extension allows to use F_UNLCK on query, which currently returns
> EINVAL. Instead it can be used to query the locks on a particular fd -
> something that is not currently possible. The basic idea is that on
> F_OFD_GETLK, F_UNLCK would "conflict" with (or query) any types of the
> lock on the same fd, and ignore any locks on other fds.
>

**[v2: fs: Provide helpers for manipulating sb->s_readonly_remount](http://lore.kernel.org/linux-fsdevel/20230619111832.3886-1-jack@suse.cz/)**

> Provide helpers to set and clear sb->s_readonly_remount including
> appropriate memory barriers. Also use this opportunity to document what
> the barriers pair with and why they are needed.
>

**[v1: blk: optimization for classic polling](http://lore.kernel.org/linux-fsdevel/3578876466-3733-1-git-send-email-nj.shetty@samsung.com/)**

> This removes the dependency on interrupts to wake up task. Set task
> state as TASK_RUNNING, if need_resched() returns true,
> while polling for IO completion.
> Earlier, polling task used to sleep, relying on interrupt to wake it up.
> This made some IO take very long when interrupt-coalescing is enabled in
> NVMe.
>

#### 网络设备

**[v2: net-next: net: dsa: vsc73xx: Make vsc73xx usable](http://lore.kernel.org/netdev/20230625115343.1603330-8-paweldembicki@gmail.com/)**

> This patch series is focused on getting vsc73xx usable.
>
> First patch was added in v2, it's switch from poll loop to
> read_poll_timeout.
>
> Second patch is simple convert to phylink, because adjust_link won't work
> anymore.
>

**[tc.8: some remarks and a patch for the manual](http://lore.kernel.org/netdev/168764283038.2838.1146738227989939935.reportbug@kassi.invalid.is.lan/)**

> Mark a full stop (.) with "\&",
> if it does not mean an end of a sentence.
> This is a preventive action,
> the paragraph could be reshaped, e.g., after changes.
>
> When typing, one does not always notice when the line wraps after the
> period.
> There are too many examples of input lines in manual pages,
> that end with an abbreviation point.
>

**[v2: net-next: Support offload LED blinking to PHY.](http://lore.kernel.org/netdev/20230624205629.4158216-1-andrew@lunn.ch/)**

> Allow offloading of the LED trigger netdev to PHY drivers and
> implement it for the Marvell PHY driver. Additionally, correct the
> handling of when the initial state of the LED cannot be represented by
> the trigger, and so an error is returned.
>

**[v1: net: lan743x: Don't sleep in atomic context](http://lore.kernel.org/netdev/20230623232949.743733-1-moritzf@google.com/)**

> dev_set_rx_mode() grabs a spin_lock, and the lan743x implementation
> proceeds subsequently to go to sleep using readx_poll_timeout().
>
> Introduce a helper wrapping the readx_poll_timeout_atomic() function
> and use it to replace the calls to readx_polL_timeout().
>

**[v1: use array_size](http://lore.kernel.org/netdev/20230623211457.102544-1-Julia.Lawall@inria.fr/)**

> Use array_size to protect against multiplication overflows.
>
> This follows up on the following patches by Kees Cook from 2018.
>
> 42bc47b35320 ("treewide: Use array_size() in vmalloc()")
> fad953ce0b22 ("treewide: Use array_size() in vzalloc()")
>

**[v2: Add support for sam9x7 SoC family](http://lore.kernel.org/netdev/20230623203056.689705-1-varshini.rajendran@microchip.com/)**

> This patch series adds support for the new SoC family - sam9x7.
>  - The device tree, configs and drivers are added
>  - Clock driver for sam9x7 is added
>  - Support for basic peripherals is added
>  - Target board SAM9X75 Curiosity is added
>

**[v1: net-next: netlink: add display-hint to ynl](http://lore.kernel.org/netdev/20230623201928.14275-1-donald.hunter@gmail.com/)**

> Add a display-hint property to the netlink schema, to be used by generic
> netlink clients as hints about how to display attribute values.
>
> A display-hint on an attribute definition is intended for letting a
> client such as ynl know that, for example, a u32 should be rendered as
> an ipv4 address. The display-hint enumeration includes a small number of
> networking domain-specific value types.
>

**[v3: io_uring: Add io_uring command support for sockets](http://lore.kernel.org/netdev/20230623193532.88760-1-kuniyu@amazon.com/)**

> Date: Thu, 22 Jun 2023 14:59:14 -0700
> > Enable io_uring commands on network sockets. Create two new
> > SOCKET_URING_OP commands that will operate on sockets.
> >
> > In order to call ioctl on sockets, use the file_operations->io_uring_cmd
> > callbacks, and map it to a uring socket function, which handles the
> > SOCKET_URING_OP accordingly, and calls socket ioctls.
> >

**[v1: net-next: dsa/88e6xxx/phylink changes after the next merge window](http://lore.kernel.org/netdev/ZJWpGCtIZ06jiBsO@shell.armlinux.org.uk/)**

> This patch series contains the minimum set of patches that I would like
> to get in for the following merge window.
>
> The first four patches are laying the groundwork for converting the
> mv88e6xxx driver to use phylink PCS support. Patches 5 through 11
> perform that conversion.
>

**[v2: net-next: net/tcp: optimise locking for blocking splice](http://lore.kernel.org/netdev/80736a2cc6d478c383ea565ba825eaf4d1abd876.1687523671.git.asml.silence@gmail.com/)**

> Even when tcp_splice_read() reads all it was asked for, for blocking
> sockets it'll release and immediately regrab the socket lock, loop
> around and break on the while check.
>
> Check tss.len right after we adjust it, and return if we're done.
> That saves us one release_sock(); lock_sock(); pair per successful
> blocking splice read.
>

**[[net-next PATCH RFC] net: dsa: qca8k: make learning configurable and keep off if standalone](http://lore.kernel.org/netdev/20230623114005.9680-1-ansuelsmth@gmail.com/)**

> Address learning should initially be turned off by the driver for port
> operation in standalone mode, then the DSA core handles changes to it
> via ds->ops->port_bridge_flags().
>
> Currently this is not the case for qca8k where learning is enabled
> unconditionally in qca8k_setup for every user port.
>

**[v2: net-next: net: phy: C45-over-C22 access](http://lore.kernel.org/netdev/20230620-feature-c45-over-c22-v2-0-def0ab9ccee2@kernel.org/)**

> [Sorry for the very late follow-up on this series, I simply haven't had
> time to look into it. Should be better now.]
>
> The goal here is to get the GYP215 and LAN8814 running on the Microchip
> LAN9668 SoC. The LAN9668 suppports one external bus and unfortunately, the
> LAN8814 has a bug which makes it impossible to use C45 on that bus.
> Fortunately, it was the intention of the GPY215 driver to be used on a C22
> bus. But I think this could have never really worked, because the
> phy_get_c45_ids() will always do c45 accesses and thus gpy_probe() will
> fail.
>

#### 安全增强

**[v1: next: openprom: Use struct_size() helper](http://lore.kernel.org/linux-hardening/ZJTYWQ5NA726baWK@work/)**

> Prefer struct_size() over open-coded versions.
>

**[v1: ACPI: APEI: Use ERST timeout for slow devices](http://lore.kernel.org/linux-hardening/20230622153554.16847-1-jeshuas@nvidia.com/)**

> Slow devices such as flash may not meet the default 1ms timeout value,
> so use the ERST max execution time value that they provide as the
> timeout if it is larger.
>

**[v1: pstore/ram: Add support for dynamically allocated ramoops memory regions](http://lore.kernel.org/linux-hardening/20230622005213.458236-1-isaacmanjarres@google.com/)**

> The reserved memory region for ramoops is assumed to be at a fixed
> and known location when read from the devicetree. This is not desirable
> in environments where it is preferred for the region to be dynamically
> allocated early during boot (i.e. the memory region is defined with
> the "alloc-ranges" property instead of the "reg" property).
>

**[v1: next: reiserfs: Replace one-element array with flexible-array member](http://lore.kernel.org/linux-hardening/ZJN9Kqhcs0ZGET%2F8@work/)**

> One-element arrays are deprecated, and we are replacing them with flexible
> array members instead. So, replace one-element array with flexible-array
> member in direntry_uarea structure, and refactor the rest of the code,
> accordingly.
>

**[v1: next: ksmbd: Use struct_size() helper in ksmbd_negotiate_smb_dialect()](http://lore.kernel.org/linux-hardening/ZJNrsjDEfe0iwQ92@work/)**

> Prefer struct_size() over open-coded versions.
>

**[v1: next: smb: Replace one-element array with flexible-array member](http://lore.kernel.org/linux-hardening/ZJNnynWOoTp6uTwF@work/)**

> One-element arrays are deprecated, and we are replacing them with flexible
> array members instead. So, replace one-element array with flexible-array
> member in struct smb_negotiate_req.
>
> This results in no differences in binary output.
>

**[v1: next: scsi: smartpqi: Replace one-element arrays with flexible-array members](http://lore.kernel.org/linux-hardening/ZJNdKDkuRbFZpASS@work/)**

> One-element arrays are deprecated, and we are replacing them with flexible
> array members instead. So, replace one-element arrays with flexible-array
> members in a couple of structures, and refactor the rest of the code,
> accordingly.
>
> This helps with the ongoing efforts to tighten the FORTIFY_SOURCE
> routines on memcpy().
>
> This results in no differences in binary output.
>

**[v1: scsi: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230621030033.3800351-1-azeemshaikh38@gmail.com/)**

> This patch series replaces strlcpy in the scsi subsystem wherever trivial
> replacement is possible, i.e return value from strlcpy is unused. The patches
> themselves are independent of each other and are included as a series for
> ease of review.
>

**[v1: net: wwan: iosm: Convert single instance struct member to flexible array](http://lore.kernel.org/linux-hardening/20230620194234.never.023-kees@kernel.org/)**

> Adjust the struct mux_adth definition and associated sizeof() math; no binary
> output differences are observed in the resulting object file.
>
> Closes: https://lore.kernel.org/lkml/dbfa25f5-64c8-5574-4f5d-0151ba95d232@gmail.com/
>

**[v1: igc: Ignore AER reset when device is suspended](http://lore.kernel.org/linux-hardening/20230620123636.1854690-1-kai.heng.feng@canonical.com/)**

> The issue is that the PTM requests are sending before driver resumes the
> device. Since the issue can also be observed on Windows, it's quite
> likely a firmware/hardwar limitation.
>
> So avoid resetting the device if it's not resumed. Once the device is
> fully resumed, the device can work normally.
>

#### 异步 IO

**[v1: liburing: Introduce '--use-libc' option](http://lore.kernel.org/io-uring/20230622172029.726710-1-ammarfaizi2@gnuweeb.org/)**

> This is an RFC patch series to introduce the '--use-libc' option to the
> configure script.
>
> Currently, when compiling liburing on x86, x86-64, and aarch64
> architectures, the resulting binary lacks the linkage with the standard
> C library (libc).
>

**[v2: io_uring/net: disable partial retries for recvmsg with cmsg](http://lore.kernel.org/io-uring/7e16d521-7c8a-3ac7-497a-04e69fee1afe@kernel.dk/)**

> We cannot sanely handle partial retries for recvmsg if we have cmsg
> attached. If we don't, then we'd just be overwriting the initial cmsg
> header on retries. Alternatively we could increment and handle this
> appropriately, but it doesn't seem worth the complication.
>

**[v2: io_uring/net: clear msg_controllen on partial sendmsg retry](http://lore.kernel.org/io-uring/312cc2b7-8229-c167-e230-bc1d7d0ed61b@kernel.dk/)**

> If we have cmsg attached AND we transferred partial data at least, clear
> msg_controllen on retry so we don't attempt to send that again.
>

#### Rust For Linux

**[v1: Rust device mapper abstractions](http://lore.kernel.org/rust-for-linux/20230625121657.3631109-1-changxian.cqs@antgroup.com/)**

> Additionally, there are some dummy codes used to wrap the block
> layer structs, i.e., `bio` and `request`, which seems being
> in the review process, so I just place it in the same file.
>

**[v1: rust: alloc: Add realloc and alloc_zeroed to the GlobalAlloc impl](http://lore.kernel.org/rust-for-linux/20230622-global_alloc_methods-v1-1-3d3561593e23@protonmail.com/)**

> While there are default impls for these methods, using the respective C
> api's is faster. Currently neither the existing nor these new
> GlobalAlloc method implementations are actually called. Instead the
> __rust_* function defined below the GlobalAlloc impl are used. With
> rustc 1.71 these functions will be gone and all allocation calls will go
> through the GlobalAlloc implementation.
>

#### BPF

**[v2: libbpf: kprobe.multi: Filter with available_filter_functions_addrs](http://lore.kernel.org/bpf/20230625011326.1729020-1-liu.yun@linux.dev/)**

> When using regular expression matching with "kprobe multi", it scans all
> the functions under "/proc/kallsyms" that can be matched. However, not all
> of them can be traced by kprobe.multi. If any one of the functions fails
> to be traced, it will result in the failure of all functions. The best
> approach is to filter out the functions that cannot be traced to ensure
> proper tracking of the functions.
>

**[v1: perf: Replace deprecated -target with --target= for Clang](http://lore.kernel.org/bpf/20230624002708.1907962-1-maskray@google.com/)**

> -target has been deprecated since Clang 3.4 in 2013. Use the preferred
> --target=bpf form instead. This matches how we use --target= in
> scripts/Makefile.clang.
>

**[v2: bpf: Replace deprecated -target with --target= for Clang](http://lore.kernel.org/bpf/20230624001856.1903733-1-maskray@google.com/)**

> -target has been deprecated since Clang 3.4 in 2013. Use the preferred
> --target=bpf form instead. This matches how we use --target= in
> scripts/Makefile.clang.
>

**[v4: lib/test_bpf: Call page_address() on page acquired with GFP_KERNEL flag](http://lore.kernel.org/bpf/20230623151644.GA434468@sumitra.com/)**

> generate_test_data() acquires a page with alloc_page(GFP_KERNEL).
> The GFP_KERNEL is typical for kernel-internal allocations.
> The caller requires ZONE_NORMAL or a lower zone for direct access.
>
> Therefore the page cannot come from ZONE_HIGHMEM. Thus there's
> no need to map it with kmap().
>

**[v5: bpf-next: bpf: Support ->fill_link_info for kprobe_multi and perf_event links](http://lore.kernel.org/bpf/20230623141546.3751-1-laoar.shao@gmail.com/)**

> This patchset enhances the usability of kprobe_multi program by introducing
> support for ->fill_link_info. This allows users to easily determine the
> probed functions associated with a kprobe_multi program. While
> `bpftool perf show` already provides information about functions probed by
> perf_event programs, supporting ->fill_link_info ensures consistent access
> to this information across all bpf links.
>

**[v4: Bring back vmlinux.h generation](http://lore.kernel.org/bpf/20230623041405.4039475-1-irogers@google.com/)**

> Commit 760ebc45746b ("perf lock contention: Add empty 'struct rq' to
> satisfy libbpf 'runqueue' type verification") inadvertently created a
> declaration of 'struct rq' that conflicted with a generated
> vmlinux.h's:
>

**[[RFC v2 PATCH bpf-next 0/4] bpf: add percpu stats for bpf_map](http://lore.kernel.org/bpf/20230622095330.1023453-1-aspsk@isovalent.com/)**

> This series adds a mechanism for maps to populate per-cpu counters of elements
> on insertions/deletions. The sum of these counters can be accessed by a new
> kfunc from a map iterator program.
>

**[v7: bpf-next: bpf, x86: allow function arguments up to 12 for TRACING](http://lore.kernel.org/bpf/20230622075715.1818144-1-imagedong@tencent.com/)**

> Therefore, let's enhance it by increasing the function arguments count
> allowed in arch_prepare_bpf_trampoline(), for now, only x86_64.
>
> In the 1st patch, we save/restore regs with BPF_DW size to make the code
> in save_regs()/restore_regs() simpler.
>
> In the 2nd patch, we make arch_prepare_bpf_trampoline() support to copy
> function arguments in stack for x86 arch. Therefore, the maximum
> arguments can be up to MAX_BPF_FUNC_ARGS for FENTRY, FEXIT and
> MODIFY_RETURN. Meanwhile, we clean the potential garbage value when we
> copy the arguments on-stack.
>

**[v1: net-next: TSN auto negotiation between 1G and 2.5G](http://lore.kernel.org/bpf/20230622041905.629430-1-yong.liang.choong@linux.intel.com/)**

> Intel platforms&#8217; integrated Gigabit Ethernet controllers support
> 2.5Gbps mode statically using BIOS programming. In the current
> implementation, the BIOS menu provides an option to select between
> programs the Phase Lock Loop (PLL) registers. The BIOS also read the
> TSN lane registers from Flexible I/O Adapter (FIA) block and provided
> auto-negotiation between 10/100/1000Mbps and 2.5Gbps is not allowed.
>

**[v3: bpf-next: BPF token](http://lore.kernel.org/bpf/20230621233809.1941811-1-andrii@kernel.org/)**

> This patch set introduces new BPF object, BPF token, which allows to delegate
> a subset of BPF functionality from privileged system-wide daemon (e.g.,
> systemd or any other container manager) to a *trusted* unprivileged
> application. Trust is the key here. This functionality is not about allowing
> unconditional unprivileged BPF usage. Establishing trust, though, is
> completely up to the discretion of respective privileged application that
> would create a BPF token, as different production setups can and do achieve it
> through a combination of different means (signing, LSM, code reviews, etc),
> and it's undesirable and infeasible for kernel to enforce any particular way
> of validating trustworthiness of particular process.
>

**[v2: bpf-next: bpf: Netdev TX metadata](http://lore.kernel.org/bpf/20230621170244.1283336-1-sdf@google.com/)**

> - Support passing metadata via XSK
>   - Showcase how to consume this metadata at TX in the selftests
> - Sample untested mlx5 implementation
> - Simplify attach/detach story with simple global fentry (Alexei)
> - Add 'return 0' in xdp_metadata selftest (Willem)
> - Add missing 'sizeof(*ip6h)' in xdp_hw_metadata selftest (Willem)
> - Document 'timestamp' argument of kfunc (Simon)
> - Not relevant due to attach/detach rework:
>   - s/devtx_sb/devtx_submit/ in netdev (Willem)
>   - s/devtx_cp/devtx_complete/ in netdev (Willem)
>   - Document 'devtx_complete' and 'devtx_submit' in netdev (Simon)
>   - Add devtx_sb/devtx_cp forward declaration (Simon)
>   - Add missing __rcu/rcu_dereference annotations (Simon)
>

**[v1: fs: new accessors for inode->i_ctime](http://lore.kernel.org/bpf/20230621144507.55591-1-jlayton@kernel.org/)**

> I've been working on a patchset to change how the inode->i_ctime is
> accessed in order to give us conditional, high-res timestamps for the
> ctime and mtime. struct timespec64 has unused bits in it that we can use
> to implement this. In order to do that however, we need to wrap all
> accesses of inode->i_ctime to ensure that bits used as flags are
> appropriately handled.
>
> This patchset first adds some new inode_ctime_* accessor functions. It
> then converts all in-tree accesses of inode->i_ctime to use those new
> functions and then renames the i_ctime field to __i_ctime to help ensure
> that use of the accessors.
>

**[v1: bpf-next: bpf: Add two new bpf helpers bpf_perf_type_[uk]probe()](http://lore.kernel.org/bpf/20230621120042.3903-3-laoar.shao@gmail.com/)**

> We are utilizing BPF LSM to monitor BPF operations within our container
> environment. Our goal is to examine the program type and perform the
> respective audits in our LSM program.
>
> When it comes to the perf_event BPF program, there are no specific
> definitions for the perf types of kprobe or uprobe. In other words, there
> is no PERF_TYPE_[UK]PROBE. It appears that defining them as UAPI at this
> stage would be impractical.
>

**[v5: bpf-next: Handle immediate reuse in bpf memory allocator](http://lore.kernel.org/bpf/20230619143231.222536-1-houtao@huaweicloud.com/)**

> V5 incorporates suggestions from Alexei and Paul (Big thanks for that).
> The main changes includes:
> *) Use per-cpu list for reusable list and freeing list to reduce lock
>    contention and retain numa-ware attribute
> *) Use multiple RCU callback for reuse as v3 did
> *) Use rcu_momentary_dyntick_idle() to reduce the peak memory footprint
>

**[v1: net-next: virtio-net: avoid XDP and _F_GUEST_CSUM](http://lore.kernel.org/bpf/20230619105738.117733-1-hengqi@linux.alibaba.com/)**

> virtio-net needs to clear the VIRTIO_NET_F_GUEST_CSUM feature when
> loading XDP. The main reason for doing this is because
> VIRTIO_NET_F_GUEST_CSUM allows to receive packets marked as
> VIRTIO_NET_HDR_F_NEEDS_CSUM. Such packets are not compatible with
> XDP programs, because we cannot guarantee that the csum_{start, offset}
> fields are correct after XDP modifies the packets.
>

**[v3: bpf-next: bpf, arm64: use BPF prog pack allocator in BPF JIT](http://lore.kernel.org/bpf/20230619100121.27534-1-puranjay12@gmail.com/)**

> BPF programs currently consume a page each on ARM64. For systems with many BPF
> programs, this adds significant pressure to instruction TLB. High iTLB pressure
> usually causes slow down for the whole system.
>
> Song Liu introduced the BPF prog pack allocator[1] to mitigate the above issue.
> It packs multiple BPF programs into a single huge page. It is currently only
> enabled for the x86_64 BPF JIT.
>

### 周边技术动态

#### Qemu

**[QEMU RISC-V](http://lore.kernel.org/qemu-devel/CAK-FQ7uOUhAhmgqBOv5fYukFmz-hSp=XEaeyrmiAi2_UBncU0A@mail.gmail.com/)**

> hello,
> I built RISC-V toolchain and QEMU as follows:
> # Install prerequisites:
> https://github.com/riscv-collab/riscv-gnu-toolchain#prerequisites
> # Install additional prerequisites:
> https://github.com/riscv-collab/riscv-gnu-toolchain/issues/1251
> git clone https://github.com/riscv-collab/riscv-gnu-toolchain
> cd riscv-gnu-toolchain
> ./configure --prefix=/home/RISCV-installed-Tools --with-arch=rv32i_zicsr
> --with-abi=ilp32
> make
> make build-qemu
>

**[v2: target/riscv: Restrict KVM-specific fields from ArchCPU](http://lore.kernel.org/qemu-devel/20230624192957.14067-1-philmd@linaro.org/)**

> These fields shouldn't be accessed when KVM is not available.
>
> Restrict the KVM timer migration state. Rename the KVM timer
> post_load() handler accordingly, because cpu_post_load() is
> too generic.
>

**[v4: Add RISC-V KVM AIA Support](http://lore.kernel.org/qemu-devel/20230621145500.25624-1-yongxuan.wang@sifive.com/)**

> This series adds support for KVM AIA in RISC-V architecture.
>
> In order to test these patches, we require Linux with KVM AIA support which can
> be found in the riscv_kvm_aia_hwaccel_v1 branch at
> https://github.com/avpatel/linux.git
>

**[v1: linux-user/riscv: Add syscall riscv_hwprobe](http://lore.kernel.org/qemu-devel/06a4543df2aa6101ca9a48f21a3198064b4f1f87.camel@rivosinc.com/)**

> This patch adds the new syscall for the
> "RISC-V Hardware Probing Interface"
> (https://docs.kernel.org/riscv/hwprobe.html).
>

#### U-Boot

**[v2: riscv: Add ACLINT mtimer and mswi devices support](http://lore.kernel.org/u-boot/20230621151147.1523273-1-bmeng@tinylab.org/)**

> This RISC-V ACLINT specification [1] defines a set of memory mapped
> devices which provide inter-processor interrupts (IPI) and timer
> functionalities for each HART on a multi-HART RISC-V platform.
>
> This seriesl updates U-Boot existing SiFive CLINT driver to handle
> the ACLINT changes, and is now able to support both CLINT and ACLINT.
>

**[CFP open for RISC-V MC at Linux Plumbers Conference 2023](http://lore.kernel.org/u-boot/CAOnJCU+uMF-brnyjA1HLhupYPqL0ebOVu+ivrSn2AewuSrhtBw@mail.gmail.com/)**

> The CFP for topic proposals for the RISC-V micro conference[1] 2023 is open now.
> Please submit your proposal before it's too late!
>
> The Linux plumbers event will be both in person and remote
> (hybrid)virtual this year. More details can be found here [2].
>

## 20230618：第 50 期

### 内核动态

#### 文件系统

**[v1: d_path: include internal.h](http://lore.kernel.org/linux-fsdevel/20230616164627.66340-1-ben.dooks@codethink.co.uk/)**

> Include internal.h to get the definition of simple_dname, to fix the
> following sparse warning:
>
> fs/d_path.c:317:6: warning: symbol 'simple_dname' was not declared. Should it be static?
>

**[v1: fs: Provide helpers for manipulating sb->s_readonly_remount](http://lore.kernel.org/linux-fsdevel/20230616163827.19377-1-jack@suse.cz/)**

> Provide helpers to set and clear sb->s_readonly_remount including
> appropriate memory barriers. Also use this opportunity to document what
> the barriers pair with and why they are needed.
>

**[v5: dax: enable dax fault handler to report VM_FAULT_HWPOISON](http://lore.kernel.org/linux-fsdevel/20230615181325.1327259-1-jane.chu@oracle.com/)**

> Change from v4:
> Add comments describing when and why dax_mem2blk_err() is used.
> Suggested by Dan.
>

**[v19: Implement IOCTL to get and optionally clear info about PTEs](http://lore.kernel.org/linux-fsdevel/20230615141144.665148-1-usama.anjum@collabora.com/)**

> At this point, we left soft-dirty considering it is too much delicate and
> userfaultfd [9] seemed like the only way forward. From there onward, we
> have been basing soft-dirty emulation on userfaultfd wp feature where
> kernel resolves the faults itself when WP_ASYNC feature is used. It was
> straight forward to add WP_ASYNC feature in userfautlfd. Now we get only
> those pages dirty or written-to which are really written in reality. (PS
> There is another WP_UNPOPULATED userfautfd feature is required which is
> needed to avoid pre-faulting memory before write-protecting [9].)
>

**[v1: fs: Protect reconfiguration of sb read-write from racing writes](http://lore.kernel.org/linux-fsdevel/20230615113848.8439-1-jack@suse.cz/)**

> The reconfigure / remount code takes a lot of effort to protect
> filesystem's reconfiguration code from racing writes on remounting
> read-only. However during remounting read-only filesystem to read-write
> mode userspace writes can start immediately once we clear SB_RDONLY
> flag. This is inconvenient for example for ext4 because we need to do
> some writes to the filesystem (such as preparation of quota files)
> before we can take userspace writes so we are clearing SB_RDONLY flag
> before we are fully ready to accept userpace writes and syzbot has found
> a way to exploit this [1]. Also as far as I'm reading the code
> the filesystem remount code was protected from racing writes in the
> legacy mount path by the mount's MNT_READONLY flag so this is relatively
> new problem. It is actually fairly easy to protect remount read-write
> from racing writes using sb->s_readonly_remount flag so let's just do
> that instead of having to workaround these races in the filesystem code.
>

**[v5: Handle notifications on overlayfs fake path files](http://lore.kernel.org/linux-fsdevel/20230615112229.2143178-1-amir73il@gmail.com/)**

> A little while ago, Jan and I realized that an unprivileged overlayfs
> mount could be used to avert fanotify permission events that were
> requested for an inode or sb on the underlying fs.
>

**[v1: exfat: get file size from DataLength](http://lore.kernel.org/linux-fsdevel/PUZPR04MB6316DB8A8CB6107D56716EBC815BA@PUZPR04MB6316.apcprd04.prod.outlook.com/)**

> From the exFAT specification, the file size should get from 'DataLength'
> of Stream Extension Directory Entry, not 'ValidDataLength'.
>

**[v3: eventfd: add a uapi header for eventfd userspace APIs](http://lore.kernel.org/linux-fsdevel/tencent_2B6A999A23E86E522D5D9859D54FFCF9AA05@qq.com/)**

> Create a uapi header include/uapi/linux/eventfd.h, move the associated
> flags to the uapi header, and include it from linux/eventfd.h.
>

**[v1: fs: use helpers for opening kernel internal files](http://lore.kernel.org/linux-fsdevel/20230614120917.2037482-1-amir73il@gmail.com/)**

> Overlayfs and cachefiles use vfs_open_tmpfile() to open a tmpfile
> without accounting for nr_files.
>
> Rename this helper to kernel_tmpfile_open() to better reflect this
> helper is used for kernel internal users.
>

**[v1: RFC: high-order folio support for I/O](http://lore.kernel.org/linux-fsdevel/20230614114637.89759-1-hare@suse.de/)**

> now, that was easy.
> Thanks to willy and his recent patchset to support large folios in
> gfs2 turns out that most of the work to support high-order folios
> for I/O is actually done.
> It only need twe rather obvious patches to allocate folios with
> the order derived from the mapping blocksize, and to adjust readahead
> to avoid reading off the end of the device.

**[v3: Add support for Vendor Defined Error Types in Einj Module](http://lore.kernel.org/linux-fsdevel/f9e8243d-78e2-4aa1-e6f2-5ac2a8c1745d@amd.com/)**

> On 6/13/2023 03:01, Greg KH wrote:
> > On Mon, Jun 12, 2023 at 09:51:36PM +0000, Avadhut Naik wrote:
> >> This patchset adds support for Vendor Defined Error types in the einj
> >> module by exporting a binary blob file in module's debugfs directory.
> >> Userspace tools can write OEM Defined Structures into the blob file as
> >> part of injecting Vendor defined errors.
> >>

**[v1: Report on physically contiguous memory in smaps](http://lore.kernel.org/linux-fsdevel/20230613160950.3554675-1-ryan.roberts@arm.com/)**

> This series adds new entries to /proc/pid/smaps[_rollup] to report on physically
> contiguous runs of memory. The first patch reports on the sizes of the runs by
> binning into power-of-2 blocks and reporting how much memory is in which bin.
> The second patch reports on how much of the memory is contpte-mapped in the page
> table (this is a hint that arm64 supports to tell the HW that a range of ptes
> map physically contiguous memory).
>

**[v1: errseq_t: split the ERRSEQ_SEEN flag into two](http://lore.kernel.org/linux-fsdevel/20230613121521.146865-1-jlayton@kernel.org/)**

> NFS wants to use the errseq_t mechanism to detect errors that occur
> during a write, but for that use-case we want to ignore anything that
> happened before the sample point.
>

**[v3: gfs2/buffer folio changes for 6.5](http://lore.kernel.org/linux-fsdevel/20230612210141.730128-1-willy@infradead.org/)**

> This kind of started off as a gfs2 patch series, then became entwined
> with buffer heads once I realised that gfs2 was the only remaining
> caller of __block_write_full_page().  For those not in the gfs2 world,
> the big point of this series is that block_write_full_page() should now
> handle large folios correctly.
>

**[v3: Create large folios in iomap buffered write path](http://lore.kernel.org/linux-fsdevel/20230612203910.724378-1-willy@infradead.org/)**

> The problem ends up being lock contention on the i_pages spinlock as we
> clear the writeback bit on each folio (and propagate that up through
> the tree).  By using larger folios, we decrease the number of folios
> to be processed by a factor of 256 for this benchmark, eliminating the
> lock contention.
>

**[v2: Landlock support for UML](http://lore.kernel.org/linux-fsdevel/20230612191430.339153-1-mic@digikod.net/)**

> Commit cb2c7d1a1776 ("landlock: Support filesystem access-control")
> introduced a new ARCH_EPHEMERAL_INODES configuration, only enabled for
> User-Mode Linux.  The reason was that UML's hostfs managed inodes in an
> ephemeral way: from the kernel point of view, the same inode struct
> could be created several times while being used by user space because
> the kernel didn't hold references to inodes.  Because Landlock (and
> probably other subsystems) ties properties (i.e. access rights) to inode
> objects, it wasn't possible to create rules that match inodes and then
> allow specific accesses.
>

**[v1: block: Add config option to not allow writing to mounted devices](http://lore.kernel.org/linux-fsdevel/20230612161614.10302-1-jack@suse.cz/)**

> Writing to mounted devices is dangerous and can lead to filesystem
> corruption as well as crashes. Furthermore syzbot comes with more and
> more involved examples how to corrupt block device under a mounted
> filesystem leading to kernel crashes and reports we can do nothing
> about. Add config option to disallow writing to mounted (exclusively
> open) block devices. Syzbot can use this option to avoid uninteresting
> crashes. Also users whose userspace setup does not need writing to
> mounted block devices can set this config option for hardening.
>

**[v5: blksnap - block devices snapshots module](http://lore.kernel.org/linux-fsdevel/20230612135228.10702-1-sergei.shtepa@veeam.com/)**

> I am happy to offer a improved version of the Block Devices Snapshots
> Module. It allows to create non-persistent snapshots of any block devices.
> The main purpose of such snapshots is to provide backups of block devices.
> See more in Documentation/block/blksnap.rst.
>

**[v1: zonefs: set FMODE_CAN_ODIRECT instead of a dummy direct_IO method](http://lore.kernel.org/linux-fsdevel/20230612053515.585428-1-hch@lst.de/)**

> Since commit a2ad63daa88b ("VFS: add FMODE_CAN_ODIRECT file flag") file
> systems can just set the FMODE_CAN_ODIRECT flag at open time instead of
> wiring up a dummy direct_IO method to indicate support for direct I/O.
> Do that for zonefs so that noop_direct_IO can eventually be removed.
>

**[v1: fs: kernel and userspace filesystem freeze](http://lore.kernel.org/linux-fsdevel/168653971691.755178.4003354804404850534.stgit@frogsfrogsfrogs/)**

> Sometimes, kernel filesystem drivers need the ability to quiesce writes
> to the filesystem so that the driver can perform some kind of
> maintenance activity.  This capability mostly already exists in the form
> of filesystem freezing but with the huge caveat that userspace can thaw
> any frozen fs at any time.  If the correctness of the fs maintenance
> program requires stillness of the filesystem, then this caveat is BAD.
>

**[v1: nilfs2: prevent general protection fault in nilfs_clear_dirty_page()](http://lore.kernel.org/linux-fsdevel/20230612021456.3682-1-konishi.ryusuke@gmail.com/)**

> In a syzbot stress test that deliberately causes file system errors on
> nilfs2 with a corrupted disk image, it has been reported that
> nilfs_clear_dirty_page() called from nilfs_clear_dirty_pages() can cause
> a general protection fault.
>

**[v1: eventfd: show flags in fdinfo](http://lore.kernel.org/linux-fsdevel/tencent_59C3AA88A8F1829226E5D3619837FC4A9E09@qq.com/)**

> The flags should be displayed in fdinfo, as different flags
> could affect the behavior of eventfd.
>

**[v1: fsnotify: move fsnotify_open() hook into do_dentry_open()](http://lore.kernel.org/linux-fsdevel/20230611122429.1499617-1-amir73il@gmail.com/)**

> fsnotify_open() hook is called only from high level system calls
> context and not called for the very many helpers to open files.
>

**[v1: sysctl: set variable sysctl_mount_point storage-class-specifier to static](http://lore.kernel.org/linux-fsdevel/20230611120725.183182-1-trix@redhat.com/)**

> smatch reports
> fs/proc/proc_sysctl.c:32:18: warning: symbol
>   'sysctl_mount_point' was not declared. Should it be static?
>
> This variable is only used in its defining file, so it should be static.
>

**[v1: blk: optimization for classic polling](http://lore.kernel.org/linux-fsdevel/3578876466-3733-1-git-send-email-nj.shetty@samsung.com/)**

> This removes the dependency on interrupts to wake up task. Set task
> state as TASK_RUNNING, if need_resched() returns true,
> while polling for IO completion.
> Earlier, polling task used to sleep, relying on interrupt to wake it up.
> This made some IO take very long when interrupt-coalescing is enabled in
> NVMe.
>

#### 网络设备

**[v2: net: revert "net: align SO_RCVMARK required privileges with SO_MARK"](http://lore.kernel.org/netdev/20230618103130.51628-1-maze@google.com/)**

> This reverts commit 1f86123b9749 ("net: align SO_RCVMARK required
> privileges with SO_MARK") because the reasoning in the commit message
> is not really correct:
>   SO_RCVMARK is used for 'reading' incoming skb mark (via cmsg), as such
>   it is more equivalent to 'getsockopt(SO_MARK)' which has no priv check
>   and retrieves the socket mark, rather than 'setsockopt(SO_MARK) which
>   sets the socket mark and does require privs.
>

**[v1: net-next: netlabel: Reorder fields in 'struct netlbl_domaddr6_map'](http://lore.kernel.org/netdev/aa109847260e51e174c823b6d1441f75be370f01.1687083361.git.christophe.jaillet@wanadoo.fr/)**

> Group some variables based on their sizes to reduce hole and avoid padding.
> On x86_64, this shrinks the size of 'struct netlbl_domaddr6_map'
> from 72 to 64 bytes.
>
> It saves a few bytes of memory and is more cache-line friendly.
>

**[v1: net-next: mptcp: Reorder fields in 'struct mptcp_pm_add_entry'](http://lore.kernel.org/netdev/e47b71de54fd3e580544be56fc1bb2985c77b0f4.1687081558.git.christophe.jaillet@wanadoo.fr/)**

> Group some variables based on their sizes to reduce hole and avoid padding.
> On x86_64, this shrinks the size of 'struct mptcp_pm_add_entry'
> from 136 to 128 bytes.
>

**[v1: net-next: mctp: Reorder fields in 'struct mctp_route'](http://lore.kernel.org/netdev/393ad1a5aef0aa28d839eeb3d7477da0e0eeb0b0.1687080803.git.christophe.jaillet@wanadoo.fr/)**

> Group some variables based on their sizes to reduce hole and avoid padding.
> On x86_64, this shrinks the size of 'struct mctp_route'
> from 72 to 64 bytes.
>

**[v1: net-next: dt-bindings: net: bluetooth: qualcomm: document VDD_CH1](http://lore.kernel.org/netdev/20230617165716.279857-1-krzysztof.kozlowski@linaro.org/)**

> WCN3990 comes with two chains - CH0 and CH1 - where each takes VDD
> regulator.  It seems VDD_CH1 is optional (Linux driver does not care
> about it), so document it to fix dtbs_check warnings like:
>
>   sdm850-lenovo-yoga-c630.dtb: bluetooth: 'vddch1-supply' does not match any of the regexes: 'pinctrl-[0-9]+'
>

**[v1: net-next: net: phy: at803x: Use devm_regulator_get_enable_optional()](http://lore.kernel.org/netdev/f5fdf1a50bb164b4f59409d3a70a2689515d59c9.1687011839.git.christophe.jaillet@wanadoo.fr/)**

> Use devm_regulator_get_enable_optional() instead of hand writing it. It
> saves some line of code.
>

**[v1: selftests: tc-testing: add one test for flushing explicitly created chain](http://lore.kernel.org/netdev/20230617032033.892064-1-renmingshuai@huawei.com/)**

> Add the test for additional reference to chains that are explicitly created
>  by RTM_NEWCHAIN message
>
> commit c9a82bec02c3 ("net/sched: cls_api: Fix lockup on flushing explicitly
>  created chain")
>

**[v3: net-next:pull request: Introduce Intel IDPF driver](http://lore.kernel.org/netdev/20230616231341.2885622-1-anthony.l.nguyen@intel.com/)**

> This patch series introduces the Intel Infrastructure Data Path Function
> (IDPF) driver. It is used for both physical and virtual functions. Except
> for some of the device operations the rest of the functionality is the
> same for both PF and VF. IDPF uses virtchnl version2 opcodes and
> structures defined in the virtchnl2 header file which helps the driver
> to learn the capabilities and register offsets from the device
> Control Plane (CP) instead of assuming the default values.
>

**[v1: net: selftests/ptp: Add support for new timestamp IOCTLs](http://lore.kernel.org/netdev/cover.1686955631.git.alex.maftei@amd.com/)**

> PTP_SYS_OFFSET_EXTENDED was added in November 2018 in
> and PTP_SYS_OFFSET_PRECISE was added in February 2016 in
> 719f1aa4a671 ("ptp: Add PTP_SYS_OFFSET_PRECISE for driver crosstimestamping")
>

**[v8: net-next: Brcm ASP 2.0 Ethernet Controller](http://lore.kernel.org/netdev/1686953664-17498-1-git-send-email-justin.chen@broadcom.com/)**

> Add support for the Broadcom ASP 2.0 Ethernet controller which is first
> introduced with 72165.
>
> 2.7.4
>
> [-- Attachment #2: S/MIME Cryptographic Signature --]
> [-- Type: application/pkcs7-signature, Size: 4206 bytes --]
>

**[v1: net-next: net: dqs: add NIC stall detector based on BQL](http://lore.kernel.org/netdev/20230616213236.2379935-1-kuba@kernel.org/)**

> softnet_data->time_squeeze is sometimes used as a proxy for
> host overload or indication of scheduling problems. In practice
> this statistic is very noisy and has hard to grasp units -
> e.g. is 10 squeezes a second to be expected, or high?
>

**[v2: net-next: gro: move the tc_ext comparison to a helper](http://lore.kernel.org/netdev/20230616204939.2373785-1-kuba@kernel.org/)**

> The double ifdefs (one for the variable declaration and
> one around the code) are quite aesthetically displeasing.
> Factor this code out into a helper for easier wrapping.
>

**[v5: net: phy: Add sysfs attribute for PHY c45 identifiers.](http://lore.kernel.org/netdev/20230616144017.12483-1-zhaojh329@gmail.com/)**

> If a phydevice use c45, its phy_id property is always 0, so
> this adds a c45_ids sysfs attribute group contains mmd id
> attributes from mmd0 to mmd31 to MDIO devices. Note that only
> mmd with valid value will exist. This attribute group can be
> useful when debugging problems related to phy drivers.
>

**[v1: net-next: Add TJA1120 support](http://lore.kernel.org/netdev/20230616135323.98215-1-radu-nicolae.pirea@oss.nxp.com/)**

> This patch series got bigger than I expected. It cleans up the
> next-c45-tja11xx driver and adds support for the TJA1120(1000BaseT1
> automotive phy).
>
> Master/slave custom implementation was replaced with the generic
> implementation (genphy_c45_config_aneg/genphy_c45_read_status).
>

**[v1: nfc: fdp: Add MODULE_FIRMWARE macros](http://lore.kernel.org/netdev/20230616122218.1036256-1-juerg.haefliger@canonical.com/)**

> The module loads firmware so add MODULE_FIRMWARE macros to provide that
> information via modinfo.
>

**[v1: ieee802154/adf7242: Add MODULE_FIRMWARE macro](http://lore.kernel.org/netdev/20230616121807.1034050-1-juerg.haefliger@canonical.com/)**

> The module loads firmware so add a MODULE_FIRMWARE macro to provide that
> information via modinfo.
>

**[v1: net-next: Add and use helper for PCS negotiation modes](http://lore.kernel.org/netdev/ZIxQIBfO9dH5xFlg@shell.armlinux.org.uk/)**

> Earlier this month, I proposed a helper for deciding whether a PCS
> should use inband negotiation modes or not. There was some discussion
> around this topic, and I believe there was no disagreement about
> providing the helper.
>

**[v1: net: dpaa2-mac: add 25gbase-r support](http://lore.kernel.org/netdev/20230616111414.1578-1-josua@solid-run.com/)**

> Layerscape MACs support 25Gbps network speed with dpmac "CAUI" mode.
> Add the mappings between DPMAC_ETH_IF_* and HY_INTERFACE_MODE_*, as well
> as the 25000 mac capability.
>

**[v1: net-next: dt-bindings: net: phy: gpy2xx: more precise description](http://lore.kernel.org/netdev/20230616-feature-maxlinear-dt-better-irq-desc-v1-1-57a8936543bf@kernel.org/)**

> Mention that the interrupt line is just asserted for a random period of
> time, not the entire time.
>

**[v2: drivers:net:ethernet:Add missing fwnode_handle_put()](http://lore.kernel.org/netdev/20230616092820.1756-1-machel@vivo.com/)**

> In device_for_each_child_node(), we should have fwnode_handle_put()
> when break out of the iteration device_for_each_child_node()
> as it will automatically increase and decrease the refcounter.
>

**[v2: net: macsec SCI assignment for ES = 0](http://lore.kernel.org/netdev/20230616092404.12644-1-carlos.fernandez@technica-engineering.de/)**

> According to 802.1AE standard, when ES and SC flags in TCI are zero, used
> SCI should be the current active SC_RX. Current kernel does not implement
> it and uses the header MAC address.
>

**[v1: net-next: ipv6: also use netdev_hold() in ip6_route_check_nh()](http://lore.kernel.org/netdev/20230616085752.3348131-1-edumazet@google.com/)**

> In blamed commit, we missed the fact that ip6_validate_gw()
> could change dev under us from ip6_route_check_nh()
>
> In this fix, I use GFP_ATOMIC in order to not pass too many additional
> arguments to ip6_validate_gw() and ip6_route_check_nh() only
> for a rarely used debug feature.
>

#### 安全增强

**[v3: Randomized slab caches for kmalloc()](http://lore.kernel.org/linux-hardening/20230616111843.3677378-1-gongruiqi@huaweicloud.com/)**

> I adapted the v2 patch to the latest linux-next tree and made the v3
> patch without "RFC", since this idea seems to be acceptable in general
> based on previous dicussion with mm and hardening folks. Please check
> the link specified below for more details of the discussion, and further
> suggestions are welcome.
>

**[v3: usbip: usbip_host: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230615180504.401169-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v2: tracing/boot: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230615180420.400769-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v2: usb: gadget: function: printer: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230615180318.400639-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v2: pstore/platform: Add check for kstrdup](http://lore.kernel.org/linux-hardening/20230615025312.48712-1-jiasheng@iscas.ac.cn/)**

> Add check for the return value of kstrdup() and return the error
> if it fails in order to avoid NULL pointer dereference.
>

**[v2: usb: ch9: Replace 1-element array with flexible array](http://lore.kernel.org/linux-hardening/20230614181307.gonna.256-kees@kernel.org/)**

> Since commit df8fc4e934c1 ("kbuild: Enable -fstrict-flex-arrays=3"),
> UBSAN_BOUNDS no longer pretends 1-element arrays are unbounded. Walking
> wData will trigger a warning, so make it a proper flexible array. Add a
> union to keep the struct size identical for userspace in case anything
> was depending on the old size.
>

**[v3: wifi: cfg80211: replace strlcpy() with strscpy()](http://lore.kernel.org/linux-hardening/20230614134956.2109252-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v2: wifi: cfg80211: replace strlcpy() with strlscpy()](http://lore.kernel.org/linux-hardening/20230614134552.2108471-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v3: SUNRPC: Use sysfs_emit in place of strlcpy/sprintf](http://lore.kernel.org/linux-hardening/20230614133757.2106902-1-azeemshaikh38@gmail.com/)**

> Part of an effort to remove strlcpy() tree-wide [1].
>
> Direct replacement is safe here since the getter in kernel_params_ops
> handles -errno return [2].
>

**[v1: pstore/ram: Add check for kstrdup](http://lore.kernel.org/linux-hardening/20230614093733.36048-1-jiasheng@iscas.ac.cn/)**

> Add check for the return value of kstrdup() and return the error
> if it fails in order to avoid NULL pointer dereference.
>

**[v3: uml: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230614003604.1021205-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
> No return values were used, so direct replacement is safe.
>

**[v1: SUNRPC: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230613004054.3539554-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: net/mediatek: strlcpy withreturn](http://lore.kernel.org/linux-hardening/20230613003458.3538812-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: netfilter: ipset: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230613003437.3538694-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: mac80211: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230613003404.3538524-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: ieee802154: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230613003326.3538391-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

**[v1: cfg80211: cfg80211: strlcpy withreturn](http://lore.kernel.org/linux-hardening/20230612232301.2572316-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
>

#### 异步 IO

**[v2: add initial io_uring_cmd support for sockets](http://lore.kernel.org/io-uring/20230614110757.3689731-1-leitao@debian.org/)**

> This patchset creates the initial plumbing for a io_uring command for
> sockets.
>
> For now, create two uring commands for sockets, SOCKET_URING_OP_SIOCOUTQ
> and SOCKET_URING_OP_SIOCINQ, which are available in TCP, UDP and RAW
> sockets.
>

**[v1: io_uring/net: save msghdr->msg_control for retries](http://lore.kernel.org/io-uring/0b0d4411-c8fd-4272-770b-e030af6919a0@kernel.dk/)**

> If the application sets ->msg_control and we have to later retry this
> command, or if it got queued with IOSQE_ASYNC to begin with, then we
> need to retain the original msg_control value. This is due to the net
> stack overwriting this field with an in-kernel pointer, to copy it
> in. Hitting that path for the second time will now fail the copy from
> user, as it's attempting to copy from a non-user address.
>

#### Rust For Linux

**[v2: `scripts/rust_is_available.sh` improvements](http://lore.kernel.org/rust-for-linux/20230616001631.463536-1-ojeda@kernel.org/)**

> This is the patch series to improve `scripts/rust_is_available.sh`.
>
> The major addition in v2 is the test suite in the last commit. I added
> it because I wanted to have a proper way to test any further changes to
> it (such as the suggested `set --` idea to avoid forking by Masahiro),
> and so that adding new checks was easier to justify too (i.e. vs. the
> added complexity).
>

**[v2: Rust abstractions for Crypto API](http://lore.kernel.org/rust-for-linux/20230615142311.4055228-1-fujita.tomonori@gmail.com/)**

> Before sending v2 of my crypto patch [1] to linux-crypto ml and
> checking the chance of Rust bindings for crypto being accepted, I'd
> like to iron out Rust issues. I'd appreciate any feedback.
>

**[v1: KUnit integration for Rust doctests](http://lore.kernel.org/rust-for-linux/20230614180837.630180-1-ojeda@kernel.org/)**

> This is the initial KUnit integration for running Rust documentation
> tests within the kernel.
>
> Thank you to the KUnit team for all the input and feedback on this
> over the months, as well as the Intel LKP 0-Day team!
>

**[v1: rust: make `UnsafeCell` the outer type in `Opaque`](http://lore.kernel.org/rust-for-linux/20230614115328.2825961-1-aliceryhl@google.com/)**

> When combining `UnsafeCell` with `MaybeUninit`, it is idiomatic to use
> `UnsafeCell` as the outer type. Intuitively, this is because a
> `MaybeUninit<T>` might not contain a `T`, but we always want the effect
> of the `UnsafeCell`, even if the inner value is uninitialized.
>
> Now, strictly speaking, this doesn't really make a difference. The
> compiler will always apply the `UnsafeCell` effect even if the inner
> value is uninitialized. But I think we should follow the convention
> here.
>

**[v1: rust: allocator: Prevents mis-aligned allocation](http://lore.kernel.org/rust-for-linux/20230613164258.3831917-1-boqun.feng@gmail.com/)**

> Currently the KernelAllocator simply passes the size of the type Layout
> to krealloc(), and in theory the alignment requirement from the type
> Layout may be larger than the guarantee provided by SLAB, which means
> the allocated object is mis-aligned.
>

**[v1: Rust abstractions for network device drivers](http://lore.kernel.org/rust-for-linux/20230613045326.3938283-1-fujita.tomonori@gmail.com/)**

> This patchset adds minimum Rust abstractions for network device
> drivers and an example of a Rust network device driver, a simpler
> version of drivers/net/dummy.c.
>

**[v1: rust: bindgen: upgrade to 0.65.1](http://lore.kernel.org/rust-for-linux/20230612194311.24826-1-aakashsensharma@gmail.com/)**

> Upgrades bindgen to code-generation for anonymous unions, structs, and enums [7]
> for LLVM-16 based toolchains.
>
> The following upgrade also incorporates `noreturn` support from bindgen
> allowing us to remove useless `loop` calls which was placed as a
> workaround.
>

#### BPF

**[v1: dwarves: dwarves: encode BTF kind layout, crcs](http://lore.kernel.org/bpf/20230616171728.530116-11-alan.maguire@oracle.com/)**

> Encode kind layout at time of BTF encoding via --btf_gen_kind_layout
> and set CRC if --btf_gen_crc is set.
>

**[v2: bpf-next: bpf: support BTF kind layout info, CRCs](http://lore.kernel.org/bpf/20230616171728.530116-1-alan.maguire@oracle.com/)**

> By separating parsing BTF from using all the information
> it provides, we allow BTF to encode new features even if
> they cannot be used.  This is helpful in particular for
> cases where newer tools for BTF generation run on an
> older kernel; BTF kinds may be present that the kernel
> cannot yet use, but at least it can parse the BTF
> provided.  Meanwhile userspace tools with newer libbpf
> may be able to use the newer information.
>

**[v2: Reduce overhead of LSMs with static calls](http://lore.kernel.org/bpf/20230616000441.3677441-1-kpsingh@kernel.org/)**

> LSM hooks (callbacks) are currently invoked as indirect function calls. These
> callbacks are registered into a linked list at boot time as the order of the
> LSMs can be configured on the kernel command line with the "lsm=" command line
> parameter.
>

**[v4: bpf-next: xsk: multi-buffer support](http://lore.kernel.org/bpf/20230615172606.349557-1-maciej.fijalkowski@intel.com/)**

> This series of patches add multi-buffer support for AF_XDP. XDP and
> various NIC drivers already have support for multi-buffer packets. With
> this patch set, programs using AF_XDP sockets can now also receive and
> transmit multi-buffer packets both in copy as well as zero-copy mode.
> ZC multi-buffer implementation is based on ice driver.
>

**[v1: nf: netfilter: conntrack: Avoid nf_ct_helper_hash uses after free](http://lore.kernel.org/bpf/20230615152918.3484699-1-revest@chromium.org/)**

> If register_nf_conntrack_bpf() fails (for example, if the .BTF section
> contains an invalid entry), nf_conntrack_init_start() calls
> nf_conntrack_helper_fini() as part of its cleanup path and
> nf_ct_helper_hash gets freed.
>

**[v1: bpf: bpf/btf: Accept function names that contain dots](http://lore.kernel.org/bpf/20230615145607.3469985-1-revest@chromium.org/)**

> When building a kernel with LLVM=1, LLVM_IAS=0 and CONFIG_KASAN=y, LLVM
> leaves DWARF tags for the "asan.module_ctor" & co symbols. In turn,
> pahole creates BTF_KIND_FUNC entries for these and this makes the BTF
> metadata validation fail because they contain a dot.
>

**[v1: bpf-next: bpf: generate 'nomerge' for map helpers in bpf_helper_defs.h](http://lore.kernel.org/bpf/20230615142520.10280-1-eddyz87@gmail.com/)**

> Update code generation for bpf_helper_defs.h by adding
> __attribute__((nomerge)) for a set of helper functions to prevent some
> verifier unfriendly compiler optimizations.
>

**[v1: fprobe: Release rethook after the ftrace_ops is unregistered](http://lore.kernel.org/bpf/20230615115236.3476617-1-jolsa@kernel.org/)**

> While running bpf selftests it's possible to get following fault:
>

**[v1: net: igc: Avoid dereference of ptr_err in igc_clean_rx_irq()](http://lore.kernel.org/bpf/20230615-igc-err-ptr-v1-1-a17145eb8d62@kernel.org/)**

> In igc_clean_rx_irq() the result of a call to igc_xdp_run_prog() is assigned
> to the skb local variable. This may be an ERR_PTR.
>
> A little later the following is executed, which seems to be a
> possible dereference of an ERR_PTR.
>
> 	total_bytes += skb->len;
>

**[v2: perf/core: Bail out early if the request AUX area is out of bound](http://lore.kernel.org/bpf/20230613123211.58393-1-xueshuai@linux.alibaba.com/)**

> 'rb->aux_pages' allocated by kcalloc() is a pointer array which is used to
> maintains AUX trace pages. The allocated page for this array is physically
> contiguous (and virtually contiguous) with an order of 0..MAX_ORDER. If the
> size of pointer array crosses the limitation set by MAX_ORDER, it reveals a
> WARNING.
>

**[v1: bpf: Force kprobe multi expected_attach_type for kprobe_multi link](http://lore.kernel.org/bpf/20230613113119.2348619-1-jolsa@kernel.org/)**

> We currently allow to create perf link for program with
> expected_attach_type == BPF_TRACE_KPROBE_MULTI.
>
> This will cause crash when we call helpers like get_attach_cookie or
> get_func_ip in such program, because it will call the kprobe_multi's
> version (current->bpf_ctx context setup) of those helpers while it
> expects perf_link's current->bpf_ctx context setup.
>

**[v2: bpf-next: Add SO_REUSEPORT support for TC bpf_sk_assign](http://lore.kernel.org/bpf/20230613-so-reuseport-v2-0-b7c69a342613@isovalent.com/)**

> We want to replace iptables TPROXY with a BPF program at TC ingress.
> To make this work in all cases we need to assign a SO_REUSEPORT socket
> to an skb, which is currently prohibited. This series adds support for
> such sockets to bpf_sk_assing. See patch 5 for details.
>

**[v6: bpf-next: Add benchmark for bpf memory allocator](http://lore.kernel.org/bpf/20230613080921.1623219-1-houtao@huaweicloud.com/)**

> This patchset includes some trivial fixes for benchmark framework and
> a new benchmark for bpf memory allocator originated from handle-reuse
> patchset. Because htab-mem benchmark depends the fixes, so I post these
> patches together.
>

**[v2: lib/test_bpf: Call page_address() on page acquired with GFP_KERNEL flag](http://lore.kernel.org/bpf/20230613071756.GA359746@sumitra.com/)**

> generate_test_data() acquires a page with alloc_page(GFP_KERNEL). Pages
> allocated with GFP_KERNEL cannot come from Highmem. This is why
> there is no need to call kmap() on them.
>
> Therefore, use a plain page_address() on that page.
>

**[v5: bpf-next: bpf, x86: allow function arguments up to 12 for TRACING](http://lore.kernel.org/bpf/20230613025226.3167956-1-imagedong@tencent.com/)**

> Therefore, let's enhance it by increasing the function arguments count
> allowed in arch_prepare_bpf_trampoline(), for now, only x86_64.
>
> In the 1st patch, we clean garbage value in upper bytes of the trampoline
> when we store the arguments from regs into stack.
>
> In the 2nd patch, we make arch_prepare_bpf_trampoline() support to copy
> function arguments in stack for x86 arch. Therefore, the maximum
> arguments can be up to MAX_BPF_FUNC_ARGS for FENTRY and FEXIT. Meanwhile,
> we clean the potentian garbage value when we copy the arguments on-stack.
>

**[v1: bpf-next: bpf: netdev TX metadata](http://lore.kernel.org/bpf/20230612172307.3923165-1-sdf@google.com/)**

> The goal of this series is to add two new standard-ish places
> in the transmit path:
>
> 1. Right before the packet is transmitted (with access to TX
>    descriptors)
> 2. Right after the packet is actually transmitted and we've received the
>    completion (again, with access to TX completion descriptors)
>

**[v5: bpf-next: verify scalar ids mapping in regsafe()](http://lore.kernel.org/bpf/20230612160801.2804666-1-eddyz87@gmail.com/)**

> This example is unsafe because not all execution paths verify r7 range.
> Because of the jump at (4) the verifier would arrive at (6) in two states:
> I.  r6{.id=b}, r7{.id=b} via path 1-6;
> II. r6{.id=a}, r7{.id=b} via path 1-4, 6.
>
> Currently regsafe() does not call check_ids() for scalar registers,
> thus from POV of regsafe() states (I) and (II) are identical.
>
> The change is split in two parts:
> - patches #1,2: update for mark_chain_precision() to propagate
>   precision marks through scalar IDs.
> - patches #3,4: update for regsafe() to use a special version of
>   check_ids() for precise scalar values.
>

**[v3: bpf-next: bpf: Support ->fill_link_info for kprobe_multi and perf_event links](http://lore.kernel.org/bpf/20230612151608.99661-1-laoar.shao@gmail.com/)**

> This patchset enhances the usability of kprobe_multi programs by introducing
> support for ->fill_link_info. This allows users to easily determine the
> probed functions associated with a kprobe_multi program. While
> `bpftool perf show` already provides information about functions probed by
> perf_event programs, supporting ->fill_link_info ensures consistent access to
> this information across all bpf links.
>

**[v4: net-next: introduce page_pool_alloc() API](http://lore.kernel.org/bpf/20230612130256.4572-1-linyunsheng@huawei.com/)**

> In [1] & [2], there are usecases for veth and virtio_net to
> use frag support in page pool to reduce memory usage, and it
> may request different frag size depending on the head/tail
> room space for xdp_frame/shinfo and mtu/packet size. When the
> requested frag size is large enough that a single page can not
> be split into more than one frag, using frag support only have
> performance penalty because of the extra frag count handling
> for frag support.
>

**[v1: lib/test_bpf: Replace kmap() with kmap_local_page()](http://lore.kernel.org/bpf/20230612103341.GA354790@sumitra.com/)**

> kmap() has been deprecated in favor of the kmap_local_page()
> due to high cost, restricted mapping space, the overhead of
> a global lock for synchronization, and making the process
> sleep in the absence of free slots.
>

**[v1: Add a sysctl option to disable bpf offensive helpers.](http://lore.kernel.org/bpf/20230610152618.105518-1-clangllvm@126.com/)**

> Some eBPF helper functions have been long regarded as problematic[1].
> More than just used for powerful rootkit, these features can also be
> exploited to harm the containers by perform various attacks to the
> processes outside the container in the enrtire VM, such as process
> DoS, information theft, and container escape.
>

### 周边技术动态

#### Qemu

**[v1: hw/riscv/virt.c: check for 'ssaia' with KVM AIA](http://lore.kernel.org/qemu-devel/20230616172141.756386-1-dbarboza@ventanamicro.com/)**

> This patch was inspired by my review and testing of the QEMU KVM AIA
> work. It's not dependent on it though, and can be reviewed and merged
> separately.
>

**[v2: target/riscv: Add support for BF16 extensions](http://lore.kernel.org/qemu-devel/20230615063302.102409-1-liweiwei@iscas.ac.cn/)**

> Specification for BF16 extensions can be found in:
> https://github.com/riscv/riscv-bfloat16
>
> The port is available here:
> https://github.com/plctlab/plct-qemu/tree/plct-bf16-upstream-v2
>

**[v1: riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20230614012017.3100663-1-alistair.francis@wdc.com/)**

> The following changes since commit fdd0df5340a8ebc8de88078387ebc85c5af7b40f:
>
>   Merge tag 'pull-ppc-20230610' of https://gitlab.com/danielhb/qemu into staging (2023-06-10 07:25:00 -0700)
>
> are available in the Git repository at:
>
>   https://github.com/alistair23/qemu.git tags/pull-riscv-to-apply-20230614
>
> for you to fetch changes up to 860029321d9ebdff47e89561de61e9441fead70a:
>

**[v2: disas/riscv: Add vendor extension support](http://lore.kernel.org/qemu-devel/20230612111034.3955227-1-christoph.muellner@vrull.eu/)**

> This series adds vendor extension support to the QEMU disassembler
> for RISC-V. The following vendor extensions are covered:
> * XThead{Ba,Bb,Bs,Cmo,CondMov,FMemIdx,Fmv,Mac,MemIdx,MemPair,Sync}
> * XVentanaCondOps
>

## 20230611：第 49 期

### 内核动态

#### RISC-V 架构支持

**[v3: Add D1/T113s thermal sensor controller support](http://lore.kernel.org/linux-riscv/20230610204225.1133473-1-bigunclemax@gmail.com/)**

> This series adds support for Allwinner D1/T113s thermal sensor controller.
> THIS controller is similar to the one on H6, but with only one sensor and
> uses a different scale and offset values.
>

**[v5: Add support for Allwinner GPADC on D1/T113s/R329/T507 SoCs](http://lore.kernel.org/linux-riscv/20230610202005.1118049-1-bigunclemax@gmail.com/)**

> This series adds support for general purpose ADC (GPADC) on new
> Allwinner's SoCs, such as D1, T113s, T507 and R329. The implemented driver
> provides basic functionality for getting ADC channels data.
>

**[v1: dt-bindings: riscv: cpus: switch to unevaluatedProperties: false](http://lore.kernel.org/linux-riscv/20230610-snaking-version-81ae5abb7573@spud/)**

> Do the various bits needed to drop the additionalProperties: true that
> we currently have in riscv/cpu.yaml, to permit actually enforcing what
> people put in cpus nodes.
>

**[v1: riscv: move memblock_allow_resize() after lm is ready](http://lore.kernel.org/linux-riscv/tencent_D656D683705F41324657ED3393C3384C7408@qq.com/)**

> The initial memblock metadata is accessed from kernel image mapping. The
> regions arrays need to "reallocated" from memblock and accessed through
> linear mapping to cover more memblock regions. So the resizing should
> not be allowed until linear mapping is ready. Note that there are
> memblock allocations when building linear mapping.
>

**[v3: RISCV: Add KVM_GET_REG_LIST API](http://lore.kernel.org/linux-riscv/cover.1686275310.git.haibo1.xu@intel.com/)**

> KVM_GET_REG_LIST will dump all register IDs that are available to
> KVM_GET/SET_ONE_REG and It's very useful to identify some platform
> regression issue during VM migration.
>

**[v2: arch: allow pte_offset_map[_lock]() to fail](http://lore.kernel.org/linux-riscv/a4963be9-7aa6-350-66d0-2ba843e1af44@google.com/)**

> Here is v2 series of patches to various architectures, based on v6.4-rc5:
> preparing for v2 of changes following in mm, affecting pte_offset_map()
> and pte_offset_map_lock().  There are very few differences from v1:
> noted patch by patch below.
>

**[v2: dt-bindings: riscv: deprecate riscv,isa](http://lore.kernel.org/linux-riscv/20230608-sitting-bath-31eddc03c5a5@spud/)**

> When the RISC-V dt-bindings were accepted upstream in Linux, the base
> ISA etc had yet to be ratified. By the ratification of the base ISA,
> incompatible changes had snuck into the specifications - for example the
> Zicsr and Zifencei extensions were spun out of the base ISA.
>

**[Patch "riscv: vmlinux.lds.S: Explicitly handle '.got' section" has been added to the 6.3-stable tree](http://lore.kernel.org/linux-riscv/2023060727-sphinx-obtain-935a@gregkh/)**

> This is a note to let you know that I've just added the patch titled
>
>     riscv: vmlinux.lds.S: Explicitly handle '.got' section
>
> to the 6.3-stable tree which can be found at:
>     http://www.kernel.org/git/?p=linux/kernel/git/stable/stable-queue.git;a=summary
>
> The filename of the patch is:
>      riscv-vmlinux.lds.s-explicitly-handle-.got-section.patch
> and it can be found in the queue-6.3 subdirectory.
>

**[v1: riscv: reserve DTB before possible memblock allocation](http://lore.kernel.org/linux-riscv/tencent_B15C0F1F3105597D0DCE7DADC96C5EB5CF0A@qq.com/)**

> It's possible that early_init_fdt_scan_reserved_mem() allocates memory
> from memblock for dynamic reserved memory in `/reserved-memory` node.
> Any fixed reservation must be done before that to avoid potential
> conflicts.
>

**[v3: tools/nolibc: add a new syscall helper](http://lore.kernel.org/linux-riscv/cover.1686135913.git.falcon@tinylab.org/)**

> This is the revision of the v2 syscall helpers [1], it is based on
> -ENOSYS patchset [3], so, it is ok to simply merge both of them.
>
> This revision mainly applied Thomas' method, removed the __syscall()
> helper and replaced it with __sysret() instead, because __syscall()
> looks like _syscall() and syscall(), it may mixlead the developers.
>

**[v4: nolibc: add part2 of support for rv32](http://lore.kernel.org/linux-riscv/cover.1686128703.git.falcon@tinylab.org/)**

> This is the v4 part2 of support for rv32 (v3 [1]), it applied the
> suggestions from Thomas, Arnd [2] and you [3]. now, the rv32 compile
> support almost aligned with x86 except the extra KARCH to make kernel
> happy, thanks very much for your nice review!
>

**[v3: riscv: Introduce KASLR](http://lore.kernel.org/linux-riscv/20230606123242.20804-1-alexghiti@rivosinc.com/)**

> The new virtual kernel location is limited by the early page table that only
> has one PUD and with the PMD alignment constraint, the kernel can only take
> < 512 positions.
>

**[v4: Add JH7110 cpufreq support](http://lore.kernel.org/linux-riscv/20230606105656.124355-1-mason.huo@starfivetech.com/)**

> This patchset adds the compatible strings into the allowlist
> for supporting the generic cpufreq driver on JH7110 SoC.
> Also, it enables the axp15060 pmic for the cpu power source.
>

**[v2: tools/nolibc: add two new syscall helpers](http://lore.kernel.org/linux-riscv/cover.1686036862.git.falcon@tinylab.org/)**

> This is the revision of the v1 syscall helpers [1], just rebased it on
> patchset [3], so, it is ok to simply merge both of them.
>
> This revision mainly applied your suggestions of v1, both of the syscall
> return and call helpers are simplified or cleaned up.
>

**[v2: Documentation: RISC-V: patch-acceptance: mention patchwork's role](http://lore.kernel.org/linux-riscv/20230606-rehab-monsoon-12c17bbe08e3@wendy/)**

> Palmer suggested at some point, not sure if it was in one of the
> weekly linux-riscv syncs, or a conversation at FOSDEM, that we
> should document the role of the automation running on our patchwork
> instance plays in patch acceptance.
>

**[v3: gpio: sifive: Add missing check for platform_get_irq](http://lore.kernel.org/linux-riscv/20230606031159.38246-1-jiasheng@iscas.ac.cn/)**

> Add the missing check for platform_get_irq() and return error code
> if it fails.
> The returned error code will be dealed with in
> builtin_platform_driver(sifive_gpio_driver) and the driver will not
> be registered.
>

**[v2: perf parse-regs: Refactor architecture functions](http://lore.kernel.org/linux-riscv/20230606014559.21783-1-leo.yan@linaro.org/)**

> This patch series is to refactor arch related functions for register
> parsing, which follows up the discussion for v1:
> https://lore.kernel.org/lkml/20230520025537.1811986-1-leo.yan@linaro.org/
>

**[v1: 6.3: riscv: vmlinux.lds.S: Explicitly handle '.got' section](http://lore.kernel.org/linux-riscv/20230605-6-3-riscv-got-orphan-warning-llvm-17-v1-1-72c4f11e020f@kernel.org/)**

> This is not an issue in mainline because handling of the .got section
> was added by commit 39b33072941f ("riscv: Introduce CONFIG_RELOCATABLE")
> and further extended by commit 26e7aacb83df ("riscv: Allow to downgrade
> paging mode from the command line") in 6.4-rc1. Neither of these changes
> are suitable for stable, so add explicit handling of the .got section in
> a standalone change to align 6.3 and mainline, which addresses the
> warning.
>

**[v21: -next: riscv: Add vector ISA support](http://lore.kernel.org/linux-riscv/20230605110724.21391-1-andy.chiu@sifive.com/)**

> This is the v21 patch series for adding Vector extension support in
> Linux. Please refer to [1] for the introduction of the patchset. The
> v21 patch series was aimed to solve build issues from v19, provide usage
> guideline for the prctl interface, and address review comments on v20.
>

**[v3: gpio: ath79: Add missing check for platform_get_irq](http://lore.kernel.org/linux-riscv/20230605015345.12801-1-jiasheng@iscas.ac.cn/)**

> Add the missing check for platform_get_irq() and return error
> if it fails.
>

#### 进程调度

**[v1: sched/deadline: merge __dequeue_dl_entity() into its sole caller](http://lore.kernel.org/lkml/20230610063554.1453342-1-linmiaohe@huawei.com/)**

> Sole caller dequeue_dl_entity() calls __dequeue_dl_entity() directly. So
> __dequeue_dl_entity() can be merged into its sole caller. No functional
> change intended.
>

**[v1: net/sched: act_pedit: Use kmemdup() to replace kmalloc + memcpy](http://lore.kernel.org/lkml/20230609070117.100507-1-jiapeng.chong@linux.alibaba.com/)**

> ./net/sched/act_pedit.c:245:21-28: WARNING opportunity for kmemdup.
>
> Closes: https://bugzilla.openanolis.cn/show_bug.cgi?id=5478
>

**[v1: sched/wait: Determine whether the wait queue is empty before waking up](http://lore.kernel.org/lkml/20230609053812.10230-1-wangyoua@uniontech.com/)**

> When we did some benchmark tests (such as pipe tests), we found
> that the wake behavior was still triggered when the wait queue
> was empty, even though it would exit later.
>

**[v3: net/sched: Set the flushing flags to false to prevent an infinite loop and add one test to tdc](http://lore.kernel.org/lkml/20230609033115.3738692-1-renmingshuai@huawei.com/)**

> [root@localhost tc-testing]# ./tdc.py -f tc-tests/infra/filter.json
> Test c2b4: Adding a new filter after flushing empty chain doesn't cause an infinite loop
> All test results:
> 1..1
> ok 1 c2b4 - Adding a new filter after flushing empty chain doesn't cause an infinite loop
>

**[v2: sched/nohz: Add HRTICK_BW for using cfs bandwidth with nohz_full](http://lore.kernel.org/lkml/20230608125228.56097-1-pauld@redhat.com/)**

> CFS bandwidth limits and NOHZ full don't play well together.  Tasks
> can easily run well past their quotas before a remote tick does
> accounting.  This leads to long, multi-period stalls before such
> tasks can run again.  Use the hrtick mechanism to set a sched
> tick to fire at remaining_runtime in the future if we are on
> a nohz full cpu, if the task has quota and if we are likely to
> disable the tick (nr_running == 1).  This allows for bandwidth
> accounting before tasks go too far over quota.
>

**[v1: sched/idle: disable tick in idle=poll idle entry](http://lore.kernel.org/lkml/ZIEqlkIASx2F2DRF@tpad/)**

> Commit a5183862e76fdc25f36b39c2489b816a5c66e2e5
> ("tick/nohz: Conditionally restart tick on idle exit") allows
> a nohz_full CPU to enter idle and return from it with the
> scheduler tick disabled (since the tick might be undesired noise).
>
> The idle=poll case still unconditionally restarts the tick when entering
> idle.
>
> To reduce the noise for that case as well, stop the tick when entering
> idle, for the idle=poll case.
>

**[v1: sched/debug,sched/core: Reset hung task detector while processing sysrq-t](http://lore.kernel.org/lkml/20230607181221.4118381-1-xqjcool@gmail.com/)**

> On devices with multiple CPUs and multiple processes, outputting lengthy
> sysrq-t content on a slow serial port can consume a significant amount
> of time. We need to reset the hung task detector to avoid false hung task
> alerts.
>

**[v1: net/sched: Set the flushing flags to false to prevent an infinite loop](http://lore.kernel.org/lkml/20230607041920.2012792-1-renmingshuai@huawei.com/)**

> >On 06/06/2023 11:45, renmingshuai wrote:
> >> When a new chain is added by using tc, one soft lockup alarm will be
> >>   generated after delete the prio 0 filter of the chain. To reproduce
> >>   the problem, perform the following steps:
> >> (1) tc qdisc add dev eth0 root handle 1: htb default 1
> >> (2) tc chain add dev eth0
> >> (3) tc filter del dev eth0 chain 0 parent 1: prio 0
> >> (4) tc filter add dev eth0 chain 0 parent 1:
> >

**[v1: sched: use kmem_cache_zalloc() to zero allocated tg](http://lore.kernel.org/lkml/20230606131057.498054-1-linmiaohe@huawei.com/)**

> It's more convenient to use kmem_cache_zalloc() to allocate zeroed tg.
> No functional change intended.
>

#### 内存管理

**[v2: lib: Replace kmap() with kmap_local_page()](http://lore.kernel.org/linux-mm/20230610175712.GA348514@sumitra.com/)**

> kmap() has been deprecated in favor of the kmap_local_page()
> due to high cost, restricted mapping space, the overhead of
> a global lock for synchronization, and making the process
> sleep in the absence of free slots.
>

**[v1: mm: compaction: mark kcompactd_run() and kcompactd_stop() __meminit](http://lore.kernel.org/linux-mm/20230610034615.997813-1-linmiaohe@huawei.com/)**

> Add __meminit to kcompactd_run() and kcompactd_stop() to ensure they're
> default to __init when memory hotplug is not enabled.
>

**[v1: mm: hugetlb: Add Kconfig option to set default nr_overcommit_hugepages](http://lore.kernel.org/linux-mm/88fc41edeb5667534cde344c9220fcdfc00047b1.1686359973.git.josh@joshtriplett.org/)**

> The default kernel configuration does not allow any huge page allocation
> until after setting nr_hugepages or nr_overcommit_hugepages to a
> non-zero value; without setting those, mmap attempts with MAP_HUGETLB
> will always fail with -ENOMEM. nr_overcommit_hugepages allows userspace
> to attempt to allocate huge pages at runtime, succeeding if the kernel
> can find or assemble a free huge page.
>

**[v1: mm/khugepaged: use DEFINE_READ_MOSTLY_HASHTABLE macro](http://lore.kernel.org/linux-mm/20230609-khugepage-v1-1-dad4e8382298@google.com/)**

> These are equivalent, but DEFINE_READ_MOSTLY_HASHTABLE exists to define
> a hashtable in the .data..read_mostly section.
>

**[v3: mm/folio: Avoid special handling for order value 0 in folio_set_order](http://lore.kernel.org/linux-mm/20230609162907.111756-1-tsahu@linux.ibm.com/)**

> folio_set_order(folio, 0) is used in kernel at two places
> __destroy_compound_gigantic_folio and __prep_compound_gigantic_folio.
> Currently, It is called to clear out the folio->_folio_nr_pages and
> folio->_folio_order.
>

**[v2: Optimize the fast path of mas_store()](http://lore.kernel.org/linux-mm/20230609120347.63936-1-zhangpeng.00@bytedance.com/)**

> Add fast paths for mas_wr_append() and mas_wr_slot_store() respectively.
> The newly added fast path of mas_wr_append() is used in fork() and how
> much it benefits fork() depends on how many VMAs are duplicated.
>

**[v1: net-next: splice, net: Some miscellaneous MSG_SPLICE_PAGES changes](http://lore.kernel.org/linux-mm/20230609100221.2620633-1-dhowells@redhat.com/)**

> Now that the splice_to_socket() has been rewritten so that nothing now uses
> the ->sendpage() file op[1], some further changes can be made, so here are
> some miscellaneous changes that can now be done.
>

**[v1: mm: compaction: skip memory hole rapidly when isolating migratable pages](http://lore.kernel.org/linux-mm/8cc668b77c8eb2fa78058b3d81386ebed9c5a9cd.1686294549.git.baolin.wang@linux.alibaba.com/)**

> On some machines, the normal zone can have a large memory hole like
> below memory layout, and we can see the range from 0x100000000 to
> scanner can meet the hole and it will take more time to skip the large
> hole. From my measurement, I can see the isolation scanner will take
> 80us
>  100us to skip the large hole [0x100000000 - 0x1800000000].
>

**[v2: mm/vmalloc: Replace the ternary conditional operator with min()](http://lore.kernel.org/linux-mm/20230609093057.27777-1-luhongfei@vivo.com/)**

> It would be better to replace the traditional ternary conditional
> operator with min() in zero_iter
>

**[v1: net-next: sock: Propose socket.urgent for sockmem isolation](http://lore.kernel.org/linux-mm/20230609082712.34889-1-wuyun.abel@bytedance.com/)**

> This is just a PoC patch intended to resume the discussion about
> tcpmem isolation opened by Google in LPC'22 [1].
>
> We are facing the same problem that the global shared threshold can
> cause isolation issues. Low priority jobs can hog TCP memory and
> adversely impact higher priority jobs. What's worse is that these
> low priority jobs usually have smaller cpu weights leading to poor
> ability to consume rx data.
>

**[v1: revert shrinker_srcu related changes](http://lore.kernel.org/linux-mm/20230609081518.3039120-1-qi.zheng@linux.dev/)**

> Kernel test robot reports -88.8% regression in stress-ng.ramfs.ops_per_sec
> test case [1], which is caused by commit f95bdb700bc6 ("mm: vmscan: make
> global slab shrink lockless"). The root cause is that SRCU has to be careful
> to not frequently check for SRCU read-side critical section exits. Therefore,
> even if no one is currently in the SRCU read-side critical section,
> synchronize_srcu() cannot return quickly. That's why unregister_shrinker()
> has become slower.
>

**[v6: mm: ioremap: Convert architectures to take GENERIC_IOREMAP way](http://lore.kernel.org/linux-mm/20230609075528.9390-1-bhe@redhat.com/)**

> Currently, many architecutres have't taken the standard GENERIC_IOREMAP
> way to implement ioremap_prot(), iounmap(), and ioremap_xx(), but make
> these functions specifically under each arch's folder. Those cause many
> duplicated codes of ioremap() and iounmap().
>

**[v1: watchdog/mm: Allow dumping memory info in pretimeout](http://lore.kernel.org/linux-mm/20230608-pretimeout-oom-v1-1-542cc91062d7@axis.com/)**

> On my (embedded) systems, the most common cause of hitting the watchdog
> (pre)timeout is due to thrashing.  Diagnosing these problems is hard
> without knowing the memory state at the point of the watchdog hit.  In
> order to make this information available, add a module parameter to the
> watchdog pretimeout panic governor to ask it to dump memory info and the
> OOM task list (using a new helper in the OOM code) before triggering the
> panic.
>

**[v1: mm/min_free_kbytes: modify min_free_kbytes calculation rules](http://lore.kernel.org/linux-mm/20230609032552.218010-1-liuq131@chinatelecom.cn/)**

> The current calculation of min_free_kbytes only uses ZONE_DMA and
> ZONE_NORMAL pages,but the ZONE_MOVABLE zone->_watermark[WMARK_MIN]
> will also divide part of min_free_kbytes.This will cause the min
> watermark of ZONE_NORMAL to be too small in the presence of ZONE_MOVEABLE.
>

**[v1: mm: kill [add|del]_page_to_lru_list()](http://lore.kernel.org/linux-mm/20230609013901.79250-1-wangkefeng.wang@huawei.com/)**

> Directly call lruvec_del_folio(), and drop unused page interfaces.
>

**[v2: mm: allow pte_offset_map[_lock]() to fail](http://lore.kernel.org/linux-mm/c1c9a74a-bc5b-15ea-e5d2-8ec34bc921d@google.com/)**

> Here is v2 series of patches to mm, based on v6.4-rc5: preparing for
> v2 effective changes to follow, probably next week (when I hope s390
> will be sorted), affecting pte_offset_map() and pte_offset_map_lock().
> There are very few differences from v1: noted patch by patch below.
>

**[v1: udmabuf: revert 'Add support for mapping hugepages (v4)'](http://lore.kernel.org/linux-mm/20230608204927.88711-1-mike.kravetz@oracle.com/)**

> This effectively reverts commit 16c243e99d33 ("udmabuf: Add support
> for mapping hugepages (v4)").  Recently, Junxiao Chang found a BUG
> with page map counting as described here [1].  This issue pointed out
> that the udmabuf driver was making direct use of subpages of hugetlb
> pages.  This is not a good idea, and no other mm code attempts such use.
> In addition to the mapcount issue, this also causes issues with hugetlb
> vmemmap optimization and page poisoning.
>

**[v1: mm: Sync percpu mm RSS counters before querying](http://lore.kernel.org/linux-mm/20230608171256.17827-1-mkoutny@suse.com/)**

> An issue was observed with stats collected in struct rusage on ppc64le
> with 64kB pages. The percpu counters use batching with
> 	percpu_counter_batch = max(32, nr*2) # in PAGE_SIZE
> i.e. with larger pages but similar RSS consumption (bytes), there'll be
> less flushes and error more noticeable.
>

**[v1: staging: lib: Use memcpy_to/from_page()](http://lore.kernel.org/linux-mm/20230608142553.GA341787@sumitra.com/)**

> Deprecate kmap() in favor of kmap_local_page() due to high
> cost, restricted mapping space, the overhead of a global lock
> for synchronization, and making the process sleep in the
> absence of free slots.
>

**[v3: Documentation/mm: Initial page table documentation](http://lore.kernel.org/linux-mm/20230608125501.3960093-1-linus.walleij@linaro.org/)**

> This is based on an earlier blog post at people.kernel.org,
> it describes the concepts about page tables that were hardest
> for me to grasp when dealing with them for the first time,
> such as the prevalent three-letter acronyms pfn, pgd, p4d,
> pud, pmd and pte.
>

**[v4: mm/migrate_device: Try to handle swapcache pages](http://lore.kernel.org/linux-mm/20230607172944.11713-1-mpenttil@redhat.com/)**

> Migrating file pages and swapcache pages into device memory is not supported.
> Try to get rid of the swap cache, and if successful, go ahead as
> with other anonymous pages.
>

**[v1: binfmt_elf: dynamically allocate note.data in parse_elf_properties](http://lore.kernel.org/linux-mm/20230607144227.8956-1-ansuelsmth@gmail.com/)**

> Dynamically allocate note.data in parse_elf_properties to fix
> compilation warning on some arch.
>

**[v1: mm/mm_init.c: add debug messsge for dma zone](http://lore.kernel.org/linux-mm/20230607090734.1259-1-haifeng.xu@shopee.com/)**

> If freesize is less than dma_reserve, print warning message to report
> this case.
>

**[v4: drm-next: v1: DRM GPUVA Manager & Nouveau VM_BIND UAPI](http://lore.kernel.org/linux-mm/20230606223130.6132-1-dakr@redhat.com/)**

> Furthermore, with the DRM GPUVA manager it provides a new DRM core feature to
> keep track of GPU virtual address (VA) mappings in a more generic way.
>
> The DRM GPUVA manager is indented to help drivers implement userspace-manageable
> GPU VA spaces in reference to the Vulkan API. In order to achieve this goal it
> serves the following purposes in this context.
>

#### 文件系统

**[v1: fs/aio: Stop allocating aio rings from HIGHMEM](http://lore.kernel.org/linux-fsdevel/20230609145937.17610-1-fmdefrancesco@gmail.com/)**

> There is no need to allocate aio rings from HIGHMEM because of very
> little memory needed here.
>
> Therefore, use GFP_USER flag in find_or_create_page() and get rid of
> kmap*() mappings.
>

**[v4: blksnap - block devices snapshots module](http://lore.kernel.org/linux-fsdevel/20230609115206.4649-1-sergei.shtepa@veeam.com/)**

> I am happy to offer a improved version of the Block Devices Snapshots
> Module. It allows to create non-persistent snapshots of any block devices.
> The main purpose of such snapshots is to provide backups of block devices.
> See more in Documentation/block/blksnap.rst.
>

**[v1: Reduce impact of overlayfs fake path files](http://lore.kernel.org/linux-fsdevel/20230609073239.957184-1-amir73il@gmail.com/)**

> This is the solution that we discussed for removing FMODE_NONOTIFY
> from overlayfs real files.
>
> My branch [1] has an extra patch for remove FMODE_NONOTIFY, but
> I am still testing the ovl-fsnotify interaction, so we can defer
> that step to later.
>

**[v1: ovl: port to new mount api](http://lore.kernel.org/linux-fsdevel/20230605-fs-overlayfs-mount_api-v1-1-a8d78c3fbeaf@kernel.org/)**

> We recently ported util-linux to the new mount api. Now the mount(8)
> tool will by default use the new mount api. While trying hard to fall
> back to the old mount api gracefully there are still cases where we run
> into issues that are difficult to handle nicely.
>

**[v1: bdev: allow buffer-head & iomap aops to co-exist](http://lore.kernel.org/linux-fsdevel/20230608032404.1887046-1-mcgrof@kernel.org/)**

> At LSFMM it was clear that for some in order to support large order folios
> we want to use iomap. So the filesystems staying and requiring buffer-heads
> cannot make use of high order folios. This simplifies support and reduces
> the scope for what we need to do in order to support high order folios for
> buffered-io.
>

**[v2: fs: avoid empty option when generating legacy mount string](http://lore.kernel.org/linux-fsdevel/20230607-fs-empty-option-v2-1-ffd0cba3bddb@weissschuh.net/)**

> As each option string fragment is always prepended with a comma it would
> happen that the whole string always starts with a comma.
> This could be interpreted by filesystem drivers as an empty option and
> may produce errors.
>

**[v2: gfs2/buffer folio changes for 6.5](http://lore.kernel.org/linux-fsdevel/20230606223346.3241328-1-willy@infradead.org/)**

> This kind of started off as a gfs2 patch series, then became entwined
> with buffer heads once I realised that gfs2 was the only remaining
> caller of __block_write_full_page().  For those not in the gfs2 world,
> the big point of this series is that block_write_full_page() should now
> handle large folios correctly.
>

#### 网络设备

**[v4: net-next: tcp: enforce receive buffer memory limits by allowing the tcp window to shrink](http://lore.kernel.org/netdev/20230611052354.2116564-1-mfreemon@cloudflare.com/)**

> Under certain circumstances, the tcp receive buffer memory limit
> set by autotuning (sk_rcvbuf) is increased due to incoming data
> packets as a result of the window not closing when it should be.
> This can result in the receive buffer growing all the way up to
> tcp_rmem[2], even for tcp sessions with a low BDP.
>

**[v1: amd-xgbe: extend 10Mbps support to MAC version 21H](http://lore.kernel.org/netdev/20230611025637.1211722-1-Raju.Rangoju@amd.com/)**

> MAC version 21H supports the 10Mbps speed. So, extend support to
> platforms that support it.
>

**[v1: dt-bindings: net: mediatek,net: add missing mediatek,mt7621-eth](http://lore.kernel.org/netdev/ZIUSZR6I3Ki6mZRO@makrotopia.org/)**

> Document the Ethernet controller found in the MediaTek MT7621 MIPS SoC
> family which is supported by the mtk_eth_soc driver.
>

**[v5: net-next: net: phy: add driver for MediaTek SoC built-in GE PHYs](http://lore.kernel.org/netdev/ZIULuiQrdgV9cZbJ@makrotopia.org/)**

> Some of MediaTek's Filogic SoCs come with built-in gigabit Ethernet
> PHYs which require calibration data from the SoC's efuse.
> Despite the similar design the driver doesn't share any code with the
> existing mediatek-ge.c.
> Add support for such PHYs by introducing a new driver with basic
> support for MediaTek SoCs MT7981 and MT7988 built-in 1GE PHYs.
>

**[v1: Add a sysctl option to disable bpf offensive helpers.](http://lore.kernel.org/netdev/20230610152618.105518-1-clangllvm@126.com/)**

> Some eBPF helper functions have been long regarded as problematic[1].
> More than just used for powerful rootkit, these features can also be
> exploited to harm the containers by perform various attacks to the
> processes outside the container in the enrtire VM, such as process
> DoS, information theft, and container escape.
>

**[v4: net-next: virtio/vsock: support datagrams](http://lore.kernel.org/netdev/20230413-b4-vsock-dgram-v4-0-0cebbb2ae899@bytedance.com/)**

> This series introduces support for datagrams to virtio/vsock.
>
> It is a spin-off (and smaller version) of this series from the summer:
>   https://lore.kernel.org/all/cover.1660362668.git.bobby.eshleman@bytedance.com/
>
> Please note that this is an RFC and should not be merged until
> associated changes are made to the virtio specification, which will
> follow after discussion from this series.
>

**[v1: net-next: net: support extack in dump and simplify ethtool uAPI](http://lore.kernel.org/netdev/20230609215331.1606292-1-kuba@kernel.org/)**

> Ethtool currently requires header nest to be always present even if
> it doesn't have to carry any attr for a given request. This inflicts
> unnecessary pain on the users.
>

**[v1: net-next: tools: ynl: generate code for the ethtool family](http://lore.kernel.org/netdev/20230609214346.1605106-1-kuba@kernel.org/)**

> And finally ethtool support. Thanks to Stan's work the ethtool family
> spec is quite complete, so there is a lot of operations to support.
>
> I chickened out of stats-get support, they require at the very least
> type-value support on a u64 scalar. Type-value is an arrangement where
> a u16 attribute is encoded directly in attribute type. Code gen can
> support this if the inside is a nest, we just throw in an extra
> field into that nest to carry the attr type. But a little more coding
> is needed to for a scalar, because first we need to turn the scalar
> into a struct with one member, then we can add the attr type.
>

**[v4: iwl-next: Implement support for SRIOV + LAG](http://lore.kernel.org/netdev/20230609211626.621968-1-david.m.ertman@intel.com/)**

> The first interface added into the aggregate will be flagged as
> the primary interface, and this primary interface will be
> responsible for managing the VF's resources.  VF's created on the
> primary are the only VFs that will be supported on the aggregate.
> Only Active-Backup mode will be supported and only aggregates whose
> primary interface is in switchdev mode will be supported.
>

**[v2: ipvs: align inner_mac_header for encapsulation](http://lore.kernel.org/netdev/20230609205842.2333727-1-terin@cloudflare.com/)**

> When using encapsulation the original packet's headers are copied to the
> inner headers. This preserves the space for an inner mac header, which
> is not used by the inner payloads for the encapsulation types supported
> by IPVS. If a packet is using GUE or GRE encapsulation and needs to be
> segmented, flow can be passed to __skb_udp_tunnel_segment() which
> calculates a negative tunnel header length. A negative tunnel header
> length causes pskb_may_pull() to fail, dropping the packet.
>

**[v1: net-next: tcp: tx path fully headless](http://lore.kernel.org/netdev/20230609204246.715667-1-edumazet@google.com/)**

> This series completes transition of TCP stack tx path
> to headless packets: All payload now reside in page frags,
> never in skb->head.
>

**[v1: net-next: net: create device lookup API with reference tracking](http://lore.kernel.org/netdev/20230609183207.1466075-1-kuba@kernel.org/)**

> We still see dev_hold() / dev_put() calls without reference tracker
> getting added in the new code. dev_get_by_name() / dev_get_by_index()
> seem to be one of the sources of those. Provide appropriate helpers.
> Allocating the tracker can obviously be done with an additional call
> to netdev_tracker_alloc(), but a single API feels cleaner.
>

**[v1: net-next: mdio: mdio-mux-mmioreg: Use of_property_read_reg() to parse "reg"](http://lore.kernel.org/netdev/20230609182615.1760266-1-robh@kernel.org/)**

> Use the recently added of_property_read_reg() helper to get the
> untranslated "reg" address value.
>

**[v1: net-next: net: add check for current MAC address in dev_set_mac_address](http://lore.kernel.org/netdev/20230609165241.827338-1-piotrx.gardocki@intel.com/)**

> In some cases it is possible for kernel to come with request
> to change primary MAC address to the address that is already
> set on the given interface.
>

**[v2: net: Check if FIPS mode is enabled when running selftests](http://lore.kernel.org/netdev/20230609164324.497813-1-magali.lemes@canonical.com/)**

> Some test cases from net/tls, net/fcnal-test and net/vrf-xfrm-tests
> that rely on cryptographic functions to work and use non-compliant FIPS
> algorithms fail in FIPS mode.
>

**[v1: net-next: tcp: Make pingpong threshold tunable](http://lore.kernel.org/netdev/1686327959-13478-1-git-send-email-haiyangz@microsoft.com/)**

> TCP pingpong threshold is 1 by default. But some applications, like SQL DB
> may prefer a higher pingpong threshold to activate delayed acks in quick
> ack mode for better performance.
>

**[v7: net-next: net: ioctl: Use kernel memory on protocol ioctl callbacks](http://lore.kernel.org/netdev/20230609152800.830401-1-leitao@debian.org/)**

> Most of the ioctls to net protocols operates directly on userspace
> argument (arg). Usually doing get_user()/put_user() directly in the
> ioctl callback.  This is not flexible, because it is hard to reuse these
> functions without passing userspace buffers.
>

**[v1: net-next: rhashtable: length helper for rhashtable and rhltable](http://lore.kernel.org/netdev/20230609151332.263152-1-pctammela@mojatatu.com/)**

> Whenever someone wants to retrieve the total number of elements in a
> rhashtable/rhltable it needs to open code the access to 'nelems'.
> Therefore provide a helper for such operation and convert two accesses as
> an example.
>

**[v1: net-next: add egress rate limit offload for Marvell 6393X family](http://lore.kernel.org/netdev/20230609141812.297521-1-alexis.lothore@bootlin.com/)**

> This series aims to give access to egress rate shaping offloading available
> on Marvell 88E6393X family (88E6393X/88E6193X/88E6191X/88E6361)
>
> The switch offers a very basic egress rate limiter: rate can be configured
> from 64kbps up to 10gbps depending on the model, with some specific
> increments depending on the targeted rate, and is "burstless".
>

**[v1: net-next: net: openvswitch: add support for l4 symmetric hashing](http://lore.kernel.org/netdev/20230609135955.3024931-1-aconole@redhat.com/)**

> Since its introduction, the ovs module execute_hash action allowed
> hash algorithms other than the skb->l4_hash to be used.  However,
> additional hash algorithms were not implemented.  This means flows
> requiring different hash distributions weren't able to use the
> kernel datapath.
>

**[v1: net-next: bnx2x: Make dmae_reg_go_c static](http://lore.kernel.org/netdev/20230609-bnx2x-static-v1-1-6c1a6888d227@kernel.org/)**

> Make dmae_reg_go_c static, it is only used in bnx2x_main.c
>
> Flagged by Sparse as:
>
>  .../bnx2x_main.c:291:11: warning: symbol 'dmae_reg_go_c' was not declared. Should it be static?
>

**[v2: net-next: net: mana: Add support for vlan tagging](http://lore.kernel.org/netdev/1686314837-14042-1-git-send-email-haiyangz@microsoft.com/)**

> To support vlan, use MANA_LONG_PKT_FMT if vlan tag is present in TX
> skb. Then extract the vlan tag from the skb struct, and save it to
> tx_oob for the NIC to transmit. For vlan tags on the payload, they
> are accepted by the NIC too.
>

**[[net PATCH v2] octeontx2-af: Move validation of ptp pointer before its usage](http://lore.kernel.org/netdev/20230609115806.2625564-1-saikrishnag@marvell.com/)**

> Moved PTP pointer validation before its use to avoid smatch warning.
> Also used kzalloc/kfree instead of devm_kzalloc/devm_kfree.
>

**[v1: net-next: phylink EEE support](http://lore.kernel.org/netdev/ZILsqV0gkSMMdinU@shell.armlinux.org.uk/)**

> There has been some recent discussion on generalising EEE support so
> that drivers implement it more consistently. This has mostly focused
> around phylib, but there are other situations where EEE may be useful.
>

**[v1: net-next: sfc: Add devlink dev info support for EF10](http://lore.kernel.org/netdev/168629745652.2744.6477682091656094391.stgit@palantir17.mph.net/)**

> Reuse the work done for EF100 to add devlink support for EF10.
> There is no devlink port support for EF10.
>

#### 安全增强

**[v1: kunit: Add test attributes API](http://lore.kernel.org/linux-hardening/20230610005149.1145665-1-rmoar@google.com/)**

> This is an RFC patch series to propose the addition of a test attributes
> framework to KUnit.
>
> There has been interest in filtering out "slow" KUnit tests. Most notably,
> a new config, CONFIG_MEMCPY_SLOW_KUNIT_TEST, has been added to exclude
> particularly slow memcpy tests
> (https://lore.kernel.org/all/20230118200653.give.574-kees@kernel.org/).
>

**[v1: Integer overflows while scanning for integers](http://lore.kernel.org/linux-hardening/20230607223755.1610-1-richard@nod.at/)**

> Lately I wondered whether users of integer scanning functions check
> for overflows.
> To detect such overflows around scanf I came up with the following
> patch. It simply triggers a WARN_ON_ONCE() upon an overflow.
>

**[[RESEND]v1: next: Replace one-element array with DECLARE_FLEX_ARRAY() helper](http://lore.kernel.org/linux-hardening/ZH+%2FrZ1R1cBjIxjS@work/)**

> One-element arrays as fake flex arrays are deprecated and we are moving
> towards adopting C99 flexible-array members, instead. So, replace
> one-element array declaration in struct ct_sns_gpnft_rsp, which is
> ultimately being used inside a union:
>
> drivers/scsi/qla2xxx/qla_def.h:
>
> Refactor the rest of the code, accordingly.
>
> This issue was found with the help of Coccinelle.
>

**[v1: um: Use HOST_DIR for mrproper](http://lore.kernel.org/linux-hardening/20230606222442.never.807-kees@kernel.org/)**

> When HEADER_ARCH was introduced, the MRPROPER_FILES (then MRPROPER_DIRS)
> list wasn't adjusted, leaving SUBARCH as part of the path argument.
> This resulted in the "mrproper" target not cleaning up arch/x86/... when
> SUBARCH was specified. Since HOST_DIR is arch/$(HEADER_ARCH), use it
> instead to get the correct path.
>

**[v2: uml: Replace strlcpy with strscpy](http://lore.kernel.org/linux-hardening/20230606182410.3976487-1-azeemshaikh38@gmail.com/)**

> strlcpy() reads the entire source buffer first.
> This read may exceed the destination size limit.
> This is both inefficient and can lead to linear read
> overflows if a source string is not NUL-terminated [1].
> In an effort to remove strlcpy() completely [2], replace
> strlcpy() here with strscpy().
> No return values were used, so direct replacement is safe.
>
> [1] https://www.kernel.org/doc/html/latest/process/deprecated.html#strlcpy
> [2] https://github.com/KSPP/linux/issues/89
>
> Closes: https://lore.kernel.org/oe-kbuild-all/202305311135.zGMT1gYR-lkp@intel.com/
>

#### 异步 IO

**[v1: Add io_uring support for futex wait/wake](http://lore.kernel.org/io-uring/20230609183125.673140-1-axboe@kernel.dk/)**

> Sending this just to the io_uring list for now so we can iron out
> details, questions, concerns, etc before going a bit broader to get
> the futex parts reviewed. Those are pretty straight forward though,
> and try not to get too entangled into futex internals.
>

**[v15: io_uring: add napi busy polling support](http://lore.kernel.org/io-uring/20230608163839.2891748-1-shr@devkernel.io/)**

> This adds the napi busy polling support in io_uring.c. It adds a new
> napi_list to the io_ring_ctx structure. This list contains the list of
> napi_id's that are currently enabled for busy polling. This list is
> used to determine which napi id's enabled busy polling. For faster
> access it also adds a hash table.
>

**[v14: io_uring: add napi busy polling support](http://lore.kernel.org/io-uring/20230605212009.1992313-1-shr@devkernel.io/)**

> This adds the napi busy polling support in io_uring.c. It adds a new
> napi_list to the io_ring_ctx structure. This list contains the list of
> napi_id's that are currently enabled for busy polling. This list is
> used to determine which napi id's enabled busy polling. For faster
> access it also adds a hash table.
>

#### Rust For Linux

**[v3: Rust scatterlist abstractions](http://lore.kernel.org/rust-for-linux/20230610104909.3202958-1-changxian.cqs@antgroup.com/)**

> This is a version of scatterlist abstractions for Rust drivers.
>
> Scatterlist is used for efficient management of memory buffers, which is
> essential for many kernel-level operations such as Direct Memory Access
> (DMA) transfers and crypto APIs.
>

**[v2: add abstractions for network device drivers](http://lore.kernel.org/rust-for-linux/01010188a42d5244-fffbd047-446b-4cbf-8a62-9c036d177276-000000@us-west-2.amazonses.com/)**

> This patchset adds minimum abstractions for network device drivers and
> Rust dummy network device driver, a simpler version of drivers/net/dummy.c.
>

**[v1: Rust PuzzleFS filesystem driver](http://lore.kernel.org/rust-for-linux/20230609063118.24852-1-amiculas@cisco.com/)**

> This is a proof of concept driver written for the PuzzleFS
> next-generation container filesystem [1]. I've included a short abstract
> about puzzlefs further below. This driver is based on the rust-next
> branch, on top of which I've backported the filesystem abstractions from
> Wedson Almeida Filho [2][3] and Miguel Ojeda's third-party crates
> support: proc-macro2, quote, syn, serde and serde_derive [4]. I've added
> the additional third-party crates serde_cbor[5] and hex [6]. Then I've
> adapted the user space puzzlefs code [1] so that the puzzlefs kernel
> module could present the directory hierarchy and implement the basic
> read functionality.
> 

**[v2: Rust enablement for AArch64](http://lore.kernel.org/rust-for-linux/20230606145606.1153715-1-Jamie.Cunliffe@arm.com/)**

> The first patch enables the basic building of Rust for AArch64. Since
> v1 this has been rewritten to avoid the use of a target.json file for
> AArch64 and use the upstream rustc target definition. x86-64 still
> uses the target.json approach though.
>

#### BPF

**[v12: evm: Do HMAC of multiple per LSM xattrs for new inodes](http://lore.kernel.org/bpf/20230610075738.3273764-1-roberto.sassu@huaweicloud.com/)**

> One of the major goals of LSM stacking is to run multiple LSMs side by side
> without interfering with each other. The ultimate decision will depend on
> individual LSM decision.
>

**[v1: tools api fs: More thread safety for global filesystem variables](http://lore.kernel.org/bpf/20230609224004.180988-1-irogers@google.com/)**

> Multiple threads, such as with "perf top", may race to initialize a
> file system path like hugetlbfs. The racy initialization of the path
> leads to at least memory leaks. To avoid this initialize each fs for
> reading the mount point path with pthread_once.
>

**[v4: bpf-next: verify scalar ids mapping in regsafe()](http://lore.kernel.org/bpf/20230609210143.2625430-1-eddyz87@gmail.com/)**

> This example is unsafe because not all execution paths verify r7 range.
> Because of the jump at (4) the verifier would arrive at (6) in two states:
> I.  r6{.id=b}, r7{.id=b} via path 1-6;
> II. r6{.id=a}, r7{.id=b} via path 1-4, 6.
>
> Currently regsafe() does not call check_ids() for scalar registers,
> thus from POV of regsafe() states (I) and (II) are identical.
>

**[v3: net-next: introduce page_pool_alloc() API](http://lore.kernel.org/bpf/20230609131740.7496-1-linyunsheng@huawei.com/)**

> In [1] & [2], there are usecases for veth and virtio_net to
> use frag support in page pool to reduce memory usage, and it
> may request different frag size depending on the head/tail
> room space for xdp_frame/shinfo and mtu/packet size. When the
> requested frag size is large enough that a single page can not
> be split into more than one frag, using frag support only have
> performance penalty because of the extra frag count handling
> for frag support.
>

**[v4: bpf-next: bpf, x86: allow function arguments up to 12 for TRACING](http://lore.kernel.org/bpf/20230609095653.1406173-1-imagedong@tencent.com/)**

> Therefore, let's enhance it by increasing the function arguments count
> allowed in arch_prepare_bpf_trampoline(), for now, only x86_64.
>
> In the 1st patch, we make arch_prepare_bpf_trampoline() support to copy
> function arguments in stack for x86 arch. Therefore, the maximum
> arguments can be up to MAX_BPF_FUNC_ARGS for FENTRY and FEXIT.
>

**[v3: Bring back vmlinux.h generation](http://lore.kernel.org/bpf/20230609043240.43890-1-irogers@google.com/)**

> Commit 760ebc45746b ("perf lock contention: Add empty 'struct rq' to
> satisfy libbpf 'runqueue' type verification") inadvertently created a
> declaration of 'struct rq' that conflicted with a generated
> vmlinux.h's:
>

**[v5: bpf-next: selftests/bpf: Add benchmark for bpf memory allocator](http://lore.kernel.org/bpf/20230609024030.2585058-1-houtao@huaweicloud.com/)**

> The benchmark could be used to compare the performance of hash map
> operations and the memory usage between different flavors of bpf memory
> allocator (e.g., no bpf ma vs bpf ma vs reuse-after-gp bpf ma). It also
> could be used to check the performance improvement or the memory saving
> provided by optimization.
>

**[v1: ftrace: Show all functions with addresses in available_filter_functions_addrs](http://lore.kernel.org/bpf/20230608212613.424070-1-jolsa@kernel.org/)**

> when ftrace based tracers we need to cross check available_filter_functions
> with /proc/kallsyms. For example for kprobe_multi bpf link (based on fprobe)
> we need to make sure that symbol regex resolves to traceable symbols and
> that we get proper addresses for them.
>

**[v5: bpf: Socket lookup BPF API from tc/xdp ingress does not respect VRF bindings.](http://lore.kernel.org/bpf/20230608114155.39367-1-gilad9366@gmail.com/)**

> When calling socket lookup from L2 (tc, xdp), VRF boundaries aren't
> respected. This patchset fixes this by regarding the incoming device's
> VRF attachment when performing the socket lookups from tc/xdp.
>

**[v2: bpf-next: bpf: Support ->fill_link_info for kprobe_multi and perf_event links](http://lore.kernel.org/bpf/20230608103523.102267-1-laoar.shao@gmail.com/)**

> This patchset enhances the usability of kprobe_multi programs by introducing
> support for ->fill_link_info. This allows users to easily determine the
> probed functions associated with a kprobe_multi program. While
> `bpftool perf show` already provides information about functions probed by
> perf_event programs, supporting ->fill_link_info ensures consistent access to
> this information across all bpf links.
>

**[v1: perf lock contention: Add -x option for CSV style output](http://lore.kernel.org/bpf/20230608053844.2872345-1-namhyung@kernel.org/)**

> Sometimes we want to process the output by external programs.  Let's add
> the -x option to specify the field separator like perf stat.
>

**[v2: bpf-next: BPF token](http://lore.kernel.org/bpf/20230607235352.1723243-1-andrii@kernel.org/)**

> This patch set introduces new BPF object, BPF token, which allows to delegate
> a subset of BPF functionality from privileged system-wide daemon (e.g.,
> systemd or any other container manager) to a *trusted* unprivileged
> application. Trust is the key here. This functionality is not about allowing
> unconditional unprivileged BPF usage. Establishing trust, though, is
> completely up to the discretion of respective privileged application that
> would create a BPF token.
>

**[v1: bpf-next: selftests/bpf: Add missing prototypes for several test kfuncs](http://lore.kernel.org/bpf/20230607224046.236510-1-jolsa@kernel.org/)**

> Adding missing prototypes for several kfuncs that are used by
> test_verifier tests. We don't really need kfunc prototypes for
> these tests, but adding them to silence 'make W=1' build and
> to have all test kfuncs declarations in bpf_testmod_kfunc.h.
>

**[v2: bpf-next: BPF link support for tc BPF programs](http://lore.kernel.org/bpf/20230607192625.22641-1-daniel@iogearbox.net/)**

> This series adds BPF link support for tc BPF programs. We initially
> presented the motivation, related work and design at last year's LPC
> conference in the networking & BPF track [0], and a recent update on
> our progress of the rework during this year's LSF/MM/BPF summit [1].
> The main changes are in first two patches and the last two have an
> extensive batch of test cases we developed along with it, please see
> individual patches for details. We tested this series with tc-testing
> selftest suite as well as BPF CI/selftests. Thanks!
>

**[v2: bpf-next: bpf, arm64: use BPF prog pack allocator in BPF JIT](http://lore.kernel.org/bpf/20230607091814.46080-1-puranjay12@gmail.com/)**

> BPF programs currently consume a page each on ARM64. For systems with many BPF
> programs, this adds significant pressure to instruction TLB. High iTLB pressure
> usually causes slow down for the whole system.
>

**[v4: bpf-next: Handle immediate reuse in bpf memory allocator](http://lore.kernel.org/bpf/20230606035310.4026145-1-houtao@huaweicloud.com/)**

> The implementation of v4 is mainly based on suggestions from Alexi [0].
> There are still pending problems for the current implementation as shown
> in the benchmark result in patch #3, but there was a long time from the
> posting of v3, so posting v4 here for further disscussions and more
> suggestions.
>

**[v1: bpf: search_bpf_extables should search subprogram extables](http://lore.kernel.org/bpf/20230605164955.GA1977@templeofstupid.com/)**

> JIT'd bpf programs that have subprograms can have a postive value for
> num_extentries but a NULL value for extable.  This is problematic if one of
> these bpf programs encounters a fault during its execution.  The fault
> handlers correctly identify that the faulting IP belongs to a bpf program.
> However, performing a search_extable call on a NULL extable leads to a
> second fault.
>

**[v3: bpf-next: xsk: multi-buffer support](http://lore.kernel.org/bpf/20230605144433.290114-1-maciej.fijalkowski@intel.com/)**

> This series of patches add multi-buffer support for AF_XDP. XDP and
> various NIC drivers already have support for multi-buffer packets. With
> this patch set, programs using AF_XDP sockets can now also receive and
> transmit multi-buffer packets both in copy as well as zero-copy mode.
> ZC multi-buffer implementation is based on ice driver.
>

**[v2: bpf: netfilter: add BPF_NETFILTER bpf_attach_type](http://lore.kernel.org/bpf/20230605131445.32016-1-fw@strlen.de/)**

> Andrii Nakryiko writes:
>
>  And we currently don't have an attach type for NETLINK BPF link.
>  Thankfully it's not too late to add it. I see that link_create() in
>  kernel/bpf/syscall.c just bypasses attach_type check. We shouldn't
>  have done that. Instead we need to add BPF_NETLINK attach type to enum
>  bpf_attach_type. And wire all that properly throughout the kernel and
>  libbpf itself.
>

**[v1: Add api to manipulate global varaible](http://lore.kernel.org/bpf/20230605085733.1833-1-yb203166@antfin.com/)**

> We (the antgroup) has a requirement to manipulate global variables.
> The platform to manage bpf bytecode has no idea about varaibles'
> type/size/address. It only has some strings (like key = value) passed
> from admin. We find a way to parse BTF and then query/update the
> variables. There may be better ways to do it. This approach is what
> we can find for now.
>

**[v1: bpf: Add extra path pointer check to d_path helper](http://lore.kernel.org/bpf/20230604140103.3542071-1-jolsa@kernel.org/)**

> Anastasios reported crash on stable 5.15 kernel with following
> bpf attached to lsm hook:
>
>   SEC("lsm.s/bprm_creds_for_exec")
>   int BPF_PROG(bprm_creds_for_exec, struct linux_binprm *bprm)
>   {
>           struct path *path = &bprm->executable->f_path;
>           char p[128] = { 0 };
>
>           bpf_d_path(path, p, 128);
>           return 0;
>   }
>
> but bprm->executable can be NULL, so bpf_d_path call will crash:
>

### 周边技术动态

#### Qemu

**[v3: linux-user/riscv: Add syscall riscv_hwprobe](http://lore.kernel.org/qemu-devel/7f8d733df6e9b6151e9efb843d55441348805e70.camel@rivosinc.com/)**

> This patch adds the new syscall for the
> "RISC-V Hardware Probing Interface"
> (https://docs.kernel.org/riscv/hwprobe.html).
>

**[v4: target/riscv: Add Smrnmi support.](http://lore.kernel.org/qemu-devel/20230608072314.3561109-1-tommy.wu@sifive.com/)**

> This patchset added support for Smrnmi Extension in RISC-V.
>
> RNMI also has higher priority than any other interrupts or exceptions
> and cannot be disabled by software.
>
> RNMI may be used to route to other devices such as Bus Error Unit or
> Watchdog Timer in the future.
>

#### Buildroot

**[[branch/2023.02.x] package/cmake: (ctest) add support for riscv architecture](http://lore.kernel.org/buildroot/20230605201932.CCFD386C93@busybox.osuosl.org/)**

> commit: https://git.buildroot.net/buildroot/commit/?id=13e4f1942cb2aca57edf5b2b7514d491690e8eeb
> branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/2023.02.x
>
> Package binaries can be successfully built for and then executed on
> RISC-V platforms including RV32 and RV64 variants. Tested in QEMU.
>

#### U-Boot

**[v4: SPL NVMe support](http://lore.kernel.org/u-boot/20230603140256.2443518-1-mchitale@ventanamicro.com/)**

> This patchset adds support to load images of the SPL's next booting
> stage from a NVMe device.
>

## 20230604：第 48 期

### 内核动态

#### RISC-V 架构支持

**[v1: tools/nolibc: add two new syscall helpers](http://lore.kernel.org/linux-riscv/cover.1685856497.git.falcon@tinylab.org/)**

> When I worked on adding new syscalls and the related library routines,
> I have seen most of the library routines share the same syscall call and
> return logic, this patchset adds two macros to simplify and shrink them.
>

**[v3: nolibc: add part2 of support for rv32](http://lore.kernel.org/linux-riscv/cover.1685780412.git.falcon@tinylab.org/)**

> This is the v3 part2 of support for rv32, differs from the v2 part2 [1],
> we only fix up compile issues in this patchset.
>
> With the v3 generic part1 [2] and this patchset, we can compile nolibc
> for rv32 now.
>
> This is based on the idea of suggestions from Arnd [3], instead of
> '#error' on the unsupported syscall on a target platform, a 'return
> -ENOSYS' allow us to compile it at first and then allow we fix up the
> test failures reported by nolibc-test one by one.
>

**[v3: nolibc: add generic part1 of prepare for rv32](http://lore.kernel.org/linux-riscv/cover.1685777982.git.falcon@tinylab.org/)**

> This is the v3 generic part1 for rv32, all of the found issues of v2
> part1 [1] have been fixed up, several generic patches have been fixed up
> and merged from v2 part2 [2] to this series, the standalone test_fork
> patch [4] is merged with a Reviewed-by line into this series too.
>

**[v2: Use MMU read lock for clear-dirty-log](http://lore.kernel.org/linux-riscv/20230602160914.4011728-1-vipinsh@google.com/)**

> This series is on top of kvmarm/next as I needed to also modify Eager
> page splitting logic in clear-dirty-log API. Eager page splitting is not
> present in Linux 6.4-rc4.
>

**[v2: Add initialization of clock for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20230602084925.215411-1-william.qiu@starfivetech.com/)**

> This patchset adds initial rudimentary support for the StarFive
> Quad SPI controller driver. And this driver will be used in
> StarFive's VisionFive 2 board. In 6.4, the QSPI_AHB and QSPI_APB
> clocks changed from the default ON state to the default OFF state,
> so these clocks need to be enabled in the driver.At the same time,
> dts patch is added to this series.
>

**[v1: Add DRM driver for StarFive SoC JH7110](http://lore.kernel.org/linux-riscv/20230602074043.33872-1-keith.zhao@starfivetech.com/)**

> This series is a DRM driver for StarFive SoC JH7110, which includes a
> display controller driver for Verisilicon DC8200 and an HMDI driver.
>
> We use GEM framework for buffer management and allocate memory by
> using DMA APIs.
>

**[v2: gpio: sifive: Add missing check for platform_get_irq](http://lore.kernel.org/linux-riscv/20230602072755.7314-1-jiasheng@iscas.ac.cn/)**

> Add the missing check for platform_get_irq and return error code
> if it fails.
>

**[v2: Add support for Allwinner GPADC on D1/T113s/R329/T507 SoCs](http://lore.kernel.org/linux-riscv/20230601223104.1243871-1-bigunclemax@gmail.com/)**

> This series adds support for general purpose ADC (GPADC) on new
> Allwinner's SoCs, such as D1, T113s, T507 and R329. The implemented driver
> provides basic functionality for getting ADC channels data.
>

**[v2: riscv/purgatory: Do not use fortified string functions](http://lore.kernel.org/linux-riscv/20230601160025.gonna.868-kees@kernel.org/)**

> This means that the memcpy() calls with "buf" as a destination in
> sha256.c's code will attempt to perform run-time bounds checking, which
> could lead to calling missing functions, specifically a potential
> WARN_ONCE, which isn't callable from purgatory.
>

**[v1: mm: jit/text allocator](http://lore.kernel.org/linux-riscv/20230601101257.530867-1-rppt@kernel.org/)**

> module_alloc() is used everywhere as a mean to allocate memory for code.
>
> Beside being semantically wrong, this unnecessarily ties all subsystmes
> that need to allocate code, such as ftrace, kprobes and BPF to modules
> and puts the burden of code allocation to the modules code.
>

**[v4: StarFive's Pulse Width Modulation driver support](http://lore.kernel.org/linux-riscv/20230601085154.36938-1-william.qiu@starfivetech.com/)**

> This patchset adds initial rudimentary support for the StarFive
> Pulse Width Modulation controller driver. And this driver will
> be used in StarFive's VisionFive 2 board.The first patch add
> Documentations for the device and Patch 2 adds device probe for
> the module.
>

**[v3: Split ptdesc from struct page](http://lore.kernel.org/linux-riscv/20230531213032.25338-1-vishal.moola@gmail.com/)**

> The MM subsystem is trying to shrink struct page. This patchset
> introduces a memory descriptor for page table tracking - struct ptdesc.
>
> This patchset introduces ptdesc, splits ptdesc from struct page, and
> converts many callers of page table constructor/destructors to use ptdescs.
>

**[v2: riscv: mm: Pre-allocate PGD entries for vmalloc/modules area](http://lore.kernel.org/linux-riscv/20230531093817.665799-1-bjorn@kernel.org/)**

> The RISC-V port requires that kernel PGD entries are to be
> synchronized between MMs. This is done via the vmalloc_fault()
> function, that simply copies the PGD entries from init_mm to the
> faulting one.
>

**[v3: RISC-V: KVM: Ensure SBI extension is enabled](http://lore.kernel.org/linux-riscv/20230530175024.354527-1-ajones@ventanamicro.com/)**

> Ensure guests can't attempt to invoke SBI extension functions when the
> SBI extension's probe function has stated that the extension is not
> available.
>

**[v1: selftests/nolibc: add user-space 'efault' handler](http://lore.kernel.org/linux-riscv/cover.1685443199.git.falcon@tinylab.org/)**

> This is not really for merge, but only let it work as a demo code to
> test whether it is possible to restore the next test when there is a bad
> pointer access in user-space [1].
>

**[v1: fdt: Mark "/reserved-memory" nodes as nosave if !reusable](http://lore.kernel.org/linux-riscv/20230530080425.18612-1-alexghiti@rivosinc.com/)**

> In the RISC-V kernel, the firmware does not mark the region it uses as
> "no-map" so that the kernel can avoid having holes in the linear mapping
> and then use larger pages.
>

**[v2: nolibc: add part3 of support for rv32](http://lore.kernel.org/linux-riscv/cover.1685428087.git.falcon@tinylab.org/)**

> Hi, Willy
>
> These two patches are based on part2 of support for rv32 [1], I have
> forgotten to send them together.
>

**[v1: riscv: mm: Pre-allocate PGD entries vmalloc/modules area](http://lore.kernel.org/linux-riscv/20230529180023.289904-1-bjorn@kernel.org/)**

> The RISC-V port requires that kernel PGD entries are to be
> synchronized between MMs. This is done via the vmalloc_fault()
> function, that simply copies the PGD entries from init_mm to the
> faulting one.
>

**[v5: Add JH7110 MIPI DPHY RX support](http://lore.kernel.org/linux-riscv/20230529121503.3544-1-changhuang.liang@starfivetech.com/)**

> This patchset adds mipi dphy rx driver for the StarFive JH7110 SoC.
> It is used to transfer CSI camera data. The series has been tested on
> the VisionFive 2 board.
>

**[v1: riscv: Enable ARCH_SUSPEND_POSSIBLE for s2idle](http://lore.kernel.org/linux-riscv/20230529101524.322076-1-songshuaishuai@tinylab.org/)**

> With this configuration opened, the basic platform-independent s2idle is
> provided by the sole "s2idle" string in `/sys/power/mem_sleep`.
>
> At the end of s2idle, harts will hit the `wfi` instruction or enter the
> SUSPENDED state through the sbi_cpuidle driver. The interrupt of possible
> wakeup devices will be kept to wake the system up.
>

**[v12: -next: riscv: Add independent irq/softirq stacks](http://lore.kernel.org/linux-riscv/20230529084600.2878130-1-guoren@kernel.org/)**

> This patch series adds independent irq/softirq stacks to decrease the
> press of the thread stack. Also, add a thread STACK_SIZE config for
> users to adjust the proper size during compile time.
>

#### 进程调度

**[v1: net: sched: wrap tc_skip_wrapper with CONFIG_RETPOLINE](http://lore.kernel.org/lkml/20230602235210.91262-1-minhuadotchen@gmail.com/)**

> This patch fixes the following sparse warning:
>
> net/sched/sch_api.c:2305:1: sparse: warning: symbol 'tc_skip_wrapper' was not declared. Should it be static?
>

**[v1: net-next: net/sched: introduce pretty printers for Qdiscs](http://lore.kernel.org/lkml/20230602162935.2380811-1-vladimir.oltean@nxp.com/)**

> Sometimes when debugging Qdiscs it may be confusing to know exactly what
> you're looking at, especially since they're hierarchical. Pretty printing
> the handle, parent handle and netdev is a bit cumbersome, so this patch
> proposes a set of wrappers around __qdisc_printk() which are heavily
> inspired from __net_printk().
>

**[v1: sched: EEVDF and latency-nice and/or slice-attr](http://lore.kernel.org/lkml/20230531115839.089944915@infradead.org/)**

> Latest version of the EEVDF [1] patches.
>
> The only real change since last time is the fix for tick-preemption [2], and a
> simple safe-guard for the mixed slice heuristic.
>
> Other than that, I've re-arranged the patches to make EEVDF come first and have
> the latency-nice or slice-attribute patches on top.
>

**[v2: sched/fair: Don't balance task to its current running CPU](http://lore.kernel.org/lkml/20230530082507.10444-1-yangyicong@huawei.com/)**

> The new_dst_cpu is chosen from the env->dst_grpmask. Currently it
> contains CPUs in sched_group_span() and if we have overlapped groups it's
> possible to run into this case. This patch makes env->dst_grpmask of
> group_balance_mask() which exclude any CPUs from the busiest group and
> solve the issue. For balancing in a domain with no overlapped groups
> the behaviour keeps same as before.
>

**[v8: sched/fair: Scan cluster before scanning LLC in wake-up path](http://lore.kernel.org/lkml/20230530070253.33306-1-yangyicong@huawei.com/)**

> This is the follow-up work to support cluster scheduler. Previously
> we have added cluster level in the scheduler for both ARM64[1] and
> X86[2] to support load balance between clusters to bring more memory
> bandwidth and decrease cache contention. This patchset, on the other
> hand, takes care of wake-up path by giving CPUs within the same cluster
> a try before scanning the whole LLC to benefit those tasks communicating
> with each other.
>

**[v1: sched: deadline: Simplify pick_earliest_pushable_dl_task()](http://lore.kernel.org/lkml/20230530181145.2880-1-kunyu@nfschina.com/)**

> Using the while statement instead of the if and goto statements is more
> concise and efficient.
>

#### 内存管理

**[v6: mm, dma, arm64: Reduce ARCH_KMALLOC_MINALIGN to 8](http://lore.kernel.org/linux-mm/20230531154836.1366225-1-catalin.marinas@arm.com/)**

> Here's version 6 of the series reducing the kmalloc() minimum alignment
> on arm64 to 8 (from 128). There are patches already to do the same for
> riscv (pretty straight-forward after this series).
>
> The first 11 patches decouple ARCH_KMALLOC_MINALIGN from
> ARCH_DMA_MINALIGN and, for arm64, limit the kmalloc() caches to those
> aligned to the run-time probed cache_line_size(). On arm64 we gain the
> kmalloc-{64,192} caches.
>

**[v1: Introduce cmpxchg128() -- aka. the demise of cmpxchg_double()](http://lore.kernel.org/linux-mm/20230531130833.635651916@infradead.org/)**

> After much breaking of things, find here the improved version.
>

**[v2: net-next: splice, net: Handle MSG_SPLICE_PAGES in AF_TLS](http://lore.kernel.org/linux-mm/20230531124528.699123-1-dhowells@redhat.com/)**

> Here are patches to make AF_TLS handle the MSG_SPLICE_PAGES internal
> sendmsg flag.  MSG_SPLICE_PAGES is an internal hint that tells the protocol
> that it should splice the pages supplied if it can.  Its sendpage
> implementations are then turned into wrappers around that.
>

**[v7: bio: check return values of bio_add_page](http://lore.kernel.org/linux-mm/cover.1685532726.git.johannes.thumshirn@wdc.com/)**

> We have two functions for adding a page to a bio, __bio_add_page() which is
> used to add a single page to a freshly created bio and bio_add_page() which is
> used to add a page to an existing bio.
>
> While __bio_add_page() is expected to succeed, bio_add_page() can fail.
>

**[v2: net-next: splice, net: Handle MSG_SPLICE_PAGES in AF_KCM](http://lore.kernel.org/linux-mm/20230531110423.643196-1-dhowells@redhat.com/)**

> Here are patches to make AF_KCM handle the MSG_SPLICE_PAGES internal
> sendmsg flag.  MSG_SPLICE_PAGES is an internal hint that tells the protocol
> that it should splice the pages supplied if it can.  Its sendpage
> implementation is then turned into a wrapper around that.
>

**[v2: net-next: splice, net: Handle MSG_SPLICE_PAGES in Chelsio-TLS](http://lore.kernel.org/linux-mm/20230531110008.642903-1-dhowells@redhat.com/)**

> Here are patches to make Chelsio-TLS handle the MSG_SPLICE_PAGES internal
> sendmsg flag.  MSG_SPLICE_PAGES is an internal hint that tells the protocol
> that it should splice the pages supplied if it can.  Its sendpage
> implementation is then turned into a wrapper around that.
>

**[v1: make unregistration of super_block shrinker more faster](http://lore.kernel.org/linux-mm/20230531095742.2480623-1-qi.zheng@linux.dev/)**

> The kernel test robot noticed a -88.8% regression of stress-ng.ramfs.ops_per_sec
> on commit f95bdb700bc6 ("mm: vmscan: make global slab shrink lockless"). More
> details can be seen from the link[1] below.
>

**[v2: mm/migrate_device: Try to handle swapcache pages](http://lore.kernel.org/linux-mm/20230531044018.17893-1-mpenttil@redhat.com/)**

> Migrating file pages and swapcache pages into device memory is not supported.
> The decision is done based on page_mapping(). For now, swapcache pages are not migrated.
>

**[v1: mm: zswap: multiple zpool support](http://lore.kernel.org/linux-mm/20230531022911.1168524-1-yosryahmed@google.com/)**

> Support using multiple zpools of the same type in zswap, for concurrency
> purposes. Add CONFIG_ZSWAP_NR_ZPOOLS_ORDER to control the number of
> zpools. The order is specific by the config rather than the absolute
> number to guarantee a power of 2. This is useful so that we can use
> deterministically link each entry to a zpool by hashing the zswap_entry
> pointer.
>

**[v3: zswap: do not shrink if cgroup may not zswap](http://lore.kernel.org/linux-mm/20230530232435.3097106-1-nphamcs@gmail.com/)**

> Before storing a page, zswap first checks if the number of stored pages
> exceeds the limit specified by memory.zswap.max, for each cgroup in the
> hierarchy. If this limit is reached or exceeded, then zswap shrinking is
> triggered and short-circuits the store attempt.
>

**[v1: mm: zswap: support exclusive loads](http://lore.kernel.org/linux-mm/20230530210251.493194-1-yosryahmed@google.com/)**

> Commit 71024cb4a0bf ("frontswap: remove frontswap_tmem_exclusive_gets")
> removed support for exclusive loads from frontswap as it was not used.
>
> Bring back exclusive loads support to frontswap by adding an
> exclusive_loads argument to frontswap_ops. Add support for exclusive
> loads to zswap behind CONFIG_ZSWAP_EXCLUSIVE_LOADS.
>

**[v1: zswap: do not shrink when memory.zswap.max is 0](http://lore.kernel.org/linux-mm/20230530162153.836565-1-nphamcs@gmail.com/)**

> Before storing a page, zswap first checks if the number of stored pages
> exceeds the limit specified by memory.zswap.max, for each cgroup in the
> hierarchy. If this limit is reached or exceeded, then zswap shrinking is
> triggered and short-circuits the store attempt.
>

**[v2: net-next: crypto, splice, net: Make AF_ALG handle sendmsg(MSG_SPLICE_PAGES)](http://lore.kernel.org/linux-mm/20230530141635.136968-1-dhowells@redhat.com/)**

> Here's the fourth tranche of patches towards providing a MSG_SPLICE_PAGES
> internal sendmsg flag that is intended to replace the ->sendpage() op with
> calls to sendmsg().  MSG_SPLICE_PAGES is a hint that tells the protocol
> that it should splice the pages supplied if it can.
>

**[v4: sock: Improve condition on sockmem pressure](http://lore.kernel.org/linux-mm/20230530114011.13368-1-wuyun.abel@bytedance.com/)**

> Currently the memcg's status is also accounted into the socket's
> memory pressure to alleviate the memcg's memstall. But there are
> still cases that can be improved. Please check the patches for
> detailed info.
>

**[v2: string: use __builtin_memcpy() in strlcpy/strlcat](http://lore.kernel.org/linux-mm/20230530083911.1104336-1-glider@google.com/)**

> lib/string.c is built with -ffreestanding, which prevents the compiler
> from replacing certain functions with calls to their library versions.
>

**[v1: -next: mm: page_alloc: simplify has_managed_dma()](http://lore.kernel.org/linux-mm/20230529144022.42927-1-wangkefeng.wang@huawei.com/)**

> The ZONE_DMA should only exists on Node 0, only check NODE_DATA(0)
> is enough, so simplify has_managed_dma() and make it inline.
>

**[v1: mm: free retracted page table by RCU](http://lore.kernel.org/linux-mm/35e983f5-7ed3-b310-d949-9ae8b130cdab@google.com/)**

> Here is the third series of patches to mm (and a few architectures), based
> on v6.4-rc3 with the preceding two series applied: in which khugepaged
> takes advantage of pte_offset_map[_lock]() allowing for pmd transitions.
>

**[v1: Do not print page type when the page has no type](http://lore.kernel.org/linux-mm/ZHI0YKzZADjr1nyq@casper.infradead.org/)**

> It is confusing and unnecessary to print the page type when the
> page has no type.
>

#### 文件系统

**[v2: Create large folios in iomap buffered write path](http://lore.kernel.org/linux-fsdevel/20230602222445.2284892-1-willy@infradead.org/)**

> Commit ebb7fb1557b1 limited the length of ioend chains to 4096 entries
> to improve worst-case latency.  Unfortunately, this had the effect of
> limiting the performance of:
>
> fio -name write-bandwidth -rw=write -bs=1024Ki -size=32Gi -runtime=30 \
>         -iodepth 1 -ioengine sync -zero_buffers=1 -direct=0 -end_fsync=1 \
>         -numjobs=4 -directory=/mnt/test
>
> The problem ends up being lock contention on the i_pages spinlock as we
> clear the writeback bit on each folio (and propagate that up through
> the tree).  By using larger folios, we decrease the number of folios
> to be processed by a factor of 256 for this benchmark, eliminating the
> lock contention.
>

**[v1: highmem: Rename put_and_unmap_page() to unmap_and_put_page()](http://lore.kernel.org/linux-fsdevel/20230602103307.5637-1-fmdefrancesco@gmail.com/)**

> With commit 849ad04cf562a ("new helper: put_and_unmap_page()"), Al Viro
> introduced the put_and_unmap_page() to use in those many places where we
> have a common pattern consisting of calls to kunmap_local() +
> put_page().
>

**[v1: fs: Rename put_and_unmap_page() to unmap_and_put_page()](http://lore.kernel.org/linux-fsdevel/20230601132317.13606-1-fmdefrancesco@gmail.com/)**

> With commit 849ad04cf562a ("new helper: put_and_unmap_page()"), Al Viro
> introduced the put_and_unmap_page() to use in those many places where we
> have a common pattern consisting of calls to kunmap_local() +
> put_page().
>

**[v2: zonefs: use iomap for synchronous direct writes](http://lore.kernel.org/linux-fsdevel/20230601125636.205191-1-dlemoal@kernel.org/)**

> Remove the function zonefs_file_dio_append() that is used to manually
> issue REQ_OP_ZONE_APPEND BIOs for processing synchronous direct writes
> and use iomap instead.
>

**[v1: fs.h: Optimize file struct to prevent false sharing](http://lore.kernel.org/linux-fsdevel/20230601092400.27162-1-zhiyin.chen@intel.com/)**

> In the syscall test of UnixBench, performance regression occurred due
> to false sharing.
>

**[v1: fuse: Abort the requests under processing queue with a spin_lock](http://lore.kernel.org/linux-fsdevel/20230531092643.45607-1-quic_pragalla@quicinc.com/)**

> There is a potential race/timing issue while aborting the
> requests on processing list between fuse_dev_release() and
> fuse_abort_conn(). This is resulting into below warnings
> and can even result into UAF issues.
>

**[v3: NFSD: recall write delegation on GETATTR conflict](http://lore.kernel.org/linux-fsdevel/1685500507-23598-1-git-send-email-dai.ngo@oracle.com/)**

> This patch series adds the recall of write delegation when there is
> conflict with a GETATTR and a counter in /proc/net/rpc/nfsd to keep
> count of this recall.
>

**[v4: fs/sysv: Null check to prevent null-ptr-deref bug](http://lore.kernel.org/linux-fsdevel/20230531013141.19487-1-princekumarmaurya06@gmail.com/)**

> sb_getblk(inode->i_sb, parent) return a null ptr and taking lock on
> that leads to the null-ptr-deref bug.
>
> Reported-by: syzbot+aad58150cbc64ba41bdc@syzkaller.appspotmail.com
> Closes: https://syzkaller.appspot.com/bug?extid=aad58150cbc64ba41bdc
>

**[v1: sysctl: move umh and keys sysctls](http://lore.kernel.org/linux-fsdevel/20230530232914.3689712-1-mcgrof@kernel.org/)**

> If you look at kernel/sysctl.c there are two sysctl arrays which
> are declared in header files but registered with no good reason now
> on kernel/sysctl.c instead of the place they belong. So just do
> the registration where it belongs.
>

**[v2: multiblock allocator improvements](http://lore.kernel.org/linux-fsdevel/cover.1685449706.git.ojaswin@linux.ibm.com/)**

> So this patch was intended to remove a dead if-condition but it was not
> actually dead code and removing it was causing a performance regression.
> Unfortunately I somehow missed that when I was reviewing his patchset
> and it already went in so I had to revert the commit. I've added details
> of the regression and root cause in the revert commit. Also attaching
> the performance numbers I observer:
>

**[v1: fs/buffer: using __bio_add_page in submit_bh_wbc()](http://lore.kernel.org/linux-fsdevel/20230530033239.17534-1-gouhao@uniontech.com/)**

> In submit_bh_wbc(), bio is newly allocated, so it
> does not need any merging logic.
>
> And using bio_add_page here will execute 'bio_flagged(
> bio, BIO_CLONED)' and 'bio_full' twice, which is unnecessary.
>

**[v1: FUSE: dev: Change the posiion of spin_lock](http://lore.kernel.org/linux-fsdevel/20230529015656.3099390-1-lijun01@kylinos.cn/)**

> just list_del need spin_lock &#65292;so the spin_lock should be close to
> "list_del(&req->list)", this may add a little benefit.
>

**[v1: Null check to prevent null-ptr-deref bug](http://lore.kernel.org/linux-fsdevel/20230528173546.593511-1-princekumarmaurya06@gmail.com/)**

> sb_getblk(inode->i_sb, parent) return a null ptr and taking lock on
> that leads to the null-ptr-deref bug.
>

#### 网络设备

**[v5: net-next: net: flower: add cfm support](http://lore.kernel.org/netdev/20230604115825.2739031-1-zahari.doychev@linux.com/)**

> The first patch adds cfm support to the flow dissector.
> The second adds the flower classifier support.
> The third adds a selftest for the flower cfm functionality.
>
> iproute2 changes will come in follow up patches.
>

**[v4: vsock: MSG_ZEROCOPY flag support](http://lore.kernel.org/netdev/20230603204939.1598818-1-AVKrasnov@sberdevices.ru/)**

> Difference with copy way is not significant. During packet allocation,
> non-linear skb is created and filled with pinned user pages.
> There are also some updates for vhost and guest parts of transport - in
> both cases i've added handling of non-linear skb for virtio part. vhost
> copies data from such skb to the guest's rx virtio buffers. In the guest,
> virtio transport fills tx virtio queue with pages from skb.
>

**[v1: Add support for sam9x7 SoC family](http://lore.kernel.org/netdev/20230603200243.243878-1-varshini.rajendran@microchip.com/)**

> This patch series adds support for the new SoC family - sam9x7.
>  - The device tree, configs and drivers are added
>  - Clock driver for sam9x7 is added
>  - Support for basic peripherals is added
>

**[v1: RDMA/siw: Fabricate a GID on tun and loopback devices](http://lore.kernel.org/netdev/168580524310.5238.13720896895363588620.stgit@oracle-102.nfsv4bat.org/)**

> LOOPBACK and NONE (tunnel) devices have all-zero MAC addresses.
> Currently, siw_device_create() falls back to copying the IB device's
> name in those cases, because an all-zero MAC address breaks the RDMA
> core address resolution mechanism.
>

**[v1: net-next: Move KSZ9477 errata handling to PHY driver](http://lore.kernel.org/netdev/20230602234019.436513-1-robert.hancock@calian.com/)**

> Patches to move handling for KSZ9477 PHY errata register fixes from
> the DSA switch driver into the corresponding PHY driver, for more
> proper layering and ordering.
>

**[v1: net: dsa: realtek: rtl8365mb: add missing case for digital interface 0](http://lore.kernel.org/netdev/40df61cc5bebe94e4d7d32f79776be0c12a37d61.1685746295.git.chunkeey@gmail.com/)**

> when bringing up the switch on a Netgear WNDAP660, I observed that
> no traffic got passed from the RTL8363 to the ethernet interface...
>
> Turns out, this was because the dropped case for
> RTL8365MB_DIGITAL_INTERFACE_SELECT_REG(0) that
> got deleted by accident.
>

**[v10: vfio: pds_vfio driver](http://lore.kernel.org/netdev/20230602220318.15323-1-brett.creeley@amd.com/)**

> This is a patchset for a new vendor specific VFIO driver
> (pds_vfio) for use with the AMD/Pensando Distributed Services Card
> (DSC). This driver makes use of the pds_core driver.
>

**[v1: RDMA/core: Handle ARPHRD_NONE devices](http://lore.kernel.org/netdev/168573386075.5660.5037682341906748826.stgit@oracle-102.nfsv4bat.org/)**

> We would like to enable the use of siw on top of a VPN that is
> constructed and managed via a tun device. That hasn't worked up
> until now because ARPHRD_NONE devices (such as tun devices) have
> no GID for the RDMA/core to look up.
>

**[v1: net: dsa: realtek: rtl8365mb: use mdio passthrough to access PHYs](http://lore.kernel.org/netdev/0df383e20e5a90494e3cbd0cf23c508c5c943ab4.1685725191.git.chunkeey@gmail.com/)**

> when bringing up the PHYs on a Netgear WNDAP660, I observed that
> none of the PHYs are getting enumerated and the rtl8365mb fails
> to load.
>

**[v1: net: rfs: annotate lockless accesses](http://lore.kernel.org/netdev/20230602163141.2115187-1-edumazet@google.com/)**

> rfs runs without locks held, so we should annotate
> read and writes to shared variables.
>

**[v5: net-next: net: ioctl: Use kernel memory on protocol ioctl callbacks](http://lore.kernel.org/netdev/20230602163044.1820619-1-leitao@debian.org/)**

> Most of the ioctls to net protocols operates directly on userspace
> argument (arg). Usually doing get_user()/put_user() directly in the
> ioctl callback.  This is not flexible, because it is hard to reuse these
> functions without passing userspace buffers.
>

**[v1: iproute2: ipaddress: accept symbolic names](http://lore.kernel.org/netdev/20230602155419.8958-1-stephen@networkplumber.org/)**

> The function rtnl_addproto_a2n() was defined but never used.
> Use it to allow for symbolic names, and fix the function signatures
> so protocol value is consistently __u8.
>

**[v1: net-next: complete Lynx mdio device handling](http://lore.kernel.org/netdev/ZHoOe9K%2FdZuW2pOe@shell.armlinux.org.uk/)**

> This series completes the mdio device lifetime handling for Lynx PCS
> users which do not create their own mdio device, but instead fetch
> it using a firmware description - namely the DPAA2 and FMAN_MEMAC
> drivers.
>

**[v1: net: net/sched: fq_pie: ensure reasonable TCA_FQ_PIE_QUANTUM values](http://lore.kernel.org/netdev/20230602123747.2056178-1-edumazet@google.com/)**

> We got multiple syzbot reports, all duplicates of the following [1]
>
> syzbot managed to install fq_pie with a zero TCA_FQ_PIE_QUANTUM,
> thus triggering infinite loops.
>

**[[PATCH RESEND net-next 0/5] Improve the taprio qdisc's relationship with its children](http://lore.kernel.org/netdev/20230602103750.2290132-1-vladimir.oltean@nxp.com/)**

> [ Original patch set was lost due to an apparent transient problem with
> kernel.org's DNSBL setup. This is an identical resend. ]
>
> Prompted by Vinicius' request to consolidate some child Qdisc
> dereferences in taprio:
> https://lore.kernel.org/netdev/87edmxv7x2.fsf@intel.com/
>

**[v1: net: enetc: correct the statistics of rx bytes](http://lore.kernel.org/netdev/20230602094659.965523-1-wei.fang@nxp.com/)**

> The purpose of this patch set is to fix the issue of rx bytes
> statistics. The first patch corrects the rx bytes statistics
> of normal kernel protocol stack path, and the second patch is
> used to correct the rx bytes statistics of XDP.
>

**[v1: net-next: ipv6: lower "link become ready"'s level message](http://lore.kernel.org/netdev/20230601-net-next-skip_print_link_becomes_ready-v1-1-7ff2b88dc9b8@tessares.net/)**

> This following message is printed in the console each time a network
> device configured with an IPv6 addresses is ready to be used:
>

**[v5: net-next: sock: Improve condition on sockmem pressure](http://lore.kernel.org/netdev/20230602081135.75424-1-wuyun.abel@bytedance.com/)**

> Currently the memcg's status is also accounted into the socket's
> memory pressure to alleviate the memcg's memstall. But there are
> still cases that can be improved. Please check the patches for
> detailed info.
>

**[v2: bpf-next: bpf, x86: allow function arguments up to 14 for TRACING](http://lore.kernel.org/netdev/20230602065958.2869555-1-imagedong@tencent.com/)**

> For now, the BPF program of type BPF_PROG_TYPE_TRACING can only be used
> on the kernel functions whose arguments count less than 6. This is not
> friendly at all, as too many functions have arguments count more than 6.
>
> Therefore, let's enhance it by increasing the function arguments count
> allowed in arch_prepare_bpf_trampoline(), for now, only x86_64.
>

**[v4: Introduce a vringh accessor for IO memory](http://lore.kernel.org/netdev/20230602055211.309960-1-mie@igel.co.jp/)**

> Vringh is a host-side implementation of virtio rings, and supports the vring
> located on three kinds of memories, userspace, kernel space and a space
> translated iotlb.
>

**[v1: net-next: tools: ynl-gen: dust off the user space code](http://lore.kernel.org/netdev/20230602023548.463441-1-kuba@kernel.org/)**

> Every now and then I wish I finished the user space part of
> the netlink specs, Python scripts kind of stole the show but
> C is useful for selftests and stuff which needs to be fast.
> Recently someone asked me how to access devlink and ethtool
> from C++ which pushed me over the edge.
>

**[v6: net-next: net: dsa: mv88e6xxx: implement USXGMII mode for mv88e6393x](http://lore.kernel.org/netdev/20230602001705.2747-1-msmulski2@gmail.com/)**

> Changes from previous version:
> * use phylink_decode_usxgmii_word() to decode USXGMII link state
> * use existing include/uapi/linux/mdio.h defines when parsing status bits
>

**[v6: net-next: Brcm ASP 2.0 Ethernet Controller](http://lore.kernel.org/netdev/1685657551-38291-1-git-send-email-justin.chen@broadcom.com/)**

> Add support for the Broadcom ASP 2.0 Ethernet controller which is first
> introduced with 72165.
>

**[v1: net: dsa: qca8k: add CONFIG_LEDS_TRIGGERS dependency](http://lore.kernel.org/netdev/20230601213111.3182893-1-arnd@kernel.org/)**

> There is a mix of 'depends on' and 'select' for LEDS_TRIGGERS, so it's
> not clear what we should use here, but in general using 'depends on'
> causes fewer problems, so use that.
>

**[v1: net: tcp: gso: really support BIG TCP](http://lore.kernel.org/netdev/20230601211732.1606062-1-edumazet@google.com/)**

> oldlen name is a bit misleading, as it is the contribution
> of skb->len on the input skb TCP checksum. I added a comment
> to clarify this point.
>

**[v3: net/sctp: Make sha1 as default algorithm if fips is enabled](http://lore.kernel.org/netdev/1685643474-18654-1-git-send-email-kashwindayan@vmware.com/)**

> MD5 is not FIPS compliant. But still md5 was used as the
> default algorithm for sctp if fips was enabled.
> Due to this, listen() system call in ltp tests was
> failing for sctp in fips environment, with below error message.
>

**[GIT PULL: Networking for v6.4-rc5](http://lore.kernel.org/netdev/20230601180906.238637-1-

> Additional napi fields such as PID association for napi
> thread etc. can be supported in a follow-on patch set.
>
> This series only supports 'get' ability for retrieving
> napi fields (specifically, napi ids and queue[s]). The 'set'
> ability for setting queue[s] associated with a napi instance
> via netdev-genl will be submitted as a separate patch series.
>

#### 安全增强

**[v5: checkpatch: Check for 0-length and 1-element arrays](http://lore.kernel.org/linux-hardening/20230601160746.up.948-kees@kernel.org/)**

> Fake flexible arrays have been deprecated since last millennium. Proper
> C99 flexible arrays must be used throughout the kernel so
> CONFIG_FORTIFY_SOURCE and CONFIG_UBSAN_BOUNDS can provide proper array
> bounds checking.
>
> Fixed-by: Joe Perches <joe@perches.com>
>

**[v1: s390/purgatory: Do not use fortified string functions](http://lore.kernel.org/linux-hardening/20230531003414.never.050-kees@kernel.org/)**

> This means that the memcpy() calls with "buf" as a destination in
> sha256.c's code will attempt to perform run-time bounds checking, which
> could lead to calling missing functions, specifically a potential
> WARN_ONCE, which isn't callable from purgatory.
>

**[v1: x86/purgatory: Do not use fortified string functions](http://lore.kernel.org/linux-hardening/20230531003345.never.325-kees@kernel.org/)**

> This means that the memcpy() calls with "buf" as a destination in
> sha256.c's code will attempt to perform run-time bounds checking, which
> could lead to calling missing functions, specifically a potential
> WARN_ONCE, which isn't callable from purgatory.
>

**[v1: next: firewire: Replace zero-length array with flexible-array member](http://lore.kernel.org/linux-hardening/ZHT0V3SpvHyxCv5W@work/)**

> Zero-length and one-element arrays are deprecated, and we are moving
> towards adopting C99 flexible-array members, instead.
>

**[v1: next: drm/amdgpu/discovery: Replace fake flex-arrays with flexible-array members](http://lore.kernel.org/linux-hardening/ZHO4%2FZ+iO+lqV4rW@work/)**

> Zero-length and one-element arrays are deprecated, and we are moving
> towards adopting C99 flexible-array members, instead.
>
> Use the DECLARE_FLEX_ARRAY() helper macro to transform zero-length
> arrays in a union into flexible-array members. And replace a one-element
> array with a C99 flexible-array member.
>

#### Rust For Linux

**[v1: add abstractions for network device drivers](http://lore.kernel.org/rust-for-linux/01010188843258ec-552cca54-4849-4424-b671-7a5bf9b8651a-000000@us-west-2.amazonses.com/)**

> This patchset adds minimum abstractions for network device drivers and
> Rust dummy network device driver, a simpler version of drivers/net/dummy.c.
>
> The dummy network device driver doesn't attach any bus such as PCI so
> the dependency is minimum. Hopefully, it would make reviewing easier.
>

**[v2: Rust scatterlist abstractions](http://lore.kernel.org/rust-for-linux/20230602101819.2134194-1-changxian.cqs@antgroup.com/)**

> This is a version of scatterlist abstractions for Rust drivers.
>
> Scatterlist is used for efficient management of memory buffers, which is
> essential for many kernel-level operations such as Direct Memory Access
> (DMA) transfers and crypto APIs.
>

**[v2: rust: workqueue: add bindings for the workqueue](http://lore.kernel.org/rust-for-linux/20230601134946.3887870-1-aliceryhl@google.com/)**

> This patchset contains bindings for the kernel workqueue.
>
> One of the primary goals behind the design used in this patch is that we
> must support embedding the `work_struct` as a field in user-provided
> types, because this allows you to submit things to the workqueue without
> having to allocate, making the submission infallible. If we didn't have
> to support this, then the patch would be much simpler. One of the main
> things that make it complicated is that we must ensure that the function
> pointer in the `work_struct` is compatible with the struct it is
> contained within.
>

**[v1: rust: error: integrate Rust error type with `errname`](http://lore.kernel.org/rust-for-linux/20230531174450.3733220-1-aliceryhl@google.com/)**

> This integrates the `Error` type with the `errname` by making it
> accessible via the `name` method or via the `Debug` trait.
>

#### BPF

**[v11: evm: Do HMAC of multiple per LSM xattrs for new inodes](http://lore.kernel.org/bpf/20230603191518.1397490-1-roberto.sassu@huaweicloud.com/)**

> One of the major goals of LSM stacking is to run multiple LSMs side by side
> without interfering with each other. The ultimate decision will depend on
> individual LSM decision.
>

**[[PATCH RESEND bpf-next 00/18] BPF token](http://lore.kernel.org/bpf/20230602150011.1657856-1-andrii@kernel.org/)**

> *Resending with trimmed CC list because original version didn't make it to
> the mailing list.*
>
> This patch set introduces new BPF object, BPF token, which allows to delegate
> a subset of BPF functionality from privileged system-wide daemon (e.g.,
> systemd or any other container manager) to a *trusted* unprivileged
> application. Trust is the key here. This functionality is not about allowing
> unconditional unprivileged BPF usage. Establishing trust, though, is
> completely up to the discretion of respective privileged application that
> would create a BPF token.
>

**[v1: selftests/bpf: Add missing selftests kconfig options](http://lore.kernel.org/bpf/20230602140108.1177900-1-void@manifault.com/)**

> Our selftests of course rely on the kernel being built with
> CONFIG_DEBUG_INFO_BTF=y, though this (nor its dependencies of
> CONFIG_DEBUG_INFO=y and CONFIG_DEBUG_INFO_DWARF4=y) are not specified.
> This causes the wrong kernel to be built, and selftests to similarly
> fail to build.
>

**[v10: vhost: virtio core prepares for AF_XDP](http://lore.kernel.org/bpf/20230602092206.50108-1-xuanzhuo@linux.alibaba.com/)**

> Now, virtio may can not work with DMA APIs when virtio features do not have
> VIRTIO_F_ACCESS_PLATFORM.
>
> 1. I tried to let DMA APIs return phy address by virtio-device. But DMA APIs just
>    work with the "real" devices.
> 2. I tried to let xsk support callballs to get phy address from virtio-net
>    driver as the dma address. But the maintainers of xsk may want to use dma-buf
>    to replace the DMA APIs. I think that may be a larger effort. We will wait
>    too long.
>

**[v1: bpf-next: bpf: Support ->fill_link_info for kprobe prog](http://lore.kernel.org/bpf/20230602085239.91138-1-laoar.shao@gmail.com/)**

> Currently, it is not easy to determine which functions are probed by a
> kprobe_multi program. This patchset supports ->fill_link_info for it,
> allowing the user to easily obtain the probed functions.
>
> Although the user can retrieve the functions probed by a perf_event
> program using `bpftool perf show`, it would be beneficial to also support
> ->fill_link_info. This way, the user can obtain it in the same manner as
> other bpf links.
>

**[v2: bpf-next: bpf_refcount followups (part 1)](http://lore.kernel.org/bpf/20230602022647.1571784-1-davemarchevsky@fb.com/)**

> This series is the first of two (or more) followups to address issues in the
> bpf_refcount shared ownership implementation discovered by Kumar.
> Specifically, this series addresses the "bpf_refcount_acquire on non-owning ref
> in another tree" scenario described in [0], and does _not_ address issues
> raised in [1]. Further followups will address the other issues.
>

**[v2: bpf-next: bpf/xdp: optimize bpf_xdp_pointer to avoid reading sinfo](http://lore.kernel.org/bpf/168563651438.3436004.17735707525651776648.stgit@firesoul/)**

> Currently we observed a significant performance degradation in
> samples/bpf xdp1 and xdp2, due XDP multibuffer "xdp.frags" handling,
> added in commit 772251742262 ("samples/bpf: fixup some tools to be able
> to support xdp multibuffer").
>

**[v1: bpf-next: bpf: getsockopt hook to get optval without checking kernel retval](http://lore.kernel.org/bpf/20230601024900.22902-1-zhoufeng.zf@bytedance.com/)**

> Remove the judgment on retval and pass bpf ctx by default. The
> advantage of this is that it is more flexible. Bpf getsockopt can
> support the new optname without using the module to call the
> nf_register_sockopt to register.
>

**[v1: bpf-next: bpf: support BTF kind metadata to separate](http://lore.kernel.org/bpf/20230531201936.1992188-1-alan.maguire@oracle.com/)**

> BTF kind metadata provides information to parse BTF kinds.
> By separating parsing BTF from using all the information
> it provides, we allow BTF to encode new features even if
> they cannot be used.  This is helpful in particular for
> cases where newer tools for BTF generation run on an
> older kernel; BTF kinds may be present that the kernel
> cannot yet use, but at least it can parse the BTF
> provided.  Meanwhile userspace tools with newer libbpf
> may be able to use the newer information.
>

**[v1: net: ice: recycle/free all of the fragments from multi-buffer frame](http://lore.kernel.org/bpf/20230531154457.3216621-1-anthony.l.nguyen@intel.com/)**

> The ice driver caches next_to_clean value at the beginning of
> ice_clean_rx_irq() in order to remember the first buffer that has to be
> freed/recycled after main Rx processing loop. The end boundary is
> indicated by first descriptor of frame that Rx processing loop has ended
> its duties. Note that if mentioned loop ended in the middle of gathering
> multi-buffer frame, next_to_clean would be pointing to the descriptor in
> the middle of the frame BUT freeing/recycling stage will stop at the
> first descriptor. This means that next iteration of ice_clean_rx_irq()
> will miss the (first_desc, next_to_clean - 1) entries.
>

**[v1: bpf/tests: Use struct_size()](http://lore.kernel.org/bpf/20230531043251.989312-1-suhui@nfschina.com/)**

> Use struct_size() instead of hand writing it.
> This is less verbose and more informative.
>

**[v1: net: bpf, sockmap: avoid potential NULL dereference in sk_psock_verdict_data_ready()](http://lore.kernel.org/bpf/20230530195149.68145-1-edumazet@google.com/)**

> syzbot found sk_psock(sk) could return NULL when called
> from sk_psock_verdict_data_ready().
>
> Just make sure to handle this case.
>

**[v2: bpf-next: verify scalar ids mapping in regsafe()](http://lore.kernel.org/bpf/20230530172739.447290-1-eddyz87@gmail.com/)**

> To represent this set I use a u32_hashset data structure derived from
> tools/lib/bpf/hashmap.h. I tested it locally (see [1]), but I think
> that ideally it should be tested using KUnit. However, AFAIK, this
> would be the first use of KUnit in context of BPF verifier.
> If people are ok with this, I will prepare the tests and necessary
> CI integration.
>

**[v1: bpf-next: samples/bpf: xdp1 and xdp2 reduce XDPBUFSIZE to 60](http://lore.kernel.org/bpf/168545704139.2996228.2516528552939485216.stgit@firesoul/)**

> Default samples/pktgen scripts send 60 byte packets as hardware
> adds 4-bytes FCS checksum, which fulfils minimum Ethernet 64 bytes
> frame size.
>
> XDP layer will not necessary have access to the 4-bytes FCS checksum.
>

**[v2: bpf-next: xsk: multi-buffer support](http://lore.kernel.org/bpf/20230529155024.222213-1-maciej.fijalkowski@intel.com/)**

> This series of patches add multi-buffer support for AF_XDP. XDP and
> various NIC drivers already have support for multi-buffer packets. With
> this patch set, programs using AF_XDP sockets can now also receive and
> transmit multi-buffer packets both in copy as well as zero-copy mode.
> ZC multi-buffer implementation is based on ice driver.
>

**[v1: net: tcp: introduce a compack timer handler in sack compression](http://lore.kernel.org/bpf/20230529113804.GA20300@didi-ThinkCentre-M920t-N000/)**

> We've got some issues when sending a compressed ack is deferred to
> release phrase due to the socket owned by another user:
> 1. a compressed ack would not be sent because of lack of ICSK_ACK_TIMER
> flag.
> 2. the tp->compressed_ack counter should be decremented by 1.
> 3. we cannot pass timeout check and reset the delack timer in
> tcp_delack_timer_handler().
> 4. we are not supposed to increment the LINUX_MIB_DELAYEDACKS counter.
> ...
>

**[v1: bpf-next: multi-buffer support for XDP_REDIRECT samples](http://lore.kernel.org/bpf/20230529110608.597534-1-tariqt@nvidia.com/)**

> This series adds multi-buffer support for two XDP_REDIRECT sample programs.
> It follows the pattern from xdp1 and xdp2.
>

**[v2: net-next: support non-frag page for page_pool_alloc_frag()](http://lore.kernel.org/bpf/20230529092840.40413-1-linyunsheng@huawei.com/)**

> In [1] & [2], there are usecases for veth and virtio_net to
> use frag support in page pool to reduce memory usage, and it
> may request different frag size depending on the head/tail
> room space for xdp_frame/shinfo and mtu/packet size. When the
> requested frag size is large enough that a single page can not
> be split into more than one frag, using frag support only have
> performance penalty because of the extra frag count handling
> for frag support.
>

**[v1: bpf-next: bpf: Support ->show_fdinfo and ->fill_link_info for kprobe prog](http://lore.kernel.org/bpf/20230528142027.5585-1-laoar.shao@gmail.com/)**

> Currently, it is not easy to determine which functions are probed by a
> kprobe_multi program. This patchset supports ->show_fdinfo and
> ->fill_link_info for it, allowing the user to easily obtain the probed
> functions.
>

### 周边技术动态

#### Qemu

**[RFC: target/riscv: Add support for Zacas extension](http://lore.kernel.org/qemu-devel/20230602121638.36342-1-rbradford@rivosinc.com/)**

> The Zacas[1] extension is a proposed unprivileged ISA extension for
> adding support for atomic compare-and-swap. Since this extension is not
> yet frozen (although no significant changes are expected) these patches
> are RFC/informational.
>

**[v2: linux-user/riscv: Add syscall riscv_hwprobe](http://lore.kernel.org/qemu-devel/f59f948fc42fdf0b250afd6dcd6f232013480d9c.camel@rivosinc.com/)**

> This patch adds the new syscall for the
> "RISC-V Hardware Probing Interface"
> (https://docs.kernel.org/riscv/hwprobe.html).
>

**[v7: hw/riscv/virt: pflash improvements](http://lore.kernel.org/qemu-devel/20230601045910.18646-1-sunilvl@ventanamicro.com/)**

> This series improves the pflash usage in RISC-V virt machine with solutions to
> below issues.
>
> 1) Currently the first pflash is reserved for ROM/M-mode firmware code. But S-mode
> payload firmware like EDK2 need both pflash devices to have separate code and variable
> store so that OS distros can keep the FW code as read-only.
>

**[v1: disas/riscv: Add vendor extension support](http://lore.kernel.org/qemu-devel/20230530131843.1186637-1-christoph.muellner@vrull.eu/)**

> This series adds vendor extension support to the QEMU disassembler
> for RISC-V. The following vendor extensions are covered:
> * XThead{Ba,Bb,Bs,Cmo,CondMov,FMemIdx,Fmv,Mac,MemIdx,MemPair,Sync}
> * XVentanaCondOps
>

#### Buildroot

**[package/openjdk{-bin}: security bump versions to 11.0.19+7 and 17.0.7+7](http://lore.kernel.org/buildroot/20230602202425.2C00186BCA@busybox.osuosl.org/)**

> For details, see the announcements:
> https://mail.openjdk.org/pipermail/jdk-updates-dev/2023-April/021899.html
> https://mail.openjdk.org/pipermail/jdk-updates-dev/2023-April/021900.html
>

#### U-Boot

**[v4: SPL NVMe support](http://lore.kernel.org/u-boot/20230603140256.2443518-1-mchitale@ventanamicro.com/)**

> This patchset adds support to load images of the SPL's next booting
> stage from a NVMe device.
>

**[v1: riscv: JH7110: move pll clocks to their own device node (Was: The latest U-boot...) visionfive2 1.3B board](http://lore.kernel.org/u-boot/20230602171054.GB27915@lst.de/)**

> Here is the revert, along with a work in progress attempt to make the DT
> match the hardware. Conor had asked me to share it, regardless of its
> early stage. It compiles, and boots Linux kernels, but there is no PLL
> driver I can find currently. So clocks are still hanging in PROBE_DEFER.
>
